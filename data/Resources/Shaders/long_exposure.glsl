//======================== VERTEX_SHADER ========================

#version 130

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec2 AltTextureSize;
uniform vec4 SourceRectangle;
uniform vec4 AltSourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;
out vec2 fAltSampleCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = (SourceRectangle.xy + (inTexCoord * SourceRectangle.zw)) / TextureSize;
	fAltSampleCoord = (AltSourceRectangle.xy + (inTexCoord * AltSourceRectangle.zw)) / AltTextureSize;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 130

uniform sampler2D Texture;
uniform sampler2D AltTexture;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;
in vec2 fAltSampleCoord;

out vec4 frag_colour;

#define M_PI 3.1415926535897932384626433832795

//Copied from https://gist.github.com/mairod/a75e7b44f68110e1576d77419d608786
vec3 hueShift(vec3 color, float hueAdjust)
{
	const vec3 kRGBToYPrime = vec3 (0.299, 0.587, 0.114);
	const vec3 kRGBToI      = vec3 (0.596, -0.275, -0.321);
	const vec3 kRGBToQ      = vec3 (0.212, -0.523, 0.311);
	
	const vec3 kYIQToR     = vec3 (1.0, 0.956, 0.621);
	const vec3 kYIQToG     = vec3 (1.0, -0.272, -0.647);
	const vec3 kYIQToB     = vec3 (1.0, -1.107, 1.704);
	
	float YPrime  = dot (color, kRGBToYPrime);
	float I       = dot (color, kRGBToI);
	float Q       = dot (color, kRGBToQ);
	float hue     = atan (Q, I);
	float chroma  = sqrt (I * I + Q * Q);
	
	hue += hueAdjust;
	
	Q = chroma * sin (hue);
	I = chroma * cos (hue);
	
	vec3    yIQ   = vec3 (YPrime, I, Q);
	
	return vec3( dot (yIQ, kYIQToR), dot (yIQ, kYIQToG), dot (yIQ, kYIQToB) );
}

float getHue(vec3 color)
{
	const vec3 kRGBToI      = vec3 (0.596, -0.275, -0.321);
	const vec3 kRGBToQ      = vec3 (0.212, -0.523, 0.311);
	float I       = dot (color, kRGBToI);
	float Q       = dot (color, kRGBToQ);
	float hue     = atan (Q, I);
	if (hue < 0) { hue += 2 * M_PI; }
	return hue;
}

void main()
{
	vec4 newColor = texture(Texture, fSampleCoord);
	vec4 oldColor = texture(AltTexture, fAltSampleCoord);
	float oldColorHue = getHue(oldColor.xyz);
	float newColorHue = getHue(newColor.xyz);
	float newGrey = 0.21 * newColor.r + 0.71 * newColor.g + 0.07 * newColor.b;
	float oldGrey = 0.21 * oldColor.r + 0.71 * oldColor.g + 0.07 * oldColor.b;
	// bool oldIsProperHue = (oldColorHue >= 6 || oldColorHue <= 1); //Orange player
	// bool newIsProperHue = (newColorHue >= 6 || newColorHue <= 1);
	bool oldIsProperHue = (oldColorHue >= 3.35 && oldColorHue <= 3.5); //Blue player (3.38 target)
	bool newIsProperHue = (newColorHue >= 3.35 && newColorHue <= 3.5);
	
	// float hueMin = -2.0; //target is 5.74 in radians
	// float hueMax = 3.0;
	// // const float hueMin = -100;
	// // const float hueMax = ((60.0/360.0) * 2 * M_PI);
	
	// if (oldColorHue < hueMin && oldColorHue > hueMax) { oldColorHue += 2*M_PI; hueMin += 2*M_PI; hueMax += 2*M_PI; }
	// if (newColorHue < hueMin && newColorHue > hueMax) { newColorHue += 2*M_PI; hueMin += 2*M_PI; hueMax += 2*M_PI; }
	if (newGrey >= 0.25)
	{
		if (oldIsProperHue && newIsProperHue)
		{
			if (newGrey <= oldGrey) { frag_colour = newColor; }
			else { frag_colour = oldColor; }
		}
		else if (newIsProperHue)
		{
			frag_colour = newColor;
		}
		else
		{
			frag_colour = oldColor;
		}
	}
	else
	{
		frag_colour = oldColor;
	}
	
	// if (newColorHue < 1) { frag_colour = vec4(1, 0, 0, 1); }
	// else if (newColorHue < 2) { frag_colour = vec4(0.7411, 0.4627, 0.156, 1); }
	// else if (newColorHue < 3) { frag_colour = vec4(1, 1, 0, 1); }
	// else if (newColorHue < 4) { frag_colour = vec4(0, 1, 0, 1); }
	// else if (newColorHue < 5) { frag_colour = vec4(0, 0, 1, 1); }
	// else if (newColorHue < 6) { frag_colour = vec4(0.6863, 0.5059, 1, 1); }
	// else if (newColorHue < 7) { frag_colour = vec4(0.5647, 0.5333, 0.3804, 1); }
	// frag_colour = vec4(newColorHue - 3, newColorHue - 3, newColorHue - 3, 1);
	
	// newColorHue = newColorHue/(2*M_PI);
	// newColorHue -= 3;
	// frag_colour = vec4(newColorHue,newColorHue,newColorHue,newColorHue);
	frag_colour = PrimaryColor * fColor * frag_colour;
}