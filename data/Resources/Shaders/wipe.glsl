//======================== VERTEX_SHADER ========================

#version 130

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

// uniform vec2 TextureSize;
uniform vec4 SourceRectangle;
// uniform vec4 MaskRectangle;
// uniform vec2 ShiftVec;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 130

uniform sampler2D Texture;
uniform vec2      TextureSize;
// uniform vec4      SourceRectangle;
// uniform vec4      MaskRectangle;
// uniform vec2      ShiftVec;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;
// uniform vec4      ReplaceColor1;
// uniform vec4      ReplaceColor2;
// uniform vec4      ReplaceColor3;
// uniform vec4      ReplaceColor4;

// uniform bool      DoGrayscaleGradient;
uniform float     CircleRadius;
// uniform float     CircleInnerRadius;
// uniform float     Saturation;
// uniform float     Brightness;
uniform float     Time;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;

out vec4 frag_color;

#define M_PI 3.1415926535897932384626433832795

float RadiusEase(float value)
{
	return -(value * (value - 2));;
}

void main()
{
	vec4 sampleColor = fColor * PrimaryColor * texture(Texture, fSampleCoord/TextureSize);
	
	frag_color = sampleColor;
	frag_color.a = 0;
	
	float wipeDir = (M_PI * 4) * Time;
	
	float distFromCenter = length(fTexCoord - vec2(0.5, 0.5)) * 2;
	
	float pixelDir1 = atan(fTexCoord.y - 0.5, fTexCoord.x - 0.5);
	if (pixelDir1 < 0) { pixelDir1 += 2*M_PI; }
	float pixelDir2 = pixelDir1 + M_PI*2;
	
	float radius1 = 1.0f - RadiusEase(pixelDir1 / (M_PI*4));
	float radius2 = 1.0f - RadiusEase(pixelDir2 / (M_PI*4));
	
	if (pixelDir1 < wipeDir && distFromCenter > radius1)
	{
		frag_color.a = sampleColor.a;
	}
	else if (pixelDir2 < wipeDir && distFromCenter > radius2)
	{
		frag_color.a = sampleColor.a;
	}
	// else if (Time > 0.9)
	// {
	// 	frag_color.a = sampleColor.a * ((Time-0.9) / 0.1);
	// }
	
	// if (CircleRadius != 0)
	// {
	// 	// frag_color.rgb = 1 - vec3(distFromCenter);
	// 	float smoothDelta = fwidth(distFromCenter);
	// 	frag_color.a *= smoothstep(CircleRadius, CircleRadius-smoothDelta, distFromCenter);
	// 	// if (distFromCenter < CircleInnerRadius)
	// 	// {
	// 	// 	frag_color.a = 0;
	// 	// }
	// 	if (CircleInnerRadius != 0)
	// 	{
	// 		frag_color.a *= smoothstep(CircleInnerRadius, CircleInnerRadius+smoothDelta, distFromCenter);
	// 	}
	// }
	
	if (frag_color.a < 0.1)
	{
		discard;
	}
}