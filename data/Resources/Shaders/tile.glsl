//======================== VERTEX_SHADER ========================

#version 130

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec4 SourceRectangle;
uniform vec4 MaskRectangle;
uniform vec2 ShiftVec;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;
out vec2 fMaskCoord;

void main(void)
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	fMaskCoord = MaskRectangle.xy + (inTexCoord * MaskRectangle.zw);
	//NOTE: For some reason our sampling is pulling 1 too many pixels from the top row of the texture
	//      I think this is probably due to rounding errors so we just add a small value here to fix it
	//      We should somehow tune this value so it works for scales >= 100
	fSampleCoord.y += 0.01;
	fMaskCoord.y += 0.01;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 130

uniform sampler2D Texture;
uniform vec2      TextureSize;
uniform vec4      SourceRectangle;
uniform vec4      MaskRectangle;
uniform vec2      ShiftVec;
// uniform float     Value0;

uniform vec4      PrimaryColor;
uniform vec4      ReplaceColors[16];
uniform float     Saturation;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;
in vec2 fMaskCoord;

out vec4 frag_colour;

vec3 czm_saturation(vec3 rgb, float adjustment)
{
	// Algorithm from Chapter 16 of OpenGL Shading Language
	const vec3 W = vec3(0.2125, 0.7154, 0.0721);
	vec3 intensity = vec3(dot(rgb, W));
	return mix(intensity, rgb, adjustment);
}

void main(void)
{
	// vec4 targetColor1 = vec4(240.0/255.0, 110.0/255.0, 170.0/255.0, 1);
	// vec4 targetColor2 = vec4(246.0/255.0, 172.0/255.0, 205.0/255.0, 1);
	// vec4 targetColor3 = vec4(236.0/255.0,   0.0/255.0, 140.0/255.0, 1);
	// vec4 targetColor4 = vec4(240.0/255.0, 136.0/255.0, 184.0/255.0, 1);
	
	vec4 targetColor1  = vec4(113.0 / 255.0, 0.0   / 255.0, 64.0  / 255.0, 1); //0xFF710040
	vec4 targetColor2  = vec4(170.0 / 255.0, 0.0   / 255.0, 96.0  / 255.0, 1); //0xFFAA0060
	vec4 targetColor3  = vec4(189.0 / 255.0, 0.0   / 255.0, 109.0 / 255.0, 1); //0xFFBD006D
	vec4 targetColor4  = vec4(212.0 / 255.0, 0.0   / 255.0, 123.0 / 255.0, 1); //0xFFD4007B
	vec4 targetColor5  = vec4(236.0 / 255.0, 0.0   / 255.0, 140.0 / 255.0, 1); //0xFFEC008C
	vec4 targetColor6  = vec4(255.0 / 255.0, 8.0   / 255.0, 151.0 / 255.0, 1); //0xFFFF0897
	vec4 targetColor7  = vec4(255.0 / 255.0, 36.0  / 255.0, 163.0 / 255.0, 1); //0xFFFF24A3
	vec4 targetColor8  = vec4(237.0 / 255.0, 78.0  / 255.0, 149.0 / 255.0, 1); //0xFFED4E95
	vec4 targetColor9  = vec4(240.0 / 255.0, 110.0 / 255.0, 170.0 / 255.0, 1); //0xFFF06EAA
	vec4 targetColor10 = vec4(240.0 / 255.0, 136.0 / 255.0, 184.0 / 255.0, 1); //0xFFF088B8
	vec4 targetColor11 = vec4(242.0 / 255.0, 148.0 / 255.0, 190.0 / 255.0, 1); //0xFFF294BE
	vec4 targetColor12 = vec4(246.0 / 255.0, 172.0 / 255.0, 205.0 / 255.0, 1); //0xFFF6ACCD
	vec4 targetColor13 = vec4(248.0 / 255.0, 184.0 / 255.0, 206.0 / 255.0, 1); //0xFFF8B8CE
	vec4 targetColor14 = vec4(249.0 / 255.0, 197.0 / 255.0, 208.0 / 255.0, 1); //0xFFF9C5D0
	vec4 targetColor15 = vec4(250.0 / 255.0, 206.0 / 255.0, 211.0 / 255.0, 1); //0xFFFACED3
	vec4 targetColor16 = vec4(252.0 / 255.0, 225.0 / 255.0, 222.0 / 255.0, 1); //0xFFFCE1DE
	// vec4 hairColor     = vec4( 96.0 / 255.0,  57.0 / 255.0,  19.0 / 255.0, 1); //0xFF603913
	
	// if (PrimaryColor.a < 0.1)
	// {
	// 	discard;
	// }
	
	vec2 realSampleCoord = fSampleCoord;
	if (ShiftVec.x != 0 || ShiftVec.y != 0)
	{
		realSampleCoord = realSampleCoord + ShiftVec;
		if (realSampleCoord.x > SourceRectangle.x + SourceRectangle.z) { discard; }//realSampleCoord.x -= SourceRectangle.z; }
		if (realSampleCoord.x < SourceRectangle.x)                     { discard; }//realSampleCoord.x += SourceRectangle.z; }
		if (realSampleCoord.y > SourceRectangle.y + SourceRectangle.w) { discard; }//realSampleCoord.y -= SourceRectangle.w; }
		if (realSampleCoord.y < SourceRectangle.y)                     { discard; }//realSampleCoord.y += SourceRectangle.w; }
		// realSampleCoord.x = round(realSampleCoord.x);
		// realSampleCoord.y = round(realSampleCoord.y);
	}
	vec4 sampleColor = texture(Texture, realSampleCoord / TextureSize);
	
	if (MaskRectangle.z > 0 && MaskRectangle.w > 0)
	{
		vec2 realMaskCoord = fMaskCoord + ShiftVec;
		// realMaskCoord.x = ceil(realMaskCoord.x);
		// realMaskCoord.y = ceil(realMaskCoord.y);
		vec4 maskSampleColor = texture(Texture, realMaskCoord / TextureSize);
		// if (maskSampleColor.a < 0.5f) { discard; }
		sampleColor.a *= maskSampleColor.a;
	}
	
	// if (PrimaryColor.a < 0.75)
	// {
	// 	if (realSampleCoord.x > SourceRectangle.x + 0.99 &&
	// 		realSampleCoord.x < SourceRectangle.x + SourceRectangle.z - 0.99 &&
	// 		realSampleCoord.y > SourceRectangle.y + 0.99 &&
	// 		realSampleCoord.y < SourceRectangle.y + SourceRectangle.w - 0.99)
	// 	{
	// 		vec4 sampleLeft  = texture(Texture, (realSampleCoord + vec2(-1, 0)) / TextureSize);
	// 		vec4 sampleRight = texture(Texture, (realSampleCoord + vec2(1, 0)) / TextureSize);
	// 		vec4 sampleUp    = texture(Texture, (realSampleCoord + vec2(0, -1)) / TextureSize);
	// 		vec4 sampleDown  = texture(Texture, (realSampleCoord + vec2(0, 1)) / TextureSize);
	// 		if (sampleLeft.a > 0.5 && sampleRight.a > 0.5 && sampleUp.a > 0.5 && sampleDown.a > 0.5)
	// 		{
	// 			discard;
	// 		}
	// 	}
	// }
	
	if (sampleColor.a > 5/256.0f)
	{
		if      (abs(sampleColor.r - targetColor1.r)  < 1/256.0f && abs(sampleColor.g - targetColor1.g)  < 1/256.0f && abs(sampleColor.b - targetColor1.b)  < 1/256.0f) { sampleColor = ReplaceColors[0];  }
		else if (abs(sampleColor.r - targetColor2.r)  < 1/256.0f && abs(sampleColor.g - targetColor2.g)  < 1/256.0f && abs(sampleColor.b - targetColor2.b)  < 1/256.0f) { sampleColor = ReplaceColors[1];  }
		else if (abs(sampleColor.r - targetColor3.r)  < 1/256.0f && abs(sampleColor.g - targetColor3.g)  < 1/256.0f && abs(sampleColor.b - targetColor3.b)  < 1/256.0f) { sampleColor = ReplaceColors[2];  }
		else if (abs(sampleColor.r - targetColor4.r)  < 1/256.0f && abs(sampleColor.g - targetColor4.g)  < 1/256.0f && abs(sampleColor.b - targetColor4.b)  < 1/256.0f) { sampleColor = ReplaceColors[3];  }
		else if (abs(sampleColor.r - targetColor5.r)  < 1/256.0f && abs(sampleColor.g - targetColor5.g)  < 1/256.0f && abs(sampleColor.b - targetColor5.b)  < 1/256.0f) { sampleColor = ReplaceColors[4];  }
		else if (abs(sampleColor.r - targetColor6.r)  < 1/256.0f && abs(sampleColor.g - targetColor6.g)  < 1/256.0f && abs(sampleColor.b - targetColor6.b)  < 1/256.0f) { sampleColor = ReplaceColors[5];  }
		else if (abs(sampleColor.r - targetColor7.r)  < 1/256.0f && abs(sampleColor.g - targetColor7.g)  < 1/256.0f && abs(sampleColor.b - targetColor7.b)  < 1/256.0f) { sampleColor = ReplaceColors[6];  }
		else if (abs(sampleColor.r - targetColor8.r)  < 1/256.0f && abs(sampleColor.g - targetColor8.g)  < 1/256.0f && abs(sampleColor.b - targetColor8.b)  < 1/256.0f) { sampleColor = ReplaceColors[7];  }
		else if (abs(sampleColor.r - targetColor9.r)  < 1/256.0f && abs(sampleColor.g - targetColor9.g)  < 1/256.0f && abs(sampleColor.b - targetColor9.b)  < 1/256.0f) { sampleColor = ReplaceColors[8];  }
		else if (abs(sampleColor.r - targetColor10.r) < 1/256.0f && abs(sampleColor.g - targetColor10.g) < 1/256.0f && abs(sampleColor.b - targetColor10.b) < 1/256.0f) { sampleColor = ReplaceColors[9];  }
		else if (abs(sampleColor.r - targetColor11.r) < 1/256.0f && abs(sampleColor.g - targetColor11.g) < 1/256.0f && abs(sampleColor.b - targetColor11.b) < 1/256.0f) { sampleColor = ReplaceColors[10]; }
		else if (abs(sampleColor.r - targetColor12.r) < 1/256.0f && abs(sampleColor.g - targetColor12.g) < 1/256.0f && abs(sampleColor.b - targetColor12.b) < 1/256.0f) { sampleColor = ReplaceColors[11]; }
		else if (abs(sampleColor.r - targetColor13.r) < 1/256.0f && abs(sampleColor.g - targetColor13.g) < 1/256.0f && abs(sampleColor.b - targetColor13.b) < 1/256.0f) { sampleColor = ReplaceColors[12]; }
		else if (abs(sampleColor.r - targetColor14.r) < 1/256.0f && abs(sampleColor.g - targetColor14.g) < 1/256.0f && abs(sampleColor.b - targetColor14.b) < 1/256.0f) { sampleColor = ReplaceColors[13]; }
		else if (abs(sampleColor.r - targetColor15.r) < 1/256.0f && abs(sampleColor.g - targetColor15.g) < 1/256.0f && abs(sampleColor.b - targetColor15.b) < 1/256.0f) { sampleColor = ReplaceColors[14]; }
		else if (abs(sampleColor.r - targetColor16.r) < 1/256.0f && abs(sampleColor.g - targetColor16.g) < 1/256.0f && abs(sampleColor.b - targetColor16.b) < 1/256.0f) { sampleColor = ReplaceColors[15]; }
		// else if (Value0 > 0 && abs(sampleColor.r - hairColor.r)     < 1/256.0f && abs(sampleColor.g - hairColor.g)     < 1/256.0f && abs(sampleColor.b - hairColor.b)     < 1/256.0f)
		// {
		// 	sampleColor = vec4(0, 0, 0, 1);
		// }
	}
	frag_colour = PrimaryColor * fColor * sampleColor;
	// frag_colour.rgb = czm_saturation(frag_colour.rgb, Saturation);
	// frag_colour.rgb = frag_colour.rgb * frag_colour.a;
	
	if (frag_colour.a < 0.1)
	{
		discard;
	}
	
	frag_colour.rgb = czm_saturation(frag_colour.rgb, Saturation);
}