/*
File:   res_palette.cpp
Author: Taylor Robbins
Date:   10\23\2018
Description: 
	** Holds functions that load and parse a color palette from a png file 
*/

void DestroyColorPalette(ColorPalette_t* palette)
{
	Assert(palette != nullptr);
	if (palette->colors != nullptr)
	{
		ArenaPop(mainHeap, palette->colors);
	}
	ClearPointer(palette);
}

bool LoadColorPalette(ColorPalette_t* palette, const char* filePath)
{
	FileInfo_t paletteFile = platform->ReadEntireFile(filePath);
	if (paletteFile.content == nullptr)
	{
		NotifyPrint_E("WARNING: Couldn't find palette at \"%s\"", filePath);
		return false;
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)paletteFile.content, paletteFile.size,
		&width, &height, &numChannels, 4);
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	if (width < NUM_REPLACE_COLORS || height < NUM_PALETTE_COLORS)
	{
		NotifyPrint_E("WARNING: Palette is not large enough! Found %dx%d palette when we need %ux%d", width, height, NUM_REPLACE_COLORS, NUM_PALETTE_COLORS);
		return false;
	}
	
	DestroyColorPalette(palette);
	palette->colors = PushStruct(mainHeap, PaletteColorList_t);
	Assert(palette->colors != nullptr);
	ClearPointer(palette->colors);
	palette->numColors = NUM_PALETTE_COLORS;
	
	for (u32 pIndex = 0; pIndex < NUM_PALETTE_COLORS; pIndex++)
	{
		for (u8 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
		{
			v2i pixelPos = NewVec2i(cIndex, pIndex);
			u8* bytePntr = &imageData[4 * ((pixelPos.y * width) + pixelPos.x)];
			palette->colors->colors[pIndex].colors[cIndex].r = bytePntr[0];
			palette->colors->colors[pIndex].colors[cIndex].g = bytePntr[1];
			palette->colors->colors[pIndex].colors[cIndex].b = bytePntr[2];
			palette->colors->colors[pIndex].colors[cIndex].a = bytePntr[3];
			if (WasProgramArgumentPassed("pastel"))
			{
				palette->colors->colors[pIndex].colors[cIndex] = GetPredefPalColorByIndex(cIndex);
			}
		}
	}
	
	stbi_image_free(imageData);
	platform->FreeFileMemory(&paletteFile);
	
	#if DEVELOPER
	if (!platform->IsWatchingFile(filePath))
	{
		platform->WatchFile(filePath);
	}
	#endif
	
	return true;
}

Color_t GetPackedIndexPaletteColor(u32 packedIndex)
{
	u32 index = ((packedIndex & 0xFFFFFFFC) >> 2);
	u8 subIndex = (u8)(packedIndex & 0x00000003);
	if (index >= NUM_PALETTE_COLORS || subIndex >= 4) { return White; }
	if (subIndex == 0) { return plt->colors[index].primary;   }
	if (subIndex == 1) { return plt->colors[index].secondary; }
	if (subIndex == 2) { return plt->colors[index].light;     }
	if (subIndex == 3) { return plt->colors[index].dark;      }
	Assert(false);
	return White;
}
u32 GetPackedIndex(u32 colorIndex, u8 subIndex)
{
	Assert(colorIndex < NUM_PALETTE_COLORS);
	u8 actualSubIndex = 4;
	if (subIndex == 4)  { actualSubIndex = 0x3; } //dark
	if (subIndex == 8)  { actualSubIndex = 0x0; } //primary
	if (subIndex == 9)  { actualSubIndex = 0x1; } //secondary
	if (subIndex == 11) { actualSubIndex = 0x2; } //light
	Assert(actualSubIndex < 4);
	return (u32)((colorIndex << 2) & 0xFFFFFFFC) | (u32)(actualSubIndex & 0x03);
}
u32 GetPaletteColorIndexByPntr(const PalColor_t* palColorPntr)
{
	for (u32 colorIndex = 0; colorIndex < NUM_PALETTE_COLORS; colorIndex++)
	{
		const PalColor_t* palColor = &plt->colors[colorIndex];
		if (palColorPntr == palColor)
		{
			return colorIndex;
		}
	}
	return NUM_PALETTE_COLORS;
}
u32 GetPackedIndexByPntr(const Color_t* palColorPntr)
{
	for (u32 colorIndex = 0; colorIndex < NUM_PALETTE_COLORS; colorIndex++)
	{
		const PalColor_t* palColor = &plt->colors[colorIndex];
		for (u8 subIndex = 0; subIndex < NUM_REPLACE_COLORS; subIndex++)
		{
			const Color_t* color = &palColor->colors[subIndex];
			if (color == palColorPntr)
			{
				return GetPackedIndex(colorIndex, subIndex);
			}
		}
	}
	return GetPackedIndex(0, 0);
}
u32 GetColorIndexFromPackedIndex(u32 packedIndex)
{
	u32 result = ((packedIndex & 0xFFFFFFFC) >> 2);
	return result;
}
u8 GetSubIndexFromPackedIndex(u32 packedIndex)
{
	u8 result = (u8)(packedIndex & 0x00000003);
	u8 actualResult = 0;
	if (result == 0x0) { actualResult = 8; } //primary
	if (result == 0x1) { actualResult = 9; } //secondary
	if (result == 0x2) { actualResult = 11; } //light
	if (result == 0x3) { actualResult = 4; } //dark
	Assert(actualResult < NUM_REPLACE_COLORS);
	return actualResult;
}

u32 GetSuperPackedColorForPaletteColor(u32 paletteIndex, u8 subIndex)
{
	Assert(paletteIndex < NUM_PALETTE_COLORS);
	Assert(subIndex < NUM_REPLACE_COLORS);
	u32 result = ((0x7FFFFFF0 & (paletteIndex << 4)) | (subIndex & 0x0F));
	return result;
}
u32 GetSuperPackedColorForColor(Color_t color)
{
	u32 result = SUPER_PACKED_COLOR_CUSTOM_BIT;
	result |= (((u32)(color.a/2) & 0x7F) << 24);
	result |= (((u32)color.r) << 16);
	result |= (((u32)color.g) << 8);
	result |= (((u32)color.b) << 0);
	return result;
}
bool IsSuperPackedColorCustom(u32 superPackedColor)
{
	return IsFlagSet(superPackedColor, SUPER_PACKED_COLOR_CUSTOM_BIT);
}
u32 GetSuperPackedColorPaletteIndex(u32 superPackedColor, u8* subIndexOut = nullptr)
{
	Assert(!IsFlagSet(superPackedColor, SUPER_PACKED_COLOR_CUSTOM_BIT));
	u8 subIndex = (u8)(superPackedColor & 0x0000000F);
	Assert(subIndex < NUM_REPLACE_COLORS);
	u32 paletteIndex = ((superPackedColor & 0x7FFFFFF0) >> 4);
	if (paletteIndex >= NUM_PALETTE_COLORS) { paletteIndex = 0; }
	if (subIndexOut != nullptr) { *subIndexOut = subIndex; }
	return paletteIndex;
}
Color_t GetSuperPackedColorValue(u32 superPackedColor)
{
	if (IsFlagSet(superPackedColor, SUPER_PACKED_COLOR_CUSTOM_BIT))
	{
		u8 alpha = ((u8)((superPackedColor >> 24) & 0x7F) * 2);
		u8 red   = (u8)((superPackedColor >> 16) & 0xFF);
		u8 green = (u8)((superPackedColor >> 8) & 0xFF);
		u8 blue  = (u8)((superPackedColor >> 0) & 0xFF);
		// PrintLine_D("Custom 0x%08X -> (%u, %u, %u, %u)", superPackedColor, red, green, blue, alpha);
		return NewColor(red, green, blue, alpha);
	}
	else
	{
		u8 subIndex = (u8)(superPackedColor & 0x0000000F);
		Assert(subIndex < NUM_REPLACE_COLORS);
		u32 paletteIndex = ((superPackedColor & 0x7FFFFFF0) >> 4);
		if (paletteIndex >= NUM_PALETTE_COLORS) { paletteIndex = 0; }
		// PrintLine_D("Palette[%u][%u]", paletteIndex, subIndex);
		return plt->colors[paletteIndex].colors[subIndex];
	}
}

PickedColor_t NewPickedColor(Color_t color)
{
	PickedColor_t result;
	result.isPalette = false;
	result.color = color;
	return result;
}
PickedColor_t NewPickedColorByIndex(u32 colorIndex, u8 subIndex)
{
	PickedColor_t result;
	result.isPalette = true;
	result.packedIndex = GetPackedIndex(colorIndex, subIndex);
	return result;
}
PickedColor_t NewPickedColorByPntr(const Color_t* palColorPntr)
{
	PickedColor_t result;
	result.isPalette = true;
	result.packedIndex = GetPackedIndexByPntr(palColorPntr);
	return result;
}
Color_t PickedColorValue(const PickedColor_t* pickedColor)
{
	Assert(pickedColor != nullptr);
	if (pickedColor->isPalette) { return GetPackedIndexPaletteColor(pickedColor->packedIndex); }
	else { return pickedColor->color; }
}
bool ArePickedColorsEqual(const PickedColor_t* color1, const PickedColor_t* color2)
{
	Assert(color1 != nullptr);
	Assert(color2 != nullptr);
	if (color1->isPalette != color2->isPalette) { return false; }
	if (color1->packedIndex != color2->packedIndex) { return false; }
	return true;
}

void SetPalColorAlpha(PalColor_t* palColor, r32 newAlpha)
{
	Assert(palColor != nullptr);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		palColor->colors[cIndex].alpha = ClampI32toU8(RoundR32i(newAlpha * 255));
	}
}
void PalColorLighten(PalColor_t* palColor, u8 amount)
{
	Assert(palColor != nullptr);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		palColor->colors[cIndex] = ColorLighten(palColor->colors[cIndex], amount);
	}
}
void PalColorDarken(PalColor_t* palColor, u8 amount)
{
	Assert(palColor != nullptr);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		palColor->colors[cIndex] = ColorDarken(palColor->colors[cIndex], amount);
	}
}