/*
File:   res_resource_list.h
Author: Taylor Robbins
Date:   05\04\2020
Description:
	** This is a special file that can be included multiple times to obtain different effects. Before the file is included you must define
	** RESOURCE_TEXTURE, RESOURCE_SHEET, RESOURCE_FONT_TTF, RESOURCE_FONT_BITMAP, RESOURCE_SHADER, and or RESOURCE_MUSIC in order for
	** this file to have any real effect (besides defining NUM_RESOURCE_... defines)
*/

// +--------------------------------------------------------------+
// |                        Sprite Sheets                         |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHEETS
#define NUM_RESOURCE_SHEETS 2
#endif
#ifndef RESOURCE_SHEET
#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) //nothing
#endif

RESOURCE_SHEET(entitiesSheet, "Entities Sheet", SHEETS_FLDR "_entities.png",      NewVec2i(32, 32), AppStateBit_Game);
RESOURCE_SHEET(cutsceneIcons, "Cutscene Icons", SHEETS_FLDR "cutscene_icons.png", NewVec2i(32,  3), AppState_None);

#undef RESOURCE_SHEET

// +--------------------------------------------------------------+
// |                           Textures                           |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_TEXTURES
#define NUM_RESOURCE_TEXTURES 2
#endif
#ifndef RESOURCE_TEXTURE
#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) //nothing
#endif

RESOURCE_TEXTURE(logoSprite, "Logo Sprite", SPRITES_FLDR "logo.png", true, false, AppStateBit_MainMenu);

RESOURCE_TEXTURE(alphaTexture, "Alpha Texture", TEXTURES_FLDR "alpha.png", true, false, AppStateBit_MainMenu);

#undef RESOURCE_TEXTURE

// +--------------------------------------------------------------+
// |                            Musics                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_MUSICS
#define NUM_RESOURCE_MUSICS 1
#endif
#ifndef RESOURCE_MUSIC
#define RESOURCE_MUSIC(varName, readableName, filePath) //nothing
#endif

RESOURCE_MUSIC(mainMenuSong, "Test Song", MUSIC_FLDR "test.ogg");

#undef RESOURCE_MUSIC

// +--------------------------------------------------------------+
// |                            Fonts                             |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_FONTS
#define NUM_RESOURCE_FONTS 2
#endif
#ifndef RESOURCE_FONT_TTF
#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) //nothing
#endif
#ifndef RESOURCE_FONT_BITMAP
#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars) //nothing
#endif

RESOURCE_FONT_TTF(debugFont,    "Debug Font", "Consolas", 18.0f, true, false, ' ', 96, 256);
RESOURCE_FONT_BITMAP(pixelFont, "Pixel Font", FONTS_FLDR "pixelFont.png", NewVec2i( 6,  8), 0, ' ', 96);

#undef RESOURCE_FONT_TTF
#undef RESOURCE_FONT_BITMAP

// +--------------------------------------------------------------+
// |                           Shaders                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHADERS
#define NUM_RESOURCE_SHADERS 5
#endif
#ifndef RESOURCE_SHADER
#define RESOURCE_SHADER(varName, readableName, filePath) //nothing
#endif

RESOURCE_SHADER(mainShader,         "Main Shader",          SHADERS_FLDR "main.glsl");
RESOURCE_SHADER(tileShader,         "Tile Shader",          SHADERS_FLDR "tile.glsl");
RESOURCE_SHADER(wipeShader,         "Wipe Shader",          SHADERS_FLDR "wipe.glsl");
RESOURCE_SHADER(fontShader,         "Font Shader",          SHADERS_FLDR "font.glsl");
RESOURCE_SHADER(longExposureShader, "Long Exposure Shader", SHADERS_FLDR "long_exposure.glsl");

#undef RESOURCE_SHADER

#ifndef NUM_RESOURCES
#define NUM_RESOURCES (NUM_RESOURCE_SHEETS + NUM_RESOURCE_TEXTURES + NUM_RESOURCE_MUSICS + NUM_RESOURCE_FONTS + NUM_RESOURCE_SHADERS)
#endif
