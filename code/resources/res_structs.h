/*
File:   res_serialization.h
Author: Taylor Robbins
Date:   09\04\2019
*/

#ifndef _RES_STRUCTS_H
#define _RES_STRUCTS_H

//u8, u16, u32, i32, r32, direction, bool, keyboardBtn, gamepadBtn, gameColor, paletteColor, tileType, animKeyType, animAspect, entityType, 
typedef enum
{
	SrlType_Type = 0,
	SrlType_String,
	SrlType_u8,
	SrlType_u16,
	SrlType_u32,
	SrlType_i32,
	SrlType_r32,
	SrlType_Bool,
	SrlType_Vec2,
	SrlType_Vec3,
	SrlType_Vec4,
	SrlType_Vec2i,
	SrlType_Vec3i,
	SrlType_Vec4i,
	SrlType_Rec,
	SrlType_Reci,
	SrlType_Dir2,
	SrlType_Dir2Diag,
	SrlType_PaletteColor,
	SrlType_PickedColor,
	SrlType_TileType,
	SrlType_EntityType,
	SrlType_Resource,
	SrlType_NumTypes,
} SrlType_t;

const char* GetSrlTypeStr(SrlType_t srlType)
{
	switch (srlType)
	{
		case SrlType_Type:         return "Type";
		case SrlType_String:       return "String";
		case SrlType_u8:           return "u8";
		case SrlType_u16:          return "u16";
		case SrlType_u32:          return "u32";
		case SrlType_i32:          return "i32";
		case SrlType_r32:          return "r32";
		case SrlType_Bool:         return "Bool";
		case SrlType_Vec2:         return "Vec2";
		case SrlType_Vec3:         return "Vec3";
		case SrlType_Vec4:         return "Vec4";
		case SrlType_Vec2i:        return "Vec2i";
		case SrlType_Vec3i:        return "Vec3i";
		case SrlType_Vec4i:        return "Vec4i";
		case SrlType_Rec:          return "Rec";
		case SrlType_Reci:         return "Reci";
		case SrlType_Dir2:         return "Dir2";
		case SrlType_Dir2Diag:     return "Dir2Diag";
		case SrlType_PaletteColor: return "PaletteColor";
		case SrlType_PickedColor:  return "PickedColor";
		case SrlType_TileType:     return "TileType";
		case SrlType_EntityType:   return "EntityType";
		case SrlType_Resource:     return "Resource";
		default: return "Unknown";
	}
}

typedef enum
{
	ParsingTokenType_Unknown = 0,
	ParsingTokenType_Header,
	ParsingTokenType_KeyValue,
	ParsingTokenType_ValueList,
	ParsingTokenType_Comment,
	ParsingTokenType_Other,
	ParsingTokenType_NumTypes,
} ParsingTokenType_t;

struct ParsingToken_t
{
	ParsingTokenType_t type;
	u32 start;
	u32 length;
	union
	{
		StrSplitPiece_t pieces[2];
		struct { StrSplitPiece_t header;  StrSplitPiece_t unused1; };
		struct { StrSplitPiece_t content; StrSplitPiece_t unused2; };
		struct { StrSplitPiece_t key;     StrSplitPiece_t value;   };
		struct { StrSplitPiece_t comment; StrSplitPiece_t unused3;   };
	};
};

struct TextParser_t
{
	const char* content;
	u32 contentLength;
	u32 position;
	u32 lineNumber;
	u32 numTokensFound;
	bool foundToken;
	u32 lineStart;
};

#endif //  _RES_STRUCTS_H
