/*
File:   res_sounds_list.h
Author: Taylor Robbins
Date:   11\21\2019
Description:
	** This is a special file that can be included multiple times to obatin different effects. Before the file is included you must define
	** SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName)
*/

#ifndef SOUND_EFFECT_ENTRY
#error res_sounds_list.h was included without SOUND_EFFECT_ENTRY being defined
#endif

#ifndef NUM_SOUND_EFFECT_RESOURCES
#define NUM_SOUND_EFFECT_RESOURCES 2
#endif

SOUND_EFFECT_ENTRY(testSound,         false, "Test",         "test.ogg");
SOUND_EFFECT_ENTRY(notificationSound, false, "Notification", "notification.ogg");

#undef SOUND_EFFECT_ENTRY
