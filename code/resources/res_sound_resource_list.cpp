/*
File:   res_sound_resource_list.cpp
Author: Taylor Robbins
Date:   11\21\2019
Description: 
	** Handles loading and re-loading sound effects as well as parsing and serializing the sounds_meta_info.txt file
*/

void DestroySoundResource(SoundResource_t* sndResource)
{
	Assert(sndResource != nullptr);
	if (sndResource->internalName != nullptr)
	{
		Assert(sndResource->allocArena != nullptr);
		ArenaPop(sndResource->allocArena, sndResource->internalName);
	}
	if (sndResource->readableName != nullptr)
	{
		Assert(sndResource->allocArena != nullptr);
		ArenaPop(sndResource->allocArena, sndResource->readableName);
	}
	if (sndResource->filePath != nullptr)
	{
		Assert(sndResource->allocArena != nullptr);
		ArenaPop(sndResource->allocArena, sndResource->filePath);
	}
	if (sndResource->sound.isValid)
	{
		DestroySound(&sndResource->sound);
	}
	ClearPointer(sndResource);
}
void CreateSoundResource(MemoryArena_t* memArena, SoundResource_t* sndResource, bool looping, const char* internalName, const char* readableName, const char* filePath)
{
	Assert(memArena != nullptr);
	Assert(sndResource != nullptr);
	Assert(readableName != nullptr);
	Assert(internalName != nullptr);
	
	ClearPointer(sndResource);
	sndResource->allocArena = memArena;
	
	sndResource->internalName = ArenaNtString(memArena, internalName);
	Assert(sndResource->internalName != nullptr);
	sndResource->readableName = ArenaNtString(memArena, readableName);
	Assert(sndResource->readableName != nullptr);
	if (filePath != nullptr)
	{
		sndResource->filePath = ArenaNtString(memArena, filePath);
		Assert(sndResource->filePath != nullptr);
	}
	
	sndResource->volume = 1.0f;
	sndResource->looping = looping;
	sndResource->filePathChanged = false;
}

bool ChangeSoundResourceFilePath(SoundResource_t* sndResource, const char* newFilePath, u32 newFilePathLength)
{
	Assert(sndResource != nullptr);
	Assert(newFilePath != nullptr || newFilePathLength == 0);
	if (sndResource->filePath == nullptr && (newFilePath == nullptr || newFilePathLength == 0)) { return false; } //already nullptr
	if (newFilePath != nullptr && newFilePathLength > 0 && sndResource->filePath != nullptr && MyStrLength32(sndResource->filePath) == newFilePathLength && MyStrCompare(sndResource->filePath, newFilePath, newFilePathLength) == 0) { return false; } //already the correct filePath
	Assert(sndResource->allocArena != nullptr);
	if (sndResource->filePath != nullptr)
	{
		#if DEVELOPER
		if (platform->IsWatchingFile(sndResource->filePath))
		{
			platform->UnwatchFile(sndResource->filePath);
		}
		#endif
		ArenaPop(sndResource->allocArena, sndResource->filePath);
		sndResource->filePath = nullptr;
	}
	if (newFilePath != nullptr && newFilePathLength > 0)
	{
		sndResource->filePath = ArenaString(sndResource->allocArena, newFilePath, newFilePathLength);
		Assert(sndResource->filePath != nullptr);
	}
	return true;
}

void DestorySoundResourceList(SoundResourceList_t* sndResourceList)
{
	Assert(sndResourceList != nullptr);
	for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++)
	{
		DestroySoundResource(&sndResourceList->sounds[sIndex]);
	}
	if (sndResourceList->missingSound.isValid) { DestroySound(&sndResourceList->missingSound); }
	if (sndResourceList->missingSoundLooping.isValid) { DestroySound(&sndResourceList->missingSoundLooping); }
	sndResourceList->filled = false;
}
void FillSoundResourceList(MemoryArena_t* memArena, SoundResourceList_t* sndResourceList)
{
	Assert(memArena != nullptr);
	Assert(sndResourceList != nullptr);
	
	ClearPointer(sndResourceList);
	#define SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName) CreateSoundResource(memArena, &sndResourceList->internalName, looping, #internalName, readableName, nullptr);
	#include "res_sounds_list.h"
	
	sndResourceList->missingSound = LoadSound(MISSING_SOUND_PATH);
	Assert(sndResourceList->missingSound.isValid || !AUDIO_ENABLED || USE_CUSTOM_AUDIO);
	sndResourceList->missingSoundLooping = LoadSound(MISSING_SOUND_PATH_LOOPING);
	Assert(sndResourceList->missingSoundLooping.isValid || !AUDIO_ENABLED || USE_CUSTOM_AUDIO);
	sndResourceList->filled = true;
}

// +--------------------------------------------------------------+
// |                       Deserialization                        |
// +--------------------------------------------------------------+

#define TEXT_SOUNDS_META_TITLE "Scrambler Sounds Meta Info :O"
#define TEXT_SOUNDS_META_TITLE_LENGTH 29

#define TEXT_SOUNDS_META_VERS_MAJOR 1
#define TEXT_SOUNDS_META_VERS_MINOR 0

bool SoundResourceListDeserializeMetaFile(SoundResourceList_t* sndResourceList, const char* fileData, u32 fileLength, u32* numFilePathsChangedOut = nullptr, u8* versionMajorOut = nullptr, u8* versionMinorOut = nullptr)
{
	Assert(sndResourceList != nullptr);
	Assert(sndResourceList->filled);
	Assert(fileData != nullptr || fileLength == 0);
	
	if (numFilePathsChangedOut != nullptr) { *numFilePathsChangedOut = 0; }
	if (fileLength < TEXT_SOUNDS_META_TITLE_LENGTH) { return false; }
	if (MyMemCompare(&fileData[0], TEXT_SOUNDS_META_TITLE, TEXT_SOUNDS_META_TITLE_LENGTH) != 0) { return false; }
	
	TextParser_t parser;
	CreateTextParser(&parser, &fileData[TEXT_SOUNDS_META_TITLE_LENGTH], fileLength - TEXT_SOUNDS_META_TITLE_LENGTH);
	
	bool foundVersion = false;
	u8 versionMajor = 0;
	u8 versionMinor = 0;
	bool isOldVersion = false;
	bool foundNumSounds = false;
	u32 numSoundsDeclared = 0;
	bool foundSoundsTag = false;
	u32 numSoundsFound = 0;
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token;
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing sounds meta file line %u: %s", parser.lineNumber, errorStr);
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		// +==============================+
		// |       Key-Value Token        |
		// +==============================+
		if (token.type == ParsingTokenType_KeyValue)
		{
			// +==============================+
			// |           Version            |
			// +==============================+
			if (StrCompareIgnoreCase(token.key.pntr, "Version", token.key.length))
			{
				v2i versionV2i = {};
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &versionV2i);
				if (errorStr != nullptr)
				{
					PrintLine_W("Error while parsing Sounds Meta File version number: %s", errorStr);
					return false;
				}
				if (versionV2i.x < 0 || versionV2i.x > 255)
				{
					PrintLine_W("Invalid Sounds Meta File major version number: %d", versionV2i.x);
					return false;
				}
				if (versionV2i.y < 0 || versionV2i.y > 255)
				{
					PrintLine_W("Invalid Sounds Meta File minor version number: %d", versionV2i.y);
					return false;
				}
				
				versionMajor = (u8)versionV2i.x;
				versionMinor = (u8)versionV2i.y;
				if (versionMajorOut != nullptr) { *versionMajorOut = versionMajor; }
				if (versionMinorOut != nullptr) { *versionMinorOut = versionMinor; }
				
				if (!(versionMajor == TEXT_SOUNDS_META_VERS_MAJOR && versionMinor == TEXT_SOUNDS_META_VERS_MINOR) &&
					!(versionMajor == TEXT_SOUNDS_META_VERS_MAJOR && versionMinor == TEXT_SOUNDS_META_VERS_MINOR-1))
				{
					PrintLine_W("Sounds Meta File is version %u.%u not %u.%u", versionMajor, versionMinor, TEXT_SOUNDS_META_VERS_MAJOR, TEXT_SOUNDS_META_VERS_MINOR);
				}
				isOldVersion = IsVersionBelow(versionMajor, versionMinor, TEXT_SOUNDS_META_VERS_MAJOR, TEXT_SOUNDS_META_VERS_MINOR);
				foundVersion = true;
			}
			// +==============================+
			// |          NumSounds           |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumSounds", token.key.length))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &numSoundsDeclared);
				if (errorStr != nullptr)
				{
					PrintLine_W("Error while parsing NumSounds in Sounds Meta File: %s", errorStr);
					return false;
				}
				foundNumSounds = true;
			}
			else
			{
				PrintLine_W("WARNING: Unknown key \"%.*s\" found in Sounds Meta File line %u", token.key.length, token.key.pntr, parser.lineNumber);
			}
		}
		// +==============================+
		// |        Comment Token         |
		// +==============================+
		else if (token.type == ParsingTokenType_Comment)
		{
			//Ignore the comment
		}
		// +==============================+
		// |       ValueList Token        |
		// +==============================+
		else if (token.type == ParsingTokenType_ValueList)
		{
			if (foundSoundsTag)
			{
				TempPushMark();
				u32 numPieces = 0;
				StrSplitPiece_t* pieces = SplitString(TempArena, token.content.pntr, token.content.length, ",", 1, &numPieces);
				Assert(pieces != nullptr || numPieces == 0);
				if (numPieces == 3)
				{
					//trim whitespace
					for (u32 pIndex = 0; pIndex < numPieces; pIndex++)
					{
						StrSplitPiece_t* piece = &pieces[pIndex];
						while (piece->length > 0 && IsCharClassWhitespace(piece->pntr[0])) { piece->pntr++; piece->length--; }
						while (piece->length > 0 && IsCharClassWhitespace(piece->pntr[piece->length - 1])) { piece->length--; }
					}
					
					r32 volumeR32 = 1.0f;
					errorStr = TryDeserializeR32(pieces[2].pntr, pieces[2].length, &volumeR32);
					if (errorStr == nullptr)
					{
						SoundResource_t* sndResourceMatch = nullptr;
						for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++)
						{
							SoundResource_t* sndResource = &sndResourceList->sounds[sIndex];
							if (sndResource->internalName != nullptr && MyStrLength32(sndResource->internalName) == pieces[0].length && StrCompareIgnoreCase(sndResource->internalName, pieces[0].pntr, pieces[0].length))
							{
								sndResourceMatch = sndResource;
								break;
							}
						}
						
						if (sndResourceMatch != nullptr)
						{
							if (sndResourceMatch->volume != volumeR32) { sndResourceMatch->volume = volumeR32; }
							
							bool filePathChanged = ChangeSoundResourceFilePath(sndResourceMatch, pieces[1].pntr, pieces[1].length);
							if (filePathChanged)
							{
								if (numFilePathsChangedOut != nullptr) { (*numFilePathsChangedOut)++; }
								sndResourceMatch->filePathChanged = true;
							}
						}
						else { PrintLine_W("WARNING: Unknown sound name in Sounds Meta File entry %u: \"%.*s\"", numSoundsFound, pieces[0].length, pieces[0].pntr); }
					}
					else { PrintLine_W("WARNING: Failed to parse volume in Sounds Meta File entry %u: \"%.*s\"", numSoundsFound, pieces[2].length, pieces[2].pntr); }
				}
				else { PrintLine_W("WARNING: Sounds Meta File entry %u had %u pieces not 3: \"%.*s\"", numSoundsFound, numPieces, token.content.length, token.content.pntr); }
				TempPopMark();
				numSoundsFound++;
			}
			else { PrintLine_W("WARNING: Found ValueList in SoundsMetaFile before [Sounds] tag on line %u", parser.lineNumber); }
		}
		// +==============================+
		// |          Tag Token           |
		// +==============================+
		else if (token.type == ParsingTokenType_Header)
		{
			if (StrCompareIgnoreCase(token.header.pntr, "Sounds", token.header.length))
			{
				if (!foundSoundsTag) { foundSoundsTag = true; }
				else { PrintLine_W("WARNING: Found second [Sounds] tag in Sounds Meta File on line %u", parser.lineNumber); }
			}
			else
			{
				PrintLine_W("WARNING: Unknown Tag in Sounds Meta File on line %u: \"%.*s\"", parser.lineNumber, token.header.length, token.header.pntr);
			}
		}
		else
		{
			PrintLine_W("WARNING: Found unknown token in Sounds Meta File line %u: \"%.*s\"", parser.lineNumber, token.pieces[0].length, token.pieces[0].pntr);
		}
	}
	
	if (!foundSoundsTag)
	{
		WriteLine_W("WARNING: Sounds Meta File contained no sounds tag");
	}
	if (numSoundsFound != numSoundsDeclared)
	{
		PrintLine_W("WARNING: Found %u sound entries instead of %u in the Sounds Meta File", numSoundsFound, numSoundsDeclared);
	}
	
	return true;
}

void LoadChangedSoundResources(SoundResourceList_t* sndResourceList, bool firstLoad, r32 baseLoadPercent, r32 soundsLoadPercent)
{
	Assert(sndResourceList != nullptr);
	Assert(sndResourceList->filled);
	
	u32 numChangedFiles = 0;
	for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++) { if (sndResourceList->sounds[sIndex].filePathChanged) { numChangedFiles++; } }
	r32 currentLoadPercent = baseLoadPercent;
	
	u32 numSoundsReloadedSuccessfully = 0;
	u32 numChangedFilesPaths = 0;
	for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++)
	{
		SoundResource_t* sndResource = &sndResourceList->sounds[sIndex];
		if (sndResource->filePathChanged)
		{
			Assert(sndResource->filePath != nullptr);
			Assert(sndResource->readableName != nullptr);
			numChangedFilesPaths++;
			sndResource->filePathChanged = false;
			
			Sound_t newSound = LoadSound(sndResource->filePath);
			if (newSound.isValid)
			{
				//TODO: Do we need to stop any currently playing sound instances?
				if (sndResource->sound.isValid) { DestroySound(&sndResource->sound); }
				sndResource->sound = newSound;
				numSoundsReloadedSuccessfully++;
				if (!firstLoad) { PrintLine_I("Reloaded sound \"%s\" from \"%s\"", sndResource->readableName, sndResource->filePath); }
				#if DEVELOPER
				platform->WatchFile(sndResource->filePath);
				#endif
			}
			else { /*PrintLine_E("Failed to load sound \"%s\" from filePath \"%s\"", sndResource->readableName, sndResource->filePath);*/ }
			if (firstLoad)
			{
				r32 nextLoadPercent = currentLoadPercent + (soundsLoadPercent / numChangedFiles);
				if (RoundR32i(nextLoadPercent*100) != RoundR32i(currentLoadPercent*100)) { platform->UpdateLoadingBar(nextLoadPercent); }
				currentLoadPercent = nextLoadPercent;
			}
		}
	}
	
	if (numChangedFilesPaths > 0)
	{
		if (numSoundsReloadedSuccessfully == numChangedFilesPaths) { PrintLine_I("%s %u sounds", firstLoad ? "Loaded" : "Reloaded", numSoundsReloadedSuccessfully); }
		else { PrintLine_E("%s %u/%u sounds", firstLoad ? "Loaded" : "Reloaded", numSoundsReloadedSuccessfully, numChangedFilesPaths); }
	}
}

void LoadSoundResourcesUsingMetaFile(SoundResourceList_t* sndResourceList, const char* metaInfoFilePath, bool firstLoad, r32 baseLoadPercent, r32 soundsLoadPercent)
{
	Assert(sndResourceList != nullptr);
	Assert(sndResourceList->filled);
	
	FileInfo_t metaFile = platform->ReadEntireFile(metaInfoFilePath);
	if (metaFile.content == nullptr)
	{
		PrintLine_E("Failed to open Sounds Meta File at \"%s\"", metaInfoFilePath);
		return;
	}
	
	for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++)
	{
		SoundResource_t* sndResource = &sndResourceList->sounds[sIndex];
		sndResource->filePathChanged = false;
		if (firstLoad) { Assert(!sndResource->sound.isValid); }
	}
	
	u32 numFilePathsChanged = 0;
	bool deserializeSuccess = SoundResourceListDeserializeMetaFile(sndResourceList, (const char*)metaFile.content, metaFile.size, &numFilePathsChanged);
	if (!deserializeSuccess) { NotifyPrint_E("Failed to deserialize the Sounds Meta File at \"%s\"", metaInfoFilePath); }
	if (firstLoad) { PrintLine_I("Found the paths declared for %u/%u sound effects", numFilePathsChanged, ArrayCount(sndResourceList->sounds)); }
	
	platform->FreeFileMemory(&metaFile);
	
	LoadChangedSoundResources(sndResourceList, firstLoad, baseLoadPercent, soundsLoadPercent);
	if (firstLoad)
	{
		platform->UpdateLoadingBar(baseLoadPercent + soundsLoadPercent);
	}
}

bool SoundResourceFileChanged(SoundResourceList_t* sndResourceList, const char* filePath)
{
	Assert(sndResourceList != nullptr);
	bool soundChanged = false;
	Assert(filePath != nullptr);
	for (u32 sIndex = 0; sIndex < ArrayCount(sndResourceList->sounds); sIndex++)
	{
		SoundResource_t* sndResource = &sndResourceList->sounds[sIndex];
		if (sndResource->filePath != nullptr && StrCompareIgnoreCaseNt(filePath, sndResource->filePath))
		{
			sndResource->filePathChanged = true;
			soundChanged = true;
		}
	}
	if (soundChanged)
	{
		LoadChangedSoundResources(sndResourceList, false, 0.0f, 0.0f);
	}
	return soundChanged;
}
