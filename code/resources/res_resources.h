/*
File:   res_resources.h
Author: Taylor Robbins
Date:   10\23\2018
*/

#ifndef _APP_RESOURCES_H
#define _APP_RESOURCES_H

#include "resources/res_resource_list.h" //#include with no prior defines to get NUM_RESOURCE_... defines

typedef enum
{
	ResourceType_SpriteSheet = 0,
	ResourceType_Texture,
	ResourceType_Music,
	ResourceType_Font,
	ResourceType_Shader,
	ResourceType_NumTypes,
	
	ResourceType_Unknown = 0xFF,
} ResourceType_t;

union ResourceSheets_t
{
	SpriteSheet_t array[NUM_RESOURCE_SHEETS];
	struct
	{
		#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) SpriteSheet_t varName
		#include "resources/res_resource_list.h"
	};
};
union ResourceTextures_t
{
	Texture_t array[NUM_RESOURCE_TEXTURES];
	struct
	{
		#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) Texture_t varName
		#include "resources/res_resource_list.h"
	};
};
union ResourceMusics_t
{
	Sound_t array[NUM_RESOURCE_MUSICS];
	struct
	{
		#define RESOURCE_MUSIC(varName, readableName, filePath) Sound_t varName
		#include "resources/res_resource_list.h"
	};
};
union ResourceFonts_t
{
	Font_t array[NUM_RESOURCE_FONTS];
	struct
	{
		#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) Font_t varName
		#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars)             Font_t varName
		#include "resources/res_resource_list.h"
	};
};
union ResourceShaders_t
{
	Shader_t array[NUM_RESOURCE_SHADERS];
	struct
	{
		#define RESOURCE_SHADER(varName, readableName, filePath) Shader_t varName
		#include "resources/res_resource_list.h"
	};
};

struct Resource_t
{
	bool wasPreloaded;
	u64 lastAccessTime;
	u64 lastLoadTime;
	char* filePath;
	char* readableName;
	ResourceType_t type;
	u32 subIndex;
	union
	{
		Texture_t texture;
		SpriteSheet_t sheet;
		Sound_t music;
		Font_t font;
		Shader_t shader;
	};
};

struct ResourceArrays_t
{
	BktArray_t sheets;
	BktArray_t textures;
	BktArray_t musics;
	BktArray_t fonts;
	BktArray_t shaders;
};

const char* GetSpriteSheetFilePath(u32 sheetIndex)
{
	u32 sIndex = 0;
	#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) if (sheetIndex == sIndex) { return filePath; } sIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetSpriteSheetReadableName(u32 sheetIndex)
{
	u32 sIndex = 0;
	#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) if (sheetIndex == sIndex) { return readableName; } sIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
v2i GetSpriteSheetSize(u32 sheetIndex)
{
	u32 sIndex = 0;
	#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) if (sheetIndex == sIndex) { return sheetSize; } sIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return Vec2i_Zero;
}
const char* GetTextureFilePath(u32 textureIndex)
{
	u32 tIndex = 0;
	#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) if (textureIndex == tIndex) { return filePath; } tIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetTextureReadableName(u32 textureIndex)
{
	u32 tIndex = 0;
	#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) if (textureIndex == tIndex) { return readableName; } tIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
bool IsTextureResourcePixelated(u32 textureIndex, bool* isLoopingOut)
{
	Assert(isLoopingOut != nullptr);
	u32 tIndex = 0;
	#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) if (textureIndex == tIndex) { *isLoopingOut = (looping); return (pixelated); } tIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	*isLoopingOut = true;
	return true;
}
const char* GetMusicFilePath(u32 musicIndex)
{
	u32 mIndex = 0;
	#define RESOURCE_MUSIC(varName, readableName, filePath) if (musicIndex == mIndex) { return filePath; } mIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetMusicReadableName(u32 musicIndex)
{
	u32 mIndex = 0;
	#define RESOURCE_MUSIC(varName, readableName, filePath) if (musicIndex == mIndex) { return readableName; } mIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetFontFilePath(u32 fontIndex)
{
	u32 fIndex = 0;
	#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) if (fontIndex == fIndex) { return fontName; } fIndex++;
	#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars)             if (fontIndex == fIndex) { return filePath; } fIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetFontReadableName(u32 fontIndex)
{
	u32 fIndex = 0;
	#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) if (fontIndex == fIndex) { return readableName; } fIndex++;
	#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars)             if (fontIndex == fIndex) { return readableName; } fIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
struct ResourceFontOptions_t
{
	r32 size;
	bool bold;
	bool italic;
	u8 firstCharacter;
	u8 numCharacters;
	i32 bitmapSize;
	v2i charSize;
	i32 charUnderhang;
};
void GetResourceFontOptions(u32 fontIndex, ResourceFontOptions_t* options)
{
	Assert(options != nullptr);
	u32 fIndex = 0;
	#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, fontBitmapSize) if (fontIndex == fIndex) { options->size = fontSize; options->bold = isBold; options->italic = isItalic; options->firstCharacter = firstChar; options->numCharacters = numChars; options->bitmapSize = fontBitmapSize; return; } fIndex++;
	#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, fontCharSize, fontCharUnderhang, firstChar, numChars)                 if (fontIndex == fIndex) { options->charSize = fontCharSize; options->charUnderhang = fontCharUnderhang; options->firstCharacter = firstChar; options->numCharacters = numChars; return; } fIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
}
const char* GetShaderFilePath(u32 shaderIndex)
{
	u32 sIndex = 0;
	#define RESOURCE_SHADER(varName, readableName, filePath) if (shaderIndex == sIndex) { return filePath; } sIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}
const char* GetShaderReadableName(u32 shaderIndex)
{
	u32 sIndex = 0;
	#define RESOURCE_SHADER(varName, readableName, filePath) if (shaderIndex == sIndex) { return readableName; } sIndex++;
	#include "resources/res_resource_list.h"
	Assert(false);
	return nullptr;
}

u32 GetNumResourcesOfType(ResourceType_t type)
{
	switch (type)
	{
		case ResourceType_SpriteSheet: return NUM_RESOURCE_SHEETS;
		case ResourceType_Texture:     return NUM_RESOURCE_TEXTURES;
		case ResourceType_Music:       return NUM_RESOURCE_MUSICS;
		case ResourceType_Font:        return NUM_RESOURCE_FONTS;
		case ResourceType_Shader:      return NUM_RESOURCE_SHADERS;
		default: return 0;
	}
}
u32 GetResourceIndexByType(ResourceType_t type, u32 subIndex)
{
	u32 baseIndex = 0;
	if (type == ResourceType_SpriteSheet) { Assert(subIndex < NUM_RESOURCE_SHEETS);   return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_SHEETS;
	if (type == ResourceType_Texture)     { Assert(subIndex < NUM_RESOURCE_TEXTURES); return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_TEXTURES;
	if (type == ResourceType_Music)       { Assert(subIndex < NUM_RESOURCE_MUSICS);   return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_MUSICS;
	if (type == ResourceType_Font)        { Assert(subIndex < NUM_RESOURCE_FONTS);    return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_FONTS;
	if (type == ResourceType_Shader)      { Assert(subIndex < NUM_RESOURCE_SHADERS);  return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_SHADERS;
	Assert(false);
	return baseIndex;
}

bool ShouldLoadResourceAlways(ResourceType_t resourceType, u32 subIndex)
{
	Assert(resourceType < ResourceType_NumTypes);
	if (resourceType == ResourceType_Texture)     { return false; }
	if (resourceType == ResourceType_SpriteSheet) { return false; }
	if (resourceType == ResourceType_Music)       { return false; }
	return true;
}

const char* GetResourceTypeStr(ResourceType_t type)
{
	switch (type)
	{
		case ResourceType_SpriteSheet: return "SpriteSheet";
		case ResourceType_Texture:     return "Texture";
		case ResourceType_Music:       return "Music";
		case ResourceType_Font:        return "Font";
		case ResourceType_Shader:      return "Shader";
		default: return "Unknown";
	};
}

#endif //_APP_RESOURCES_H
