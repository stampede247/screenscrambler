/*
File:   plat_monitors.cpp
Author: Taylor Robbins
Date:   09\26\2019
Description: 
	** This file handles keeping track of the monitors through GLFW. We keep track of the current monitor for the window and listen for changes to monitors
*/

void Plat_FillMonitorInfo()
{
	int numMonitors = 0;
	GLFWmonitor** monitors = glfwGetMonitors(&numMonitors);
	if (numMonitors <= 0)
	{
		Win32_ExitOnError("Couldn't find information about the available monitors through GLFW");
	}
	GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();
	if (primaryMonitor == nullptr) { primaryMonitor = monitors[0]; }
	
	platform.primaryMonitorIndex = 0;
	CreateDynamicArray(&platform.glfwMonitors, &platform.mainHeap, sizeof(MonitorInfo_t), 4, (u32)numMonitors);
	for (u32 mIndex = 0; mIndex < (u32)numMonitors; mIndex++)
	{
		GLFWmonitor* monitor = monitors[mIndex];
		MonitorInfo_t* info = DynArrayAdd(&platform.glfwMonitors, MonitorInfo_t);
		Assert(info != nullptr);
		ClearPointer(info);
		info->glfwMonitor = monitor;
		if (monitor == primaryMonitor) { platform.primaryMonitorIndex = mIndex; }
		glfwGetMonitorPos(monitor, &info->offset.x, &info->offset.y);
		info->originalOffset = info->offset;
		glfwGetMonitorPhysicalSize(monitor, &info->physicalSize.width, &info->physicalSize.height);
		glfwGetMonitorContentScale(monitor, &info->contentScale.x, &info->contentScale.y);
		glfwGetMonitorWorkarea(monitor, &info->workArea.x, &info->workArea.y, &info->workArea.width, &info->workArea.height);
		
		const char* monitorName = glfwGetMonitorName(monitor);
		if (monitorName == nullptr) { monitorName = "(No-Name)"; }
		info->name = ArenaNtString(&platform.mainHeap, monitorName);
		Assert(info->name != nullptr);
		
		i32 numVideoModes = 0;
		const GLFWvidmode* videoModes = glfwGetVideoModes(monitor, &numVideoModes);
		Assert(videoModes != nullptr);
		Assert(numVideoModes > 0);
		
		const GLFWvidmode* currentVideoMode = glfwGetVideoMode(monitor);
		if (currentVideoMode == nullptr) { currentVideoMode = &videoModes[0]; }
		
		info->numModes = (u32)numVideoModes;
		info->modes = PushArray(&platform.mainHeap, MonitorDisplayMode_t, info->numModes);
		Assert(info->modes != nullptr);
		
		info->currentMode = 0;
		for (u32 dIndex = 0; dIndex < info->numModes; dIndex++)
		{
			MonitorDisplayMode_t* mode = &info->modes[dIndex];
			const GLFWvidmode* vidMode = &videoModes[dIndex];
			ClearPointer(mode);
			mode->glfwMode = vidMode;
			if (Win32_AreGlfwVideoModesEqual(vidMode, currentVideoMode)) { info->currentMode = dIndex; }
			mode->resolution = NewVec2i(vidMode->width, vidMode->height);
			mode->bitDepth = vidMode->redBits + vidMode->greenBits + vidMode->blueBits;
			mode->refreshRate = (r32)vidMode->refreshRate;
		}
		info->originalMode = info->currentMode;
	}
}

//NOTE: Turns out this function wasn't actually needed right now since all of the monitor index variables
//      get recalculated every frame after monitors have changed
bool Plat_ShiftMonitorIndexAfterRemove(u32* indexVariablePntr, u32 removedIndex)
{
	Assert(indexVariablePntr != nullptr);
	if (*indexVariablePntr == removedIndex) { *indexVariablePntr = 0; return true; }
	else if (*indexVariablePntr > removedIndex) { *indexVariablePntr -= 1; return true; }
	else { return false; }
}

bool Plat_CheckForMonitorChanges()
{
	bool changesOccurred = false;
	i32 numMonitors = 0;
	GLFWmonitor** monitors = glfwGetMonitors(&numMonitors);
	Assert(monitors != nullptr);
	Assert(numMonitors > 0);
	
	TempPushMark();
	
	//Figure out which monitors disappeared
	bool* monitorRemoved = PushArray(TempArena, bool, platform.glfwMonitors.length);
	MyMemSet(monitorRemoved, 0x01, sizeof(bool) * platform.glfwMonitors.length);
	for (u32 mIndex1 = 0; mIndex1 < platform.glfwMonitors.length; mIndex1++)
	{
		MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex1);
		Assert(monitorInfo != nullptr);
		for (i32 mIndex2 = 0; mIndex2 < numMonitors; mIndex2++)
		{
			GLFWmonitor* monitor = monitors[mIndex2];
			Assert(monitor != nullptr);
			if (monitorInfo->glfwMonitor == monitor) { monitorRemoved[mIndex1] = false; break; }
		}
	}
	
	//Figure out which monitors are new
	bool* monitorAdded = PushArray(TempArena, bool, (u32)numMonitors);
	MyMemSet(monitorAdded, 0x01, sizeof(bool) * numMonitors);
	for (i32 mIndex1 = 0; mIndex1 < numMonitors; mIndex1++)
	{
		GLFWmonitor* monitor = monitors[mIndex1];
		Assert(monitor != nullptr);
		for (u32 mIndex2 = 0; mIndex2 < platform.glfwMonitors.length; mIndex2++)
		{
			MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex2);
			Assert(monitorInfo != nullptr);
			if (monitorInfo->glfwMonitor == monitor) { monitorAdded[mIndex1] = false; break; }
		}
	}
	
	//Remove monitors
	u32 removeIndex = 0;
	u32 numMonitorsBeforeRemove = platform.glfwMonitors.length;
	for (u32 mIndex = 0; mIndex < numMonitorsBeforeRemove; mIndex++)
	{
		if (monitorRemoved[mIndex])
		{
			changesOccurred = true;
			PrintLine_W("Monitor[%u] disconnected!", mIndex);
			MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, removeIndex);
			Assert(monitorInfo != nullptr);
			if (monitorInfo->name != nullptr) { ArenaPop(&platform.mainHeap, monitorInfo->name); }
			if (monitorInfo->modes != nullptr) { ArenaPop(&platform.mainHeap, monitorInfo->modes); }
			DynArrayRemove(&platform.glfwMonitors, removeIndex);
			//don't increment removeIndex
		}
		else { removeIndex++; }
	}
	
	//Add new monitors
	for (i32 mIndex = 0; mIndex < numMonitors; mIndex++)
	{
		if (monitorAdded[mIndex])
		{
			changesOccurred = true;
			GLFWmonitor* newMonitor = monitors[mIndex];
			Assert(newMonitor != nullptr);
			MonitorInfo_t* newMonitorInfo = DynArrayAdd(&platform.glfwMonitors, MonitorInfo_t);
			Assert(newMonitorInfo != nullptr);
			ClearPointer(newMonitorInfo);
			newMonitorInfo->glfwMonitor = newMonitor;
			glfwGetMonitorPos(newMonitor, &newMonitorInfo->offset.x, &newMonitorInfo->offset.y);
			newMonitorInfo->originalOffset = newMonitorInfo->offset;
			glfwGetMonitorPhysicalSize(newMonitor, &newMonitorInfo->physicalSize.width, &newMonitorInfo->physicalSize.height);
			glfwGetMonitorContentScale(newMonitor, &newMonitorInfo->contentScale.x, &newMonitorInfo->contentScale.y);
			glfwGetMonitorWorkarea(newMonitor, &newMonitorInfo->workArea.x, &newMonitorInfo->workArea.y, &newMonitorInfo->workArea.width, &newMonitorInfo->workArea.height);
			
			const char* monitorName = glfwGetMonitorName(newMonitor);
			if (monitorName == nullptr) { monitorName = "(No-Name)"; }
			newMonitorInfo->name = ArenaNtString(&platform.mainHeap, monitorName);
			Assert(newMonitorInfo->name != nullptr);
			
			i32 numVideoModes = 0;
			const GLFWvidmode* videoModes = glfwGetVideoModes(newMonitor, &numVideoModes);
			Assert(videoModes != nullptr);
			Assert(numVideoModes > 0);
			
			const GLFWvidmode* currentVideoMode = glfwGetVideoMode(newMonitor);
			if (currentVideoMode == nullptr) { currentVideoMode = &videoModes[0]; }
			
			newMonitorInfo->numModes = (u32)numVideoModes;
			newMonitorInfo->modes = PushArray(&platform.mainHeap, MonitorDisplayMode_t, newMonitorInfo->numModes);
			Assert(newMonitorInfo->modes != nullptr);
			
			newMonitorInfo->currentMode = 0;
			for (u32 dIndex = 0; dIndex < newMonitorInfo->numModes; dIndex++)
			{
				MonitorDisplayMode_t* mode = &newMonitorInfo->modes[dIndex];
				const GLFWvidmode* vidMode = &videoModes[dIndex];
				ClearPointer(mode);
				mode->glfwMode = vidMode;
				if (Win32_AreGlfwVideoModesEqual(vidMode, currentVideoMode)) { newMonitorInfo->currentMode = dIndex; }
				mode->resolution = NewVec2i(vidMode->width, vidMode->height);
				mode->bitDepth = vidMode->redBits + vidMode->greenBits + vidMode->blueBits;
				mode->refreshRate = (r32)vidMode->refreshRate;
			}
			newMonitorInfo->originalMode = newMonitorInfo->currentMode;
			
			PrintLine_N("Monitor[%u] connected: \"%s\" (%d, %d) (%dx%d)",
				platform.glfwMonitors.length-1, newMonitorInfo->name,
				newMonitorInfo->offset.x, newMonitorInfo->offset.y,
				currentVideoMode->width, currentVideoMode->height
			);
		}
	}
	TempPopMark();
	
	//Check for changes in primaryMonitor and each monitor's displayMode, offset, scale, etc.
	bool foundPrimaryMonitor = false;
	GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();
	if (primaryMonitor == nullptr) { primaryMonitor = monitors[0]; }
	for (u32 mIndex = 0; mIndex < platform.glfwMonitors.length; mIndex++)
	{
		MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex);
		Assert(monitorInfo != nullptr);
		if (monitorInfo->glfwMonitor == primaryMonitor)
		{
			if (platform.primaryMonitorIndex != mIndex)
			{
				PrintLine_D("Primary monitor changed from monitor[%u] to monitor[%u]", platform.primaryMonitorIndex, mIndex);
				platform.primaryMonitorIndex = mIndex;
				changesOccurred = true;
			}
			foundPrimaryMonitor = true;
		}
		
		v2i monitorPos = Vec2i_Zero;
		v2i monitorPhysicalSize = Vec2i_Zero;
		v2 monitorContentScale = Vec2_Zero;
		reci monitorWorkarea = Reci_Zero;
		glfwGetMonitorPos(monitorInfo->glfwMonitor, &monitorPos.x, &monitorPos.y);
		glfwGetMonitorPhysicalSize(monitorInfo->glfwMonitor, &monitorPhysicalSize.width, &monitorPhysicalSize.height);
		glfwGetMonitorContentScale(monitorInfo->glfwMonitor, &monitorContentScale.x, &monitorContentScale.y);
		glfwGetMonitorWorkarea(monitorInfo->glfwMonitor, &monitorWorkarea.x, &monitorWorkarea.y, &monitorWorkarea.width, &monitorWorkarea.height);
		
		if (monitorPos != monitorInfo->offset)
		{
			PrintLine_I("Monitor[%u] offset changed (%d, %d) -> (%d, %d)", mIndex, monitorInfo->offset.x, monitorInfo->offset.y, monitorPos.x, monitorPos.y);
			monitorInfo->offset = monitorPos;
			changesOccurred = true;
		}
		if (monitorPhysicalSize != monitorInfo->physicalSize)
		{
			PrintLine_I("Monitor[%u] physical size changed (%d, %d) -> (%d, %d)", mIndex, monitorInfo->physicalSize.x, monitorInfo->physicalSize.y, monitorPhysicalSize.x, monitorPhysicalSize.y);
			monitorInfo->physicalSize = monitorPhysicalSize;
			changesOccurred = true;
		}
		if (monitorContentScale != monitorInfo->contentScale)
		{
			PrintLine_I("Monitor[%u] content scale changed (%f, %f) -> (%f, %f)", mIndex, monitorInfo->contentScale.x, monitorInfo->contentScale.y, monitorContentScale.x, monitorContentScale.y);
			monitorInfo->contentScale = monitorContentScale;
			changesOccurred = true;
		}
		if (monitorWorkarea != monitorInfo->workArea)
		{
			PrintLine_I("Monitor[%u] work area changed (%d, %d, %d, %d) -> (%d, %d, %d, %d)", mIndex,
				monitorInfo->workArea.x, monitorInfo->workArea.y, monitorInfo->workArea.width, monitorInfo->workArea.height,
				monitorWorkarea.x, monitorWorkarea.y, monitorWorkarea.width, monitorWorkarea.height
			);
			monitorInfo->workArea = monitorWorkarea;
			changesOccurred = true;
		}
		
		i32 numVideoModes = 0;
		const GLFWvidmode* videoModes = glfwGetVideoModes(monitorInfo->glfwMonitor, &numVideoModes);
		Assert(videoModes != nullptr);
		Assert(numVideoModes > 0);
		Assert((u32)numVideoModes == monitorInfo->numModes); //we currently don't support changes to video modes list on an existing monitor
		const GLFWvidmode* currentVideoMode = glfwGetVideoMode(monitorInfo->glfwMonitor);
		if (currentVideoMode == nullptr) { currentVideoMode = &videoModes[0]; }
		
		bool foundCurrentDisplayMode = false;
		for (u32 dIndex = 0; dIndex < monitorInfo->numModes; dIndex++)
		{
			Assert(monitorInfo->modes != nullptr);
			MonitorDisplayMode_t* displayMode = &monitorInfo->modes[dIndex];
			if (Win32_AreGlfwVideoModesEqual(displayMode->glfwMode, currentVideoMode))
			{
				if (monitorInfo->currentMode != dIndex)
				{
					PrintLine_I("Monitor[%u] changed to display mode: %dx%d %ubit %.1f", mIndex, displayMode->resolution.width, displayMode->resolution.height, displayMode->bitDepth, displayMode->refreshRate);
					monitorInfo->currentMode = dIndex;
					changesOccurred = true;
				}
				foundCurrentDisplayMode = true;
				break;
			}
		}
		if (!foundCurrentDisplayMode)
		{
			if (monitorInfo->currentMode != 0)
			{
				PrintLine_W("Out of %u video modes on monitor %u we couldn't match the current video mode: %dx%d %dbit %dHz",
					monitorInfo->numModes, mIndex,
					currentVideoMode->width, currentVideoMode->height,
					currentVideoMode->redBits + currentVideoMode->greenBits + currentVideoMode->blueBits,
					currentVideoMode->refreshRate
				);
				#if 1
				for (u32 dIndex = 0; dIndex < monitorInfo->numModes; dIndex++)
				{
					Assert(monitorInfo->modes != nullptr);
					MonitorDisplayMode_t* displayMode = &monitorInfo->modes[dIndex];
					PrintLine_W("  Mode[%u]: %dx%d %dbit %dHz", dIndex,
						displayMode->glfwMode->width, displayMode->glfwMode->height,
						displayMode->glfwMode->redBits + displayMode->glfwMode->greenBits + displayMode->glfwMode->blueBits,
						displayMode->glfwMode->refreshRate
					);
				}
				#endif
				monitorInfo->currentMode = 0;
			}
			foundCurrentDisplayMode = true;
		}
		Assert(foundCurrentDisplayMode);
	}
	Assert(foundPrimaryMonitor);
	
	return changesOccurred;
}

bool Plat_GetWindowMonitorIndex(GLFWwindow* window, u32* windowMonitorIndexOut = nullptr, u32* windowDisplayModeIndexOut = nullptr, bool* windowIsFullscreenOut = nullptr, r32* windowTargetFramerateOut = nullptr)
{
	Assert(window != nullptr);
	
	GLFWmonitor* monitor = glfwGetWindowMonitor(window);
	
	if (monitor != nullptr) //fullscreen
	{
		for (u32 mIndex = 0; mIndex < platform.glfwMonitors.length; mIndex++)
		{
			MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex);
			Assert(monitorInfo != nullptr);
			if (monitorInfo->glfwMonitor == monitor)
			{
				Assert(monitorInfo->currentMode < monitorInfo->numModes);
				Assert(monitorInfo->modes != nullptr);
				MonitorDisplayMode_t* displayMode = &monitorInfo->modes[monitorInfo->currentMode];
				
				if (windowMonitorIndexOut != nullptr) { *windowMonitorIndexOut = mIndex; }
				if (windowDisplayModeIndexOut != nullptr) { *windowDisplayModeIndexOut = monitorInfo->currentMode; }
				if (windowIsFullscreenOut != nullptr) { *windowIsFullscreenOut = true; }
				if (windowTargetFramerateOut != nullptr) { *windowTargetFramerateOut = displayMode->refreshRate; }
				return true;
			}
		}
		return false;
	}
	else //windowed
	{
		rec windowRec = NewRec((r32)platform.windowPosition.x, (r32)platform.windowPosition.y, (r32)platform.screenSize.width, (r32)platform.screenSize.height);
		r32 foundMonitorOverlap = 0;
		for (u32 mIndex = 0; mIndex < platform.glfwMonitors.length; mIndex++)
		{
			MonitorInfo_t* monitorInfo = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex);
			Assert(monitorInfo != nullptr);
			Assert(monitorInfo->currentMode < monitorInfo->numModes);
			Assert(monitorInfo->modes != nullptr);
			MonitorDisplayMode_t* displayMode = &monitorInfo->modes[monitorInfo->currentMode];
			rec monitorRec = NewRec((r32)monitorInfo->offset.x, (r32)monitorInfo->offset.y, (r32)displayMode->resolution.width, (r32)displayMode->resolution.height);
			rec overlapRec = RecOverlap(monitorRec, windowRec);
			r32 overlapAmount = overlapRec.width * overlapRec.height;
			if (overlapAmount > foundMonitorOverlap || (mIndex == platform.primaryMonitorIndex && foundMonitorOverlap <= 0))
			{
				if (windowMonitorIndexOut != nullptr) { *windowMonitorIndexOut = mIndex; }
				if (windowDisplayModeIndexOut != nullptr) { *windowDisplayModeIndexOut = monitorInfo->currentMode; }
				if (windowIsFullscreenOut != nullptr) { *windowIsFullscreenOut = false; }
				if (windowTargetFramerateOut != nullptr) { *windowTargetFramerateOut = displayMode->refreshRate; }
				foundMonitorOverlap = overlapAmount;
			}
		}
		return true;
	}
}

void Plat_UpdateAppInputMonitorInfo(AppInput_t* appInput)
{
	Assert(appInput != nullptr);
	
	bool foundWindowMonitor = Plat_GetWindowMonitorIndex(platform.window, &platform.windowMonitorIndex, &platform.windowDisplayModeIndex, &platform.windowIsFullscreen, &platform.windowTargetFramerate);
	Assert(foundWindowMonitor);
	
	if (appInput->currentMonitorIndex != platform.windowMonitorIndex)
	{
		PrintLine_D("Window changed from monitor %u to %u", appInput->currentMonitorIndex, platform.windowMonitorIndex);
		appInput->currentMonitorIndex = platform.windowMonitorIndex;
		appInput->currentMonitorChanged = true;
		platform.doingManualUpdateLoopWait = false;
	}
	if (appInput->currentDisplayModeIndex != platform.windowDisplayModeIndex || appInput->currentMonitorIndex != platform.windowMonitorIndex)
	{
		WriteLine_D("Window changed displayMode");
		appInput->currentDisplayModeIndex = platform.windowDisplayModeIndex;
		appInput->currentDisplayModeChanged = true;
		platform.doingManualUpdateLoopWait = false;
	}
	if (appInput->targetFramerate != platform.windowTargetFramerate)
	{
		PrintLine_D("Window changed targetFramerate %f -> %f", appInput->targetFramerate, platform.windowTargetFramerate);
		appInput->targetFramerate = platform.windowTargetFramerate;
		appInput->targetFramerateChanged = true;
		platform.doingManualUpdateLoopWait = false;
	}
	if (appInput->isFullscreen != platform.windowIsFullscreen)
	{
		PrintLine_D("Window became %s", platform.windowIsFullscreen ? "Fullscreen" : "Windowed");
		appInput->isFullscreen = platform.windowIsFullscreen;
		appInput->fullscreenChanged = true;
		platform.doingManualUpdateLoopWait = false;
	}
}

