/*
File:   app_version.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_VERSION_H
#define _APP_VERSION_H

#define APP_VERSION_MAJOR    0
#define APP_VERSION_MINOR    10
//NOTE: Auto-incremented by a python script before each build
#define APP_VERSION_BUILD    3511
//0.09 built 1257 times
//0.08 built 805 times
//0.07 built 1246 times
//0.06 built 41 times
//0.05 built 1780 times
//0.04 built 1067 times
//0.03 built 262 times
//0.02 built 320 times
//0.01 built 18022 times

#endif //  _APP_VERSION_H
