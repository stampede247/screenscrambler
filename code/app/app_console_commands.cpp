/*
File:   app_console_commands.cpp
Author: Taylor Robbins
Date:   03\22\2019
Description: 
	** Holds the HandleConsoleCommand function
*/

const char* consoleCommands[] =
{
	// +==============================+
	// |       General Commands       |
	// +==============================+
	"help {page}",               "Displays a list of available commands. A page number can be passed optionally",
	"test",                      "Used for testing various things",
	
	// +==============================+
	// |        Other Commands        |
	// +==============================+
	"show_gutter",   "Enables the gutter line numbers in the debug console",
	"hide_gutter",   "Disables the gutter line numbers in the debug console",
	"toggle_gutter", "Toggles the gutter line numbers in the debug console",
	"show_files",    "Enables the file names display in the debug console",
	"hide_files",    "Disables the file names display in the debug console",
	"toggle_files",  "Toggles the file names display in the debug console",
	"show_lines",    "Enables the file lines numbers in the debug console",
	"hide_lines",    "Disables the file lines numbers in the debug console",
	"toggle_lines",  "Toggles the file lines numbers in the debug console",
	"show_funcs",    "Enables the function names display in the debug console",
	"hide_funcs",    "Disables the function names display in the debug console",
	"toggle_funcs",  "Toggles the function names display in the debug console",
};

//NOTE: Also defined in app_func_defs.h
void HandleConsoleCommand(const char* command)
{
	u32 commandLength = MyStrLength32(command);
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitString(TempArena, command, commandLength, " ", 1, &numPieces);
	if (numPieces == 0 || pieces[0].length == 0) { return; }
	char* baseCmd = ArenaString(TempArena, pieces[0].pntr, pieces[0].length);
	
	// +--------------------------------------------------------------+
	// |                       General Commands                       |
	// +--------------------------------------------------------------+
	// +==============================+
	// |             help             |
	// +==============================+
	if (MyStrCompareNt(baseCmd, "help") == 0)
	{
		if (numPieces == 1)
		{ 
			PrintLine_N("%u Commands:", ArrayCount(consoleCommands)/2);
			for (u32 cIndex = 0; cIndex < ArrayCount(consoleCommands); cIndex += 2)
			{
				PrintLine_I("[%d] %s", cIndex/2, consoleCommands[cIndex]);
			}
		}
		else if (numPieces == 2)
		{
			u32 helpId = 0;
			if (TryParseU32(pieces[1].pntr, pieces[1].length, &helpId))
			{
				if (helpId*2 >= ArrayCount(consoleCommands)) { WriteLine_E("Invalid number. If you try again it might work! :)"); return; }
				PrintLine_I("%s - %s", consoleCommands[helpId*2], consoleCommands[helpId*2 + 1]);
			}
			else
			{
				bool foundCommand = false;
				for (u32 cIndex = 0; cIndex < ArrayCount(consoleCommands); cIndex += 2)
				{
					if (StrCompareIgnoreCase(consoleCommands[cIndex], pieces[1].pntr, pieces[1].length))
					{
						PrintLine_I("%s - %s", consoleCommands[cIndex], consoleCommands[cIndex + 1]);
						foundCommand = true;
						break;
					}
				}
				if (!foundCommand) { PrintLine_E("The command %.*s was not recognized you fool!!!",pieces[1].length, pieces[1].pntr); }
			}
		}
		else
		{
			WriteLine_W("Usage: help {command}");
		}
	}
	// +==============================+
	// |             test             |
	// +==============================+
	else if (MyStrCompareNt(baseCmd, "test") == 0)
	{
		PrintLine_I("Nothing to test right now");
		
		// SoundEffectSingle(testSound, Vec2i_Zero);
		// Notify_D("Test");
		
		// TempPushMark();
		// OtherWindowIter_t windowIter = {};
		// OtherWindow_t window = {};
		// while (platform->GetOtherWindow(&windowIter, TempArena, &window))
		// {
		// 	Assert(window.name != nullptr);
		// 	PrintLine_D("Window[%u]: \"%s\"", window.index, window.name);
		// }
		// PrintLine_D("Found %u windows", windowIter.windowIndex);
		// TempPopMark();
		
		OtherWindow_t targetWindow = {}; //TODO: Fill me!
		ScreenCapture_t capture = {};
		if (platform->CaptureScreenBitmap(&targetWindow, Reci_Zero, &capture))
		{
			PrintLine_I("Captured %dx%d screenshot of target program", capture.size.width, capture.size.height);
			
			game->captureTexture = CreateTexture(capture.data, capture.size.width, capture.size.height, true, false);
			
			platform->FreeScreenCaptureMemory(&capture);
		}
		else
		{
			WriteLine_E("Failed to capture the screen of target program");
		}
	}
	
	// +--------------------------------------------------------------+
	// |                        Other Commands                        |
	// +--------------------------------------------------------------+
	// +==========================================+
	// | show_gutter, hide_gutter, toggle_gutter  |
	// +==========================================+
	else if (MyStrCompareNt(baseCmd, "show_gutter") == 0 || MyStrCompareNt(baseCmd, "hide_gutter") == 0 || MyStrCompareNt(baseCmd, "toggle_gutter") == 0)
	{
		bool show = app->dbgConsole.showGutterNumbers;
		if (MyStrCompareNt(baseCmd, "show_gutter") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_gutter") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_gutter") == 0) { show = !show; }
		if (show != app->dbgConsole.showGutterNumbers)
		{
			app->dbgConsole.showGutterNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Gutter Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_files, hide_files, toggle_files |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_files") == 0 || MyStrCompareNt(baseCmd, "hide_files") == 0 || MyStrCompareNt(baseCmd, "toggle_files") == 0)
	{
		bool show = app->dbgConsole.showFileNames;
		if (MyStrCompareNt(baseCmd, "show_files") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_files") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_files") == 0) { show = !show; }
		if (show != app->dbgConsole.showFileNames)
		{
			app->dbgConsole.showFileNames = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console File Names", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_lines, hide_lines, toggle_lines |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_lines") == 0 || MyStrCompareNt(baseCmd, "hide_lines") == 0 || MyStrCompareNt(baseCmd, "toggle_lines") == 0)
	{
		bool show = app->dbgConsole.showLineNumbers;
		if (MyStrCompareNt(baseCmd, "show_lines") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_lines") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_lines") == 0) { show = !show; }
		if (show != app->dbgConsole.showLineNumbers)
		{
			app->dbgConsole.showLineNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Line Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_funcs, hide_funcs, toggle_funcs |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_funcs") == 0 || MyStrCompareNt(baseCmd, "hide_funcs") == 0 || MyStrCompareNt(baseCmd, "toggle_funcs") == 0)
	{
		bool show = app->dbgConsole.showFuncNames;
		if (MyStrCompareNt(baseCmd, "show_funcs") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_funcs") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_funcs") == 0) { show = !show; }
		if (show != app->dbgConsole.showFuncNames)
		{
			app->dbgConsole.showFuncNames = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Function Names", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_tasks, hide_tasks, toggle_tasks |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_tasks") == 0 || MyStrCompareNt(baseCmd, "hide_tasks") == 0 || MyStrCompareNt(baseCmd, "toggle_tasks") == 0)
	{
		bool show = app->dbgConsole.showTaskNumbers;
		if (MyStrCompareNt(baseCmd, "show_tasks") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_tasks") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_tasks") == 0) { show = !show; }
		if (show != app->dbgConsole.showTaskNumbers)
		{
			app->dbgConsole.showTaskNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Task Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	
	// +--------------------------------------------------------------+
	// |                       Unknown Commands                       |
	// +--------------------------------------------------------------+
	else
	{
		PrintLine_W("Unknown command");
	}
}
