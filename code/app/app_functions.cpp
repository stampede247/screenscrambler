/*
File:   app_functions.cpp
Author: Taylor Robbins
Date:   10\21\2018
Description: 
	** Holds some functions that the app uses to do various things 
*/

//NOTE: This function is also called when threaded tasks start but none of the locations should move unless the app memory has been reallocated
//      and that only happens when all tasks have been stopped and nothing in the application DLL is running so it shouldn't be a problem.
void AppEntryPoint(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput, bool workerThread)
{
	platform = PlatformInfo;
	if (!workerThread)
	{
		appInput = AppInput;
		appOutput = AppOutput;
	}
	app = (AppData_t*)AppMemory->permanantPntr;
	game = &app->gameData;
	mess = &app->messData;
	giffer = &app->gifferData;
	TempArena = &app->tempArena;
	plt = app->colorPalette.colors;
	resources = &app->resources;
	soundsList = &app->sndResourceList;
	rc = &app->renderContext;
	sett = &app->settings;
	mainHeap = &app->mainHeap;
	staticHeap = &app->staticHeap;
	mainMenu = &app->mainMenuData;
	appRand = &app->appRandom;
	
	MyLibDebugOutputFunc = AppDebugOutput;
	MyLibDebugPrintFunc = AppDebugPrint;
	
	if (appInput != nullptr && !workerThread)
	{
		RenderScreenSize = NewVec2((r32)appInput->screenSize.x, (r32)appInput->screenSize.y);
		RenderMousePos = NewVec2(appInput->mousePos.x, appInput->mousePos.y);
		RenderMouseStartPos = NewVec2(appInput->mouseStartPos[MouseButton_Left].x, appInput->mouseStartPos[MouseButton_Left].y);
		ProgramTime = appInput->programTime;
		ElapsedMs = (r32)appInput->elapsedMs;
		if (app->gifStream.isActive)
		{
			ElapsedMs = (1000.0f / 60.0f);//GIF_FRAME_TIME;
		}
		SystemTime = appInput->systemTime;
		LocalTime = appInput->localTime;
	}
	
	if (app != nullptr && platform != nullptr && app->mainHeap.base != nullptr && !workerThread)
	{
		MemoryArenaUpdateAllocateFunction(&app->mainHeap, platform->AllocateMemory);
	}
	if (app != nullptr && platform != nullptr && app->platHeap.type == MemoryArenaType_External && !workerThread)
	{
		MemoryArenaUpdateAllocateFunction(&app->platHeap, platform->AllocateMemory);
		MemoryArenaUpdateFreeFunction(&app->platHeap, platform->FreeMemory);
	}
	if (app != nullptr && platform != nullptr && !workerThread)
	{
		MainThreadId = platform->GetThisThreadId();
	}
	if (app != nullptr)
	{
		DbgConsoleUpdateFunctionPointers(&app->dbgConsole);
	}
}
