/*
File:   app_gif.h
Author: Taylor Robbins
Date:   09\27\2019
*/

#ifndef _APP_GIF_H
#define _APP_GIF_H

#define GIF_SIGNATURE_STRING      "GIF89a"
#define GIF_IMAGE_DELIM_CHAR      0x2C //','
#define GIF_EXTENSION_DELIM_CHAR  0x21 //'!'
#define GIF_EXT_COMMENT_COMMAND   0xFE
#define GIF_GRAPHIC_CONTROL_LABEL 0xF9
#define GIF_APP_EXT_BLOCK_LABEL   0xFF
#define GIF_APP_EXT_BLOCK_STR     "NETSCAPE2.0"
#define GIF_REP_COUNT_INFINITE    0x0000
#define GIF_DISPOSAL_METHOD_NONE  0b000
#define GIF_DISPOSAL_METHOD_KEEP  0b001
#define GIF_DISPOSAL_METHOD_CLEAR 0b010
#define GIF_DISPOSAL_METHOD_RESET 0b011
#define GIF_EXT_COMMENT_STRING    "Creating by " GAME_WINDOW_TITLE
#define GIF_TERMINATION_CHAR      ';'
#define GIF_CLEAR_CODE            0b100000000
#define GIF_END_OF_INFO_CODE      (GIF_CLEAR_CODE+1)
#define APPROX_GIF_OVERHEAD       (6+6+256+6)
#define MAX_GIF_LZW_CODE_WIDTH    12 //bits
#define GIF_FRAME_TIME            20 //ms (must be a multiple of 10)

struct GifFrame_t
{
	GifFrame_t* next;
	u32 pixelDataSize;
	u8* pixelData;
	reci changedRec;
};

struct GifStream_t
{
	bool isActive;
	MemoryArena_t* allocArena;
	
	union
	{
		struct { v2i topLeft, size; };
		struct { i32 x, y, width, height; };
		reci sourceRec;
	};
	char* filePath;
	
	u32 numFrames;
	GifFrame_t* firstFrame;
	GifFrame_t* lastFrame;
	GifFrame_t currentFrame;
	
	u32 fileSize;
	OpenFile_t file;
	bool encodedSuccessfully;
};

void GifU8(GifStream_t* stream, u8 value);

#endif //  _APP_GIF_H
