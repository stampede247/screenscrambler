/*
File:   app.cpp
Author: Taylor Robbins
Date:   06\06\2017
Description:
	** Contains all the exported functions and #includes
	** the rest of the source code files.
*/

#define STREAMING false

#define RESET_APPLICATION              false
#define USE_MY_STD_LIB                 false
#define USE_MIN_STD_LIB                true
#define USE_ASSERT_FAILURE_FUNCTION    true
#define THREAD_SAFE_ASSERT             true
#define MAX_CRASH_DUMPS                4

#include "plat/plat_interface.h"
const PlatformInfo_t* platform = nullptr;
const AppInput_t* appInput = nullptr;
AppOutput_t* appOutput = nullptr;
ThreadId_t MainThreadId = 0;

#include "app/app_version.h"

#define STBI_MALLOC(size)             platform->AllocateMemory((u32)(size))
#define STBI_REALLOC(pntr, newSize)   platform->ReallocateMemory(pntr, (u32)(newSize))
#define STBI_FREE(pntr)               platform->FreeMemory(pntr)
#define STBI_ASSERT(expression)       Assert(expression)
#define STBI_NO_STDIO
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

#define STBIW_MALLOC(size)                  platform->AllocateMemory((u32)(size))
#define STBIW_REALLOC(pntr, newSize)        platform->ReallocateMemory(pntr, (u32)(newSize))
#define STBIW_FREE(pntr)                    platform->FreeMemory(pntr)
#define STBIW_MEMMOVE(dest, source, length) MyMemMove(dest, source, length)
#define STBIW_ASSERT(expression)            Assert(expression)
#define STBI_WRITE_NO_STDIO
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"

// #define STBTT_ifloor
// #define STBTT_iceil
// #define STBTT_sqrt
// #define STBTT_fabs
#define STBTT_malloc(size, u)              ((void)(u),platform->AllocateMemory((u32)(size)))
#define STBTT_free(pntr, u)                ((void)(u),platform->FreeMemory(pntr))
#define STBTT_assert(expression)           Assert(expression)
#define STBTT_strlen(str)                  MyStrLength(str)
#define STBTT_memcpy(dest, source, length) MyMemCopy(dest, source, length)
#define STBTT_memset(dest, value, length)  MyMemSet(dest, value, length)
#define STBTT_assert(expression) Assert(expression)
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb/stb_truetype.h"

#include "mylib/my_tempMemory.h"
#include "mylib/my_tempMemory.cpp"
#include "mylib/my_linkedList.h"
#include "mylib/my_boundedStrList.h"
#include "mylib/my_dynamicArray.h"
#include "mylib/my_stringArray.h"
#include "mylib/my_bucketArray.h"

// +--------------------------------------------------------------+
// |                     Application Includes                     |
// +--------------------------------------------------------------+
#include "app/app_defines.h"
#include "app/app_structs.h"
#include "app/app_performance.h"
#include "app/app_audio_interface.h"
#include "app/app_tasks.h"
#include "resources/res_sound_resource_list.h"
#include "app/app_gif.h"
#include "app/app_input.h"
#include "app/app_settings.h"
#include "resources/res_palette.h"
#include "app/app_dynamic_color.h"
#include "app/app_sprite.h"
#include "app/app_render_context.h"
#include "app/app_text_box.h"
#include "app/app_notifications.h"
#include "app/app_screen_wipe.h"
#include "app/app_states.h"
#include "app/app_console_arena.h"
#include "app/app_console.h"
#include "resources/res_resources.h"
#include "resources/res_structs.h"

// +==============================+
// |          App States          |
// +==============================+
#include "game/game.h"
#include "main_menu/main_menu.h"
#include "mess/mess.h"
#include "giffer/giffer.h"

#include "app/app.h"

// +--------------------------------------------------------------+
// |                 Application Global Variables                 |
// +--------------------------------------------------------------+
AppData_t*          app          = nullptr;
MainMenuData_t*     mainMenu     = nullptr;
GameData_t*         game         = nullptr;
MessData_t*         mess         = nullptr;
GifferData_t*       giffer       = nullptr;

ResourceArrays_t* resources = nullptr;
SoundResourceList_t* soundsList = nullptr;
RenderContext_t* rc = nullptr;
const PaletteColorList_t* plt = nullptr;
v2 RenderScreenSize = {};
v2 RenderMousePos = {};
v2 RenderMouseStartPos = {};
AppSettings_t* sett = nullptr;
MemoryArena_t* mainHeap = nullptr;
MemoryArena_t* staticHeap = nullptr;
u64 ProgramTime = 0;
r32 ElapsedMs = 0;
RealTime_t SystemTime = {};
RealTime_t LocalTime = {};
RandomSeries_t* appRand = nullptr;

// +--------------------------------------------------------------+
// |                   Application Source Files                   |
// +--------------------------------------------------------------+
#include "app/app_func_defs.h"
#include "app/app_settings.cpp"
#include "app/app_debug.cpp"
#include "app/app_performance.cpp"
#include "app/app_input.cpp"
#include "app/app_helpers.cpp"
#include "app/app_font_helpers.cpp"
#include "app/app_audio_interface.cpp"
#include "app/app_wav.cpp"
#include "app/app_ogg.cpp"
#include "app/app_lzw.cpp"
#include "app/app_gif.cpp"
#include "app/app_loading_functions.cpp"
#include "resources/res_palette.cpp"
#include "resources/res_resources.cpp"
#include "app/app_sprite.cpp"
#include "app/app_sound.cpp"
#include "app/app_tasks.cpp"
#include "app/app_render_context.cpp"
#include "app/app_render_functions.cpp"
#include "app/app_notifications.cpp"
#include "app/app_screen_wipe.cpp"
#include "app/app_text_box.cpp"
#include "app/app_console_arena.cpp"
#include "app/app_console.cpp"
#include "resources/res_serialization_helpers.cpp"
#include "resources/res_deserialization_helpers.cpp"
#include "resources/res_bin_deserializer.cpp"
#include "resources/res_bitmap_font_loading.cpp"
#include "resources/res_sound_resource_list.cpp"

// +==============================+
// |          App States          |
// +==============================+
#include "game/game.cpp"
#include "main_menu/main_menu.cpp"
#include "mess/mess.cpp"
#include "giffer/giffer.cpp"

#include "app/app_settings_serialization.cpp" //has to come after game/game.cpp
#include "app/app_states.cpp"
#include "app/app_functions.cpp"

#include "app/app_console_commands.cpp"

// +--------------------------------------------------------------+
// |                       App Get Version                        |
// +--------------------------------------------------------------+
// Version_t App_GetVersion(bool* resetApplication)
EXPORT AppGetVersion_DEFINITION(App_GetVersion)
{
	Version_t version = {
		APP_VERSION_MAJOR,
		APP_VERSION_MINOR,
		APP_VERSION_BUILD,
	};
	
	if (resetApplication != nullptr)
	{
		*resetApplication = RESET_APPLICATION;
	}
	
	return version;
}

// +--------------------------------------------------------------+
// |                   App Get Startup Options                    |
// +--------------------------------------------------------------+
// void App_GetStartupOptions(const StartupInfo_t* StartupInfo, StartupOptions_t* StartupOptions)
EXPORT AppGetStartupOptions_DEFINITION(App_GetStartupOptions)
{
	Assert_(StartupInfo != nullptr);
	Assert_(StartupInfo->platformTempArena != nullptr);
	Assert_(StartupInfo->platformMainHeap != nullptr);
	Assert_(StartupInfo->platformStdHeap != nullptr);
	Assert_(StartupOptions != nullptr);
	
	TempArena = StartupInfo->platformTempArena;
	TempPushMark();
	
	AppSettings_t settings = {};
	InitAppSettingsStruct(StartupInfo->platformTempArena, &settings);
	FillDefaultAppSettings(&settings);
	
	char* settingsFolder = StartupInfo->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder != nullptr)
	{
		char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
		if (StartupInfo->DoesFileExist(settingsFilePath))
		{
			FileInfo_t settingsFile = StartupInfo->ReadEntireFile(settingsFilePath);
			if (settingsFile.content != nullptr && settingsFile.size > 0)
			{
				bool deserializedSuccessfully = DeserializeSettingsText(StartupInfo->platformTempArena, &settings, (const char*)settingsFile.content, settingsFile.size);
				//TODO: Should we do anything if it doesn't deserialize successfully?
			}
		}
	}
	
	#if STREAMING
	StartupOptions->windowSize             = NewVec2i(1018, 808);
	StartupOptions->fullscreenMonitorIndex = 0;
	#else
	StartupOptions->windowSize             = GetResolutionValue(settings.resolution);
	StartupOptions->fullscreenMonitorIndex = StartupInfo->primaryMonitorIndex;
	#endif
	StartupOptions->minWindowSize          = NewVec2i(200, 100);
	StartupOptions->maxWindowSize          = NewVec2i(-1, -1);
	StartupOptions->windowPosition         = NewVec2i(0, 0);
	StartupOptions->fullscreen             = settings.fullscreenEnabled;
	StartupOptions->autoIconify            = settings.autoMinimize;
	// +==============================+
	// |           Topmost            |
	// +==============================+
	StartupOptions->topmostWindow          = (true && STREAMING && DEBUG);
	StartupOptions->decoratedWindow        = true;
	StartupOptions->resizableWindow        = true;
	StartupOptions->forceAspectRatio       = false;
	StartupOptions->aspectRatio            = NewVec2i(16, 9);
	StartupOptions->antialiasingNumSamples = 0;
	StartupOptions->temporaryMemorySize    = Megabytes(64);
	StartupOptions->permanantMemorySize    = Megabytes(2);
	StartupOptions->mouseMode              = MouseMode_Default;
	
	StartupOptions->loadingColor = NewColor(0xFF6ACAE4);
	BufferPrint(StartupOptions->iconFilePaths[0], SPRITES_FLDR "icon16.png");
	BufferPrint(StartupOptions->iconFilePaths[1], SPRITES_FLDR "icon24.png");
	BufferPrint(StartupOptions->iconFilePaths[2], SPRITES_FLDR "icon32.png");
	BufferPrint(StartupOptions->iconFilePaths[3], SPRITES_FLDR "icon64.png");
	#if DEMO_BUILD
	BufferPrint(StartupOptions->loadingImagePath, SPRITES_FLDR "demologo.png");
	#else
	BufferPrint(StartupOptions->loadingImagePath, SPRITES_FLDR "logo.png");
	#endif
	
	for (u32 aIndex = 0; aIndex < StartupInfo->numProgramArguments; aIndex++)
	{
		const char* argument = StartupInfo->programArguments[aIndex];
		u32 argumentLength = (u32)strlen(argument);
		if (StrCompareIgnoreCaseNt(argument, "-fullscreen"))
		{
			StartupOptions->fullscreen = true;
		}
		if (argumentLength >= 9 && StrCompareIgnoreCase(argument, "-monitor=", 9))
		{
			u32 monitorIndex = 0;
			const char* monitorStr = &argument[9];
			u32 monitorStrLength = argumentLength-9;
			if (TryParseU32(monitorStr, monitorStrLength, &monitorIndex))
			{
				if (monitorIndex < StartupInfo->numMonitors)
				{
					StartupOptions->fullscreenMonitorIndex = monitorIndex;
				}
			}
			else if (monitorStrLength > 0)
			{
				for (u32 mIndex = 0; mIndex < StartupInfo->numMonitors; mIndex++)
				{
					const MonitorInfo_t* monitor = &StartupInfo->monitors[mIndex];
					if (monitor->name != nullptr && (u32)strlen(monitor->name) >= monitorStrLength && StrCompareIgnoreCase(monitor->name, monitorStr, monitorStrLength))
					{
						StartupOptions->fullscreenMonitorIndex = mIndex;
					}
				}
			}
		}
		if (argumentLength >= 12 && StrCompareIgnoreCase(argument, "-resolution=", 12))
		{
			u32 numValuePieces = 0;
			StrSplitPiece_t* valuePieces = SplitString(StartupInfo->platformTempArena, &argument[12], argumentLength-12, "x", 1, &numValuePieces);
			if (numValuePieces == 2 && valuePieces[0].length > 0 && valuePieces[1].length > 0)
			{
				u32 widthValue = 0;
				u32 heightValue = 0;
				if (TryParseU32(valuePieces[0].pntr, valuePieces[0].length, &widthValue) && TryParseU32(valuePieces[1].pntr, valuePieces[1].length, &heightValue))
				{
					StartupOptions->windowSize = NewVec2i((i32)widthValue, (i32)heightValue);
				}
			}
		}
	}
	
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	
	StartupOptions->audioDeviceIndex = StartupInfo->primaryAudioDeviceIndex;
	Assert_(StartupInfo->audioDevices != nullptr || StartupInfo->numAudioDevices == 0);
	//TODO: Add some settings that allow you to force the game to use a specific audio device and check those settings here to decide
	
	#endif //AUDIO_ENABLED && USE_CUSTOM_AUDIO
	
	TempPopMark();
}

// +--------------------------------------------------------------+
// |                        App Initialize                        |
// +--------------------------------------------------------------+
// void App_Initialize(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput)
EXPORT AppInitialize_DEFINITION(App_Initialize)
{
	AppEntryPoint(PlatformInfo, AppMemory, AppInput, nullptr, false);
	
	// +==================================+
	// |          Memory Arenas           |
	// +==================================+
	MainThreadId = platform->GetThisThreadId();
	PerfTime_t initStart = platform->GetPerfTime();
	ClearPointer(app);
	u32 staticHeapSize = AppMemory->permanantSize - sizeof(AppData_t);
	InitializeMemoryArenaHeap(&app->staticHeap, app + 1, staticHeapSize);
	InitializeMemoryArenaExternal(&app->platHeap, platform->AllocateMemory, platform->FreeMemory);
	InitializeMemoryArenaTemp(&app->tempArena, AppMemory->transientPntr, AppMemory->transientSize, TRANSIENT_MAX_NUMBER_MARKS);
	InitializeMemoryArenaGrowingHeap(&app->mainHeap, platform->AllocateMemory, GROWING_HEAP_PAGE_SIZE, 1, GROWING_HEAP_MAX_NUM_PAGES);
	// MemoryArenaAddMetaArena(&app->mainHeap, &app->platHeap); //NOTE: Only enable this if you are okay with the game crashing after too many undos are done
	
	app->baseAvailGpuMemory = GetGpuMemoryUsage();
	
	void* dbgFifoSpace        = ArenaPush(&app->mainHeap, CSL_FIFO_SPACE_SIZE);   Assert(dbgFifoSpace        != nullptr);
	void* dbgLineBuildSpace   = ArenaPush(&app->mainHeap, CSL_LINE_BUILD_SIZE);   Assert(dbgLineBuildSpace   != nullptr);
	void* dbgPrintBufferSpace = ArenaPush(&app->mainHeap, CSL_PRINT_BUFFER_SIZE); Assert(dbgPrintBufferSpace != nullptr);
	AppDebugOuputInit();
	DbgConsoleInit(&app->dbgConsole, dbgFifoSpace, CSL_FIFO_SPACE_SIZE, dbgLineBuildSpace, CSL_LINE_BUILD_SIZE, dbgPrintBufferSpace, CSL_PRINT_BUFFER_SIZE);
	PerfTime_t afterArenas = platform->GetPerfTime();
	
	#if 0
	// +==============================+
	// |       Loading Bar Test       |
	// +==============================+
	u32 loopIndex = 0;
	while (true)
	{
		platform->UpdateLoadingBar((loopIndex%100) / 100.0f);
		if (platform->WasWindowCloseRequested()) { return; }
		loopIndex++;
	}
	#endif
	
	TempPushMark();
	{
		PrintLine_I("Initializing App v%u.%02u(%u)...", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		PrintLine_D("Main Heap:   %u bytes", staticHeapSize);
		PrintLine_D("Temp Arena:  %u bytes", AppMemory->transientSize);
		PrintLine_D("Available GPU Memory: %s", FormattedSizeStr(app->baseAvailGpuMemory));
		
		// +================================+
		// |    External Initializations    |
		// +================================+
		r32 currentLoadPercent = 0.0f;
		#if DEVELOPER
		app->developerModeEnabled = true;
		#endif
		if (WasProgramArgumentPassed("developer") || WasProgramArgumentPassed("dev")) { app->developerModeEnabled = true; }
		CreateRandomSeries(appRand);
		u64 randomSeed = appInput->systemTime.timestamp;
		i32 argSeedValue = 0;
		if (GetProgramArgumentValueI32("seed", &argSeedValue) && argSeedValue >= 0) { randomSeed = (u64)argSeedValue; }
		PrintLine_D("Random Seed: %lu (0x%08lX)", randomSeed, randomSeed);
		SeedRandomSeriesU64(appRand, randomSeed);
		InitNotifications();
		const char* paletteFilePath = PALETTE_FILE_PATH;
		char* argPaletteStr = GetProgramArgumentValueStr("palette", TempArena);
		if (argPaletteStr != nullptr) { paletteFilePath = argPaletteStr; }
		LoadColorPalette(&app->colorPalette, paletteFilePath);
		plt = app->colorPalette.colors; //update the global pointer
		FillDefaultAppSettings(&app->settings);
		if (LoadAppSettings(&app->settings))
		{
			WriteLine_I("Loaded settings successfully");
			app->settingsChanged = false;
		}
		else 
		{
			Notify_E("Failed to load settings");
			app->settingsChanged = true;
		}
		InitializeRenderContext();
		WriteLine_I("Loading Resources...");
		InitializeResourceArrays(resources, currentLoadPercent, RESOURCES_LOAD_PERCENT);
		currentLoadPercent += RESOURCES_LOAD_PERCENT;
		DbgConsoleInitAfterResources(&app->dbgConsole);
		FillSoundResourceList(mainHeap, &app->sndResourceList);
		LoadSoundResourcesUsingMetaFile(&app->sndResourceList, SOUNDS_META_FILE_PATH, true, currentLoadPercent, SOUNDS_LOAD_PERCENT);
		currentLoadPercent += SOUNDS_LOAD_PERCENT;
		#if DEVELOPER
		platform->WatchFile(SOUNDS_META_FILE_PATH);
		#endif
		
		app->inputMethod = InputMethod_KeyboardMouse;
		
		AppStateChange_t change = {};
		change.type = AppStateChangeType_Push;
		change.newAppState = INITIAL_APP_STATE;
		PushAppState_(&change);
		
		app->mainFrameBuffer = CreateFrameBuffer(Vec2Roundi(RenderScreenSize), false, true);
		app->secondaryBuffer = CreateFrameBuffer(Vec2Roundi(RenderScreenSize), false, true);
		
		CreateDynamicArray(&app->trackedKeys, mainHeap, sizeof(KeyTrackerButton_t));
		
		if (WasProgramArgumentPassed("cheats") && sett->cheatsEnabled == false)
		{
			WriteLine_I("Cheats enabled from program arguments");
			sett->cheatsEnabled = true;
			app->settingsChanged = true;
		}
	}
	TempPopMark();
	WriteLine_I("Initialization Done!");
	
	app->appInitTempHighWaterMark = ArenaGetHighWaterMark(TempArena);
	ArenaResetHighWaterMark(TempArena);
}

// +--------------------------------------------------------------+
// |                         App Reloaded                         |
// +--------------------------------------------------------------+
// void App_Reloaded(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloaded_DEFINITION(App_Reloaded)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr, false);
	
	Notify_W("App Reloaded");
	
	CheckResourceArrayChanges(resources);
	
	if (sett->lastPlayedVersion.major == APP_VERSION_MAJOR && sett->lastPlayedVersion.minor == APP_VERSION_MINOR)
	{
		sett->lastPlayedVersion.build = APP_VERSION_BUILD;
		app->settingsChanged = true;
	}
	if (app->colorPalette.numColors != NUM_PALETTE_COLORS)
	{
		const char* paletteFilePath = PALETTE_FILE_PATH;
		char* argPaletteStr = GetProgramArgumentValueStr("palette", TempArena);
		if (argPaletteStr != nullptr) { paletteFilePath = argPaletteStr; }
		if (LoadColorPalette(&app->colorPalette, paletteFilePath))
		{
			Notify_I("Reloaded color palette because num colors changed");
		}
		else
		{
			Notify_E("Failed to reload the color palatte");
		}
		plt = app->colorPalette.colors; //update the global pointer
	}
}

// +--------------------------------------------------------------+
// |                          App Update                          |
// +--------------------------------------------------------------+
// void App_Update(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
EXPORT AppUpdate_DEFINITION(App_Update)
{
	AppEntryPoint(PlatformInfo, AppMemory, AppInput, AppOutput, false);
	appOutput->cursorType = Cursor_Default;
	
	#if RECORD_RC_DEBUG_INFO
	if (app->rcDebugInfo == nullptr || app->rcDebugInfoSize != sizeof(RcDebugInfo_t))
	{
		if (app->rcDebugInfo != nullptr) { ArenaPop(mainHeap, app->rcDebugInfo); app->rcDebugInfo = nullptr; }
		app->rcDebugInfo = PushStruct(mainHeap, RcDebugInfo_t);
		app->rcDebugInfoSize = sizeof(RcDebugInfo_t);
	}
	Assert(app->rcDebugInfo != nullptr && app->rcDebugInfoSize == sizeof(RcDebugInfo_t));
	ClearPointer(app->rcDebugInfo);
	#endif
	
	// +==============================+
	// | Temp Arena Update Loop Push  |
	// +==============================+
	TempPushMark();
	
	AppInputUpdate();
	
	// +==============================+
	// |     Update Debug Console     |
	// +==============================+
	NotificationsCaptureMouse();
	NotificationsUpdate();
	DbgConsoleCaptureMouse(&app->dbgConsole);
	DbgConsoleUpdate(&app->dbgConsole);
	
	// +==============================+
	// |     Toggle Debug Overlay     |
	// +==============================+
	if (app->developerModeEnabled && ButtonPressed(Button_Pipe))
	{
		HandleButton(Button_Pipe);
		app->showDebugOverlay = !app->showDebugOverlay;
		PrintLine_D("%s Debug Overlay", app->showDebugOverlay ? "Showing" : "Hiding");
	}
	// +==============================+
	// |     Gif Stream Shortcut      |
	// +==============================+
	if (ButtonPressed(Button_F2) && CurrentAppState() != AppState_Giffer && sett->gifRecordHotekyEnabled)
	{
		HandleButton(Button_F2);
		if (app->gifStream.isActive)
		{
			WriteLine_I("Finished GIF stream. Going to Giffer");
			app->goToGiffer = true;
		}
		else
		{
			char* gifFolderPath = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Screenshots, GAME_NAME_FOR_FOLDERS);
			if (gifFolderPath == nullptr)//just save it in the local directory next to the game executable instead
			{
				Notify_E("Failed to get regular screenshots folder path. Saving to game directory instead");
				gifFolderPath = "";
			}
			else if (!platform->DoesFolderExist(gifFolderPath))
			{
				bool createSuccess = platform->CreateFolder(gifFolderPath);
				if (!createSuccess)
				{
					NotifyPrint_E("Failed to create folder at \"%s\". Saving to game directory instead", gifFolderPath);
					gifFolderPath = "";
				}
			}
			char* gifFileName = TempPrint("%scapture_%u-%u-%u_%u-%u-%u.gif", gifFolderPath, LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
			reci sourceRec = NewReci(Vec2i_Zero, appInput->screenSize);
			if (ButtonDownNoHandling(Button_Shift))
			{
				// if (CurrentAppState() == AppState_Game)
				// {
				// 	sourceRec = NewReci((i32)game->instance.gameViews[0].screenRec.x, (i32)game->instance.gameViews[0].screenRec.y, (i32)game->instance.gameViews[0].screenRec.width, (i32)game->instance.gameViews[0].screenRec.height);
				// 	sourceRec = ReciInflate(sourceRec, (i32)game->instance.gameViews[0].scale);
				// 	sourceRec = ReciOverlap(sourceRec, NewReci(0, 0, (i32)RenderScreenSize.width, (i32)RenderScreenSize.height));
				// }
				// if (CurrentAppState() == AppState_Editor)
				// {
				// 	sourceRec = NewReci((i32)editor->pickMenuRec.x, (i32)editor->pickMenuRec.y, (i32)editor->pickMenuRec.width, (i32)editor->pickMenuRec.height);
				// 	sourceRec = ReciOverlap(sourceRec, NewReci(0, 0, (i32)RenderScreenSize.width, (i32)RenderScreenSize.height));
				// }
			}
			bool streamStarted = StartGifStream(&app->platHeap, gifFileName, sourceRec, &app->gifStream);
			if (streamStarted) { WriteLine_I("Started GIF stream"); }
			else { Notify_E("GIF streaming failed"); }
		}
	}
	// +==============================+
	// |     Screenshot Shortcut      |
	// +==============================+
	if (ButtonPressed(Button_F3))
	{
		HandleButton(Button_F3);
		app->takeScreenshot = true;
		app->takeGameWindowScreenshot = ButtonDownNoHandling(Button_Shift);
	}
	// +==============================+
	// |     Toggle Debug Console     |
	// +==============================+
	if (ButtonPressed(Button_F6) && (sett->developerConsoleEnabled || app->dbgConsole.isOpen))
	{
		HandleButton(Button_F6);
		if (ButtonDownNoHandling(Button_Shift))
		{
			if (!app->dbgConsole.isOpen || !app->dbgConsole.openLarge)
			{
				if (app->dbgConsole.isOpen) { app->dbgConsole.openAnimProgress /= 3; }
				app->dbgConsole.isOpen = true;
				app->dbgConsole.openLarge = true;
				
			}
			else { app->dbgConsole.isOpen = false; }
		}
		else
		{
			app->dbgConsole.isOpen = !app->dbgConsole.isOpen;
			app->dbgConsole.openLarge = false;
		}
		if (app->dbgConsole.isOpen)
		{
			app->dbgConsole.inputBoxSelected = true;
			app->dbgConsole.fullyOpaque = true;
		}
		
		// PrintLine_D("%s Debug Console%s", app->dbgConsole.isOpen ? "Showing" : "Hiding", app->dbgConsole.openLarge ? " (Large)" : "");
	}
	// +==============================+
	// |    Toggle Framerate Graph    |
	// +==============================+
	if (ButtonPressed(Button_F7))
	{
		HandleButton(Button_F7);
		if (ButtonDownNoHandling(Button_Control) && ButtonDownNoHandling(Button_Shift))
		{
			app->showAudioOverlay = !app->showAudioOverlay;
			PrintLine_D("%s Audio Overlay", app->showAudioOverlay ? "Showing" : "Hiding");
		}
		else if (ButtonDownNoHandling(Button_Shift))
		{
			app->showMemoryUsage = !app->showMemoryUsage;
			PrintLine_D("%s Memory Usage", app->showMemoryUsage ? "Showing" : "Hiding");
		}
		#if RECORD_RC_DEBUG_INFO
		else if (ButtonDownNoHandling(Button_Control))
		{
			app->showRcDebugInfo = !app->showRcDebugInfo;
			PrintLine_D("%s Rc Debug Info", app->showRcDebugInfo ? "Showing" : "Hiding");
		}
		#endif
		else
		{
			app->showFramerateGraph = !app->showFramerateGraph;
			PrintLine_D("%s Framerate Graph", app->showFramerateGraph ? "Showing" : "Hiding");
		}
	}
	
	// +==============================+
	// |     Handle File Changes      |
	// +==============================+
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_FileChanged)
		{
			Assert(event->filePath != nullptr);
			
			u32 resourceIndex = 0;
			#if DEVELOPER
			const char* paletteFilePath = PALETTE_FILE_PATH;
			char* argPaletteStr = GetProgramArgumentValueStr("palette", TempArena);
			if (argPaletteStr != nullptr) { paletteFilePath = argPaletteStr; }
			if (MyStrCompareNt(event->filePath, paletteFilePath) == 0)
			{
				if (LoadColorPalette(&app->colorPalette, PALETTE_FILE_PATH))
				{
					Notify_I("Color Palette reloaded");
				}
				else
				{
					Notify_E("Color Palette reloading failed!");
				}
				plt = app->colorPalette.colors; //update the global pointer
			}
			else if (MyStrCompareNt(event->filePath, SOUNDS_META_FILE_PATH) == 0)
			{
				Notify_I("Reloading Sounds Meta File");
				LoadSoundResourcesUsingMetaFile(&app->sndResourceList, SOUNDS_META_FILE_PATH, false, 0.0f, 0.0f);
			}
			else if (IsFileResource(event->filePath, &resourceIndex))
			{
				Resource_t* resource = GetResourceByResourceIndex(resourceIndex);
				if (IsResourceLoaded(resource))
				{
					NotifyPrint_I("Reloading %s: \"%s\"", GetResourceTypeStr(resource->type), GetFileNamePart(event->filePath));
					LoadResource(resource);
				}
				else
				{
					NotifyPrint_I("Unloaded %s changed: \"%s\"", GetResourceTypeStr(resource->type), GetFileNamePart(event->filePath));
				}
			}
			else if (SoundResourceFileChanged(&app->sndResourceList, event->filePath))
			{
				NotifyPrint_I("Reloading Sound: \"%s\"", GetFileNamePart(event->filePath));
			}
			else
			{
				NotifyPrint_W("Unknown file changed: \"%s\"", event->filePath);
			}
			#else
			NotifyPrint_W("File changed in release mode? \"%s\"", event->filePath);
			#endif
		}
	}
	
	// +==============================+
	// |     Handle Dropped Files     |
	// +==============================+
	if (appInput->numDroppedFiles > 0)
	{
		PrintLine_I("%u files dropped on window", appInput->numDroppedFiles);
	}
	
	// +==============================+
	// | Handle Audio Device Changes  |
	// +==============================+
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_AudioReset)                { WriteLine_E("Audio was reset!"); }
		if (event->type == InputEventType_AudioDevicesChanged)       { WriteLine_W("Audio devices changed"); }
		if (event->type == InputEventType_AudioShutdown)             { WriteLine_E("Audio shutdown!"); }
		if (event->type == InputEventType_PrimaryAudioDeviceChanged) { WriteLine_W("Primary audio device changed!"); }
	}
	
	// +==============================+
	// |     Record Elapsed Time      |
	// +==============================+
	for (u32 eIndex = FRAMERATE_GRAPH_WIDTH-1; eIndex > 0; eIndex--)
	{
		app->elapsedTimesGraphValues[eIndex] = app->elapsedTimesGraphValues[eIndex-1];
		app->updateElapsedTimesGraphValues[eIndex] = app->updateElapsedTimesGraphValues[eIndex-1];
		app->waitElapsedTimesGraphValues[eIndex] = app->waitElapsedTimesGraphValues[eIndex-1];
	}
	app->elapsedTimesGraphValues[0] = (r32)appInput->elapsedMs;
	app->updateElapsedTimesGraphValues[0] = (r32)appInput->lastUpdateElapsedMs;
	app->waitElapsedTimesGraphValues[0] = appInput->timeSpentWaitingLastFrame;
	
	// +==============================+
	// |       External Updates       |
	// +==============================+
	UpdateResources();
	UpdateScreenWipe();
	UpdateSndInstances();
	
	// +==============================+
	// |  Update Frame Buffer Sizes   |
	// +==============================+
	if (RenderScreenSize.width > 0 && RenderScreenSize.height > 0 && RenderScreenSize != NewVec2(app->mainFrameBuffer.texture.size))
	{
		DestroyFrameBuffer(&app->mainFrameBuffer);
		DestroyFrameBuffer(&app->secondaryBuffer);
		app->mainFrameBuffer = CreateFrameBuffer(Vec2Roundi(RenderScreenSize), false, true);
		app->secondaryBuffer = CreateFrameBuffer(Vec2Roundi(RenderScreenSize), false, true);
	}
	
	// +==========================================+
	// | Update and Render AppStates and AppMenus |
	// +==========================================+
	{
		for (u32 sIndex = 0; sIndex < app->numActiveStates; sIndex++)
		{
			PreloadResourcesForAppState(app->activeStates[sIndex]);
		}
		
		if (CurrentAppMenu() != AppMenu_None) { UpdateAppMenu(CurrentAppMenu(), CurrentAppState(), AppMenu_None); }
		if (CurrentAppState() != AppState_None) { UpdateAppState(CurrentAppState(), CurrentAppMenu()); }
		
		if (app->numActiveStates > 0)
		{
			u32 startAppState = 0;
			for (u32 sIndex = app->numActiveStates; sIndex > 0; sIndex--)
			{
				if (DoesAppStateCoverBelow(app->activeStates[sIndex-1], (sIndex >= 2) ? app->activeStates[sIndex-2] : AppState_None))
				{
					startAppState = sIndex-1;
					break;
				}
			}
			for (u32 sIndex = startAppState; sIndex < app->numActiveStates; sIndex++)
			{
				RenderAppState(app->activeStates[sIndex], &app->mainFrameBuffer, (sIndex+1 < app->numActiveStates) ? app->activeStates[sIndex+1] : AppState_None, CurrentAppMenu());
			}
		}
		
		if (app->numActiveMenus > 0)
		{
			for (u32 mIndex = 0; mIndex < app->numActiveMenus; mIndex++)
			{
				RenderAppMenu(app->activeMenus[mIndex], &app->mainFrameBuffer, CurrentAppState(), (mIndex+1 < app->numActiveMenus) ? app->activeMenus[mIndex+1] : AppMenu_None);
			}
		}
		
		if (app->appStateChanged)
		{
			switch (app->appStateChange.type)
			{
				case AppStateChangeType_Push:   PushAppState_(&app->appStateChange);   break;
				case AppStateChangeType_Pop:    PopAppState_(&app->appStateChange);    break;
				case AppStateChangeType_Change: ChangeAppState_(&app->appStateChange); break;
				default: Assert(false); break;
			}
			app->appStateChanged = false;
		}
		if (app->appMenuChanged)
		{
			switch (app->appMenuChange.type)
			{
				case AppStateChangeType_Push:   PushAppMenu_(&app->appMenuChange);   break;
				case AppStateChangeType_Pop:    PopAppMenu_(&app->appMenuChange);    break;
				case AppStateChangeType_Change: ChangeAppMenu_(&app->appMenuChange); break;
				default: Assert(false); break;
			}
			app->appMenuChanged = false;
		}
	}
	
	// +==============================+
	// |      Update Input After      |
	// +==============================+
	AppInputUpdateAfter();
	
	// +==============================+
	// |       Save Screenshot        |
	// +==============================+
	if (app->screenshotCountdown > 0)
	{
		app->screenshotCountdown -= ElapsedMs;
		if (app->screenshotCountdown <= 0)
		{
			app->takeScreenshot = true;
			app->screenshotCountdown = 0;
		}
	}
	if (app->takeScreenshot)
	{
		app->takeScreenshot = false;
		WriteLine_Ix(DbgFlag_Inverted, "Taking screenshot...");
		
		TempPushMark();
		char* screenshotFolderPath = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Screenshots, GAME_NAME_FOR_FOLDERS);
		if (screenshotFolderPath == nullptr)//just save it in the local directory next to the game executable instead
		{
			Notify_E("Failed to get regular screenshots folder path. Saving to game directory instead");
			screenshotFolderPath = "";
		}
		else if (!platform->DoesFolderExist(screenshotFolderPath))
		{
			bool createSuccess = platform->CreateFolder(screenshotFolderPath);
			if (!createSuccess)
			{
				NotifyPrint_E("Failed to create folder at \"%s\". Saving to game directory instead", screenshotFolderPath);
				screenshotFolderPath = "";
			}
		}
		char* screenshotFilePath = TempPrint("%sscreenshot_%u-%u-%u_%u-%u-%u.png", screenshotFolderPath, LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
		
		u32 textureWidth = (u32)app->mainFrameBuffer.texture.width;
		u32 textureHeight = (u32)app->mainFrameBuffer.texture.height;
		u8 pixelWidth = (app->mainFrameBuffer.hasTransparency ? 4 : 3);
		reci sourceRec = NewReci(0, 0, (i32)textureWidth, (i32)textureHeight);
		u32 rowWidth = textureWidth * pixelWidth;
		rowWidth = (u32)(CeilR32i((r32)rowWidth / 4) * 4);
		u32 pixelDataSize = textureHeight * rowWidth;
		u8* pixelData = PushArray(TempArena, u8, pixelDataSize);
		Assert(pixelData != nullptr);
		MyMemSet(pixelData, 0x00, pixelDataSize);
		
		// if (CurrentAppState() == AppState_Game && app->takeGameWindowScreenshot)
		// {
		// 	sourceRec = NewReci((i32)game->instance.gameViews[0].screenRec.x, (i32)game->instance.gameViews[0].screenRec.y, (i32)game->instance.gameViews[0].screenRec.width, (i32)game->instance.gameViews[0].screenRec.height);
		// 	sourceRec = ReciInflate(sourceRec, (i32)game->instance.gameViews[0].scale);
		// 	sourceRec = ReciOverlap(sourceRec, NewReci(0, 0, (i32)textureWidth, (i32)textureHeight));
		// }
		
		glBindTexture(GL_TEXTURE_2D, app->mainFrameBuffer.texture.id);
		glGetTexImage(GL_TEXTURE_2D, 0, (app->mainFrameBuffer.hasTransparency ? GL_RGBA : GL_RGB), GL_UNSIGNED_BYTE, (GLvoid*)pixelData);
		
		OpenFile_t screenshotFile;
		if (platform->OpenFile(screenshotFilePath, true, &screenshotFile))
		{
			if (stbi_write_png_to_func(StbImageWriteCallback, &screenshotFile, sourceRec.width, sourceRec.height, pixelWidth, pixelData + (sourceRec.x*pixelWidth + sourceRec.y*rowWidth), rowWidth) != 0)
			{
				NotifyPrintTimed_I(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Screenshot saved to \"%s\"", screenshotFilePath);
			}
			else
			{
				NotifyPrintTimed_E(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Failed to write %u pixels to \"%s\"", sourceRec.width * sourceRec.height, screenshotFilePath);
			}
			platform->CloseFile(&screenshotFile);
		}
		else
		{
			NotifyPrintTimed_E(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Failed to create screenshot file at \"%s\"", screenshotFilePath);
		}
		
		TempPopMark();
	}
	
	#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
	// +==============================+
	// |     Render Audio Overlay     |
	// +==============================+
	if (app->showAudioOverlay)
	{
		rec bufferRec = NewRec(0, 10, 400, 100);
		if (platform->audioInfo.bufferDisplay.chunkValues != nullptr && platform->audioInfo.bufferDisplay.numChunks > 0)
		{
			bufferRec.width = (r32)platform->audioInfo.bufferDisplay.numChunks*1;
		}
		bufferRec.x = RenderScreenSize.width - 10 - bufferRec.width;
		RcDrawButton(bufferRec, ColorTransparent(Black, 0.5f), White, 1);
		
		r32 playCursorPos = bufferRec.x + bufferRec.width * ((r32)platform->audioInfo.playCursorPos / (r32)platform->audioInfo.bufferLength);
		r32 writeCursorPos = bufferRec.x + bufferRec.width * ((r32)platform->audioInfo.writeCursorPos / (r32)platform->audioInfo.bufferLength);
		rec playCursorRec = NewRec(playCursorPos, bufferRec.y, 1, bufferRec.height);
		rec writeCursorRec = NewRec(writeCursorPos, bufferRec.y, 1, bufferRec.height);
		RcDrawRectangle(playCursorRec, PalOrange);
		RcDrawRectangle(writeCursorRec, PalPurple);
		
		if (platform->audioInfo.bufferDisplay.chunkValues != nullptr && platform->audioInfo.bufferDisplay.numChunks > 0)
		{
			const SampleDisplay_t* display = &platform->audioInfo.bufferDisplay;
			rec baseChunkRec = NewRec(bufferRec.x, bufferRec.y, bufferRec.width / display->numChunks, bufferRec.height/2);
			for (u32 cIndex = 0; cIndex < display->numChunks; cIndex++)
			{
				rec chunkRec = baseChunkRec + NewVec2(baseChunkRec.width * cIndex, 0);
				r32 chunkValue = display->chunkValues[cIndex];
				chunkRec.height *= chunkValue;
				chunkRec.y = bufferRec.y + bufferRec.height/2 - chunkRec.height;
				if (chunkRec.height < 0)
				{
					chunkRec.height = -chunkRec.height;
					chunkRec.y -= chunkRec.height;
				}
				RcDrawRectangle(chunkRec, ColorTransparent(PalBlueLight, 0.8f));
			}
		}
	}
	#endif
	
	// +==============================+
	// |       Feed Gif Stream        |
	// +==============================+
	if (app->gifStream.isActive && CurrentAppState() != AppState_Giffer)
	{
		bool frameFed = GifStreamAddFrame(&app->gifStream, &app->mainFrameBuffer);
		if (!frameFed) { Notify_E("Gif stream ended prematurely"); }
	}
	
	rec screenRec = NewRec(0, 0, RenderScreenSize.width, RenderScreenSize.height);
	RcBegin(GetShader(mainShader), screenRec, GetFont(pixelFont), nullptr);
	RcClear(Black, 1.0f);
	RcBindTexture(&app->mainFrameBuffer.texture);
	RcDrawTexturedRec(screenRec, White);
	
	// +==============================+
	// |       Draw Screen Wipe       |
	// +==============================+
	if (app->screenWipeProgress > 0.0f)
	{
		ApplyScreenWipe(NewRec(Vec2_Zero, RenderScreenSize)); //TODO: Change how this rectangle is decided!
	}
	
	// +--------------------------------------------------------------+
	// |                  Render Debug Overlay Items                  |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |  Render Screenshot Message   |
		// +==============================+
		if (app->screenshotCountdown > 0)
		{
			RcBindFont(GetFont(pixelFont), 2.0f);
			r32 timeSecs = app->screenshotCountdown / 1000.0f;
			r32 barPercent = timeSecs - FloorR32(timeSecs);
			char* timeStr = TempPrint("%.0f", CeilR32(timeSecs));
			v2 timeStrSize = RcMeasureStringNt(timeStr);
			rec messageRec = NewRec(RenderScreenSize.width/2, 0, timeStrSize.width + 10*2, timeStrSize.height + 5*2);
			messageRec.x -= messageRec.width/2;
			messageRec.topLeft = Vec2Round(messageRec.topLeft);
			v2 timeStrPos = NewVec2(messageRec.x + messageRec.width/2 - timeStrSize.width/2, messageRec.y + messageRec.height/2 - timeStrSize.height/2 + RcMaxExtendUp());
			timeStrPos = Vec2Round(timeStrPos);
			rec barGutterRec1 = NewRec(messageRec.x + messageRec.width, messageRec.y, 3, messageRec.height);
			rec barGutterRec2 = barGutterRec1;
			barGutterRec2.x = messageRec.x - barGutterRec2.width;
			rec barRec1 = barGutterRec1;
			barRec1.height = RoundR32(barRec1.height * barPercent);
			barRec1.y = barGutterRec1.y + barGutterRec1.height - barRec1.height;
			rec barRec2 = barRec1;
			barRec2.x = barGutterRec2.x;
			RcDrawRectangle(messageRec, MonokaiBack);
			RcDrawStringNt(timeStr, timeStrPos, VisWhite);
			RcDrawRectangle(barGutterRec1, Black);
			RcDrawRectangle(barRec1, VisWhite);
			RcDrawRectangle(barGutterRec2, Black);
			RcDrawRectangle(barRec2, VisWhite);
		}
		
		// +==============================+
		// |      Render Key Tracker      |
		// +==============================+
		if (app->trackedKeys.length > 0)
		{
			RcBindFont(GetFont(pixelFont), 2.0f);
			rec baseBlockRec = NewRec(0, 0, 12, 40);
			baseBlockRec.x = RenderScreenSize.width - ((baseBlockRec.width + 3) * KEY_TRACKER_LENGTH);
			baseBlockRec.y = RenderScreenSize.height - ((baseBlockRec.height + 3) * app->trackedKeys.length);
			for (u32 tIndex = 0; tIndex < app->trackedKeys.length; tIndex++)
			{
				KeyTrackerButton_t* key = DynArrayGet(&app->trackedKeys, KeyTrackerButton_t, tIndex);
				Assert(key != nullptr);
				for (u32 hIndex = 0; hIndex < KEY_TRACKER_LENGTH; hIndex++)
				{
					rec blockRec = baseBlockRec + Vec2Multiply(baseBlockRec.size + NewVec2(3), NewVec2((r32)hIndex, (r32)tIndex));
					u8 flags = key->history[hIndex];
					Color_t blockColor = ColorTransparent(Black, 0.25f);
					if (IsFlagSet(flags, KeyTrackerFlag_Pressed))       { blockColor = VisLightBlue; }
					else if (IsFlagSet(flags, KeyTrackerFlag_Released)) { blockColor = VisYellow;    }
					else if (IsFlagSet(flags, KeyTrackerFlag_Down))     { blockColor = VisOrange;    }
					RcDrawRectangle(blockRec, blockColor);
					if (app->keyTrackerIndex == hIndex)
					{
						RcDrawButton(RecInflate(blockRec, 1), NoColor, VisLightBlue, 2);
					}
					else if (IsFlagSet(flags, KeyTrackerFlag_Handled))
					{
						RcDrawButton(RecInflate(blockRec, 1), NoColor, VisWhite, 2);
					}
					
					if (hIndex == 0) { RcPrintStringAligned(NewVec2(blockRec.x - 5, blockRec.y + blockRec.height/2 + RcCenterOffset()), White, 1.0f, Alignment_Right, "%s", GetButtonName(key->button)); }
				}
			}
		}
		
		
		// +==============================+
		// |     Render Debug Console     |
		// +==============================+
		DbgConsoleRender(&app->dbgConsole);
		
		// +==============================+
		// |     Render Rc Debug Info     |
		// +==============================+
		#if RECORD_RC_DEBUG_INFO
		if (app->showRcDebugInfo && app->rcDebugInfoLast != nullptr && app->rcDebugInfoSizeLast == sizeof(RcDebugInfo_t))
		{
			RcBindFont(GetFont(debugFont));
			v2 textPos = NewVec2(RenderScreenSize.width - 5, RenderScreenSize.height - 5 - RcMaxExtendDown());
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Shader Binds",         app->rcDebugInfoLast->numShaderBinds);        textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Sheet Frame Draws",    app->rcDebugInfoLast->numSheetFrameDraws);    textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Sheet Frames Batched", app->rcDebugInfoLast->numSheetFramesBatched); textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Texture Binds",        app->rcDebugInfoLast->numTextureBinds);       textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Draw Calls",           app->rcDebugInfoLast->numDrawCalls);          textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Batched Chars",        app->rcDebugInfoLast->numBatchedGlyphs);      textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%u Drawn Chars",          app->rcDebugInfoLast->numDrawnGlyphs);        textPos.y -= RcLineHeight();
		}
		#endif
		
		// +==============================+
		// |       Render GPU Stats       |
		// +==============================+
		if (app->showGpuStats)
		{
			RcBindFont(GetFont(debugFont));
			v2 textPos = NewVec2(RenderScreenSize.width - 5, RenderScreenSize.height - 5 - RcMaxExtendDown());
			u64 gpuMemoryUsage = GetGpuMemoryUsage();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%s Memory", FormattedSizeStr(app->baseAvailGpuMemory)); textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%s Used (%.1f%%)", FormattedSizeStr(gpuMemoryUsage), ((r32)gpuMemoryUsage / (r32)app->baseAvailGpuMemory) * 100); textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%s Textures", FormattedSizeStr(app->expectedTexturesSize)); textPos.y -= RcLineHeight();
			RcPrintStringEmbossedAligned(textPos, White, Black, 1.0f, 1, Alignment_Right, "%s Frame Buffers", FormattedSizeStr(app->expectedFramebuffersSize)); textPos.y -= RcLineHeight();
		}
		
		// +==============================+
		// |     Render Resource List     |
		// +==============================+
		if (app->showLoadedResources)
		{
			RcBindFont(GetFont(debugFont));
			v2 textPos = NewVec2(RenderScreenSize.width - 5, RenderScreenSize.height - 5 - RcMaxExtendDown());
			r32 rowWidth = 300; //px
			u32 numLoadedTextures = 0;
			for (u32 tIndex = 0; tIndex < NUM_RESOURCE_TEXTURES; tIndex++)
			{
				Resource_t* textureResource = GetResourceByType(ResourceType_Texture, tIndex);
				Assert(textureResource != nullptr);
				Assert(textureResource->readableName != nullptr);
				bool isLoaded = IsResourceLoaded(textureResource);
				Color_t textColor = isLoaded ? (textureResource->wasPreloaded ? White : PalYellow) : Grey4;
				if (isLoaded) { numLoadedTextures++; }
				RcPrintStringEmbossedAligned(textPos, textColor, Black, 1.0f, 1, Alignment_Right, "%s", textureResource->readableName); textPos.y -= RcLineHeight();
				if (textPos.y < RcMaxExtendUp()) { textPos.y = RenderScreenSize.height - 5 - RcMaxExtendDown(); textPos.x -= rowWidth; /*rowWidth = 0;*/ }
			}
			RcPrintStringEmbossedAligned(textPos, PalGreen, PalGreenDarker, 1.0f, 1, Alignment_Right, "%u/%u Textures Loaded", numLoadedTextures, NUM_RESOURCE_TEXTURES); textPos.y -= RcLineHeight()*2;
			textPos.y = RenderScreenSize.height - 5 - RcMaxExtendDown(); textPos.x -= rowWidth; /*rowWidth = 0;*/
			u32 numLoadedSheets = 0;
			for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHEETS; sIndex++)
			{
				Resource_t* sheetResource = GetResourceByType(ResourceType_SpriteSheet, sIndex);
				Assert(sheetResource != nullptr);
				Assert(sheetResource->readableName != nullptr);
				bool isLoaded = IsResourceLoaded(sheetResource);
				Color_t textColor = isLoaded ? (sheetResource->wasPreloaded ? White : PalYellow) : Grey4;
				if (isLoaded) { numLoadedSheets++; }
				RcPrintStringEmbossedAligned(textPos, textColor, Black, 1.0f, 1, Alignment_Right, "%s", sheetResource->readableName); textPos.y -= RcLineHeight();
				if (textPos.y < RcMaxExtendUp()) { textPos.y = RenderScreenSize.height - 5 - RcMaxExtendDown(); textPos.x -= rowWidth; /*rowWidth = 0;*/ }
			}
			RcPrintStringEmbossedAligned(textPos, PalGreen, PalGreenDarker, 1.0f, 1, Alignment_Right, "%u/%u Sheets Loaded", numLoadedSheets, NUM_RESOURCE_SHEETS); textPos.y -= RcLineHeight()*2;
			textPos.y = RenderScreenSize.height - 5 - RcMaxExtendDown(); textPos.x -= rowWidth; /*rowWidth = 0;*/
		}
		
		// +==============================+
		// |    Render Framerate Graph    |
		// +==============================+
		if (app->showFramerateGraph)
		{
			RcBindFont(GetFont(pixelFont), 1.0f);
			
			rec graphRec = NewRec(10, 10, FRAMERATE_GRAPH_WIDTH*5, 100);
			r32 maxElapsedTime = 25.0f;
			for (u32 eIndex = 0; eIndex < FRAMERATE_GRAPH_WIDTH; eIndex++)
			{
				r32 elapsedTime = app->elapsedTimesGraphValues[eIndex];
				r32 updateElapsedTime = app->updateElapsedTimesGraphValues[eIndex];
				if (elapsedTime+2 > maxElapsedTime) { maxElapsedTime = elapsedTime+2; }
				if (updateElapsedTime+2 > maxElapsedTime) { maxElapsedTime = updateElapsedTime+2; }
			}
			
			r32 vertScale = (graphRec.height / maxElapsedTime);
			u32 numVertTicks = (u32)MaxI32(0, FloorR32i(maxElapsedTime)) / FRAMERATE_VERT_TICK_VAL;
			r32 vertTickOffset = FRAMERATE_VERT_TICK_VAL * vertScale;
			u32 numTicksToSkip = MaxU32(1, numVertTicks / 5);
			u32 numHorTicks = FRAMERATE_GRAPH_WIDTH;
			r32 horTickOffset = graphRec.width / numHorTicks;
			
			RcDrawButton(graphRec, ColorTransparent(Black, 0.5f), VisWhite);
			
			r32 target1 = graphRec.y + graphRec.height - (1000.0f / 60.0f) * vertScale;
			if (target1 > graphRec.y)
			{
				RcDrawRectangle(NewRec(graphRec.x+1, target1, graphRec.width-2, 1), VisGreen);
			}
			
			r32 target2 = graphRec.y + graphRec.height - (1000.0f / 30.0f) * vertScale;
			if (target2 > graphRec.y)
			{
				RcDrawRectangle(NewRec(graphRec.x+1, target2, graphRec.width-2, 1), VisLightGray);
			}
			
			for (u32 vIndex = 1; vIndex <= numVertTicks; vIndex += numTicksToSkip)
			{
				u32 msValue = vIndex * FRAMERATE_VERT_TICK_VAL;
				r32 yPos = graphRec.y + graphRec.height - (vertTickOffset * vIndex);
				rec tickRec = NewRec(graphRec.x + graphRec.width - 2, yPos, 2, 1);
				v2 textPos = NewVec2(tickRec.x + tickRec.width + 1, tickRec.y + RcLineHeight()/2 - RcMaxExtendDown());
				RcDrawRectangle(tickRec, VisWhite);
				RcPrintString(textPos, VisWhite, 1.0f, "%u", msValue);
			}
			for (u32 hIndex = 1; hIndex < numHorTicks; hIndex+=2)
			{
				u32 tValue = hIndex;
				r32 xPos = graphRec.x + horTickOffset*hIndex;
				rec tickRec = NewRec(xPos, graphRec.y + graphRec.height - 2, 1, 2);
				RcDrawRectangle(tickRec, VisWhite);
			}
			
			v2 lastPos = Vec2_Zero;
			v2 lastUpdatePos = Vec2_Zero;
			v2 lastWaitPos = Vec2_Zero;
			r32 avgElapsedTime = 0;
			r32 avgUpdateElapsedTime = 0;
			r32 avgWaitElapsedTime = 0;
			for (u32 eIndex = 0; eIndex < FRAMERATE_GRAPH_WIDTH; eIndex++)
			{
				r32 elapsedTime = app->elapsedTimesGraphValues[eIndex];
				avgElapsedTime += elapsedTime;
				r32 xPos = graphRec.x + (graphRec.width / FRAMERATE_GRAPH_WIDTH) * eIndex;
				r32 yPos = graphRec.y + graphRec.height - (vertScale * elapsedTime);
				v2 newPos = NewVec2(xPos, yPos);
				
				r32 updateElapsedTime = app->updateElapsedTimesGraphValues[eIndex];
				avgUpdateElapsedTime += updateElapsedTime;
				xPos = graphRec.x + (graphRec.width / FRAMERATE_GRAPH_WIDTH) * eIndex;
				yPos = graphRec.y + graphRec.height - (vertScale * updateElapsedTime);
				v2 newUpdatePos = NewVec2(xPos, yPos);
				
				r32 waitElapsedTime = app->updateElapsedTimesGraphValues[eIndex] + app->waitElapsedTimesGraphValues[eIndex];
				if (app->waitElapsedTimesGraphValues[eIndex] == 0) { waitElapsedTime = 0; }
				avgWaitElapsedTime += waitElapsedTime;
				xPos = graphRec.x + (graphRec.width / FRAMERATE_GRAPH_WIDTH) * eIndex;
				yPos = graphRec.y + graphRec.height - (vertScale * waitElapsedTime);
				v2 newWaitPos = NewVec2(xPos, yPos);
				
				if (eIndex > 0)
				{
					RcDrawLine(lastPos, newPos, 1.0f, VisRed);
					RcDrawLine(lastUpdatePos, newUpdatePos, 1.0f, VisYellow);
					if (newWaitPos.y < graphRec.y + graphRec.height || lastWaitPos.y < graphRec.y + graphRec.height)
					{
						RcDrawLine(lastWaitPos, newWaitPos, 1.0f, ColorTransparent(VisLightBlue, 0.5f));
					}
				}
				lastPos = newPos;
				lastUpdatePos = newUpdatePos;
				lastWaitPos = newWaitPos;
			}
			avgElapsedTime /= (r32)FRAMERATE_GRAPH_WIDTH;
			avgUpdateElapsedTime /= (r32)FRAMERATE_GRAPH_WIDTH;
			avgWaitElapsedTime /= (r32)FRAMERATE_GRAPH_WIDTH;
			
			{
				v2 fpsDisplayPos = NewVec2(graphRec.x + graphRec.width/2, graphRec.y + graphRec.height + 2 + RcMaxExtendUp());
				fpsDisplayPos = Vec2Round(fpsDisplayPos);
				char* fpsDisplayStr = TempPrint("%.1fms %.1ffps (target %.1ffps)%s", avgUpdateElapsedTime, appInput->avgFramerate, appInput->targetFramerate, appInput->doingManualUpdateLoopWait ? " (Manual)" : "");
				RcDrawStringEmbossedNt(fpsDisplayStr, fpsDisplayPos, White, Black, 1, 1.0f, Alignment_Center);
				fpsDisplayPos.y += RcLineHeight();
				if (appInput->numTotalThreadedTasks > 0)
				{
					RcPrintStringEmbossedAligned(fpsDisplayPos, White, Black , 1.0f, 1, Alignment_Center, "%u Active Tasks (%u Queued)", appInput->numActiveThreadedTasks, appInput->numQueuedThreadedTasks);
					fpsDisplayPos.y += RcLineHeight();
				}
			}
		}
		
		// +==============================+
		// |     Render Memory Usage      |
		// +==============================+
		if (app->showMemoryUsage)
		{
			MemoryArena_t* arenas[3] = { &app->mainHeap, &app->staticHeap, &app->tempArena };
			const char* arenaNames[3] = { "Main", "Static", "Temp" };
			
			RcBindFont(GetFont(debugFont));
			v2 drawPos = NewVec2(5, 200);
			v2 drawSize = NewVec2(25, RenderScreenSize.height-5 - drawPos.y);
			for (u32 aIndex = 0; aIndex < ArrayCount(arenas); aIndex++)
			{
				MemoryArena_t* arena = arenas[aIndex];
				const char* arenaName = arenaNames[aIndex];
				
				bool drawBasic = true;
				r32 basicFillPercent = (arena->size > 0) ? ((r32)arena->used / (r32)arena->size) : 0.0f;
				r32 subFillPercent = 0.0f;
				if (arena->type == MemoryArenaType_Temp)
				{
					u32 highWaterMark = ArenaGetHighWaterMark(arena);
					basicFillPercent = (r32)highWaterMark / (r32)arena->size;
					subFillPercent = (r32)app->appInitTempHighWaterMark / (r32)arena->size;
				}
				else if (arena->type == MemoryArenaType_GrowingHeap)
				{
					Assert(arena->base != nullptr);
					GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
					GrowingHeapPageHeader_t* pageHeader = mainHeader->firstPage;
					v2 namePos = drawPos + NewVec2(0, -RcMaxExtendDown() - RcLineHeight()*2);
					v2 percentStrPos = namePos + NewVec2(0, RcLineHeight());
					v2 pagesStrPos = namePos + NewVec2(0, RcLineHeight()*2);
					
					u32 numPages = 0;
					bool drawName = false;
					while (pageHeader != nullptr)
					{
						r32 fillPercent = (r32)pageHeader->used / (r32)pageHeader->size;
						rec drawRec = NewRec(drawPos, drawSize);
						rec fillRec = drawRec;
						fillRec.height = FloorR32(fillRec.height * fillPercent);
						bool insideDrawRec = IsInsideRec(drawRec, RenderMousePos);
						bool insideFillRec = IsInsideRec(fillRec, RenderMousePos);
						Color_t backColor = VisBlue;
						Color_t fillColor = VisOrange;
						Color_t borderColor = VisWhite;
						if (insideDrawRec)
						{
							backColor = VisLightBlue;
							drawName = true;
							basicFillPercent = fillPercent;
						}
						if (insideFillRec) { fillColor = VisYellow; }
						
						RcDrawRectangle(drawRec, backColor);
						RcDrawRectangle(fillRec, fillColor);
						RcDrawButton(drawRec, NoColor, borderColor, 1);
						
						drawPos.x += drawRec.width + 5;
						numPages++;
						pageHeader = pageHeader->next;
					}
					
					if (drawName)
					{
						char* percentStr = TempPrint("%.1f%%", basicFillPercent*100);
						char* pagesStr = TempPrint("%u/%u", numPages, mainHeader->maxNumPages);
						RcDrawStringNt(percentStr, percentStrPos, VisWhite);
						RcDrawStringNt(pagesStr, pagesStrPos, VisWhite);
						RcDrawStringNt(arenaName, namePos, VisWhite);
					}
					
					drawBasic = false;	
				}
				
				if (drawBasic)
				{
					rec drawRec = NewRec(drawPos, drawSize);
					bool insideDrawRec = IsInsideRec(drawRec, RenderMousePos);
					Color_t backColor = VisBlue;
					Color_t borderColor = VisWhite;
					
					if (insideDrawRec) { backColor = VisLightBlue; }
					
					RcDrawRectangle(drawRec, backColor);
					if (subFillPercent > 0)
					{
						rec subFillRec = drawRec;
						subFillRec.height = FloorR32(subFillRec.height * subFillPercent);
						bool insideSubFillRec = IsInsideRec(subFillRec, RenderMousePos);
						Color_t subFillColor = VisPurple;
						if (insideSubFillRec) { subFillColor = VisLightPurple; }
						RcDrawRectangle(subFillRec, subFillColor);
					}
					if (basicFillPercent > 0)
					{
						rec fillRec = drawRec;
						fillRec.height = FloorR32(fillRec.height * basicFillPercent);
						bool insideFillRec = IsInsideRec(fillRec, RenderMousePos);
						Color_t fillColor = VisOrange;
						if (insideFillRec) { fillColor = VisYellow; }
						RcDrawRectangle(fillRec, fillColor);
					}
					RcDrawButton(drawRec, NoColor, borderColor, 1);
					
					if (insideDrawRec)
					{
						v2 namePos = drawRec.topLeft + NewVec2(0, -RcMaxExtendDown() - RcLineHeight()*2);
						v2 percentStrPos = namePos + NewVec2(0, RcLineHeight());
						v2 sizeStrPos = namePos + NewVec2(0, RcLineHeight()*2);
						if (arena->size > 0)
						{
							char* percentStr = TempPrint("%.1f%%", basicFillPercent*100);
							RcDrawStringNt(percentStr, percentStrPos, VisWhite);
							RcDrawStringNt(FormattedSizeStr(arena->used), sizeStrPos, VisWhite);
						}
						else { namePos.y += RcLineHeight()*2; }
						RcDrawStringNt(arenaName, namePos, VisWhite);
					}
					
					drawPos.x += drawRec.width + 5;
				}
			}
		}
		
		// +==============================+
		// | Render Console Arena Visual  |
		// +==============================+
		if (app->showConsoleArenaVisual)
		{
			RcBindFont(GetFont(debugFont));
			rec arenaRec = NewRec(10, 10, 40, RenderScreenSize.height - 10*2);
			RcDrawRectangle(arenaRec, Black);
			
			u32 lIndex = 0;
			ConsoleLine_t* line = app->dbgConsole.arena.firstLine;
			bool foundHoverLine = false;
			while (line != nullptr)
			{
				u32 lineStartIndex = GetConsoleArenaFifoIndex(&app->dbgConsole.arena, line);
				u32 lineEndIndex = GetConsoleArenaFifoIndex(&app->dbgConsole.arena, GetPntrAfterConsoleLine(line));
				Assert(lineEndIndex >= lineStartIndex);
				u32 lineSize = lineEndIndex - lineStartIndex;
				
				rec lineRec = arenaRec;
				lineRec.height *= (r32)lineSize / (r32)app->dbgConsole.arena.fifoSize;
				lineRec.y += arenaRec.height * ((r32)lineStartIndex / (r32)app->dbgConsole.arena.fifoSize);
				
				Color_t lineRecColor = GetConsoleLineFunctionColor(line); //GetPredefPalColorByIndex((line->gutterNumber)%6);
				RcDrawRectangle(lineRec, lineRecColor);
				if (lIndex == 0)
				{
					rec markRec = lineRec;
					markRec.height = 2;
					markRec.x += markRec.width;
					markRec.width = 15;
					RcDrawRectangle(RecInflateY(markRec, 2), Black);
					RcDrawRectangle(markRec, White);
					v2 textPos = NewVec2(markRec.x + markRec.width + 2, markRec.y + RcCenterOffset());
					RcPrintStringEmbossed(textPos, White, Black, 1.0f, 1, "%u (%s unused)", app->dbgConsole.arena.nextGutterNumber, FormattedSizeStr(app->dbgConsole.arena.fifoSize - app->dbgConsole.arena.fifoUsage));
				}
				
				if (IsCoordInsideRec(lineRec, RenderMousePos) && !foundHoverLine)
				{
					foundHoverLine = true;
					v2 textPos = NewVec2(lineRec.x + lineRec.width + 3, lineRec.y + lineRec.height/2 + RcCenterOffset());
					textPos = Vec2Round(textPos);
					RcPrintStringEmbossed(textPos, White, Black, 1.0f, 1, "[%u] %u: %s (%s total)", lIndex, line->gutterNumber, FormattedSizeStr(line->filePathLength+1 + line->funcNameLength+1 + line->messageLength+1), FormattedSizeStr(lineSize));
				}
				
				line = line->next;
				lIndex++;
			}
		}
		
		// +==============================+
		// |     Render Sound Samples     |
		// +==============================+
		if (app->showDebugOverlay && app->audioSampleBuffer.samples != nullptr)
		{
			rec graphRec = NewRec(0, 0, RenderScreenSize.width, RenderScreenSize.height/2);
			u32 barWidth = 1;
			u32 graphWidth = (u32)FloorR32i(graphRec.width / (r32)barWidth);
			u32 numSamplesPerBar = app->audioSampleBuffer.numSamples / graphWidth; 
			u32 baseIndex = 0;
			
			RcDrawButton(graphRec, White, Black);
			for (u32 bIndex = 0; bIndex < graphWidth; bIndex++)
			{
				u32 barSampleIndex = baseIndex + bIndex*numSamplesPerBar;
				i16 maxValue = 0;
				i16 minValue = 0;
				for (u32 sIndex = 0; sIndex < numSamplesPerBar; sIndex++)
				{
					i16 sample = 0;
					u32 sampleIndex = barSampleIndex + sIndex;
					if (sampleIndex < app->audioSampleBuffer.numSamples) { sample = app->audioSampleBuffer.samples[sampleIndex]; }
					if (sample < minValue) { minValue = sample; }
					if (sample > maxValue) { maxValue = sample; }
				}
				
				r32 maxValuePercent = (r32)maxValue / 32767.0f;
				r32 minValuePercent = (r32)-minValue / 32768.0f;
				r32 barTop = graphRec.height/2 - (graphRec.height/2 * maxValuePercent);
				if (barTop > graphRec.height/2 - 1) { barTop = graphRec.height/2 - 1; }
				r32 barBottom = graphRec.height/2 + (graphRec.height/2 * minValuePercent);
				if (barBottom < graphRec.height/2 + 1) { barBottom = graphRec.height/2 + 1; }
				rec barRec = NewRec(graphRec.x + (r32)barWidth*bIndex, graphRec.y + barTop, (r32)barWidth, barBottom - barTop);
				RcDrawRectangle(barRec, VisRed);
				
				rec collisionRec = NewRec(barRec.x, graphRec.y, barRec.width+1, graphRec.height);
				if (IsInsideRec(collisionRec, RenderMousePos, false))
				{
					RcBindFont(GetFont(pixelFont), 1.0f);
					v2 textPos = collisionRec.topLeft + collisionRec.size + NewVec2(1, -1 - RcMaxExtendDown());
					RcPrintString(textPos, Black, 1.0f, "%u [%d,%d]", barSampleIndex, minValue, maxValue);
				}
			}
		}
		
		// +==============================+
		// |     Render Notifications     |
		// +==============================+
		NotificationsRender();
		
		// +==============================+
		// |    Render Mouse Over Name    |
		// +==============================+
		if (app->showMouseOverName)
		{
			v2 textPos = RenderMousePos + NewVec2(0, -RcMaxExtendDown());
			if (textPos.y < RcMaxExtendUp()) { textPos.y = RcMaxExtendUp(); }
			if (app->mouseOverOther && app->mouseOverName != nullptr)
			{
				RcBindFont(GetFont(debugFont));
				v2 nameSize = RcMeasureStringNt(app->mouseOverName);
				if (textPos.x > RenderScreenSize.width - nameSize.width/2) { textPos.x = RenderScreenSize.width - nameSize.width/2; }
				if (textPos.x < nameSize.width/2) { textPos.x = nameSize.width/2; }
				RcDrawStringEmbossedNt(app->mouseOverName, textPos, VisYellow, Black, 1, 1.0f, Alignment_Center);
				textPos.y -= RcLineHeight();
			}
			for (u32 bIndex = 0; bIndex < ArrayCount(app->mouseStartedOverName); bIndex++)
			{
				Buttons_t button = (Buttons_t)bIndex;
				if (app->mouseStartedOverName[bIndex] != nullptr)
				{
					RcPrintStringEmbossedAligned(textPos, VisYellow, Black, 1.0f, 1, Alignment_Center, "%s: %s", GetButtonName(button), app->mouseStartedOverName[bIndex]);
					textPos.y -= RcLineHeight();
				}
			}
		}
	}
	
	// +==============================+
	// |     Update Window Title      |
	// +==============================+
	if (DEBUG)
	{
		BufferPrint(appOutput->windowTitle, GAME_WINDOW_TITLE_ABBREV "_DEBUG");
	}
	else if (DEVELOPER)
	{
		BufferPrint(appOutput->windowTitle, GAME_WINDOW_TITLE_ABBREV "_DEV v%u.%02u(%03u)", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
	}
	else
	{
		BufferPrint(appOutput->windowTitle,  GAME_WINDOW_TITLE);
	}
	
	// +==============================+
	// |         Go To Giffer         |
	// +==============================+
	if (app->goToGiffer)
	{
		Assert(app->gifStream.isActive);
		AppStateChange_t change = {};
		change.type = AppStateChangeType_Push;
		change.newAppState = AppState_Giffer;
		PushAppState_(&change);
		app->goToGiffer = false;
	}
	
	// +==============================+
	// |       Track Key Inputs       |
	// +==============================+
	app->keyTrackerIndex++;
	if (app->keyTrackerIndex >= KEY_TRACKER_LENGTH) { app->keyTrackerIndex = 0; }
	for (u32 tIndex = 0; tIndex < app->trackedKeys.length; tIndex++)
	{
		KeyTrackerButton_t* key = DynArrayGet(&app->trackedKeys, KeyTrackerButton_t, tIndex);
		Assert(key != nullptr);
		key->history[app->keyTrackerIndex] = 0x00;
		if (ButtonDownNoHandling(key->button))     { FlagSet(key->history[app->keyTrackerIndex], KeyTrackerFlag_Down);     }
		if (ButtonPressedNoHandling(key->button))  { FlagSet(key->history[app->keyTrackerIndex], KeyTrackerFlag_Pressed);  }
		if (ButtonReleasedNoHandling(key->button)) { FlagSet(key->history[app->keyTrackerIndex], KeyTrackerFlag_Released); }
		if (app->buttonHandled[key->button])       { FlagSet(key->history[app->keyTrackerIndex], KeyTrackerFlag_Handled);  }
	}
	
	// +==============================+
	// |     Record Rc Debug Info     |
	// +==============================+
	#if RECORD_RC_DEBUG_INFO
	if (app->rcDebugInfo != nullptr && app->rcDebugInfoSize == sizeof(RcDebugInfo_t))
	{
		if (app->rcDebugInfoLast == nullptr || app->rcDebugInfoSizeLast != sizeof(RcDebugInfo_t))
		{
			if (app->rcDebugInfoLast != nullptr) { ArenaPop(mainHeap, app->rcDebugInfoLast); app->rcDebugInfoLast = nullptr; }
			app->rcDebugInfoLast = PushStruct(mainHeap, RcDebugInfo_t);
			app->rcDebugInfoSizeLast = sizeof(RcDebugInfo_t);
		}
		Assert(app->rcDebugInfoLast != nullptr);
		MyMemCopy(app->rcDebugInfoLast, app->rcDebugInfo, sizeof(RcDebugInfo_t));
	}
	#endif
	
	// +==============================+
	// |  Temp Arena Update Loop Pop  |
	// +==============================+
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
	if (ButtonPressed(Button_T) && ButtonDownNoHandling(Button_Control)) { HandleButton(Button_T); ArenaResetHighWaterMark(TempArena); }
}

#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
// +--------------------------------------------------------------+
// |                     App Get Audio Output                     |
// +--------------------------------------------------------------+
// void App_GetAudioOutput(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, AudioOutput_t* AudioOutput)
AppGetAudioOutput_DEFINITION(App_GetAudioOutput)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr, false);
	
	Assert(AudioOutput->samples != nullptr || AudioOutput->numSamples == 0);
	for (u32 sIndex = 0; sIndex < AudioOutput->numSamples; sIndex++)
	{
		u64 sampleIndex = AudioOutput->sampleIndex + sIndex;
		i16 sampleValue = (sampleIndex % Sample16_MaxValue);
		if ((sampleIndex % (Sample16_MaxValue*2)) >= Sample16_MaxValue) { sampleValue = Sample16_MaxValue - sampleValue; }
		for (u32 cIndex = 0; cIndex < AudioOutput->numChannels; cIndex++)
		{
			AudioOutput->samples[(sIndex * AudioOutput->numChannels) + cIndex] = sampleValue;
		}
	}
}
#endif //AUDIO_ENABLED && USE_CUSTOM_AUDIO

// +--------------------------------------------------------------+
// |                       App Perform Task                       |
// +--------------------------------------------------------------+
// void App_PerformTask(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, ThreadedTask_t* task, const TaskThreadInfo_t* threadInfo)
EXPORT AppPerformTask_DEFINITION(App_PerformTask)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr, true);
	CarryOutAppTask(task, threadInfo);
}

// +--------------------------------------------------------------+
// |                        Task Finished                         |
// +--------------------------------------------------------------+
// void App_TaskFinished(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, ThreadedTask_t* task)
EXPORT AppTaskFinished_DEFINITION(App_TaskFinished)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr, false);
	TempPushMark();
	Assert(task != nullptr);
	
	// PrintLine_I("Task %u finished in %s", task->id, FormattedMillisecondsR64(platform->GetPerfTimeDiff(&task->startTime, &task->endTime)));
	//TODO: Anybody else that wants to get notified of the task finishing?
	CleanUpAfterTaskFinished(task);
	
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
}

// +--------------------------------------------------------------+
// |                         App Closing                          |
// +--------------------------------------------------------------+
// void App_Closing(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppClosing_DEFINITION(App_Closing)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr, false);
	WriteLine_E("Application closing!");
	
	if (app->settingsChanged)
	{
		if (SaveAppSettings(&app->settings))
		{
			WriteLine_I("Saved settings successfully");
		}
		else
		{
			WriteLine_E("Failed to save settings");
		}
	}
	
	if (app->gifStream.isActive)
	{
		FinishGifStream(&app->gifStream);
	}
}

#if (ASSERTIONS_ACTIVE && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	
	if (platform != nullptr && platform->WriteEntireFile != nullptr && platform->AllocateMemory != nullptr && app != nullptr && CRASH_DUMP_ASSERTS && IsThisMainThread())
	{
		MemoryArena_t platformArena;
		InitializeMemoryArenaExternal(&platformArena, platform->AllocateMemory, platform->FreeMemory);
		if (app->numCrashDumps < MAX_CRASH_DUMPS)
		{
			u32 crashDumpSize = 0;
			char* crashDump = nullptr;
			crashDump = DbgConsoleCrashDump(&app->dbgConsole, &platformArena, &crashDumpSize,
				function, &filename[fileNameStart], lineNumber, expressionStr,
				&SystemTime, ProgramTime
			);
			
			if (crashDump != nullptr)
			{
				bool deallocateFilename = true;
				char* crashFilename = ArenaPrint(&platformArena, "crash_%u-%u-%u_%u-%u-%u.txt", LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
				if (crashFilename == nullptr) { crashFilename = (char*)"crash.txt"; deallocateFilename = false; }
				platform->WriteEntireFile(crashFilename, crashDump, crashDumpSize);
				app->numCrashDumps++;
				PrintLine_E("Saved crash dump to \"%s\"", crashFilename);
				ArenaPop(&platformArena, crashDump);
				if (deallocateFilename) { ArenaPop(&platformArena, crashFilename); }
			}
			else
			{
				WriteLine_E("No crash dump generated");
			}
		}
	}
	else if (platform != nullptr)
	{
		ThreadId_t thisThreadId = platform->GetThisThreadId();
		TaskThreadInfo_t* threadInfo = platform->GetThreadInfo(thisThreadId);
		if (platform->IsMainThread(thisThreadId))
		{
			platform->DebugPrint(&filename[fileNameStart], lineNumber, function, DbgLevel_Error, true,
				"Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr
			);
		}
		else if (threadInfo != nullptr)
		{
			platform->DebugPrint(&filename[fileNameStart], lineNumber, function, DbgLevel_Error, true,
				"Assertion Failure on Thread %u(0x%X)! %s in \"%s\" line %d: (%s) is not true", threadInfo->index, thisThreadId, function, &filename[fileNameStart], lineNumber, expressionStr
			);
		}
		else
		{
			platform->DebugPrint(&filename[fileNameStart], lineNumber, function, DbgLevel_Error, true,
				"Assertion Failure on Thread 0x%X! %s in \"%s\" line %d: (%s) is not true", thisThreadId, function, &filename[fileNameStart], lineNumber, expressionStr
			);
		}
	}
	else
	{
		WriteLine_E("platform not available for assertion failure!");
	}
}
#endif
