/*
File:   app_settings_serialization.cpp
Author: Taylor Robbins
Date:   06\17\2019
Description: 
	** Holds the functions that help us serialize and deserialize the AppSettings_t structure to be saved or loaded from a file 
*/

#define TEXT_SETTINGS_TITLE "Scrambler Settings :O"
#define TEXT_SETTINGS_TITLE_LENGTH 21

#define TEXT_SETTINGS_VERS_MAJOR 1
#define TEXT_SETTINGS_VERS_MINOR 2

#define BIN_SETTINGS_TITLE "ScrSett!"
#define BIN_SETTINGS_TITLE_LENGTH 8

#define BIN_SETTINGS_VERS_MAJOR 1
#define BIN_SETTINGS_VERS_MINOR 0

#pragma pack(push, 1)
struct BinAppSettingsHeader_t
{
	char headerTitle[BIN_SETTINGS_TITLE_LENGTH];
	u32 headerSize;
	u8 versionMajor;
	u8 versionMinor;
	Version_t gameVersion;
	Version_t platformVersion;
	u32 keyBindingStructSize;
	u32 padBindingStructSize;
	u32 numSettings;
};
#pragma pack(pop)

// +--------------------------------------------------------------+
// |                     Settings Save Format                     |
// +--------------------------------------------------------------+
char* SerializeSettingsText(MemoryArena_t* arenaPntr, const AppSettings_t* settingsPntr, u32* numBytesOut)
{
	Assert(settingsPntr != nullptr);
	
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 offset = 0;
		
		SrlPrint(result, resultSize, &offset, "%s\n", TEXT_SETTINGS_TITLE);
		SrlPrint(result, resultSize, &offset, "// Saved on %s %s %u\n", GetMonthStr((Month_t)LocalTime.month), GetDayOfMonthString(LocalTime.day), LocalTime.year);
		SrlPrint(result, resultSize, &offset, "Version: %u.%u\n", TEXT_SETTINGS_VERS_MAJOR, TEXT_SETTINGS_VERS_MINOR);
		
		u32 numSettings = GetNumAppSettings();
		for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
		{
			const char* settingName = nullptr;
			const void* settingValuePntr = nullptr;
			SettingType_t settingType = GetSettingInfo(settingsPntr, sIndex, &settingName, &settingValuePntr);
			Assert(settingType != SettingType_None);
			
			if (settingType == SettingType_Boolean) { bool boolValue = *((bool*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %s\n", settingName, boolValue ? "True" : "False"); }
			if (settingType == SettingType_u8)      { u8  u8Value  = *((u8*)settingValuePntr);  SrlPrint(result, resultSize, &offset, "%s: %u\n",  settingName, u8Value);  }
			if (settingType == SettingType_u32)     { u32 u32Value = *((u32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %u\n",  settingName, u32Value); }
			if (settingType == SettingType_i32)     { i32 i32Value = *((i32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %d\n",  settingName, i32Value); }
			if (settingType == SettingType_r32)     { r32 r32Value = *((r32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %f\n",  settingName, r32Value); }
			if (settingType == SettingType_Version) { Version_t versionValue = *((Version_t*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %u.%u(%u)\n",  settingName, versionValue.major, versionValue.minor, versionValue.build); }
		}
		
		if (pass == 0)
		{
			Assert(result == nullptr);
			resultSize = offset + 1; //add 1 byte to compensate for vsnprintf wierdness in SrlPrint
			if (numBytesOut != nullptr) { *numBytesOut = resultSize-1; }
			if (arenaPntr == nullptr) { return nullptr; }
			if (resultSize == 0) { return nullptr; }
			result = PushArray(arenaPntr, char, resultSize);
		}
		else
		{
			Assert(offset+1 == resultSize);
		}
	}
	Assert(result != nullptr);
	return result;
}

//NOTE: This function needs to support using the TempArena which might be the platform layer's temparena when called in App_GetStartupOptions
bool DeserializeSettingsText(MemoryArena_t* arenaPntr, AppSettings_t* settings, const void* fileData, u32 fileLength, u8* versionMajorOut = nullptr, u8* versionMinorOut = nullptr)
{
	Assert(arenaPntr != nullptr);
	Assert(settings != nullptr);
	const char* charPntr = (const char*)fileData;
	u32 numCharsLeft = fileLength;
	
	if (fileData == nullptr) { return false; }
	if (numCharsLeft < TEXT_SETTINGS_TITLE_LENGTH) { return false; }
	if (MyMemCompare(charPntr, TEXT_SETTINGS_TITLE, TEXT_SETTINGS_TITLE_LENGTH) != 0) { return false; }
	charPntr += TEXT_SETTINGS_TITLE_LENGTH;
	numCharsLeft -= TEXT_SETTINGS_TITLE_LENGTH;
	
	InitAppSettingsStruct(arenaPntr, settings);
	FillDefaultAppSettings(settings);
	
	TextParser_t parser;
	CreateTextParser(&parser, charPntr, numCharsLeft);
	
	bool foundVersion     = false;
	u8   versionMajor     = 0;
	u8   versionMinor     = 0;
	bool isOldVersion     = false;
	
	// +--------------------------------------------------------------+
	// |                      Main Parsing Loop                       |
	// +--------------------------------------------------------------+
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token;
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing settings line %u: %s", parser.lineNumber, errorStr);
			DestroyAppSettingsStruct(settings);
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		// +==============================+
		// |          Key-Value           |
		// +==============================+
		if (token.type == ParsingTokenType_KeyValue)
		{
			// +==============================+
			// |           Version            |
			// +==============================+
			if (StrCompareIgnoreCase(token.key.pntr, "Version", token.key.length))
			{
				v2i versionV2i = {};
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &versionV2i);
				if (errorStr != nullptr)
				{
					PrintLine_W("Error while parsing settings version number: %s", errorStr);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				if (versionV2i.x < 0 || versionV2i.x > 255)
				{
					PrintLine_W("Invalid settings major version number: %d", versionV2i.x);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				if (versionV2i.y < 0 || versionV2i.y > 255)
				{
					PrintLine_W("Invalid settings minor version number: %d", versionV2i.y);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				
				versionMajor = (u8)versionV2i.x;
				versionMinor = (u8)versionV2i.y;
				if (versionMajorOut != nullptr) { *versionMajorOut = versionMajor; }
				if (versionMinorOut != nullptr) { *versionMinorOut = versionMinor; }
				
				if (!(versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor == TEXT_SETTINGS_VERS_MINOR) &&
					!(versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor == TEXT_SETTINGS_VERS_MINOR-1))
				{
					PrintLine_W("Settings is version %u.%u not %u.%u", versionMajor, versionMinor, TEXT_SETTINGS_VERS_MAJOR, TEXT_SETTINGS_VERS_MINOR);
					DestroyAppSettingsStruct(settings);
					return false; //deal breaker
				}
				isOldVersion = (versionMajor < TEXT_SETTINGS_VERS_MAJOR || (versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor < TEXT_SETTINGS_VERS_MINOR));
				foundVersion = true;
			}
			else
			{
				u32 settingIndex = GetSettingIndexByName(token.key.pntr, token.key.length);
				if (settingIndex < GetNumAppSettings())
				{
					const char* settingName = nullptr;
					void* settingValuePntr = nullptr;
					SettingType_t settingType = GetSettingInfo(settings, settingIndex, &settingName, &settingValuePntr);
					Assert(settingType != SettingType_None);
					Assert(settingName != nullptr);
					Assert(settingValuePntr != nullptr);
					
					if (settingType == SettingType_Boolean)
					{
						bool* boolPntr = (bool*)settingValuePntr;
						bool boolValue = false;
						errorStr = TryDeserializeBool(token.value.pntr, token.value.length, &boolValue);
						if (errorStr == nullptr)
						{
							*boolPntr = boolValue;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as boolean for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_u8)
					{
						u8* u8Pntr = (u8*)settingValuePntr;
						u8 u8Value = 0;
						errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &u8Value);
						if (errorStr == nullptr)
						{
							*u8Pntr = u8Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as u8 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_u32)
					{
						u32* u32Pntr = (u32*)settingValuePntr;
						u32 u32Value = 0;
						errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &u32Value);
						if (errorStr == nullptr)
						{
							*u32Pntr = u32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as u32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_i32)
					{
						i32* i32Pntr = (i32*)settingValuePntr;
						i32 i32Value = 0;
						errorStr = TryDeserializeI32(token.value.pntr, token.value.length, &i32Value);
						if (errorStr == nullptr)
						{
							*i32Pntr = i32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as i32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_r32)
					{
						r32* r32Pntr = (r32*)settingValuePntr;
						r32 r32Value = 0;
						errorStr = TryDeserializeR32(token.value.pntr, token.value.length, &r32Value);
						if (errorStr == nullptr)
						{
							*r32Pntr = r32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as r32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_Version)
					{
						Version_t* versionPntr = (Version_t*)settingValuePntr;
						u32 numNumbers = 0;
						u32* numbers = ParseNumbersInString(TempArena, token.value.pntr, token.value.length, &numNumbers);
						if (numNumbers == 3)
						{
							if (numbers[0] > 255)
							{
								PrintLine_E("Major version number is too high for setting \"%s\": %u > 255", settingName, numbers[0]);
							}
							else if (numbers[1] > 255)
							{
								PrintLine_E("Minor version number is too high for setting \"%s\": %u > 255", settingName, numbers[1]);
							}
							else if (numbers[2] > 65535)
							{
								PrintLine_E("Build version number is too high for setting \"%s\": %u > 65535", settingName, numbers[2]);
							}
							else
							{
								versionPntr->major = (u8)numbers[0];
								versionPntr->minor = (u8)numbers[1];
								versionPntr->build = (u16)numbers[2];
							}
						}
						else
						{
							PrintLine_E("Found %u/3 numbers in Version setting \"%s\": %.*s", numNumbers, settingName, token.value.length, token.value.pntr);
						}
					}
					else
					{
						PrintLine_E("Unhandled setting type in deserialization: %u", settingType);
					}
				}
				else
				{
					PrintLine_E("Unknown setting found: \"%.*s\"", token.key.length, token.key.pntr);
				}
			}
		}
		// +==============================+
		// |           Comment            |
		// +==============================+
		else if (token.type == ParsingTokenType_Comment)
		{
			//Ignore the comment
		}
		// +==============================+
		// |       Unhandled Token        |
		// +==============================+
		else
		{
			if (token.pieces[0].length >= 7 && MyStrCompare(token.pieces[0].pntr, "# Saved", 7) == 0)
			{
				//This was the old format for the "# Saved On ..." comment
			}
			else
			{
				PrintLine_W("Found unsupported token in settings line %u: \"%.*s\"", parser.lineNumber, token.pieces[0].length, token.pieces[0].pntr);
				DestroyAppSettingsStruct(settings);
				return false;
			}
		}
	}
	
	// +==============================+
	// | Check Everything was defined |
	// +==============================+
	if (!foundVersion)
	{
		PrintLine_W("Settings contained no Version!");
		DestroyAppSettingsStruct(settings);
		return false;
	}
	
	return true;
}

bool SaveAppSettingsTo(const AppSettings_t* settings, const char* filePath) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	Assert(filePath != nullptr);
	
	TempPushMark();
	u32 serializedSize = 0;
	char* serializedContents = SerializeSettingsText(TempArena, settings, &serializedSize);
	if (serializedContents == nullptr || serializedSize == 0)
	{
		WriteLine_E("Failed to serialize the settings structure");
		TempPopMark();
		return false;
	}
	
	if (!platform->WriteEntireFile(filePath, serializedContents, serializedSize))
	{
		PrintLine_E("Failed to write %u byte settings file to \"%s\"", serializedSize, filePath);
		TempPopMark();
		return false;
	}
	
	TempPopMark();
	return true;
}

bool SaveAppSettings(const AppSettings_t* settings) //pre-declared in app_settings.h
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return false;
	}
	if (!platform->DoesFolderExist(settingsFolder))
	{
		// bool createSuccess = platform->CreateFolder(settingsFolder);
		bool createSuccess = CreateDirectories(settingsFolder);
		if (!createSuccess)
		{
			PrintLine_E("Failed to create directory for settings file at \"%s\"", settingsFolder);
			return false;
		}
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	Assert(settingsFilePath != nullptr);
	return SaveAppSettingsTo(settings, settingsFilePath);
}

bool LoadAppSettingsFrom(AppSettings_t* settings, const char* filePath) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	Assert(filePath != nullptr);
	
	FileInfo_t settingsFile = platform->ReadEntireFile(filePath);
	if (settingsFile.content == nullptr || settingsFile.size == 0)
	{
		PrintLine_E("Couldn't open settings file at \"%s\"", filePath);
		return false;
	}
	
	TempPushMark();
	AppSettings_t newSettings = {};
	if (!DeserializeSettingsText(TempArena, &newSettings, (const char*)settingsFile.content, settingsFile.size))
	{
		PrintLine_E("Settings file is corrupt at \"%s\"", filePath);
		TempPopMark();
		platform->FreeFileMemory(&settingsFile);
		return false;
	}
	DestroyAppSettingsStruct(settings);
	CopyAppSettingsStruct(mainHeap, settings, &newSettings);
	TempPopMark();
	
	return true;
}

bool LoadAppSettings(AppSettings_t* settings)
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return false;
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	Assert(settingsFilePath != nullptr);
	return LoadAppSettingsFrom(settings, settingsFilePath);
}

bool DeleteSettingsFile()
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return true; //I guess we can just treat this as success since we aren't sure the file exists
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	if (platform->DoesFileExist(settingsFilePath))
	{
		if (platform->DeleteFile(settingsFilePath))
		{
			return true;
		}
		else
		{
			PrintLine_E("Failed to delete settings file at \"%s\"", settingsFilePath);
			return false;
		}
	}
	else { return true; } //file already doesn't exist. That's success
}

// +--------------------------------------------------------------+
// |              Binary App Settings Serialization               |
// +--------------------------------------------------------------+
u32 GetSerializedAppSettingsBinSize(const AppSettings_t* settings) //pre-declared in app_func_defs.h
{
	Assert(settings != nullptr);
	u32 result = sizeof(BinAppSettingsHeader_t);
	u32 numSettings = GetNumAppSettings();
	for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
	{
		const char* settingName = nullptr;
		const void* settingValuePntr = nullptr;
		SettingType_t settingType = GetSettingInfo(settings, sIndex, &settingName, &settingValuePntr);
		Assert(settingName != nullptr);
		Assert(settingValuePntr != nullptr);
		Assert(settingType != SettingType_None && settingType < SettingType_NumTypes);
		u32 settingNameLength = MyStrLength32(settingName);
		result += sizeof(u8) + sizeof(u32) + settingNameLength;
		switch (settingType)
		{
			case SettingType_Boolean:    result += sizeof(bool); break;
			case SettingType_u8:         result += sizeof(u8); break;
			case SettingType_u32:        result += sizeof(u32); break;
			case SettingType_i32:        result += sizeof(i32); break;
			case SettingType_r32:        result += sizeof(r32); break;
			case SettingType_Version:    result += sizeof(Version_t); break;
			default: Assert(false);
		}
	}
	return result;
}
u8* SerializeAppSettingsBin(MemoryArena_t* arenaPntr, const AppSettings_t* settings, u32* numBytesOut) //pre-declared in app_func_defs.h
{
	Assert(arenaPntr != nullptr);
	Assert(settings != nullptr);
	
	u32 resultSize = GetSerializedAppSettingsBinSize(settings);
	u8* result = PushArray(arenaPntr, u8, resultSize);
	if (result == nullptr) { return result; }
	MyMemSet(result, 0x00, resultSize);
	
	BinAppSettingsHeader_t* mainHeader = (BinAppSettingsHeader_t*)result;
	u8* contentBytePntr = (u8*)(result + sizeof(BinAppSettingsHeader_t));
	u32 contentSize = resultSize - sizeof(BinAppSettingsHeader_t);
	u32 numSettings = GetNumAppSettings();
	
	ClearPointer(mainHeader);
	MyMemCopy(&mainHeader->headerTitle[0], BIN_SETTINGS_TITLE, BIN_SETTINGS_TITLE_LENGTH);
	mainHeader->headerSize = sizeof(BinAppSettingsHeader_t);
	mainHeader->versionMajor = BIN_SETTINGS_VERS_MAJOR;
	mainHeader->versionMinor = BIN_SETTINGS_VERS_MINOR;
	mainHeader->gameVersion.major = APP_VERSION_MAJOR;
	mainHeader->gameVersion.minor = APP_VERSION_MINOR;
	mainHeader->gameVersion.build = APP_VERSION_BUILD;
	mainHeader->platformVersion = platform->version;
	mainHeader->keyBindingStructSize = 0;
	mainHeader->padBindingStructSize = 0;
	mainHeader->numSettings = numSettings;
	
	u32 cIndex = 0;
	for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
	{
		const char* settingName = nullptr;
		const void* settingValuePntr = nullptr;
		SettingType_t settingType = GetSettingInfo(settings, sIndex, &settingName, &settingValuePntr);
		Assert(settingName != nullptr);
		Assert(settingValuePntr != nullptr);
		Assert(settingType != SettingType_None && settingType < SettingType_NumTypes);
		u32 settingNameLength = MyStrLength32(settingName);
		Assert(cIndex + sizeof(u8) + sizeof(u32) + settingNameLength <= contentSize);
		*((u8*)(contentBytePntr + cIndex)) = (u8)settingType; cIndex += sizeof(u8);
		*((u32*)(contentBytePntr + cIndex)) = settingNameLength; cIndex += sizeof(u32);
		MyMemCopy(contentBytePntr + cIndex, settingName, settingNameLength); cIndex += settingNameLength;
		switch (settingType)
		{
			case SettingType_Boolean:    Assert(cIndex+sizeof(bool)      <= contentSize); *((bool*)(contentBytePntr + cIndex)) = *((const bool*)settingValuePntr); cIndex += sizeof(bool); break;
			case SettingType_u8:         Assert(cIndex+sizeof(u8)        <= contentSize); *((u8*)(contentBytePntr + cIndex))   = *((const u8*)settingValuePntr);   cIndex += sizeof(u8);   break;
			case SettingType_u32:        Assert(cIndex+sizeof(u32)       <= contentSize); *((u32*)(contentBytePntr + cIndex))  = *((const u32*)settingValuePntr);  cIndex += sizeof(u32);  break;
			case SettingType_i32:        Assert(cIndex+sizeof(i32)       <= contentSize); *((i32*)(contentBytePntr + cIndex))  = *((const i32*)settingValuePntr);  cIndex += sizeof(i32);  break;
			case SettingType_r32:        Assert(cIndex+sizeof(r32)       <= contentSize); *((r32*)(contentBytePntr + cIndex))  = *((const r32*)settingValuePntr);  cIndex += sizeof(r32);  break;
			case SettingType_Version:    Assert(cIndex+sizeof(Version_t) <= contentSize); *((Version_t*)(contentBytePntr + cIndex)) = *((const Version_t*)settingValuePntr); cIndex += sizeof(Version_t); break;
			default: Assert(false);
		}
	}
	Assert(cIndex == contentSize);
	
	if (numBytesOut != nullptr) { *numBytesOut = resultSize; }
	return result;
}

bool DeserializeAppSettingsBin(MemoryArena_t* arenaPntr, AppSettings_t* settingsOut, const u8* dataPntr, u32 dataSize) //pre-declared in app_func_defs.h
{
	Assert(arenaPntr != nullptr);
	Assert(settingsOut != nullptr);
	if (dataPntr == 0 || dataSize == 0) { return false; }
	if (dataSize < sizeof(BinAppSettingsHeader_t))
	{
		PrintLine_E("Expected %u bytes in BinAppSettings not %u", sizeof(BinAppSettingsHeader_t), dataSize);
		return false;
	}
	
	const BinAppSettingsHeader_t* mainHeader = (const BinAppSettingsHeader_t*)dataPntr;
	if (MyMemCompare(&mainHeader->headerTitle[0], BIN_SETTINGS_TITLE, BIN_SETTINGS_TITLE_LENGTH) != 0)
	{
		WriteLine_E("Invalid title in BinAppSettings");
		return false;
	}
	if (mainHeader->versionMajor != BIN_SETTINGS_VERS_MAJOR || mainHeader->versionMinor != BIN_SETTINGS_VERS_MINOR)
	{
		PrintLine_E("BinAppSettings is v%u.%u not v%u.%u", mainHeader->versionMajor, mainHeader->versionMinor, BIN_SETTINGS_VERS_MAJOR, BIN_SETTINGS_VERS_MINOR);
		return false;
	}
	if (mainHeader->headerSize != sizeof(BinAppSettingsHeader_t))
	{
		PrintLine_E("BinAppSettings declares %u byte header not %u", mainHeader->headerSize, sizeof(BinAppSettingsHeader_t));
		return false;
	}
	//TODO: Should we check the gameVersion or platformVersion for any reason?
	
	u32 numSettings = GetNumAppSettings();
	InitAppSettingsStruct(arenaPntr, settingsOut);
	FillDefaultAppSettings(settingsOut);
	
	const u8* contentBytePntr = (const u8*)(dataPntr + sizeof(BinAppSettingsHeader_t));
	u32 contentSize = dataSize - sizeof(BinAppSettingsHeader_t);
	u32 cIndex = 0;
	for (u32 sIndex = 0; sIndex < mainHeader->numSettings; sIndex++)
	{
		if (cIndex + sizeof(u8) + sizeof(u32) > contentSize)
		{
			PrintLine_E("Expected %u more bytes in BinAppSettings not %u", sizeof(u8) + sizeof(u32), contentSize - cIndex);
			DestroyAppSettingsStruct(settingsOut);
			return false;
		}
		SettingType_t type = (SettingType_t)(*((u8*)(contentBytePntr + cIndex))); cIndex += sizeof(u8);
		u32 nameLength = *((u32*)(contentBytePntr + cIndex)); cIndex += sizeof(u32);
		if (cIndex + nameLength > contentSize)
		{
			PrintLine_E("Expected %u more bytes in BinAppSettings not %u", nameLength, contentSize - cIndex);
			DestroyAppSettingsStruct(settingsOut);
			return false;
		}
		const char* namePntr = (const char*)(contentBytePntr + cIndex); cIndex += nameLength;
		if (type == SettingType_None || type >= SettingType_NumTypes)
		{
			PrintLine_E("Invalid setting type 0x%02X found in BinAppSettings", type);
			DestroyAppSettingsStruct(settingsOut);
			return false;
		}
		const void* valuePntr = (const void*)(contentBytePntr + cIndex);
		u32 valueSize = 0;
		switch (type)
		{
			case SettingType_Boolean:    valueSize = sizeof(bool); break;
			case SettingType_u8:         valueSize = sizeof(u8); break;
			case SettingType_u32:        valueSize = sizeof(u32); break;
			case SettingType_i32:        valueSize = sizeof(i32); break;
			case SettingType_r32:        valueSize = sizeof(r32); break;
			case SettingType_Version:    valueSize = sizeof(Version_t); break;
		}
		if (cIndex + valueSize > contentSize)
		{
			PrintLine_E("Expected %u more bytes in BinAppSettings not %u", valueSize, contentSize - cIndex);
			DestroyAppSettingsStruct(settingsOut);
			return false;
		}
		cIndex += valueSize;
		
		u32 settingIndex = GetSettingIndexByName(namePntr, nameLength);
		if (settingIndex >= numSettings)
		{
			PrintLine_W("Unknown setting in BinAppSettings: \"%.*s\"", nameLength, namePntr);
			continue;
		}
		
		const char* settingName = nullptr;
		void* settingValuePntr = nullptr;
		SettingType_t settingType = GetSettingInfo(settingsOut, settingIndex, &settingName, &settingValuePntr);
		Assert(settingName != nullptr);
		Assert(settingValuePntr != nullptr);
		if (settingType != type)
		{
			PrintLine_W("BinAppSetting setting has wrong type 0x%02X not 0x%02X", type, settingType);
			continue;
		}
		
		switch (settingType)
		{
			case SettingType_Boolean: *((bool*)settingValuePntr) = *((const bool*)valuePntr); break;
			case SettingType_u8:      *((u8*)settingValuePntr)   = *((const u8*)valuePntr);   break;
			case SettingType_u32:     *((u32*)settingValuePntr)  = *((const u32*)valuePntr);  break;
			case SettingType_i32:     *((i32*)settingValuePntr)  = *((const i32*)valuePntr);  break;
			case SettingType_r32:     *((r32*)settingValuePntr)  = *((const r32*)valuePntr);  break;
			case SettingType_Version: *((Version_t*)settingValuePntr) = *((const Version_t*)valuePntr); break;
		}
	}
	
	if (cIndex < contentSize) { PrintLine_W("WARNING: %u Extra bytes in BinAppSettings serialization", contentSize - cIndex); }
	return true;
}
