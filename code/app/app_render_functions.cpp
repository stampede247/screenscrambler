/*
File:   app_render_functions.cpp
Author: Taylor Robbins
Date:   01\30\2019
Description: 
	** Holds all the Rc functions that help us render various sorts
	** of geometry to the screen using the RenderContext_t
*/

void RcDrawTexturedRec(rec rectangle, Color_t color)
{
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)), //Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f)));  //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}
void RcDrawTexturedRec(rec rectangle, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcBindTexture(rc->boundTexture);
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),//Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawRectangle(rec rectangle, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),//Position
		Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f))); //Scale
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawTriangle(v2 point, v2 dirVec, r32 height, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 triangleHeight = SqrtR32(3)/2;
	r32 direction = AtanR32(dirVec.y, dirVec.x);
	mat4 worldMatrix = Mat4Multiply(
		Mat4Translate(NewVec3(point.x, point.y, rc->depth)),//Position
		Mat4Scale(NewVec3(height/triangleHeight, height/triangleHeight, 1.0f)), //Scale
		Mat4RotateZ(Pi32/2.0f + direction)); //Rotation
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->equilTriangleBuffer);
	RcDrawBufferTriangles();
}
void RcDrawStretchedTriangle(v2 point, v2 dirVec, r32 width, r32 height, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 triangleWidth = 1.0f;
	r32 triangleHeight = SqrtR32(3)/2;
	r32 direction = AtanR32(dirVec.y, dirVec.x);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale(NewVec3(width/triangleWidth, height/triangleHeight, 1.0f))); //Scale
	Mat4Transform(worldMatrix, Mat4RotateZ(Pi32/2.0f + direction)); //Rotation
	Mat4Transform(worldMatrix, Mat4Translate(NewVec3(point.x, point.y, rc->depth))); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->equilTriangleBuffer);
	RcDrawBufferTriangles();
}

void RcDrawTriangleBordered(v2 point, v2 dirVec, r32 height, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 normDirVec = Vec2Normalize(dirVec);
	r32 pointDist = Vec2Length(NewVec2(borderWidth * (1 + CosR32(ToRadians(60))), borderWidth * SinR32(ToRadians(60))));
	RcDrawTriangle(point + normDirVec*pointDist, dirVec, height + borderWidth + pointDist, borderColor);
	RcDrawTriangle(point, dirVec, height, color);
}

void RcDrawButton(rec rectangle, Color_t backgroundColor, Color_t borderColor, r32 borderWidth = 1.0f)
{
	RcDrawRectangle(rectangle, backgroundColor);
	
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + rectangle.height - borderWidth, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + rectangle.width - borderWidth, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
}

void RcDrawPixelatedButton(rec rectangle, Color_t backgroundColor, Color_t borderColor, r32 borderWidth = 1.0f)
{
	RcDrawRectangle(RecDeflate(rectangle, borderWidth), backgroundColor);
	
	RcDrawRectangle(NewRec(rectangle.x + borderWidth, rectangle.y, rectangle.width - borderWidth*2, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + borderWidth, rectangle.y + rectangle.height - borderWidth, rectangle.width - borderWidth*2, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + rectangle.width - borderWidth, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
}

void RcDrawRotatedRectangle(v2 center, v2 size, r32 rotation, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-0.5f, -0.5f));                   //center
	Mat4Transform(worldMatrix, Mat4Scale2(size.width, size.height));            //scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                          //rotate
	Mat4Transform(worldMatrix, Mat4Translate3(center.x, center.y, rc->depth));  //position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}
void RcDrawObb2(obb2 box, Color_t color)
{
	RcDrawRotatedRectangle(box.center, box.size, box.rotation, color);
}

void RcDrawTexturedRotatedRectangle(v2 center, v2 size, r32 rotation, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-0.5f, -0.5f));                   //center
	Mat4Transform(worldMatrix, Mat4Scale2(size.width, size.height));            //scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                          //rotate
	Mat4Transform(worldMatrix, Mat4Translate3(center.x, center.y, rc->depth));  //position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}
void RcDrawTexturedRotatedRectangle(v2 center, v2 size, r32 rotation, Color_t color, bool flippedX = false, bool flippedY = false)
{
	rec sourceRec = NewRec(
		flippedX ? (r32)rc->boundTexture->width : 0,
		flippedY ? (r32)rc->boundTexture->height : 0,
		flippedX ? (r32)-rc->boundTexture->width : (r32)rc->boundTexture->width,
		flippedY ? (r32)-rc->boundTexture->height : (r32)rc->boundTexture->height
	);
	RcDrawTexturedRotatedRectangle(center, size, rotation, color, sourceRec);
}
void RcDrawTexturedObb2(obb2 box, Color_t color, rec sourceRectangle)
{
	RcDrawTexturedRotatedRectangle(box.center, box.size, box.rotation, color, sourceRectangle);
}
void RcDrawTexturedObb2(obb2 box, Color_t color, bool flippedX = false, bool flippedY = false)
{
	rec sourceRec = NewRec(
		flippedX ? (r32)rc->boundTexture->width : 0,
		flippedY ? (r32)rc->boundTexture->height : 0,
		flippedX ? (r32)-rc->boundTexture->width : (r32)rc->boundTexture->width,
		flippedY ? (r32)-rc->boundTexture->height : (r32)rc->boundTexture->height
	);
	RcDrawTexturedRotatedRectangle(box.center, box.size, box.rotation, color, sourceRec);
}

void RcDrawBorderedObb2(obb2 box, Color_t centerColor, Color_t outlineColor, r32 borderWidth = 1.0f)
{
	v2 rightVec = NewVec2(CosR32(box.rotation), SinR32(box.rotation));
	v2 downVec = Vec2PerpRight(rightVec);
	RcDrawObb2(box, centerColor);
	RcDrawObb2(NewObb2(box.x - downVec.x*(box.height/2 - borderWidth/2), box.y - downVec.y*(box.height/2 - borderWidth/2), box.width, borderWidth, box.rotation), outlineColor);
	RcDrawObb2(NewObb2(box.x + downVec.x*(box.height/2 - borderWidth/2), box.y + downVec.y*(box.height/2 - borderWidth/2), box.width, borderWidth, box.rotation), outlineColor);
	RcDrawObb2(NewObb2(box.x - rightVec.x*(box.width/2 - borderWidth/2), box.y - rightVec.y*(box.width/2 - borderWidth/2), borderWidth, box.height, box.rotation), outlineColor);
	RcDrawObb2(NewObb2(box.x + rightVec.x*(box.width/2 - borderWidth/2), box.y + rightVec.y*(box.width/2 - borderWidth/2), borderWidth, box.height, box.rotation), outlineColor);
}

void RcDrawLine(v2 p1, v2 p2, r32 thickness, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 length = Vec2Length(p2 - p1);
	r32 rotation = AtanR32(p2.y - p1.y, p2.x - p1.x); 
	mat4 worldMatrix = Mat4_Identity;
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(0.0f, -0.5f, 0.0f)),       worldMatrix); //Centering
	worldMatrix = Mat4Multiply(Mat4Scale(NewVec3(length, thickness, 1.0f)),     worldMatrix); //Scale
	worldMatrix = Mat4Multiply(Mat4RotateZ(rotation),                           worldMatrix); //Rotation
	worldMatrix = Mat4Multiply(Mat4Translate(NewVec3(p1.x, p1.y, rc->depth)), worldMatrix); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

// +--------------------------------------------------------------+
// |                   Special Render Functions                   |
// +--------------------------------------------------------------+
void RcDrawGradient(rec rectangle, Color_t color1, Color_t color2, Dir2_t direction)
{
	RcBindTexture(&rc->gradientTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color1);
	RcSetSecondaryColor(color2);
	RcSetGradientEnabled(true);
	
	mat4 worldMatrix = Mat4_Identity;
	switch (direction)
	{
		case Dir2_Right:
		default:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x, rectangle.y, rc->depth)),
				Mat4Scale(NewVec3(rectangle.width, rectangle.height, 1.0f)));
		} break;
		
		case Dir2_Left:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y, rc->depth)),
				Mat4Scale(NewVec3(-rectangle.width, rectangle.height, 1.0f)));
		} break;
		
		case Dir2_Down:
		{
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y, rc->depth)),
				Mat4RotateZ(ToRadians(90)),
				Mat4Scale(NewVec3(rectangle.height, rectangle.width, 1.0f)));
		} break;
		
		case Dir2_Up:
		{	
			worldMatrix = Mat4Multiply(
				Mat4Translate(NewVec3(rectangle.x + rectangle.width, rectangle.y + rectangle.height, rc->depth)),
				Mat4RotateZ(ToRadians(90)),
				Mat4Scale(NewVec3(-rectangle.height, rectangle.width, 1.0f)));
		} break;
	};
	RcSetWorldMatrix(worldMatrix);
	
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
	
	RcSetGradientEnabled(false);
}

void RcDrawCircle(v2 center, r32 radius, Color_t color)
{
	RcSetCircleRadius(1.0f, 0.0f);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(0.0f, 0.0f);
}

void RcDrawDonut(v2 center, r32 radius, r32 innerRadius, Color_t color)
{
	r32 realInnerRadius = ClampR32(innerRadius / radius, 0.0f, 1.0f);
	RcSetCircleRadius(1.0f, realInnerRadius);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(0.0f, 0.0f);
}

// +--------------------------------------------------------------+
// |                Sprite Sheet Render Functions                 |
// +--------------------------------------------------------------+
void RcDrawSheetFrame(const SpriteSheet_t* spriteSheet, v2i frame, rec drawRec, Color_t color, Dir2_t rotation = Dir2_Down, bool flippedX = false, bool flippedY = false)
{
	Assert(spriteSheet != nullptr);
	//TODO: Only try to batch if this frame isn't being drawn with special settings like stenciling, and a special shader
	if (!RcBatchSheetFrame(spriteSheet, drawRec, color, frame, rotation, flippedX, flippedY))
	{
		RcBindTexture(&spriteSheet->texture);
		rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
		RcSetSourceRectangle(sourceRec);
		RcSetColor(color);
		
		mat4 worldMatrix = Mat4_Identity;
		if (flippedX)
		{
			Mat4Transform(worldMatrix, Mat4Scale2(-1, 1));
			Mat4Transform(worldMatrix, Mat4Translate2(1, 0));
		}
		if (flippedY)
		{
			Mat4Transform(worldMatrix, Mat4Scale2(1, -1));
			Mat4Transform(worldMatrix, Mat4Translate2(0, 1));
		}
		if (rotation == Dir2_Left)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(90))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y, rc->depth)); //Position
		}
		else if (rotation == Dir2_Right)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(-90))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y + drawRec.height, rc->depth)); //Position
		}
		else if (rotation == Dir2_Up)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(180))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y + drawRec.height, rc->depth)); //Position
		}
		else
		{
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y, rc->depth)); //Position
		}
		RcSetWorldMatrix(worldMatrix);
		
		RcBindBuffer(&rc->squareBuffer);
		RcDrawBufferTriangles();
		RcDebugIncrement(numSheetFrameDraws);
	}
}
void RcDrawSheetFrameObb2(const SpriteSheet_t* spriteSheet, v2i frame, obb2 drawBox, Color_t color)
{
	rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
	RcBindTexture(&spriteSheet->texture);
	RcDrawTexturedObb2(drawBox, color, sourceRec);
}
void RcDrawPartialSheetFrame(const SpriteSheet_t* spriteSheet, v2i frame, rec drawRec, rec partRec, Color_t color, Dir2_t rotation = Dir2_Down, bool flippedX = false, bool flippedY = false)
{
	//TODO: Only try to batch if this frame isn't being drawn with special settings like stenciling, and a special shader
	if (!RcBatchSheetFrame(spriteSheet, drawRec, color, frame, rotation, flippedX, flippedY))
	{
		RcBindTexture(&spriteSheet->texture);
		rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
		v2 cellSize = sourceRec.size;
		u8 numTurnsClockwise = GetDir2NumTurnsClockwise(Dir2_Down, rotation);
		// sourceRec = RecExtend(sourceRec, Dir2CounterClockwise(Dir2_Left, numTurnsClockwise), -(partRec.x * cellSize.width));
		// sourceRec = RecExtend(sourceRec, Dir2CounterClockwise(Dir2_Up, numTurnsClockwise), -(partRec.y * cellSize.height));
		sourceRec.topLeft += NewVec2(Dir2CounterClockwise(Dir2_Right, numTurnsClockwise)) * partRec.x * cellSize.width;
		sourceRec = RecExtend(sourceRec, Dir2CounterClockwise(Dir2_Right, numTurnsClockwise), -((1.0f - partRec.width) * cellSize.width));
		sourceRec = RecExtend(sourceRec, Dir2CounterClockwise(Dir2_Down, numTurnsClockwise), -((1.0f - partRec.height) * cellSize.height));
		// sourceRec.x += partRec.x * (r32)spriteSheet->frameX;
		// sourceRec.y += partRec.y * (r32)spriteSheet->frameY;
		// sourceRec.width *= partRec.width;
		// sourceRec.height *= partRec.height;
		RcSetSourceRectangle(sourceRec);
		RcSetColor(color);
		
		mat4 worldMatrix = Mat4_Identity;
		if (flippedX)
		{
			Mat4Transform(worldMatrix, Mat4Scale2(-1, 1));
			Mat4Transform(worldMatrix, Mat4Translate2(1, 0));
		}
		if (flippedY)
		{
			Mat4Transform(worldMatrix, Mat4Scale2(1, -1));
			Mat4Transform(worldMatrix, Mat4Translate2(0, 1));
		}
		if (rotation == Dir2_Left)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(90))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y, rc->depth)); //Position
		}
		else if (rotation == Dir2_Right)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(-90))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y + drawRec.height, rc->depth)); //Position
		}
		else if (rotation == Dir2_Up)
		{
			Mat4Transform(worldMatrix, Mat4RotateZ(ToRadians(180))); //Rotation
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y + drawRec.height, rc->depth)); //Position
		}
		else
		{
			Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height)); //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y, rc->depth)); //Position
		}
		RcSetWorldMatrix(worldMatrix);
		
		RcBindBuffer(&rc->squareBuffer);
		RcDrawBufferTriangles();
		RcDebugIncrement(numSheetFrameDraws);
	}
}

void RcDrawMultiSheetFrames(const SpriteSheet_t* spriteSheet, v2i topLeftFrame, v2i numFrames, rec drawRec, Color_t color, Dir2_t rotation = Dir2_Down, bool flippedX = false, bool flippedY = false)
{
	Assert(spriteSheet != nullptr);
	v2 topLeft = drawRec.topLeft;
	v2 rightVec = Vec2_Right;
	v2 downVec = Vec2_Down;
	v2 frameSize = Vec2Divide(drawRec.size, NewVec2(numFrames));
	if (flippedX && flippedY)
	{
		rotation = Dir2Opposite(rotation);
		flippedX = false;
		flippedY = false;
	}
	if (rotation == Dir2_Left)
	{
		topLeft += rightVec * (drawRec.width - frameSize.width);
		v2 temp = rightVec;
		rightVec = downVec;
		downVec = -temp;
	}
	else if (rotation == Dir2_Up)
	{
		topLeft += rightVec * (drawRec.width - frameSize.width);
		topLeft += downVec * (drawRec.height - frameSize.height);
		rightVec = -rightVec;
		downVec = -downVec;
	}
	else if (rotation == Dir2_Right)
	{
		topLeft += downVec * (drawRec.height - frameSize.height);
		v2 temp = rightVec;
		rightVec = -downVec;
		downVec = temp;
	}
	if (flippedX)
	{
		topLeft += rightVec * (drawRec.width - frameSize.width);
		rightVec = -rightVec;
	}
	else if (flippedY)
	{
		topLeft += downVec * (drawRec.height - frameSize.height);
		downVec = -downVec;
	}
	for (i32 yOffset = 0; yOffset < numFrames.y; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < numFrames.x; xOffset++)
		{
			rec frameRec = NewRec(topLeft + rightVec*frameSize.x*(r32)xOffset + downVec*frameSize.y*(r32)yOffset, frameSize);
			RcDrawSheetFrame(spriteSheet, topLeftFrame + NewVec2i(xOffset, yOffset), frameRec, color, rotation, flippedX, flippedY);
		}
	}
}

void RcDrawSprite(const Sprite_t* spritePntr, rec drawRec)
{
	Assert(spritePntr != nullptr);
	Assert(spritePntr->sheet != nullptr);
	RcDrawSheetFrame(spritePntr->sheet, spritePntr->frame, drawRec, spritePntr->drawColor, spritePntr->rotation);
}
void RcDrawSprite(const SpriteEx_t* spritePntr, rec drawRec)
{
	Assert(spritePntr != nullptr);
	Assert(spritePntr->sheet != nullptr);
	RcSetReplaceColors(spritePntr->palColor);
	rec oldMaskRec = rc->maskRectangle;
	if (spritePntr->maskFrame != Vec2i_Zero) { RcSetMaskSheetFrame(spritePntr->sheet, spritePntr->maskFrame); }
	RcDrawSheetFrame(spritePntr->sheet, spritePntr->frame, drawRec, spritePntr->drawColor, spritePntr->rotation, spritePntr->flippedX, spritePntr->flippedY);
	if (spritePntr->maskFrame != Vec2i_Zero) { RcSetMaskRectangle(oldMaskRec); }
}

// +--------------------------------------------------------------+
// |                Bezier Curve Render Functions                 |
// +--------------------------------------------------------------+
void RcDrawBezierCurve3(v2 start, v2 control, v2 end, Color_t color, u32 numVerts,
	r32 thickness = 1.0f, bool drawCircles = true, bool roundedEnds = false)
{
	for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
	{
		r32 t1 = (r32)vIndex * (1.0f/numVerts);
		r32 t2 = (r32)(vIndex+1) * (1.0f/numVerts);
		
		r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
		r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
		v2 pos1 = NewVec2(xPos1, yPos1);
		
		r32 xPos2 = ((1 - t2)*(1 - t2)*start.x) + (2*t2*(1 - t2)*control.x) + t2*t2*end.x;
		r32 yPos2 = ((1 - t2)*(1 - t2)*start.y) + (2*t2*(1 - t2)*control.y) + t2*t2*end.y;
		v2 pos2 = NewVec2(xPos2, yPos2);
		
		if (drawCircles)
		{
			if (vIndex == 0 && roundedEnds)
			{
				RcDrawCircle(pos1, thickness/2, color);
			}
			if (vIndex+1 < numVerts || roundedEnds)
			{
				RcDrawCircle(pos2, thickness/2, color);
			}
		}
		RcDrawLine(pos1, pos2, thickness, color);
	}
}

void RcDrawBezierArrow3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	
	RcDrawTriangle(end + normal*(arrowSize/2), end - pos1, arrowSize, color);
}

void RcDrawBezierArrowBordered3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	
	RcDrawTriangleBordered(end + normal*(arrowSize/2), end - pos1, arrowSize, color, borderColor, borderWidth);
}

void RcDrawBezierCurve4(v2 start, v2 control1, v2 control2, v2 end, Color_t color, u32 numVerts, r32 thickness = 1.0f, bool drawCircles = true, bool roundedEnds = false)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierCurve3(start, control1, midPoint, color, numVerts, thickness, drawCircles, roundedEnds);
	RcDrawBezierCurve3(midPoint, control2, end, color, numVerts, thickness, drawCircles, roundedEnds);
}

void RcDrawBezierArrow4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrow3(midPoint, control2, end, numVerts, arrowSize, color);
}

void RcDrawBezierArrowBordered4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrowBordered3(midPoint, control2, end, numVerts, arrowSize, color, borderColor, borderWidth);
}

void RcDrawConnectionArc(v2 p1, v2 p2, v2 arcUp, r32 arcCoefficient, u32 numVerts, r32 thickness, Color_t color)
{
	if (p1 == p2) { return; }
	v2 perp = Vec2PerpLeft(p2 - p1);
	if (Vec2Dot(perp, arcUp) < 0) { perp = -perp; }
	r32 arcLength = Vec2Length(p2 - p1);
	perp = perp / arcLength;
	r32 arcHeight = arcLength * arcCoefficient;
	RcDrawBezierCurve3(p1, p1 + (p2 - p1)/2 + perp*arcHeight, p2, color, numVerts, thickness, false, false);
}

// +--------------------------------------------------------------+
// |                  String Rendering Functions                  |
// +--------------------------------------------------------------+
void RcDrawCharacter(u32 charIndex, v2 bottomLeft, Color_t color, r32 scale = 1.0f)
{
	Assert(rc->boundFont != nullptr);
	Assert(rc->boundFont->charInfos != nullptr);
	const FontCharInfo_t* charInfo = &rc->boundFont->charInfos[charIndex];
	rec sourceRectangle = NewRec((r32)charInfo->x, (r32)charInfo->y, (r32)charInfo->width, (r32)charInfo->height);
	rec drawRectangle = NewRec(
		bottomLeft.x + scale*rc->fontScale*charInfo->offset.x, 
		bottomLeft.y + scale*rc->fontScale*charInfo->offset.y, 
		scale*rc->fontScale*charInfo->width, 
		scale*rc->fontScale*charInfo->height
	);
	bool batched = RcBatchGlyph(&rc->boundFont->bitmap, drawRectangle.topLeft, color, sourceRectangle, scale*rc->fontScale);
	if (!batched)
	{
		// RcDrawRectangle(drawRectangle, ColorComplimentary(color));
		if (rc->boundTexture != &rc->boundFont->bitmap) { RcBindTexture(&rc->boundFont->bitmap); }
		RcDrawTexturedRec(drawRectangle, color, sourceRectangle);
		RcDebugIncrement(numDrawnGlyphs);
	}
}

void RcDrawString(const char* string, u32 numCharacters, v2 position, Color_t color, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left)
{
	Assert(rc->boundFont != nullptr);
	rc->lastTextLength = numCharacters;
	rc->lastTextNumLines = 1;
	rc->lastTextSize = Vec2_Zero;
	rc->lastTextStartPos = position;
	rc->lastTextEndPos = position;
	if (rc->boundFont->numChars == 0) { return; } //can't draw anything with an invalid font
	Assert(rc->boundFont->charInfos != nullptr);
	
	v2 stringSize = MeasureString(rc->boundFont, string, numCharacters);
	rc->lastTextSize = stringSize;
	v2 currentPos = position;
	switch (alignment)
	{
		case Alignment_Center: currentPos.x -= stringSize.x/2 * scale * rc->fontScale; break;
		case Alignment_Right:  currentPos.x -= stringSize.x * scale * rc->fontScale; break;
		case Alignment_Left:   break;
	};
	if (rc->strPosSnapEnabled) { currentPos = Vec2Round(currentPos / (/*rc->fontScale * */scale)) * (/*rc->fontScale * */scale); }
	rc->lastTextStartPos = currentPos;
	
	for (u32 cIndex = 0; cIndex < numCharacters; cIndex++)
	{
		if (string[cIndex] == '\t')
		{
			u32 spaceIndex = GetFontCharIndex(rc->boundFont, ' ');
			currentPos.x += rc->boundFont->charInfos[spaceIndex].advanceX * TAB_WIDTH * scale * rc->fontScale;
		}
		else if ((!rc->boundFont->isBitmapFont && !IsCharClassPrintable(string[cIndex])) || (rc->boundFont->isBitmapFont && !IsCharClassASCIIPrintable(string[cIndex])))
		{
			//Don't do anything
		}
		else
		{
			u32 charIndex = GetFontCharIndex(rc->boundFont, string[cIndex]);
			RcDrawCharacter(charIndex, currentPos, color, scale);
			currentPos.x += rc->boundFont->charInfos[charIndex].advanceX * scale * rc->fontScale;
		}
	}
	
	rc->lastTextEndPos = currentPos;
}

void RcDrawStringNt(const char* nullTermString, v2 position, Color_t color, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left)
{
	RcDrawString(nullTermString, MyStrLength32(nullTermString), position, color, scale, alignment);
}

void RcDrawStringEmbossed(const char* strPntr, u32 strLength, v2 position, Color_t color, Color_t embossColor, r32 embossOffset = 1.0f, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left)
{
	RcDrawString(strPntr, strLength, position + NewVec2(0, embossOffset*scale*rc->fontScale), embossColor, scale, alignment);
	RcDrawString(strPntr, strLength, position, color, scale, alignment);
}

void RcDrawStringEmbossedNt(const char* nullTermString, v2 position, Color_t color, Color_t embossColor, r32 embossOffset = 1.0f, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left)
{
	RcDrawStringEmbossed(nullTermString, MyStrLength32(nullTermString), position, color, embossColor, embossOffset, scale, alignment);
}

void RcPrintString(v2 position, Color_t color, r32 scale, const char* formatString, ...)
{
	char printBuffer[256] = {};
	va_list args;
	
	va_start(args, formatString);
	u32 length = (u32)vsnprintf(printBuffer, 256-1, formatString, args);
	va_end(args);
	
	RcDrawString(printBuffer, length, position, color, scale);
}
void RcPrintStringAligned(v2 position, Color_t color, r32 scale, Alignment_t alignment, const char* formatString, ...)
{
	char printBuffer[256] = {};
	va_list args;
	
	va_start(args, formatString);
	u32 length = (u32)vsnprintf(printBuffer, 256-1, formatString, args);
	va_end(args);
	
	RcDrawString(printBuffer, length, position, color, scale, alignment);
}

void RcPrintStringEmbossed(v2 position, Color_t color, Color_t embossColor, r32 scale, r32 embossOffset, const char* formatString, ...)
{
	char printBuffer[256] = {};
	va_list args;
	
	va_start(args, formatString);
	u32 length = (u32)vsnprintf(printBuffer, 256-1, formatString, args);
	va_end(args);
	
	RcDrawString(printBuffer, length, position + NewVec2(0, embossOffset*scale*rc->fontScale), embossColor, scale);
	RcDrawString(printBuffer, length, position, color, scale);
}
void RcPrintStringEmbossedAligned(v2 position, Color_t color, Color_t embossColor, r32 scale, r32 embossOffset, Alignment_t alignment, const char* formatString, ...)
{
	char printBuffer[256] = {};
	va_list args;
	
	va_start(args, formatString);
	u32 length = (u32)vsnprintf(printBuffer, 256-1, formatString, args);
	va_end(args);
	
	RcDrawString(printBuffer, length, position + NewVec2(0, embossOffset*scale*rc->fontScale), embossColor, scale, alignment);
	RcDrawString(printBuffer, length, position, color, scale, alignment);
}

void RcDrawFormattedString(const char* string, u32 numCharacters, v2 position, r32 maxWidth, Color_t color, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left, bool preserveWords = true)
{
	u32 cIndex = 0;
	v2 drawPos = position;
	while (cIndex < numCharacters)
	{
		u32 numChars = FindNextFormatChunk(rc->boundFont, &string[cIndex], numCharacters - cIndex, maxWidth/(scale*rc->fontScale), preserveWords);
		if (numChars == 0) { numChars = 1; }
		
		while (numChars > 1 && IsCharClassWhitespace(string[cIndex + numChars-1]))
		{
			numChars--;
		}
		
		RcDrawString(&string[cIndex], numChars, drawPos, color, scale, alignment);
		
		if (cIndex+numChars < numCharacters && string[cIndex+numChars] == '\r')
		{
			numChars++;
		}
		if (cIndex+numChars < numCharacters && string[cIndex+numChars] == '\n')
		{
			numChars++;
		}
		while (cIndex+numChars < numCharacters && string[cIndex+numChars] == ' ')
		{
			numChars++;
		}
		drawPos.y += rc->boundFont->lineHeight * scale * rc->fontScale;
		cIndex += numChars;
	}
}

void RcDrawFormattedString(const char* nullTermString, v2 position, r32 maxWidth, Color_t color, r32 scale = 1.0f, Alignment_t alignment = Alignment_Left, bool preserveWords = true)
{
	u32 numCharacters = MyStrLength32(nullTermString);
	RcDrawFormattedString(nullTermString, numCharacters, position, maxWidth, color, scale, alignment, preserveWords);
}

// +--------------------------------------------------------------+
// |                         Other Stuff                          |
// +--------------------------------------------------------------+
void RcDrawLoadingBar(rec barRec, r32 fillPercent, Color_t fillColor, Color_t backgroundColor, bool drawPercentageString = true)
{
	RcDrawRectangle(barRec, backgroundColor);
	
	rec fillRec = barRec;
	fillRec.width *= fillPercent;
	if (fillPercent < 0) { fillRec.x += barRec.width; }
	RcDrawRectangle(fillRec, fillColor);
	rec divideRec = fillRec;
	divideRec.x += divideRec.width;
	divideRec.width = 1;
	RcDrawRectangle(divideRec, Black);
	
	char* percentString = ArenaPrint(TempArena, "%.0f%%", RoundR32(fillPercent*100));
	RcDrawStringNt(percentString, barRec.topLeft + NewVec2(barRec.width+2, barRec.height), White, 1.0f, Alignment_Left);
}

void RcDrawTiledUiRec(SpriteSheet_t* spriteSheet, v2i baseFrame, rec drawRec, Color_t color, r32 scale = 1.0f)
{
	Assert(spriteSheet != nullptr);
	
	v2 tileSize = NewVec2(spriteSheet->innerFrameSize) * scale;
	v2 centerSize = NewVec2(drawRec.width - tileSize.width*2, drawRec.height - tileSize.height*2);
	
	for (r32 yOffset = 0; yOffset < centerSize.height; yOffset += tileSize.height)
	{
		for (r32 xOffset = 0; xOffset < centerSize.width; xOffset += tileSize.width)
		{
			rec tileRec = NewRec(drawRec.x + tileSize.width + xOffset, drawRec.y + tileSize.height + yOffset, tileSize.width, tileSize.height);
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 1), tileRec, color);
		}
	}
	
	for (r32 xOffset = 0; xOffset < centerSize.width; xOffset += tileSize.width)
	{
		rec topRec = NewRec(drawRec.x + tileSize.width + xOffset, drawRec.y, tileSize.width, tileSize.height);
		rec bottomRec = NewRec(drawRec.x + tileSize.width + xOffset, drawRec.y + (drawRec.height - tileSize.height), tileSize.width, tileSize.height);
		RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 0), topRec, color);
		RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 2), bottomRec, color);
	}
	
	for (r32 yOffset = 0; yOffset < centerSize.height; yOffset += tileSize.height)
	{
		rec leftRec = NewRec(drawRec.x, drawRec.y + tileSize.height + yOffset, tileSize.width, tileSize.height);
		rec rightRec = NewRec(drawRec.x + (drawRec.width - tileSize.width), drawRec.y + tileSize.height + yOffset, tileSize.width, tileSize.height);
		RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 1), leftRec, color);
		RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 1), rightRec, color);
	}
	
	rec topLeftRec = NewRec(drawRec.x, drawRec.y, tileSize.width, tileSize.height);
	rec topRightRec = NewRec(drawRec.x + (drawRec.width - tileSize.width), drawRec.y, tileSize.width, tileSize.height);
	rec bottomLeftRec = NewRec(drawRec.x, drawRec.y + (drawRec.height - tileSize.height), tileSize.width, tileSize.height);
	rec bottomRightRec = NewRec(drawRec.x + (drawRec.width - tileSize.width), drawRec.y + (drawRec.height - tileSize.height), tileSize.width, tileSize.height);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 0), topLeftRec,     color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 0), topRightRec,    color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 2), bottomLeftRec,  color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 2), bottomRightRec, color);
}

void RcDrawTiledUiOutline(SpriteSheet_t* spriteSheet, v2i baseFrame, rec drawRec, Color_t color, r32 scale = 1.0f)
{
	Assert(spriteSheet != nullptr);
	
	v2 tileSize = NewVec2(spriteSheet->innerFrameSize) * scale;
	v2 centerSize = NewVec2(drawRec.width, drawRec.height);
	
	for (r32 xOffset = 0; xOffset < centerSize.width; xOffset += tileSize.width)
	{
		rec topRec = NewRec(drawRec.x + xOffset, drawRec.y - tileSize.height, tileSize.width, tileSize.height);
		rec bottomRec = NewRec(drawRec.x + xOffset, drawRec.y + drawRec.height, tileSize.width, tileSize.height);
		if (centerSize.width - xOffset < tileSize.width)
		{
			r32 actualWidth = RoundR32((centerSize.width - xOffset)/scale) * scale;
			rec partialTopRec = NewRec(topRec.x, topRec.y, actualWidth, topRec.height);
			rec partialBottomRec = NewRec(bottomRec.x, bottomRec.y, actualWidth, bottomRec.height);
			rec tileSrcRec = NewRec(0, 0, (actualWidth / tileSize.width), 1.0f);
			RcDrawPartialSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 0), partialTopRec, tileSrcRec, color);
			RcDrawPartialSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 2), partialBottomRec, tileSrcRec, color);
		}
		else
		{
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 0), topRec, color);
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 2), bottomRec, color);
		}
	}
	
	for (r32 yOffset = 0; yOffset < centerSize.height; yOffset += tileSize.height)
	{
		rec leftRec = NewRec(drawRec.x - tileSize.width, drawRec.y + yOffset, tileSize.width, tileSize.height);
		rec rightRec = NewRec(drawRec.x + drawRec.width, drawRec.y + yOffset, tileSize.width, tileSize.height);
		if (centerSize.height - yOffset < tileSize.height)
		{
			r32 actualHeight = RoundR32((centerSize.height - yOffset)/scale) * scale;
			rec partialLeftRec = NewRec(leftRec.x, leftRec.y, leftRec.width, actualHeight);
			rec partialRightRec = NewRec(rightRec.x, rightRec.y, rightRec.width, actualHeight);
			rec tileSrcRec = NewRec(0, 0, 1.0f, (actualHeight / tileSize.height));
			RcDrawPartialSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 1), partialLeftRec, tileSrcRec, color);
			RcDrawPartialSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 1), partialRightRec, tileSrcRec, color);
		}
		else
		{
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 1), leftRec, color);
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 1), rightRec, color);
		}
	}
	
	rec topLeftRec = NewRec(drawRec.x - tileSize.width, drawRec.y - tileSize.height, tileSize.width, tileSize.height);
	rec topRightRec = NewRec(drawRec.x + drawRec.width, drawRec.y - tileSize.height, tileSize.width, tileSize.height);
	rec bottomLeftRec = NewRec(drawRec.x - tileSize.width, drawRec.y + drawRec.height, tileSize.width, tileSize.height);
	rec bottomRightRec = NewRec(drawRec.x + drawRec.width, drawRec.y + drawRec.height, tileSize.width, tileSize.height);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 0), topLeftRec,     color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 0), topRightRec,    color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 2), bottomLeftRec,  color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 2), bottomRightRec, color);
}

#if 0
void RcDrawTiledUiButton(SpriteSheet_t* spriteSheet, v2i baseFrame, rec drawRec, Color_t color, r32 scale = 1.0f, rec* viewportOut = nullptr)
{
	Assert(spriteSheet != nullptr);
	
	v2 tileSize = NewVec2(spriteSheet->innerFrameSize) * scale;
	r32 centerSize = drawRec.width - tileSize.width*2;
	
	for (r32 xOffset = 0; xOffset < centerSize; xOffset += tileSize.width)
	{
		rec centerRec = NewRec(drawRec.x + tileSize.x + xOffset, drawRec.y + drawRec.height/2 - tileSize.height/2, tileSize.width, tileSize.height);
		if (centerSize - xOffset < tileSize.width)
		{
			r32 actualWidth = RoundR32((centerSize - xOffset)/scale) * scale;
			rec partialRec = NewRec(centerRec.x, centerRec.y, actualWidth, centerRec.height);
			rec tileSrcRec = NewRec(0, 0, (actualWidth / tileSize.width), 1.0f);
			RcDrawPartialSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 0), partialRec, tileSrcRec, color);
		}
		else
		{
			RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(1, 0), centerRec, color);
		}
	}
	
	rec leftCapRec = NewRec(drawRec.x, drawRec.y + drawRec.height/2 - tileSize.height/2, tileSize.width, tileSize.height);
	rec rightCapRec = NewRec(drawRec.x + drawRec.width - tileSize.width, drawRec.y + drawRec.height/2 - tileSize.height/2, tileSize.width, tileSize.height);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(0, 0), leftCapRec,  color);
	RcDrawSheetFrame(spriteSheet, baseFrame + NewVec2i(2, 0), rightCapRec, color);
	
	if (viewportOut != nullptr)
	{
		*viewportOut = drawRec;
		*viewportOut = RecDeflateX(*viewportOut, NMM_BTN_EDGE_LR_SIZE * scale);
		*viewportOut = RecDeflateY(*viewportOut, NMM_BTN_EDGE_TB_SIZE * scale);
		*viewportOut = RecExtend(*viewportOut, Dir2_Left, -NMM_BTN_WATERMARK_SIZE * scale);
	}
}
#endif

