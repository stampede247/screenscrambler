/*
File:   app_gif.cpp
Author: Taylor Robbins
Date:   09\27\2019
Description: 
	** Handles recording footage and encoding it into a .gif file
*/

bool GifIsColorInPalette(u32 numPaletteColors, const u32* paletteColors, u32 color)
{
	Assert(paletteColors != nullptr);
	for (u32 cIndex = 0; cIndex < numPaletteColors; cIndex++)
	{
		if ((paletteColors[cIndex] & 0xFFFFFF) == (color & 0xFFFFFF)) { return true; }
	}
	return false;
}
u8 GifGetPaletteIndexForColor(u32 numPaletteColors, const u32* paletteColors, u32 color)
{
	Assert(paletteColors != nullptr);
	Assert(numPaletteColors <= 256);
	u8 red   = (u8)((color>>16) & 0xFF);
	u8 green = (u8)((color>>8)  & 0xFF);
	u8 blue  = (u8)((color>>0)  & 0xFF);
	u8 resultIndex = 0;
	i32 resultCloseness = 255+255+255;
	for (u32 cIndex = 0; cIndex < numPaletteColors; cIndex++)
	{
		if ((paletteColors[cIndex] & 0xFFFFFF) == (color & 0xFFFFFF)) { return (u8)cIndex; }
		else
		{
			u8 palRed   = (u8)((paletteColors[cIndex]>>16) & 0xFF);
			u8 palGreen = (u8)((paletteColors[cIndex]>>8)  & 0xFF);
			u8 palBlue  = (u8)((paletteColors[cIndex]>>0)  & 0xFF);
			i32 closeness = AbsI32((i32)palRed - red) + AbsI32((i32)palGreen - green) + AbsI32((i32)palBlue - blue);
			if (closeness < resultCloseness)
			{
				resultIndex = (u8)cIndex;
				resultCloseness = closeness;
			}
		}
	}
	return resultIndex;
}

void GifPrint(GifStream_t* stream, const char* formatStr, ...)
{
	Assert(stream != nullptr);
	Assert(stream->isActive);
	TempPushMark();
	TempPrintVa(dataStr, dataLength, formatStr);
	if (dataLength > 0)
	{
		Assert(dataStr != nullptr);
		// Write_D("{");
		// for (u32 cIndex = 0; cIndex < dataLength; cIndex++)
		// {
		// 	Print_D(" %02X", dataStr[cIndex]);
		// }
		// WriteLine_D(" }");
		if (!platform->WriteToFile(&stream->file, dataStr, dataLength))
		{
			stream->encodedSuccessfully = false;
		}
	}
	TempPopMark();
}
void GifBytes(GifStream_t* stream, const u8* dataPntr, u32 dataLength)
{
	Assert(stream != nullptr);
	Assert(stream->isActive);
	Assert(dataPntr != nullptr || dataLength == 0);
	if (dataLength == 0) { return; }
	if (!platform->WriteToFile(&stream->file, dataPntr, dataLength))
	{
		stream->encodedSuccessfully = false;
	}
}
void GifU8(GifStream_t* stream, u8 value) //pre-declared in app_gif.h
{
	Assert(stream != nullptr);
	Assert(stream->isActive);
	// PrintLine_D("{%02X}", value);
	if (!platform->WriteToFile(&stream->file, &value, 1))
	{
		stream->encodedSuccessfully = false;
	}
}
void GifU16(GifStream_t* stream, u16 value)
{
	Assert(stream != nullptr);
	Assert(stream->isActive);
	u8 values[2] = { (u8)((value >> 0) & 0xFF), (u8)((value >> 8) & 0xFF) };
	// PrintLine_D("{%02X}{%02X}", values[0], values[1]);
	if (!platform->WriteToFile(&stream->file, &values[0], 2))
	{
		stream->encodedSuccessfully = false;
	}
}

void EncodeGifStream(GifStream_t* stream)
{
	PrintLine_Nx(DbgFlag_Inverted, "Encoding %dx%d gif with %u frames (at most %s)", stream->width, stream->height, stream->numFrames, FormattedSizeStr(APPROX_GIF_OVERHEAD + stream->width * stream->height * stream->numFrames));
	
	TempPushMark();
	bool tooManyColors = false;
	u32 numPaletteColors = 0;
	u32* paletteColors = PushArray(TempArena, u32, 256);
	Assert(paletteColors != nullptr);
	paletteColors[numPaletteColors] = Black_Value; numPaletteColors++;
	paletteColors[numPaletteColors] = White_Value; numPaletteColors++;
	
	//Find Palette Colors
	u32 fIndex = 0;
	GifFrame_t* frame = stream->firstFrame;
	while (frame != nullptr)
	{
		Assert(fIndex < stream->numFrames);
		for (u32 yPos = 0; yPos < (u32)frame->changedRec.height; yPos++)
		{
			for (u32 xPos = 0; xPos < (u32)frame->changedRec.width; xPos++)
			{
				u32 valuesIndex = (xPos + yPos * frame->changedRec.width) * 3;
				Assert(valuesIndex < frame->pixelDataSize);
				u8 red   = frame->pixelData[valuesIndex + 0];
				u8 green = frame->pixelData[valuesIndex + 1];
				u8 blue  = frame->pixelData[valuesIndex + 2];
				u32 newColor = ((u32)red<<16) | ((u32)green<<8) | ((u32)blue<<0);
				if (!GifIsColorInPalette(numPaletteColors, paletteColors, newColor))
				{
					u8 closestColorIndex = GifGetPaletteIndexForColor(numPaletteColors, paletteColors, newColor);
					u32 closestColor = paletteColors[closestColorIndex];
					u8 palRed   = (u8)((closestColor>>16) & 0xFF);
					u8 palGreen = (u8)((closestColor>>8)  & 0xFF);
					u8 palBlue  = (u8)((closestColor>>0)  & 0xFF);
					i32 closeness = AbsI32((i32)palRed - red) + AbsI32((i32)palGreen - green) + AbsI32((i32)palBlue - blue);
					if (closeness > 15)
					{
						if (numPaletteColors < 256)
						{
							paletteColors[numPaletteColors] = newColor;
							numPaletteColors++;
						}
						else { tooManyColors = true; }
					}
				}
			}
		}
		frame = frame->next;
		fIndex++;
	}
	PrintLine_D("Chose %u palette colors...", numPaletteColors);
	if (tooManyColors) { Notify_W("Warning: Too many colors present for gif color palette"); }
	
	// Gif Signature
	GifPrint(stream, GIF_SIGNATURE_STRING);
	
	//Screen Descriptor
	bool useGlobalColorMap = true;
	u8 colorResolution = 2; //bits
	u8 pixelWidth = 2; //bits
	u8 backgroundColorIndex = 1;
	u32 paddedPaletteColorCount = 4;
	while (paddedPaletteColorCount < numPaletteColors && paddedPaletteColorCount < 256)
	{
		paddedPaletteColorCount *= 2;
		colorResolution++;
		pixelWidth++;
	}
	PrintLine_D("pixelWidth is %u", pixelWidth);
	if (paddedPaletteColorCount != numPaletteColors) { PrintLine_D("Padding color palette to %u colors", paddedPaletteColorCount); }
	GifU16(stream, (u16)stream->width);
	GifU16(stream, (u16)stream->height);
	u8 colorInfoByte = 0x00;
	if (useGlobalColorMap) { FlagSet(colorInfoByte, 0x80); }
	colorInfoByte |= (u8)(((colorResolution-1) << 4) & 0x70);
	colorInfoByte |= (u8)(((pixelWidth-1) << 0) & 0x07);
	GifU8(stream, colorInfoByte);
	GifU8(stream, backgroundColorIndex);
	GifU8(stream, 0x00); //block terminator
	
	//Global Color Palette
	for (u32 cIndex = 0; cIndex < paddedPaletteColorCount; cIndex++)
	{
		u32 color = (cIndex < numPaletteColors) ? paletteColors[cIndex] : 0x00000000;
		GifU8(stream, (u8)((color>>16) & 0xFF)); //r
		GifU8(stream, (u8)((color>>8)  & 0xFF)); //g
		GifU8(stream, (u8)((color>>0)  & 0xFF)); //b
	}
	
	//Application Extension Block
	u16 repetitionCount = GIF_REP_COUNT_INFINITE;
	GifU8(stream, GIF_EXTENSION_DELIM_CHAR);
	GifU8(stream, GIF_APP_EXT_BLOCK_LABEL);
	GifU8(stream, (u8)MyStrLength(GIF_APP_EXT_BLOCK_STR));
	GifPrint(stream, "%s", GIF_APP_EXT_BLOCK_STR);
	GifU8(stream, 3); //bytes
	GifU8(stream, 0x01); //required by spec
	GifU16(stream, repetitionCount);
	GifU8(stream, 0x00); //block terminator
	
	//insert comment extension block
	#if 1
	u32 commentLength = MyStrLength32(GIF_EXT_COMMENT_STRING);
	Assert(commentLength <= 255);
	PrintLine_D("Inserting %u character comment...", commentLength);
	GifU8(stream, GIF_EXTENSION_DELIM_CHAR);
	GifU8(stream, GIF_EXT_COMMENT_COMMAND);
	GifU8(stream, (u8)commentLength);
	GifPrint(stream, GIF_EXT_COMMENT_STRING);
	GifU8(stream, 0x00); //block terminator
	#endif
	
	WriteLine_D("Wrote header...");
	
	DynArray_t compressionBuffer;
	CreateDynamicArray(&compressionBuffer, TempArena, sizeof(u8), 256, (u32)stream->width * (u32)stream->height);
	
	//Frame Data
	fIndex = 0;
	frame = stream->firstFrame;
	while (frame != nullptr)
	{
		Assert(fIndex < stream->numFrames);
		
		if (frame->changedRec.width > 0 || frame->changedRec.height > 0)
		{
			u32 numUnchangedFramesComing = 0;
			GifFrame_t* nextFrame = frame->next;
			while (nextFrame != nullptr && nextFrame->changedRec.width == 0 && nextFrame->changedRec.height == 0) { numUnchangedFramesComing++; nextFrame = nextFrame->next; }
			
			//Graphics Control Extension Block
			bool transparentColorEnable = false;
			u8 transparentColorIndex = 0xFF;
			bool userInputEnable = false;
			u8 disposalMethod = GIF_DISPOSAL_METHOD_KEEP;
			u32 delayTime = GIF_FRAME_TIME * (1+numUnchangedFramesComing);
			GifU8(stream, GIF_EXTENSION_DELIM_CHAR);
			GifU8(stream, GIF_GRAPHIC_CONTROL_LABEL);
			GifU8(stream, 4); //bytes
			u8 graphicInfo = 0x00;
			if (transparentColorEnable) { FlagSet(graphicInfo, 0x01); }
			if (userInputEnable) { FlagSet(graphicInfo, 0x02); }
			graphicInfo |= (u8)((disposalMethod << 2) & 0x1C);
			// PrintLine_D("Graphic info[%u] = 0x%02X", fIndex, graphicInfo);
			GifU8(stream, graphicInfo);
			GifU16(stream, (u16)(delayTime / 10));
			GifU8(stream, transparentColorIndex);
			GifU8(stream, 0x00); //block terminator
			
			// Image Descriptor
			GifU8(stream, GIF_IMAGE_DELIM_CHAR);
			GifU16(stream, (u16)frame->changedRec.x);
			GifU16(stream, (u16)frame->changedRec.y);
			GifU16(stream, (u16)frame->changedRec.width);
			GifU16(stream, (u16)frame->changedRec.height);
			
			bool isInterlaced = false;
			colorInfoByte = 0x00;
			if (!useGlobalColorMap) { FlagSet(colorInfoByte, 0x80); }
			if (isInterlaced) { FlagSet(colorInfoByte, 0x40); }
			if (!useGlobalColorMap) { colorInfoByte |= (u8)(((pixelWidth-1) << 0) & 0x07); }
			GifU8(stream, colorInfoByte);
			
			// Turn the pixelData into palette colors
			u8* indexOutPntr = frame->pixelData;
			for (u32 yPos = 0; yPos < (u32)frame->changedRec.height; yPos++)
			{
				for (u32 xPos = 0; xPos < (u32)frame->changedRec.width; xPos++)
				{
					u32 valuesIndex = (xPos + yPos * frame->changedRec.width) * 3;
					Assert(valuesIndex < frame->pixelDataSize);
					u8 red   = frame->pixelData[valuesIndex + 0];
					u8 green = frame->pixelData[valuesIndex + 1];
					u8 blue  = frame->pixelData[valuesIndex + 2];
					u32 newColor = ((u32)red<<16) | ((u32)green<<8) | ((u32)blue<<0);
					u8 paletteIndex = GifGetPaletteIndexForColor(numPaletteColors, paletteColors, newColor);
					Assert(indexOutPntr < frame->pixelData + frame->pixelDataSize);
					*indexOutPntr = paletteIndex;
					indexOutPntr++;
				}
			}
			
			compressionBuffer.length = 0;
			LzwEncodeBuffer(&compressionBuffer, &app->platHeap, pixelWidth, frame->pixelData, (u32)frame->changedRec.width * (u32)frame->changedRec.height);
			PrintLine_D("Compressed frame from %s to %s", FormattedSizeStr(frame->changedRec.width * frame->changedRec.height), FormattedSizeStr(compressionBuffer.length));
			
			GifU8(stream, pixelWidth);
			for (u32 bIndex = 0; bIndex < compressionBuffer.length; bIndex += 255)
			{
				u8* bytesPntr = DynArrayGet(&compressionBuffer, u8, bIndex);
				Assert(bytesPntr != nullptr);
				u32 numBytesLeft = compressionBuffer.length - bIndex;
				if (numBytesLeft > 255) { numBytesLeft = 255; }
				GifU8(stream, (u8)numBytesLeft);
				GifBytes(stream, bytesPntr, numBytesLeft);
			}
			GifU8(stream, 0); //Output a zero length chunk to terminate the data stream
			
			PrintLine_D("Wrote frame %u/%u...", fIndex+1, stream->numFrames);
		}
		else
		{
			PrintLine_D("Frame %u/%u is empty", fIndex+1, stream->numFrames);
		}
		frame = frame->next;
		fIndex++;
	}
	
	//Gif Terminator
	GifU8(stream, GIF_TERMINATION_CHAR);
	
	TempPopMark();
}

void FinishGifStream(GifStream_t* stream, bool saveToFile = true)
{
	Assert(stream != nullptr);
	
	if (stream->isActive)
	{
		if (stream->numFrames > 0 && saveToFile)
		{
			EncodeGifStream(stream);
			platform->CloseFile(&stream->file);
		}
		else
		{
			platform->CloseFile(&stream->file);
			platform->DeleteFile(stream->filePath);
		}
		
		stream->isActive = false;
	}
	
	if (stream->filePath != nullptr)
	{
		Assert(stream->allocArena != nullptr);
		ArenaPop(stream->allocArena, stream->filePath);
	}
	if (stream->currentFrame.pixelData != nullptr)
	{
		ArenaPop(stream->allocArena, stream->currentFrame.pixelData);
	}
	GifFrame_t* frame = stream->firstFrame;
	while (frame != nullptr)
	{
		GifFrame_t* nextFrame = frame->next;
		ArenaPop(stream->allocArena, frame);
		frame = nextFrame;
	}
	ClearPointer(stream);
}

bool StartGifStream(MemoryArena_t* memArena, const char* filePath, reci sourceRec, GifStream_t* stream)
{
	Assert(memArena != nullptr);
	Assert(filePath != nullptr);
	Assert(stream != nullptr);
	
	ClearPointer(stream);
	
	if (!platform->OpenFile(filePath, true, &stream->file))
	{
		NotifyPrint_E("Failed to open gif file for streaming: \"%s\"", filePath);
		return false;
	}
	
	stream->sourceRec = ReciOverlap(sourceRec, NewReci(Vec2i_Zero, appInput->screenSize));
	if (stream->width <= 0) { Notify_E("Gif Source rec is not on the screen"); return false; }
	if (stream->height <= 0) { Notify_E("Gif Source rec is not on the screen"); return false; }
	Assert(stream->width <= 0xFFFF);
	Assert(stream->height <= 0xFFFF);
	
	stream->allocArena = memArena;
	stream->filePath = ArenaNtString(memArena, filePath);
	Assert(stream->filePath != nullptr);
	stream->fileSize = 0;
	stream->numFrames = 0;
	
	stream->currentFrame.pixelDataSize = stream->width * stream->height * 3;
	stream->currentFrame.pixelData = PushArray(memArena, u8, stream->currentFrame.pixelDataSize);
	Assert(stream->currentFrame.pixelData != nullptr);
	MyMemSet(stream->currentFrame.pixelData, 0x00, stream->currentFrame.pixelDataSize);
	
	stream->isActive = true;
	
	return true;
}

bool GifStreamAddFrame(GifStream_t* stream, const FrameBuffer_t* frameBuffer)
{
	Assert(stream != nullptr);
	Assert(frameBuffer != nullptr);
	if (!stream->isActive) { return false; }
	Assert(stream->x >= 0);
	Assert(stream->y >= 0);
	Assert(stream->x + stream->width <= frameBuffer->texture.width);
	Assert(stream->y + stream->height <= frameBuffer->texture.height);
	
	TempPushMark();
	// PrintLine_D("Frame Buffer Size: (%d, %d)", frameBuffer->texture.width, frameBuffer->texture.height);
	u32 textureWidth = (u32)frameBuffer->texture.width;
	u32 textureHeight = (u32)frameBuffer->texture.height;
	u8 pixelWidth = (frameBuffer->hasTransparency ? 4 : 3);
	u32 rowWidth = textureWidth * pixelWidth;
	if ((rowWidth % 4) != 0) { rowWidth += 4 - (rowWidth%4); }
	u32 pixelDataSize = textureHeight * rowWidth;
	u8* pixelData = PushArray(TempArena, u8, pixelDataSize);
	Assert(pixelData != nullptr);
	MyMemSet(pixelData, 0x00, pixelDataSize);
	
	glBindTexture(GL_TEXTURE_2D, frameBuffer->texture.id);
	glGetTexImage(GL_TEXTURE_2D, 0, (frameBuffer->hasTransparency ? GL_RGBA : GL_RGB), GL_UNSIGNED_BYTE, (GLvoid*)pixelData);
	
	reci changedRec = Reci_Zero;
	bool foundDifference = false;
	if (stream->numFrames == 0)
	{
		changedRec = stream->sourceRec;
		foundDifference = true;
	}
	else
	{
		Assert(stream->currentFrame.pixelData != nullptr);
		u32 numDifferences = 0;
		for (u32 yOffset = 0; yOffset < (u32)stream->height; yOffset++)
		{
			for (u32 xOffset = 0; xOffset < (u32)stream->width; xOffset++)
			{
				u32 newIndex = (stream->x + xOffset) * pixelWidth + (stream->y + yOffset) * rowWidth;
				Assert(newIndex+3 <= pixelDataSize);
				u8* newPntr = &pixelData[newIndex];
				u32 prevIndex = xOffset * 3 + yOffset * stream->width * 3;
				Assert(prevIndex+3 <= stream->currentFrame.pixelDataSize);
				u8* prevPntr = &stream->currentFrame.pixelData[prevIndex];
				if (MyMemCompare(newPntr, prevPntr, 3) != 0)
				{
					// if (numDifferences < 3)
					// {
					// 	PrintLine_D("Pixel (%u,%u) changed (%u,%u,%u) -> (%u,%u,%u)", xOffset, yOffset,
					// 		prevPntr[0], prevPntr[1], prevPntr[2],
					// 		newPntr[0], newPntr[1], newPntr[2]
					// 	);
					// }
					if (!foundDifference)
					{
						changedRec = NewReci(stream->x + xOffset, stream->y + yOffset, 1, 1);
					}
					else
					{
						if (stream->x + (i32)xOffset >= changedRec.x + changedRec.width) { i32 addWidth = stream->x + xOffset + 1 - (changedRec.x + changedRec.width); changedRec.width += addWidth; }
						if (stream->x + (i32)xOffset <= changedRec.x) { i32 addWidth = changedRec.x - (stream->x + xOffset); changedRec.x -= addWidth; changedRec.width += addWidth; }
						if (stream->y + (i32)yOffset >= changedRec.y + changedRec.height) { i32 addHeight = stream->y + yOffset + 1 - (changedRec.y + changedRec.height); changedRec.height += addHeight; }
						if (stream->y + (i32)yOffset <= changedRec.y) { i32 addHeight = changedRec.y - (stream->y + yOffset); changedRec.y -= addHeight; changedRec.height += addHeight; }
					}
					foundDifference = true;
					numDifferences++;
				}
			}
		}
	}
	
	GifFrame_t* newFrame = nullptr;
	if (foundDifference)
	{
		u32 framePixelDataSize = 3 * (u32)changedRec.width * (u32)changedRec.height;
		u32 allocSize = sizeof(GifFrame_t) + framePixelDataSize;
		newFrame = (GifFrame_t*)ArenaPush(stream->allocArena, allocSize);
		if (newFrame == nullptr)
		{
			NotifyPrint_E("Failed to allocate %s for gif frame %u", FormattedSizeStr(allocSize), stream->numFrames);
			FinishGifStream(stream);
			TempPopMark();
			return false;
		}
		newFrame->next = nullptr;
		newFrame->pixelData = ((u8*)newFrame) + sizeof(GifFrame_t);
		newFrame->pixelDataSize = framePixelDataSize;
		newFrame->changedRec.topLeft = changedRec.topLeft - stream->topLeft;
		newFrame->changedRec.size = changedRec.size;
		// PrintLine_D("%dx%d = %u bytes", newFrame->changedRec.width, newFrame->changedRec.height, newFrame->pixelDataSize);
		
		//Copy the pixels into the allocated space for the frame
		for (u32 yOffset = 0; yOffset < (u32)changedRec.height; yOffset++)
		{
			for (u32 xOffset = 0; xOffset < (u32)changedRec.width; xOffset++)
			{
				u32 srcIndex = (xOffset + changedRec.x)*pixelWidth + (yOffset + changedRec.y)*rowWidth;
				Assert(srcIndex+3 <= pixelDataSize);
				u8* srcPntr = &pixelData[srcIndex];
				u32 destIndex = xOffset * 3 + yOffset * changedRec.width * 3;
				Assert(destIndex+3 <= newFrame->pixelDataSize);
				u8* destPntr = &newFrame->pixelData[destIndex];
				u32 prevIndex = (xOffset + newFrame->changedRec.x) * 3 + (yOffset + newFrame->changedRec.y) * stream->width * 3;
				Assert(prevIndex+3 <= stream->currentFrame.pixelDataSize);
				u8* prevPntr = &stream->currentFrame.pixelData[prevIndex];
				MyMemCopy(destPntr, srcPntr, 3);
				MyMemCopy(prevPntr, srcPntr, 3);
			}
		}
	}
	else
	{
		newFrame = PushStruct(stream->allocArena, GifFrame_t);
		Assert(newFrame != nullptr);
		ClearPointer(newFrame);
		newFrame->pixelDataSize = 0;
		newFrame->changedRec = Reci_Zero;
	}
	
	if (stream->firstFrame == nullptr || stream->lastFrame == nullptr)
	{
		Assert(stream->firstFrame == nullptr && stream->lastFrame == nullptr);
		stream->firstFrame = newFrame;
		stream->lastFrame = newFrame;
	}
	else 
	{
		stream->lastFrame->next = newFrame;
		stream->lastFrame = newFrame;
	}
	stream->numFrames++;
	
	TempPopMark();
	
	return true;
}

//TODO: Update me to work with the new changedRec in each frame
bool GifStreamAddFramePixels(GifStream_t* stream, const u32* pixelData, u32 numPixels)
{
	Assert(stream != nullptr);
	Assert(pixelData != nullptr);
	Assert(numPixels > 0);
	if (!stream->isActive) { return false; }
	
	u32 framePixelDataSize = 3 * (u32)stream->width * (u32)stream->height;
	u32 allocSize = sizeof(GifFrame_t) + framePixelDataSize;
	GifFrame_t* newFrame = (GifFrame_t*)ArenaPush(stream->allocArena, allocSize);
	if (newFrame == nullptr)
	{
		NotifyPrint_E("Failed to allocate %s for gif frame %u", FormattedSizeStr(allocSize), stream->numFrames);
		FinishGifStream(stream);
		TempPopMark();
		return false;
	}
	newFrame->next = nullptr;
	newFrame->pixelData = ((u8*)newFrame) + sizeof(GifFrame_t);
	newFrame->pixelDataSize = framePixelDataSize;
	
	//Copy the pixels into the allocated space for the frame
	for (u32 yOffset = 0; yOffset < (u32)stream->height; yOffset++)
	{
		for (u32 xOffset = 0; xOffset < (u32)stream->width; xOffset++)
		{
			u32 srcIndex = xOffset + yOffset*stream->width;
			const u8* srcPntr = (const u8*)&pixelData[srcIndex % numPixels];
			
			u32 destIndex = xOffset*3 + yOffset*3*stream->width;
			Assert(destIndex+3 <= newFrame->pixelDataSize);
			u8* destPntr = &newFrame->pixelData[destIndex];
			
			destPntr[0] = srcPntr[0];
			destPntr[1] = srcPntr[1];
			destPntr[2] = srcPntr[2];
		}
	}
	
	if (stream->firstFrame == nullptr || stream->lastFrame == nullptr)
	{
		Assert(stream->firstFrame == nullptr && stream->lastFrame == nullptr);
		stream->firstFrame = newFrame;
		stream->lastFrame = newFrame;
	}
	else 
	{
		stream->lastFrame->next = newFrame;
		stream->lastFrame = newFrame;
	}
	stream->numFrames++;
	
	return true;
}

GifFrame_t* GetGifStreamFrame(GifStream_t* stream, u32 frameIndex)
{
	Assert(stream != nullptr);
	Assert(frameIndex < stream->numFrames);
	GifFrame_t* frame = stream->firstFrame;
	u32 fIndex = 0;
	while (fIndex < frameIndex)
	{
		Assert(frame != nullptr);
		frame = frame->next;
		fIndex++;
	}
	Assert(frame != nullptr);
	return frame;
}
const GifFrame_t* GetGifStreamFrame(const GifStream_t* stream, u32 frameIndex) //const-variant
{
	return (const GifFrame_t*)GetGifStreamFrame((GifStream_t*)stream, frameIndex);
}

u8* GetGifStreamFramePixelData(MemoryArena_t* memArena, const GifStream_t* stream, u32 frameIndex, bool includeAlpha, u32* pixelDataSizeOut = nullptr)
{
	Assert(memArena != nullptr);
	Assert(stream != nullptr);
	Assert(stream->isActive);
	Assert(frameIndex < stream->numFrames);
	
	u32 prevKeyFrame = frameIndex;
	while (prevKeyFrame > 0)
	{
		const GifFrame_t* frame = GetGifStreamFrame(stream, prevKeyFrame);
		if (frame->changedRec.x == 0 && frame->changedRec.y == 0 && frame->changedRec.width == stream->width && frame->changedRec.height == stream->height) { break; }
		else { prevKeyFrame--; }
	}
	
	u8 pixelWidth = includeAlpha ? 4 : 3;
	u32 pixelDataSize = stream->width * stream->height * pixelWidth;
	u8* pixelData = PushArray(memArena, u8, pixelDataSize);
	Assert(pixelData != nullptr);
	MyMemSet(pixelData, 0x00, pixelDataSize);
	
	const GifFrame_t* frame = GetGifStreamFrame(stream, prevKeyFrame);
	for (u32 fIndex = prevKeyFrame; fIndex <= frameIndex; fIndex++)
	{
		Assert(frame != nullptr);
		if (frame->changedRec.width > 0 && frame->changedRec.height > 0)
		{
			for (u32 yOffset = 0; yOffset < (u32)frame->changedRec.height; yOffset++)
			{
				for (u32 xOffset = 0; xOffset < (u32)frame->changedRec.width; xOffset++)
				{
					u32 srcIndex = (xOffset + yOffset * frame->changedRec.width) * 3;
					Assert(srcIndex+3 <= frame->pixelDataSize);
					u8* srcPntr = &frame->pixelData[srcIndex];
					u32 destIndex = ((frame->changedRec.x + xOffset) + (frame->changedRec.y + yOffset) * stream->width) * pixelWidth;
					Assert(destIndex < pixelDataSize);
					u8* destPntr = &pixelData[destIndex];
					destPntr[0] = srcPntr[0]; //R
					destPntr[1] = srcPntr[1]; //G
					destPntr[2] = srcPntr[2]; //B
					if (includeAlpha) { destPntr[3] = 0xFF; } //A
				}
			}
		}
		frame = frame->next;
	}
	
	if (pixelDataSizeOut != nullptr) { *pixelDataSizeOut = pixelDataSize; }
	return pixelData;
}

void GifStreamCut(GifStream_t* stream, u32 startIndex, u32 endIndex)
{
	Assert(stream != nullptr);
	Assert(stream->isActive);
	Assert(startIndex < endIndex);
	Assert(startIndex < stream->numFrames);
	Assert(endIndex <= stream->numFrames);
	
	//Chop off frames at end
	if (endIndex < stream->numFrames)
	{
		GifFrame_t* frame = GetGifStreamFrame(stream, endIndex-1);
		u32 fIndex = 0;
		while (frame != nullptr)
		{
			GifFrame_t* nextFrame = frame->next;
			if (fIndex == 0) { frame->next = nullptr; stream->lastFrame = frame; }
			else { ArenaPop(stream->allocArena, frame); }
			frame = nextFrame;
			fIndex++;
		}
		stream->numFrames = endIndex;
	}
	
	//Chop off frames at beginning (make sure the first frame fills the entire area)
	if (startIndex > 0)
	{
		TempPushMark();
		{
			u32 newPixelDataSize = 0;
			u8* newPixelData = GetGifStreamFramePixelData(TempArena, stream, startIndex, false, &newPixelDataSize);
			Assert(newPixelData != nullptr);
			Assert((i32)newPixelDataSize == 3 * stream->width * stream->height);
			
			GifFrame_t* frame = stream->firstFrame;
			u32 fIndex = 0;
			while (fIndex < startIndex)
			{
				Assert(frame != nullptr);
				GifFrame_t* nextFrame = frame->next;
				ArenaPop(stream->allocArena, frame);
				frame = nextFrame;
				fIndex++;
			}
			Assert(fIndex == startIndex && frame != nullptr);
			GifFrame_t* secondFrame = frame->next;
			ArenaPop(stream->allocArena, frame);
			
			u32 allocSize = sizeof(GifFrame_t) + newPixelDataSize;
			GifFrame_t* newFrame = (GifFrame_t*)ArenaPush(stream->allocArena, allocSize);
			Assert(newFrame != nullptr);
			ClearPointer(newFrame);
			newFrame->pixelDataSize = newPixelDataSize;
			newFrame->pixelData = ((u8*)newFrame) + sizeof(GifFrame_t);
			MyMemCopy(newFrame->pixelData, newPixelData, newPixelDataSize);
			newFrame->changedRec = NewReci(0, 0, stream->width, stream->height);
			newFrame->next = secondFrame;
			stream->firstFrame = newFrame;
			stream->numFrames -= startIndex;
		}
		TempPopMark();
	}
}
