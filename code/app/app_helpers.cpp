/*
File:   app_helpers.cpp
Author: Taylor Robbins
Date:   10\21\2018
Description: 
	** Holds functions that the various AppStates might use for random tasks.
	** It's mostly just a dumping ground for functions that have no proper home
*/

bool IsThisMainThread() //pre-declared in app_func_defs.h
{
	Assert_(platform != nullptr);
	ThreadId_t thisThreadId = platform->GetThisThreadId();
	return platform->IsMainThread(thisThreadId);
}
MemoryArena_t* GetThreadTempArena() //pre-declared in app_func_defs.h
{
	ThreadId_t thisThreadId = platform->GetThisThreadId();
	if (platform->IsMainThread(thisThreadId)) { return TempArena; }
	else
	{
		TaskThreadInfo_t* threadInfo = platform->GetThreadInfo(thisThreadId);
		if (threadInfo == nullptr) { return nullptr; }
		if (threadInfo->tempArena.size == 0 || threadInfo->tempArena.base == nullptr) { return nullptr; }
		return &threadInfo->tempArena;
	}
}

char* DupStrN(const char* str, u32 strLength, MemoryArena_t* memoryArenaPntr)
{
	char* result;
	
	result = PushArray(memoryArenaPntr, char, (u32)strLength+1);
	strncpy(result, str, strLength);
	result[strLength] = '\0';
	
	return result;
}
char* DupStr(const char* str, MemoryArena_t* memoryArenaPntr)
{
	u32 strLength = MyStrLength32(str);
	return DupStrN(str, strLength, memoryArenaPntr);
}

char* FormattedTimeStr(RealTime_t realTime)
{
	char* result;
	
	result = TempPrint("%s %u:%02u:%02u%s (%s %s, %u)",
		GetDayOfWeekStr((DayOfWeek_t)realTime.dayOfWeek),
		Convert24HourTo12Hour(realTime.hour), realTime.minute, realTime.second,
		IsPostMeridian(realTime.hour) ? "pm" : "am",
		GetMonthStr((Month_t)realTime.month), GetDayOfMonthString(realTime.day), realTime.year
	);
	
	return result;
}

char* FormattedTimeStr(u64 timestamp)
{
	RealTime_t realTime;
	GetRealTime(timestamp, true, &realTime);
	return FormattedTimeStr(realTime);
}

char* SanatizeStringAdvanced(const char* strPntr, u32 numChars, MemoryArena_t* arenaPntr,
	bool keepNewLines, bool keepBackspaces, bool convertInvalidToHex)
{
	char* result = nullptr;
	u32 resultLength = 0;
	
	for (u8 round = 0; round < 2; round++)
	{
		Assert(round == 0 || result != nullptr);
		
		//Run this code twice, once to measure and once to actually copy into buffer
		u32 fromIndex = 0;
		u32 toIndex = 0;
		while (fromIndex < numChars)
		{
			char c = strPntr[fromIndex];
			char lastChar = '\0';
			if (fromIndex > 0) { lastChar = strPntr[fromIndex-1]; }
			char nextChar = strPntr[fromIndex+1];
			
			bool charHandled = false;
			if (c == '\n')
			{
				if (keepNewLines)
				{
					if (result != nullptr) { result[toIndex] = '\n'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (c == '\r')
			{
				if (nextChar == '\n' || lastChar == '\n')
				{
					//Drop the \r character
					charHandled = true;
				}
				else if (keepNewLines)
				{
					if (result != nullptr) { result[toIndex] = '\n'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (c == '\b')
			{
				if (keepBackspaces)
				{
					if (result != nullptr) { result[toIndex] = '\b'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (IsCharClassPrintable(c) || c == '\t')
			{
				if (result != nullptr) { result[toIndex] = c; }
				toIndex++;
				charHandled = true;
			}
			
			if (charHandled == false && convertInvalidToHex)
			{
				if (result != nullptr)
				{
					result[toIndex+0] = '0';
					result[toIndex+1] = 'x';
					result[toIndex+2] = UpperHexChar(c);
					result[toIndex+3] = LowerHexChar(c);
				}
				toIndex += 4;
			}
			
			fromIndex++;
		}
		
		resultLength = toIndex;
		if (resultLength == 0) { break; }
		if (round == 0)
		{
			result = (char*)ArenaPush(arenaPntr, resultLength+1);
		}
		result[resultLength] = '\0';
	}
	
	return result;
}

char* GetElapsedString(u64 timespan)
{
	char* result = nullptr;
	
	u32 numDays = (u32)(timespan/(60*60*24));
	u32 numHours = (u32)(timespan/(60*60)) - (numDays*24);
	u32 numMinutes = (u32)(timespan/60) - (numDays*60*24) - (numHours*60);
	u32 numSeconds = (u32)(timespan) - (numDays*60*60*24) - (numHours*60*60) - (numMinutes*60);
	if (numDays > 0)
	{
		result = TempPrint("%ud %uh %um %us", numDays, numHours, numMinutes, numSeconds);
	}
	else if (numHours > 0)
	{
		result = TempPrint("%uh %um %us", numHours, numMinutes, numSeconds);
	}
	else if (numMinutes > 0)
	{
		result = TempPrint("%um %us", numMinutes, numSeconds);
	}
	else
	{
		result = TempPrint("%us", numSeconds);
	}
	
	return result;
}

r32 OscillateBy(u64 timeSource, r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	r32 lerpValue = (SinR32((((timeSource + offset) % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 Oscillate(r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	return OscillateBy(ProgramTime, min, max, periodMs, offset);
}
r32 OscillatePhaseBy(u64 timeSource, r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	r32 lerpValue = (SawR32((((timeSource + offset) % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	lerpValue = Ease(EasingStyle_CubicOut, lerpValue);
	return min + (max - min) * lerpValue;
}
r32 OscillatePhase(r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	return OscillatePhaseBy(ProgramTime, min, max, periodMs, offset);
}
r32 OscillateSawBy(u64 timeSource, r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	r32 lerpValue = (SawR32((((timeSource + offset) % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 OscillateSaw(r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	return OscillateSawBy(ProgramTime, min, max, periodMs, offset);
}
r32 AnimateBy(u64 timeSource, r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	r32 lerpValue = ((timeSource + offset) % periodMs) / (r32)periodMs;
	return min + (max - min) * lerpValue;
}
r32 Animate(r32 min, r32 max, u32 periodMs, u32 offset = 0)
{
	return AnimateBy(ProgramTime, min, max, periodMs, offset);
}

u64 TimeSince(u64 programTimeSnapshot)
{
	u64 programTime = ProgramTime;
	if (programTimeSnapshot <= programTime)
	{
		return programTime - programTimeSnapshot;
	}
	else 
	{
		return 0;
	}
}

void ExtendSampleBuffer(SampleBuffer_t* buffer, u32 numSamplesToFit)
{
	if (buffer->numSamplesAlloc >= numSamplesToFit) { return; }
	// PrintLine_D("Reallocating %u < %u", buffer->numSamplesAlloc, numSamplesToFit);
	u32 newSpaceSize = buffer->numSamplesAlloc;
	while (newSpaceSize < numSamplesToFit) { newSpaceSize += SAMPLE_BUFFER_ALLOC_SIZE; }
	i16* newSpace = PushArray(&app->platHeap, i16, newSpaceSize);
	Assert(newSpace != nullptr);
	MyMemSet(newSpace, 0x00, sizeof(i16) * newSpaceSize); //clear the space to make sure we don't have crazy data
	if (buffer->samples != nullptr)
	{
		if (buffer->numSamples > 0)
		{
			MyMemCopy(newSpace, buffer->samples, buffer->numSamples * sizeof(i16));
		}
		ArenaPop(&app->platHeap, buffer->samples);
	}
	buffer->samples = newSpace;
	buffer->numSamplesAlloc = newSpaceSize;
}
void DestroySampleBuffer(SampleBuffer_t* buffer)
{
	Assert(buffer != nullptr);
	if (buffer->samples != nullptr)
	{
		ArenaPop(&app->platHeap, buffer->samples);
	}
	ClearPointer(buffer);
}

const char* BinaryString(u32 value, u8 numBits)
{
	Assert(numBits <= 32);
	if (numBits == 0) { return ""; }
	if (numBits == 1) { return IsFlagSet(value, 0x01) ? "1" : "0"; }
	char* result = PushArray(TempArena, char, numBits+1);
	Assert(result != nullptr);
	for (u32 bIndex = 0; bIndex < numBits; bIndex++)
	{
		result[numBits-1 - bIndex] = IsFlagSet(value, (1 << bIndex)) ? '1' : '0';
	}
	result[numBits] = '\0';
	return result;
}

#define TIME_SCALED_ANIM(animationTime, timeScale) (((1000.0f / 60.0f) / (animationTime)) * (timeScale))

u64 GetGpuMemoryUsage()
{
	//TODO: Check if the graphics card is NVIDIA or AMD?
	// GLint totalMemKb = 0;
	// glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &totalMemKb);
	GLint availableMemKb = 0;
	glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &availableMemKb);
	//TODO: Check for OpenGL errors
	
	if (app->baseAvailGpuMemory == 0) { return Kilobytes((u64)availableMemKb); }
	else if (app->baseAvailGpuMemory >= Kilobytes((u64)availableMemKb)) { return app->baseAvailGpuMemory - Kilobytes((u64)availableMemKb); }
	else { return 0; }
}

void StbImageWriteCallback(void* context, void* data, int size)
{
	Assert(context != nullptr);
	Assert(data != nullptr || size == 0);
	Assert(size >= 0);
	if (size == 0) { return; }
	OpenFile_t* imageFile = (OpenFile_t*)context;
	Assert(imageFile->isOpen);
	bool writeSuccess = platform->WriteToFile(imageFile, data, (u32)size);
	Assert(writeSuccess);
}
void StbImageWriteCallbackBktArray(void* context, void* data, int size)
{
	Assert(context != nullptr);
	Assert(data != nullptr || size == 0);
	Assert(size >= 0);
	if (size == 0) { return; }
	BktArray_t* dataArray = (BktArray_t*)context;
	Assert(dataArray->allocArena != nullptr);
	Assert(dataArray->itemSize == sizeof(u8));
	u8* newSpace = BktArrayAddBulk(dataArray, u8, (u32)size, true);
	Assert(newSpace != nullptr);
	MyMemCopy(data, newSpace, (u32)size);
}
void StbImageWriteCallbackMeasure(void* context, void* data, int size)
{
	Assert(context != nullptr);
	Assert(data != nullptr || size == 0);
	Assert(size >= 0);
	if (size == 0) { return; }
	u32* sizePntr = (u32*)context;
	*sizePntr += (u32)size;
}
void StbImageWriteCallbackPrealloc(void* context, void* data, int size)
{
	Assert(context != nullptr);
	Assert(data != nullptr || size == 0);
	Assert(size >= 0);
	if (size == 0) { return; }
	u8** dataPntrPntr = (u8**)context;
	MyMemCopy(*dataPntrPntr, data, (u32)size);
	*dataPntrPntr += (u32)size;
}

u8* CreatePngFromTextureData(MemoryArena_t* memArena, const u8* textureData, v2i textureSize, u32* pngSizeOut, u32 extraAllocAmount = 0)
{
	Assert(textureData != nullptr);
	Assert(textureSize.width > 0 && textureSize.height > 0);
	u32 pngFileSize = 0;
	int writeResult1 = stbi_write_png_to_func(StbImageWriteCallbackMeasure, &pngFileSize, textureSize.width, textureSize.height, 4, textureData, textureSize.width*4);
	if (writeResult1 == 0)
	{
		return nullptr;
	}
	if (pngFileSize == 0) { return nullptr; }
	if (pngSizeOut != nullptr) { *pngSizeOut = pngFileSize; }
	if (memArena == nullptr) { return nullptr; }
	u8* result = PushArray(memArena, u8, pngFileSize + extraAllocAmount);
	if (result == nullptr) { return nullptr; }
	MyMemSet(result, 0x00, pngFileSize);
	u8* writePntr = result;
	int writeResult2 = stbi_write_png_to_func(StbImageWriteCallbackPrealloc, &writePntr, textureSize.width, textureSize.height, 4, textureData, textureSize.width*4);
	Assert(writeResult2 == writeResult1);
	Assert(writePntr == result + pngFileSize);
	return result;
}

bool WasProgramArgumentPassed(const char* argName)
{
	Assert(platform != nullptr);
	Assert(argName != nullptr);
	u32 argNameLength = (u32)strlen(argName);
	Assert(argNameLength > 0);
	for (u32 aIndex = 0; aIndex < platform->numProgramArguments; aIndex++)
	{
		const char* arg = platform->programArguments[aIndex];
		u32 argLength = (u32)strlen(arg);
		if (argLength == 1+argNameLength && arg[0] == '-' && StrCompareIgnoreCase(&arg[1], argName, argNameLength))
		{
			return true;
		}
	}
	return false;
}
bool GetProgramArgumentValueI32(const char* argName, i32* valueOut = nullptr)
{
	Assert(argName != nullptr);
	u32 argNameLength = (u32)strlen(argName);
	Assert(argNameLength > 0);
	for (u32 aIndex = 0; aIndex < platform->numProgramArguments; aIndex++)
	{
		const char* arg = platform->programArguments[aIndex];
		u32 argLength = (u32)strlen(arg);
		if (argLength > 1+argNameLength+1 && arg[0] == '-' && arg[1+argNameLength] == '=' && StrCompareIgnoreCase(&arg[1], argName, argNameLength))
		{
			const char* numberStr = &arg[1+argNameLength+1];
			u32 numberStrLength = argLength - (1+argNameLength+1);
			i32 resultValue = 0;
			if (TryParseI32(numberStr, numberStrLength, &resultValue))
			{
				if (valueOut != nullptr) { *valueOut = resultValue; }
				return true;
			}
		}
	}
	return false;
}
char* GetProgramArgumentValueStr(const char* argName, MemoryArena_t* memArena)
{
	Assert(argName != nullptr);
	u32 argNameLength = (u32)strlen(argName);
	Assert(argNameLength > 0);
	for (u32 aIndex = 0; aIndex < platform->numProgramArguments; aIndex++)
	{
		const char* arg = platform->programArguments[aIndex];
		u32 argLength = (u32)strlen(arg);
		if (argLength >= 1+argNameLength+1 && arg[0] == '-' && arg[1+argNameLength] == '=' && StrCompareIgnoreCase(&arg[1], argName, argNameLength))
		{
			char* result = ArenaString(memArena, &arg[1+argNameLength+1], argLength - (1+argNameLength+1));
			return result;
		}
	}
	return nullptr;
}
