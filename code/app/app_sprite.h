/*
File:   app_sprite.h
Author: Taylor Robbins
Date:   06\11\2020
*/

#ifndef _APP_SPRITE_H
#define _APP_SPRITE_H

struct Sprite_t
{
	const SpriteSheet_t* sheet;
	v2i frame;
	Dir2_t rotation;
	Color_t drawColor;
};
struct SpriteEx_t
{
	union
	{
		Sprite_t sprite;
		struct
		{
			const SpriteSheet_t* sheet;
			v2i frame;
			Dir2_t rotation;
			Color_t drawColor;
		};
	};
	PalColor_t palColor;
	v2i maskFrame;
	bool flippedX;
	bool flippedY;
};

#endif //  _APP_SPRITE_H
