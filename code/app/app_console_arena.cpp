/*
File:   app_console_arena.cpp
Author: Taylor Robbins
Date:   06\23\2020
Description: 
	** A Console Arena is a special type of memory management device that helps various parts of the code keep track of a bunch of lines of text
	** in a pre-defined amount of memory by setting up a doubly linked list fifo kind of structure with dynamically sized elements
*/

//NOTE: All Assertions in this file must be Assert_, no debug output is allowed, and no implicit memory arena usage is allowed.
//      We should generally minimize the number of external functions that this file uses

void DestroyConsoleArena(ConsoleArena_t* arena)
{
	Assert_(arena != nullptr);
	if (arena->fifoSpace != nullptr && !arena->isExternallyAllocated)
	{
		Assert_(arena->allocArena != nullptr);
		ArenaPop(arena->allocArena, arena->fifoSpace);
	}
	if (arena->lineBuildSpace != nullptr && !arena->isExternallyAllocated)
	{
		Assert_(arena->allocArena != nullptr);
		ArenaPop(arena->allocArena, arena->lineBuildSpace);
	}
	if (arena->printBuffer != nullptr && !arena->isExternallyAllocated)
	{
		Assert_(arena->allocArena != nullptr);
		ArenaPop(arena->allocArena, arena->printBuffer);
	}
	ClearPointer(arena);
}

void InitializeConsoleArenaExternAlloc(ConsoleArena_t* arena, void* fifoSpace, u32 fifoSize)
{
	Assert_(arena != nullptr);
	Assert_(fifoSpace != nullptr);
	Assert_(fifoSize > 0);
	Assert_(arena->lineBuildSpace != nullptr || arena->lineBuildSize == 0);
	ClearPointer(arena);
	
	arena->isExternallyAllocated = true;
	arena->allocArena = nullptr;
	arena->saveFilePaths = true;
	arena->saveFunctionNames = true;
	
	arena->fifoSpace = (u8*)fifoSpace;
	arena->fifoSize = fifoSize;
	arena->fifoUsage = 0;
	
	arena->numLines = 0;
	arena->nextGutterNumber = 0;
	arena->firstLine = nullptr;
	arena->lastLine = nullptr;
}
void InitializeConsoleArena(ConsoleArena_t* arena, MemoryArena_t* memArena, u32 fifoSize, u32 lineBuildSize, u32 printBufferSize)
{
	Assert_(arena != nullptr);
	Assert_(memArena != nullptr);
	Assert_(fifoSize > 0);
	ClearPointer(arena);
	
	arena->isExternallyAllocated = false;
	arena->allocArena = memArena;
	arena->saveFilePaths = true;
	arena->saveFunctionNames = true;
	
	arena->fifoSpace = PushArray(memArena, u8, fifoSize);
	Assert_(arena->fifoSpace != nullptr);
	arena->fifoSize = fifoSize;
	arena->fifoUsage = 0;
	
	if (lineBuildSize > 0)
	{
		arena->lineBuildSpace = PushArray(memArena, char, lineBuildSize);
		Assert_(arena->lineBuildSpace != nullptr);
		arena->lineBuildSize = lineBuildSize;
		arena->lineBuildUsage = 0;
	}
	
	if (printBufferSize > 0)
	{
		arena->printBuffer = PushArray(memArena, char, printBufferSize);
		Assert_(arena->printBuffer != nullptr);
		arena->printBufferSize = printBufferSize;
	}
	
	arena->numLines = 0;
	arena->nextGutterNumber = 0;
	arena->firstLine = nullptr;
	arena->lastLine = nullptr;
}

void ConsoleArenaClearLines(ConsoleArena_t* arena)
{
	Assert(arena != nullptr);
	arena->numLines = 0;
	arena->nextGutterNumber = 0;
	arena->firstLine = nullptr;
	arena->lastLine = nullptr;
	arena->fifoUsage = 0;
	arena->lineBuildUsage = 0;
}

ConsoleLine_t* GetConsoleLine(ConsoleArena_t* arena, u32 lineIndex)
{
	Assert_(arena != nullptr);
	if (lineIndex >= arena->numLines) { return nullptr; }
	ConsoleLine_t* line = arena->firstLine;
	u32 lIndex = 0;
	while (lIndex < lineIndex && line != nullptr)
	{
		line = line->next;
		lIndex++;
	}
	return line;
}

u32 GetConsoleLineTotalMemUsage(const ConsoleLine_t* line)
{
	Assert_(line != nullptr);
	return sizeof(ConsoleLine_t) + line->filePathLength+1 + line->funcNameLength+1 + line->messageLength+1;
}

char* GetConsoleLineFilePath(ConsoleLine_t* line)
{
	u8* bytePntr = (u8*)(line);
	bytePntr += sizeof(ConsoleLine_t);
	Assert_(bytePntr[line->filePathLength] == 0x00);
	return (char*)bytePntr;
}
const char* GetConsoleLineFilePath(const ConsoleLine_t* line) //const-version
{
	return (const char*)GetConsoleLineFilePath((ConsoleLine_t*)line);
}
char* GetConsoleLineFuncName(ConsoleLine_t* line)
{
	u8* bytePntr = (u8*)(line);
	bytePntr += sizeof(ConsoleLine_t);
	bytePntr += line->filePathLength+1;
	Assert_(bytePntr[line->funcNameLength] == 0x00);
	return (char*)bytePntr;
}
const char* GetConsoleLineFuncName(const ConsoleLine_t* line) //const-version
{
	return (const char*)GetConsoleLineFuncName((ConsoleLine_t*)line);
}
char* GetConsoleLineMessage(ConsoleLine_t* line)
{
	u8* bytePntr = (u8*)(line);
	bytePntr += sizeof(ConsoleLine_t);
	bytePntr += line->filePathLength+1;
	bytePntr += line->funcNameLength+1;
	Assert_(bytePntr[line->messageLength] == 0x00);
	return (char*)bytePntr;
}
const char* GetConsoleLineMessage(const ConsoleLine_t* line)
{
	return (const char*)GetConsoleLineMessage((ConsoleLine_t*)line);
}
u8* GetPntrAfterConsoleLine(ConsoleLine_t* line)
{
	u8* bytePntr = (u8*)(line);
	bytePntr += sizeof(ConsoleLine_t);
	bytePntr += line->filePathLength+1;
	bytePntr += line->funcNameLength+1;
	bytePntr += line->messageLength+1;
	return bytePntr;
}
const u8* GetPntrAfterConsoleLine(const ConsoleLine_t* line)
{
	return (const u8*)GetPntrAfterConsoleLine((ConsoleLine_t*)line);
}

u32 GetConsoleArenaFifoIndex(const ConsoleArena_t* arena, const void* pntr)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	Assert_(pntr != nullptr);
	Assert_(pntr >= arena->fifoSpace && pntr <= arena->fifoSpace + arena->fifoSize);
	return (u32)(((u8*)pntr) - arena->fifoSpace);
}
u32 GetSpaceAfterConsoleLine(const ConsoleArena_t* arena, const ConsoleLine_t* line)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	Assert_(line != nullptr);
	Assert_((u8*)line >= arena->fifoSpace && (u8*)line < arena->fifoSpace + arena->fifoSize);
	const u8* lineEndPntr = GetPntrAfterConsoleLine(line);
	u32 lineEndIndex = GetConsoleArenaFifoIndex(arena, lineEndPntr);
	return arena->fifoSize - lineEndIndex;
}
u32 GetSpaceBetweenConsoleLines(const ConsoleArena_t* arena, const ConsoleLine_t* firstLine, const ConsoleLine_t* secondLine)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	Assert_(firstLine != nullptr);
	Assert_(secondLine != nullptr);
	Assert_((u8*)firstLine >= arena->fifoSpace && (u8*)firstLine < arena->fifoSpace + arena->fifoSize);
	Assert_((u8*)secondLine >= arena->fifoSpace && (u8*)secondLine < arena->fifoSpace + arena->fifoSize);
	Assert_(firstLine < secondLine);
	const u8* firstLineEndPntr = GetPntrAfterConsoleLine(firstLine);
	u32 firstLineEndIndex = GetConsoleArenaFifoIndex(arena, firstLineEndPntr);
	const u8* secondLineStartPntr = (const u8*)secondLine;
	u32 secondLineStartIndex = GetConsoleArenaFifoIndex(arena, secondLineStartPntr);
	Assert_(secondLineStartIndex >= firstLineEndIndex);
	return secondLineStartIndex - firstLineEndIndex;
}

bool ConsoleArenaRemoveFirstLine(ConsoleArena_t* arena)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	
	if (arena->firstLine != nullptr)
	{
		Assert_(arena->lastLine != nullptr);
		
		if (arena->removeLineCallback != nullptr) { arena->removeLineCallback(arena, arena->firstLine, arena->removeLineCallbackUserPntr); }
		
		u32 firstLineSize = GetConsoleLineTotalMemUsage(arena->firstLine);
		Assert_(arena->fifoUsage >= firstLineSize);
		arena->fifoUsage -= firstLineSize;
		if (arena->lastLine == arena->firstLine)
		{
			arena->firstLine = nullptr;
			arena->lastLine = nullptr;
		}
		else
		{
			arena->firstLine = arena->firstLine->next;
			Assert_(arena->firstLine != nullptr);
		}
		arena->numLines--;
		return true;
	}
	else
	{
		Assert_(arena->lastLine == nullptr);
		Assert_(arena->fifoUsage == 0);
		Assert_(arena->numLines == 0);
		return false;
	}
}

//NOTE: Returns whether or not the first line on the FIFO needs to be removed in order to make space for the proposed new line of a specific size
//      The calling code should react by looking at arena->firstLine and doing anything necassary with it's information before it gets removed
//      and then call ConsoleArenaRemoveFirstLine to remove it.
bool DoesArenaNeedToMakeSpaceForConsoleLine(ConsoleArena_t* arena, u32 newLineTextSize)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	
	u32 newLineSize = sizeof(ConsoleLine_t) + newLineTextSize;
	
	if (arena->firstLine == nullptr || arena->lastLine == nullptr)
	{
		Assert_(arena->firstLine == nullptr && arena->lastLine == nullptr);
		Assert_(arena->numLines == 0);
		Assert_(arena->fifoUsage == 0);
		return false;
	}
	else if (arena->firstLine <= arena->lastLine)
	{
		//Look to insert after lastLine and before end (if space)
		u32 spaceTillEnd = GetSpaceAfterConsoleLine(arena, arena->lastLine);
		if (spaceTillEnd >= newLineSize)
		{
			return false;
		}
		else
		{
			//There's no space at the end, see if there is space at the beginning before the firstLine
			u32 firstLineIndex = GetConsoleArenaFifoIndex(arena, arena->firstLine);
			if (firstLineIndex >= newLineSize)
			{
				return false;
			}
			else
			{
				//Not enough space at the beginning, need to remove the firstLine
				return true;
			}
		}
	}
	else
	{
		u32 spaceBetween = GetSpaceBetweenConsoleLines(arena, arena->lastLine, arena->firstLine);
		if (spaceBetween >= newLineSize)
		{
			return false;
		}
		else
		{
			//Not enough space between lastLine and firstLine, need to remove the firstLine
			return true;
		}
	}
}

ConsoleLine_t* ConsoleArenaAllocLine(ConsoleArena_t* arena, u32 filePathLength, u32 funcNameLength, u32 messageLength)
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	Assert_(arena->fifoSize > sizeof(ConsoleLine_t) + filePathLength+1 + funcNameLength+1);
	
	u32 lineTextSize = filePathLength+1 + funcNameLength+1 + messageLength+1;
	while (DoesArenaNeedToMakeSpaceForConsoleLine(arena, lineTextSize))
	{
		ConsoleArenaRemoveFirstLine(arena);
	}
	
	u32 newLineSize = sizeof(ConsoleLine_t) + lineTextSize;
	ConsoleLine_t* result = nullptr;
	while (result == nullptr)
	{
		if (arena->firstLine == nullptr || arena->lastLine == nullptr)
		{
			//Fifo empty, insert first line
			Assert_(arena->firstLine == nullptr && arena->lastLine == nullptr);
			Assert_(arena->numLines == 0);
			Assert_(arena->fifoUsage == 0);
			
			if (arena->fifoSize >= newLineSize)
			{
				result = (ConsoleLine_t*)arena->fifoSpace;
				ClearPointer(result);
				result->next = nullptr;
				result->prev = nullptr;
				arena->firstLine = result;
				arena->lastLine = result;
				arena->fifoUsage += newLineSize;
				arena->numLines++;
			}
			else
			{
				//NOTE: To avoid recursion we use the platform layer DebugPrint instead of the normal AppDebugPrint macros
				platform->DebugPrint(__FILE__, __LINE__, __func__, DbgLevel_Error, true,
					"Debug console fifo cannot fit space for new %u char line (%u total bytes, %u available)",
					messageLength, newLineSize, arena->fifoSize
				);
				return nullptr;
			}
		}
		else if (arena->firstLine <= arena->lastLine)
		{
			//Look to insert after lastLine and before end (if space)
			u32 spaceTillEnd = GetSpaceAfterConsoleLine(arena, arena->lastLine);
			if (spaceTillEnd >= newLineSize)
			{
				result = (ConsoleLine_t*)GetPntrAfterConsoleLine(arena->lastLine);
				ClearPointer(result);
				result->next = nullptr;
				result->prev = arena->lastLine;
				arena->lastLine->next = result;
				arena->lastLine = result;
				arena->fifoUsage += newLineSize;
				arena->numLines++;
			}
			else
			{
				//There's no space at the end, see if there is space at the beginning before the firstLine
				u32 firstLineIndex = GetConsoleArenaFifoIndex(arena, arena->firstLine);
				Assert_(firstLineIndex >= newLineSize); //This should be guaranteed by the calls to DoesArenaNeedToMakeSpaceForConsoleLine above
				
				result = (ConsoleLine_t*)arena->fifoSpace;
				ClearPointer(result);
				result->next = nullptr;
				result->prev = arena->lastLine;
				arena->lastLine->next = result;
				arena->lastLine = result;
				arena->fifoUsage += newLineSize;
				arena->numLines++;
			}
		}
		else
		{
			u32 spaceBetween = GetSpaceBetweenConsoleLines(arena, arena->lastLine, arena->firstLine);
			Assert_(spaceBetween >= newLineSize); //This should be guaranteed by the calls to DoesArenaNeedToMakeSpaceForConsoleLine above
			
			result = (ConsoleLine_t*)GetPntrAfterConsoleLine(arena->lastLine);
			ClearPointer(result);
			result->next = nullptr;
			result->prev = arena->lastLine;
			arena->lastLine->next = result;
			arena->lastLine = result;
			arena->fifoUsage += newLineSize;
			arena->numLines++;
		}
	}
	
	Assert_(result != nullptr);
	result->filePathLength = filePathLength;
	result->funcNameLength = funcNameLength;
	result->messageLength = messageLength;
	if (platform != nullptr)
	{
		result->threadId = platform->GetThisThreadId();
		if (!IsThisMainThread())
		{
			FlagSet(result->flags, DbgFlag_TaskThread);
			TaskThreadInfo_t* threadInfo = platform->GetThreadInfo(result->threadId);
			if (threadInfo != nullptr)
			{
				result->threadIndex = threadInfo->index;
				result->taskId = threadInfo->currentTaskId;
			}
		}
	}
	result->gutterNumber = arena->nextGutterNumber;
	arena->nextGutterNumber++;
	
	return result;
}
ConsoleLine_t* ConsoleArenaAllocLine(ConsoleArena_t* arena, const char* filePath, const char* functionName, const char* message)
{
	Assert_(arena != nullptr);
	Assert_(message != nullptr);
	
	u32 filePathLength = (filePath != nullptr) ? MyStrLength32(filePath) : 0;
	u32 functionNameLength = (functionName != nullptr) ? MyStrLength32(functionName) : 0;
	u32 messageLength = MyStrLength32(message);
	
	ConsoleLine_t* newLine = ConsoleArenaAllocLine(arena, filePathLength, functionNameLength, messageLength);
	if (newLine == nullptr) { return nullptr; }
	
	//NOTE: The GetConsoleFilePath and similar functions below Assert that a null-term char exists for each text element
	//      We need to fill the text space with 0's before we can use the functions below to get the pointers and fill the space properly
	char* textSpace = (char*)(newLine + 1);
	MyMemSet(textSpace, 0x00, filePathLength+1 + functionNameLength+1 + messageLength+1);
	
	char* filePathSpace     = GetConsoleLineFilePath(newLine); Assert_(filePathSpace     != nullptr);
	char* functionNameSpace = GetConsoleLineFuncName(newLine); Assert_(functionNameSpace != nullptr);
	char* messageSpace      = GetConsoleLineMessage(newLine);  Assert_(messageSpace      != nullptr);
	
	MyMemCopy(filePathSpace, filePath, filePathLength+1);
	MyMemCopy(functionNameSpace, functionName, functionNameLength+1);
	MyMemCopy(messageSpace, message, messageLength+1);
	
	return newLine;
}

ConsoleLine_t* ConsoleArenaProcessChars(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* message) //pre-declared in app_console_arena.h
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	
	if (funcName == nullptr || !arena->saveFunctionNames) { funcName = ""; }
	if (filePath == nullptr || !arena->saveFilePaths) { filePath = ""; }
	if (message == nullptr) { message = ""; }
	
	ConsoleLine_t* result = nullptr;
	u32 messageLength = MyStrLength32(message);
	u32 lineStart = 0;
	for (u32 cIndex = 0; cIndex <= messageLength; cIndex++)
	{
		char nextChar = (cIndex < messageLength) ? message[cIndex] : (addNewLine ? '\n' : '\0');
		if (nextChar == '\n')
		{
			u32 lineLength = cIndex - lineStart;
			const char* linePntr = &message[lineStart];
			if (arena->lineBuildUsage > 0)
			{
				u32 actualMessageLength = arena->lineBuildInfo.messageLength + lineLength;
				u32 totalTextLength = arena->lineBuildInfo.filePathLength+1 + arena->lineBuildInfo.funcNameLength+1 + actualMessageLength+1;
				ConsoleLine_t* newLine = ConsoleArenaAllocLine(arena, arena->lineBuildInfo.filePathLength, arena->lineBuildInfo.funcNameLength, actualMessageLength);
				if (newLine != nullptr)
				{
					//NOTE: The GetConsoleFilePath and similar functions below Assert that a null-term char exists for each text element
					//      We need to fill the text space with 0's before we can use the functions below to get the pointers and fill the space properly
					char* textSpace = (char*)(newLine + 1);
					MyMemSet(textSpace, 0x00, totalTextLength);
					
					char* filePathSpace     = GetConsoleLineFilePath(newLine); Assert_(filePathSpace     != nullptr);
					char* functionNameSpace = GetConsoleLineFuncName(newLine); Assert_(functionNameSpace != nullptr);
					char* messageSpace      = GetConsoleLineMessage(newLine);  Assert_(messageSpace      != nullptr);
					
					char* lineBuildFilePathPntr = &arena->lineBuildSpace[0];
					char* lineBuildFuncNamePntr = &arena->lineBuildSpace[arena->lineBuildInfo.filePathLength+1];
					char* lineBuildMessagePntr = &arena->lineBuildSpace[arena->lineBuildInfo.filePathLength+1 + arena->lineBuildInfo.funcNameLength+1];
					
					MyMemCopy(filePathSpace,     lineBuildFilePathPntr, arena->lineBuildInfo.filePathLength+1);
					MyMemCopy(functionNameSpace, lineBuildFuncNamePntr, arena->lineBuildInfo.funcNameLength+1);
					MyMemCopy(messageSpace,      lineBuildMessagePntr,  arena->lineBuildInfo.messageLength+1);
					if (lineLength > 0) { MyMemCopy(&messageSpace[arena->lineBuildInfo.messageLength], linePntr, lineLength); }
					messageSpace[arena->lineBuildInfo.messageLength + lineLength] = '\0';
					
					newLine->flags = arena->lineBuildInfo.flags;
					newLine->time = arena->lineBuildInfo.time;
					newLine->dbgLevel = arena->lineBuildInfo.dbgLevel;
					newLine->fileLineNumber = arena->lineBuildInfo.fileLineNumber;
					
					result = newLine;
				}
				
				arena->lineBuildUsage = 0;
			}
			else
			{
				u32 filePathLength = MyStrLength32(filePath);
				u32 funcNameLength = MyStrLength32(funcName);
				u32 totalTextLength = filePathLength+1 + funcNameLength+1 + lineLength+1;
				ConsoleLine_t* newLine = ConsoleArenaAllocLine(arena, filePathLength, funcNameLength, lineLength);
				if (newLine != nullptr)
				{
					//NOTE: The GetConsoleFilePath and similar functions below Assert that a null-term char exists for each text element
					//      We need to fill the text space with 0's before we can use the functions below to get the pointers and fill the space properly
					char* textSpace = (char*)(newLine + 1);
					MyMemSet(textSpace, 0x00, totalTextLength);
					
					char* filePathSpace     = GetConsoleLineFilePath(newLine); Assert_(filePathSpace     != nullptr);
					char* functionNameSpace = GetConsoleLineFuncName(newLine); Assert_(functionNameSpace != nullptr);
					char* messageSpace      = GetConsoleLineMessage(newLine);  Assert_(messageSpace      != nullptr);
					
					MyMemCopy(filePathSpace,     filePath, filePathLength+1);
					MyMemCopy(functionNameSpace, funcName, funcNameLength+1);
					if (lineLength > 0) { MyMemCopy(messageSpace, linePntr, lineLength); }
					messageSpace[lineLength] = '\0';
					
					newLine->flags = flags;
					newLine->time = ProgramTime;
					newLine->dbgLevel = dbgLevel;
					newLine->fileLineNumber = lineNumber;
					
					result = newLine;
				}
			}
			lineStart = cIndex+1;
		}
	}
	if (lineStart < messageLength && arena->lineBuildUsage+1 < arena->lineBuildSize)
	{
		Assert_(arena->lineBuildSpace != nullptr);
		u32 remainingTextLength = messageLength - lineStart;
		if (arena->lineBuildUsage > 0)
		{
			if (remainingTextLength >= arena->lineBuildSize - arena->lineBuildUsage) { remainingTextLength = (arena->lineBuildSize - arena->lineBuildUsage) - 1; }
			if (remainingTextLength > 0)
			{
				MyMemCopy(&arena->lineBuildSpace[arena->lineBuildUsage], &message[lineStart], remainingTextLength);
				arena->lineBuildUsage += remainingTextLength;
				arena->lineBuildInfo.messageLength += remainingTextLength;
			}
		}
		else
		{
			u32 filePathLength = MyStrLength32(filePath);
			u32 funcNameLength = MyStrLength32(funcName);
			ClearStruct(arena->lineBuildInfo);
			
			if (filePathLength+1 + funcNameLength+1 < arena->lineBuildSize)
			{
				if (filePathLength+1 + funcNameLength+1 + remainingTextLength+1 > arena->lineBuildSize) { remainingTextLength = (arena->lineBuildSize-1 - (filePathLength+1) - (funcNameLength+1)); }
				
				arena->lineBuildInfo.filePathLength = filePathLength;
				arena->lineBuildInfo.funcNameLength = funcNameLength;
				arena->lineBuildInfo.messageLength = remainingTextLength;
				arena->lineBuildInfo.flags = flags;
				arena->lineBuildInfo.time = ProgramTime;
				arena->lineBuildInfo.dbgLevel = dbgLevel;
				arena->lineBuildInfo.fileLineNumber = lineNumber;
				
				arena->lineBuildUsage = filePathLength+1 + funcNameLength+1 + remainingTextLength;
				
				u32 cIndex = 0;
				if (filePathLength > 0)
				{
					Assert_(cIndex + filePathLength <= arena->lineBuildSize);
					MyMemCopy(&arena->lineBuildSpace[cIndex], filePath, filePathLength); cIndex += filePathLength;
				}
				arena->lineBuildSpace[cIndex] = '\0'; cIndex++;
				if (funcNameLength > 0)
				{
					Assert_(cIndex + funcNameLength <= arena->lineBuildSize);
					MyMemCopy(&arena->lineBuildSpace[cIndex], funcName, funcNameLength); cIndex += funcNameLength;
				}
				arena->lineBuildSpace[cIndex] = '\0'; cIndex++;
				if (remainingTextLength > 0)
				{
					Assert_(cIndex + remainingTextLength <= arena->lineBuildSize);
					MyMemCopy(&arena->lineBuildSpace[cIndex], &message[lineStart], remainingTextLength); cIndex += remainingTextLength;
				}
				arena->lineBuildSpace[cIndex] = '\0'; cIndex++;
				Assert_(cIndex == arena->lineBuildUsage+1);
				
				if (result == nullptr) { result = &arena->lineBuildInfo; }
			}
		}
	}
	
	return result;
}

ConsoleLine_t* ConsoleArenaPrintAndProcessVa(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* formatStr, va_list args, u32 printSize) //pre-declared in app_console_arena.h
{
	Assert_(arena != nullptr);
	Assert_(arena->fifoSpace != nullptr);
	Assert_(formatStr != nullptr);
	if (arena->printArena != nullptr)
	{
		if (printSize > 0)
		{
			if (arena->printArena->type == MemoryArenaType_Temp) { ArenaPushMark(arena->printArena); }
			char* printBuffer = PushArray(arena->printArena, char, (u32)printSize+1);
			if (printBuffer != nullptr)
			{
				i32 printResult = vsnprintf(printBuffer, printSize+1, formatStr, args);
				Assert_(printResult == (i32)printSize);
				printBuffer[printSize] = '\0';
				ConsoleLine_t* result = ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, printBuffer);
				if (arena->printArena->type == MemoryArenaType_Temp) { ArenaPopMark(arena->printArena); }
				else if (ArenaCanPop(arena->printArena, printBuffer)) { ArenaPop(arena->printArena, printBuffer); }
				return result;
			}
			else
			{
				if (arena->printArena->type == MemoryArenaType_Temp) { ArenaPopMark(arena->printArena); }
				return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr);
			}
		}
		else
		{
			return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr);
		}
	}
	else if (arena->printBufferSize > 0)
	{
		Assert(arena->printBuffer != nullptr);
		int printResultLength = vsnprintf(arena->printBuffer, arena->printBufferSize, formatStr, args);
		if (printResultLength >= 0 && (u32)printResultLength < arena->printBufferSize)
		{
			arena->printBuffer[printResultLength] = '\0';
			return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, arena->printBuffer);
		}
		else
		{
			return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr);
		}
	}
	else
	{
		return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr);
	}
}
ConsoleLine_t* ConsoleArenaPrintAndProcess(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* formatStr, ...) //pre-declared in app_console_arena.h
{
	va_list args;
	va_start(args, formatStr);
	i32 printResult = vsnprintf(nullptr, 0, formatStr, args);
	va_end(args);
	if (printResult >= 0)
	{
		va_start(args, formatStr);
		ConsoleLine_t* result = ConsoleArenaPrintAndProcessVa(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr, args, (u32)printResult);
		va_end(args);
		return result;
	}
	else
	{
		return ConsoleArenaProcessChars(arena, flags, filePath, lineNumber, funcName, dbgLevel, addNewLine, formatStr);
	}
}

#define CslArenaWrite_D(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, message)
#define CslArenaPrint_D(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_D(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  message)
#define CslArenaPrintLine_D(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Dx(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Debug, false, message)
#define CslArenaPrint_Dx(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Dx(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  message)
#define CslArenaPrintLine_Dx(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_R(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, message)
#define CslArenaPrint_R(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_R(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  message)
#define CslArenaPrintLine_R(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Rx(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Regular, false, message)
#define CslArenaPrint_Rx(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Rx(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  message)
#define CslArenaPrintLine_Rx(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_I(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Info, false, message)
#define CslArenaPrint_I(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_I(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  message)
#define CslArenaPrintLine_I(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Ix(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Info, false, message)
#define CslArenaPrint_Ix(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Ix(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Info, true,  message)
#define CslArenaPrintLine_Ix(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_N(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, message)
#define CslArenaPrint_N(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_N(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  message)
#define CslArenaPrintLine_N(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Nx(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Notify, false, message)
#define CslArenaPrint_Nx(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Nx(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  message)
#define CslArenaPrintLine_Nx(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_O(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Other, false, message)
#define CslArenaPrint_O(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_O(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  message)
#define CslArenaPrintLine_O(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Ox(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Other, false, message)
#define CslArenaPrint_Ox(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Ox(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Other, true,  message)
#define CslArenaPrintLine_Ox(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_W(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, message)
#define CslArenaPrint_W(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_W(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  message)
#define CslArenaPrintLine_W(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Wx(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Warning, false, message)
#define CslArenaPrint_Wx(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Wx(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  message)
#define CslArenaPrintLine_Wx(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)

#define CslArenaWrite_E(arena, message)                       ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Error, false, message)
#define CslArenaPrint_E(arena, formatString, ...)             ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_E(arena, message)                   ConsoleArenaProcessChars   ((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  message)
#define CslArenaPrintLine_E(arena, formatString, ...)         ConsoleArenaPrintAndProcess((arena), DbgFlags_None, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)
#define CslArenaWrite_Ex(arena, flags, message)               ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Error, false, message)
#define CslArenaPrint_Ex(arena, flags, formatString, ...)     ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define CslArenaWriteLine_Ex(arena, flags, message)           ConsoleArenaProcessChars   ((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Error, true,  message)
#define CslArenaPrintLine_Ex(arena, flags, formatString, ...) ConsoleArenaPrintAndProcess((arena), (flags),       __FILE__, __LINE__, __func__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)

