/*
File:   main_menu.h
Author: Taylor Robbins
Date:   01\28\2020
*/

#ifndef _MAIN_MENU_H
#define _MAIN_MENU_H

struct MainMenuData_t
{
	bool initialized;
	bool activatedWipe;
	bool pushWipeDoneState;
	TransInfoType_t wipeDoneTransInfoType;
	void* wipeDoneTransInfoPntr;
	AppState_t wipeDoneState;
};

#endif //  _MAIN_MENU_H
