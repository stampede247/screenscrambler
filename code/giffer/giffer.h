/*
File:   giffer.h
Author: Taylor Robbins
Date:   09\30\2019
*/

#ifndef _GIFFER_H
#define _GIFFER_H

struct GifferData_t
{
	bool initialized;
	
	u32 frameStart;
	u32 frameEnd;
	
	bool isPlaying;
	r32 frameTime;
	bool frameChanged;
	u32 currentFrame;
	Texture_t viewTexture;
	
	r32 timelineScroll;
	r32 timelineScrollGoto;
	r32 timelineMaxScroll;
	r32 timelineZoom;
	
	r32 viewScale;
	rec viewRec;
	rec tickGutterRec;
	rec timelineRec;
	rec begBtnRec;
	rec endBtnRec;
	rec exportBtnRec;
	rec discardBtnRec;
	
	GifStream_t stream;
	bool exportGif;
};

#endif //  _GIFFER_H
