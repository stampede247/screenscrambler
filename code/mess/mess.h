/*
File:   mess.h
Author: Taylor Robbins
Date:   10\21\2018
*/

#ifndef _MESS_H
#define _MESS_H

struct MessData_t
{
	bool initialized;
	
	bool activatedWipe;
	AppState_t wipeDoneState;
	
	v2 viewPos;
	v2 targetPos;
	
	r32 shaderTime;
};

#endif //  _MESS_H
