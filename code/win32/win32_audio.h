/*
File:   win32_audio.h
Author: Taylor Robbins
Date:   05\13\2020
*/

#ifndef _WIN_32_AUDIO_H
#define _WIN_32_AUDIO_H

#if AUDIO_ENABLED && USE_CUSTOM_AUDIO

#define DIRECT_SOUND_CREATE_DEFINITION(name) HRESULT WINAPI name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef DIRECT_SOUND_CREATE_DEFINITION(DirectSoundCreate_f);

#define DIRECT_SOUND_ENUMERATE_DEFINITION(name) HRESULT name(LPDSENUMCALLBACK lpDSEnumCallback, LPVOID lpContext)
typedef DIRECT_SOUND_ENUMERATE_DEFINITION(DirectSoundEnumerate_f);

#define GET_DEVICE_ID_DEFINITION(name) HRESULT name(LPCGUID pGuidSrc, LPGUID pGuidDest)
typedef GET_DEVICE_ID_DEFINITION(GetDeviceID_f);

struct AudioDevice_t
{
	MemoryArena_t* allocArena;
	char* name;
	char* subName;
	char* moduleCode;
	
	GUID guid;
};

class EndpointNotificationClient : public IMMNotificationClient
{
public:
	EndpointNotificationClient() { }
	~EndpointNotificationClient() { }
	
	HRESULT OnDefaultDeviceChanged(EDataFlow flow, ERole role, LPCWSTR pwstrDefaultDeviceId);
	HRESULT OnDeviceAdded(LPCWSTR pwstrDeviceId);
	HRESULT OnDeviceRemoved(LPCWSTR pwstrDeviceId);
	HRESULT OnDeviceStateChanged(LPCWSTR pwstrDeviceId, DWORD dwNewState);
	HRESULT OnPropertyValueChanged(LPCWSTR pwstrDeviceId, const PROPERTYKEY key);
	
	HRESULT QueryInterface(const IID& something1, void** something2) { return S_OK; }
	ULONG AddRef() { return 0; }
	ULONG Release() { return 0; }
};

struct Win32_AudioBuffer_t
{
	u32 sampleSize; //bytes/sample/channel
	u32 sampleRate; //samples/sec
	u32 numChannels;
	u32 numSamples; //samples/channel
	
	LPDIRECTSOUNDBUFFER pntr;
};

struct SampleDisplay_t
{
	u32 numChunks;
	r32* chunkValues;
};

struct AudioInfo_t
{
	bool loaded;
	char* loadError;
	
	u32 currentAudioDeviceIndex;
	u32 bufferLength;
	u32 playCursorPos;
	u32 writeCursorPos;
	SampleDisplay_t bufferDisplay;
	u32 lastWriteBufferIndex;
	u64 lastWriteSampleIndex;
	
	HMODULE module;
	LPDIRECTSOUND interfacePntr;
	Win32_AudioBuffer_t primaryBuffer;
	Win32_AudioBuffer_t secondaryBuffer;
	DirectSoundCreate_f* DirectSoundCreate;
	
	IMMDeviceEnumerator* deviceEnumerator;
	EndpointNotificationClient* notificationClient;
};

struct AudioOutput_t
{
	u64 sampleIndex;
	u32 sampleRate;
	u32 numChannels;
	u32 sampleSize;
	
	u32 numSamples; //not taking 2 channels into account
	i16* samples; //these samples start at writeCursor
};

#endif //AUDIO_ENABLED && USE_CUSTOM_AUDIO

#endif //  _WIN_32_AUDIO_H
