/*
File:   win32_performance.cpp
Author: Taylor Robbins
Date:   01\26\2020
Description: 
	** Holds functions that help us profile and keep track of time spent in various aspects of the application
*/

void _Win32_PerformanceInit()
{
	LARGE_INTEGER perfCountFrequencyLarge;
	QueryPerformanceFrequency(&perfCountFrequencyLarge);
	platform.perfCountFrequency = (i64)perfCountFrequencyLarge.QuadPart;
}

// +==============================+
// |      Win32_GetPerfTime       |
// +==============================+
// PerfTime_t GetPerfTime()
GetPerfTime_DEFINITION(Win32_GetPerfTime)
{
	PerfTime_t result = {};
	BOOL queryResult = QueryPerformanceCounter(&result.perfCount);
	Assert(queryResult != 0);
	result.cycleCount = __rdtsc();
	return result;
}

// +==============================+
// |    Win32_GetPerfTimeDiff     |
// +==============================+
// r64 GetPerfTimeDiff(const PerfTime_t* tStart, const PerfTime_t* tEnd)
GetPerfTimeDiff_DEFINITION(Win32_GetPerfTimeDiff)
{
	Assert(tStart != nullptr);
	Assert(tEnd != nullptr);
	r64 resultSecs = ((r64)(tEnd->perfCount.QuadPart - tStart->perfCount.QuadPart) / (r64)platform.perfCountFrequency);
	return resultSecs * 1000.0;
}
