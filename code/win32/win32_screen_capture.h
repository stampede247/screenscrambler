/*
File:   win32_screen_capture.h
Author: Taylor Robbins
Date:   09\13\2020
*/

#ifndef _WIN_32_SCREEN_CAPTURE_H
#define _WIN_32_SCREEN_CAPTURE_H

struct OtherWindowIter_t
{
	u32 windowIndex;
	HWND windowHandle;
};

struct OtherWindow_t
{
	u32 index;
	char* name;
	HWND handle;
};

struct ScreenCapture_t
{
	bool filled;
	v2i size;
	u32 dataSize;
	u8* data;
};

struct ScreenRecording_t
{
	bool recordingStarted;
	bool recordingScreen;
	OtherWindow_t window;
	reci sourceRec;
	v2i desktopSize;
	HWND desktopWindow;
	HDC windowContext;
	HDC bitmapContext;
	HBITMAP bitmapHandle;
	HDC deviceContext;
	BITMAPINFO bmpInfo;
	u8 padding[32]; //TODO: Figure out why we need padding after BITMAPINFO structure
	v2i bitmapSize;
	u32 bitmapDataSize;
	u8* bitmapData;
};

#endif //  _WIN_32_SCREEN_CAPTURE_H
