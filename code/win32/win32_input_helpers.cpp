/*
File:   win32_input_helpers.cpp
Author: Taylor Robbins
Date:   09\19\2020
Description: 
	** Holds some extra functions that the application can use to query input on each platform 
*/

i32 Win32_GetVirtualKeyCode(i32 glfwKeyCode)
{
	switch (glfwKeyCode)
	{
		case GLFW_KEY_ENTER:         return VK_RETURN;
		case GLFW_KEY_BACKSPACE:     return VK_BACK;
		case GLFW_KEY_TAB:           return VK_TAB;
		case GLFW_KEY_CAPS_LOCK:     return VK_CAPITAL;
		case GLFW_KEY_ESCAPE:        return VK_ESCAPE;
		case GLFW_KEY_SPACE:         return VK_SPACE;
		case GLFW_KEY_PAGE_UP:       return VK_PRIOR;
		case GLFW_KEY_PAGE_DOWN:     return VK_NEXT;
		case GLFW_KEY_END:           return VK_END;
		case GLFW_KEY_HOME:          return VK_HOME;
		case GLFW_KEY_LEFT:          return VK_LEFT;
		case GLFW_KEY_UP:            return VK_UP;
		case GLFW_KEY_RIGHT:         return VK_RIGHT;
		case GLFW_KEY_DOWN:          return VK_DOWN;
		case GLFW_KEY_INSERT:        return VK_INSERT;
		case GLFW_KEY_DELETE:        return VK_DELETE;
		case GLFW_KEY_0:             return 0x30;
		case GLFW_KEY_1:             return 0x31;
		case GLFW_KEY_2:             return 0x32;
		case GLFW_KEY_3:             return 0x33;
		case GLFW_KEY_4:             return 0x34;
		case GLFW_KEY_5:             return 0x35;
		case GLFW_KEY_6:             return 0x36;
		case GLFW_KEY_7:             return 0x37;
		case GLFW_KEY_8:             return 0x38;
		case GLFW_KEY_9:             return 0x39;
		case GLFW_KEY_A:             return 0x41;
		case GLFW_KEY_B:             return 0x42;
		case GLFW_KEY_C:             return 0x43;
		case GLFW_KEY_D:             return 0x44;
		case GLFW_KEY_E:             return 0x45;
		case GLFW_KEY_F:             return 0x46;
		case GLFW_KEY_G:             return 0x47;
		case GLFW_KEY_H:             return 0x48;
		case GLFW_KEY_I:             return 0x49;
		case GLFW_KEY_J:             return 0x4A;
		case GLFW_KEY_K:             return 0x4B;
		case GLFW_KEY_L:             return 0x4C;
		case GLFW_KEY_M:             return 0x4D;
		case GLFW_KEY_N:             return 0x4E;
		case GLFW_KEY_O:             return 0x4F;
		case GLFW_KEY_P:             return 0x50;
		case GLFW_KEY_Q:             return 0x51;
		case GLFW_KEY_R:             return 0x52;
		case GLFW_KEY_S:             return 0x53;
		case GLFW_KEY_T:             return 0x54;
		case GLFW_KEY_U:             return 0x55;
		case GLFW_KEY_V:             return 0x56;
		case GLFW_KEY_W:             return 0x57;
		case GLFW_KEY_X:             return 0x58;
		case GLFW_KEY_Y:             return 0x59;
		case GLFW_KEY_Z:             return 0x5A;
		case GLFW_KEY_KP_0:          return VK_NUMPAD0;
		case GLFW_KEY_KP_1:          return VK_NUMPAD1;
		case GLFW_KEY_KP_2:          return VK_NUMPAD2;
		case GLFW_KEY_KP_3:          return VK_NUMPAD3;
		case GLFW_KEY_KP_4:          return VK_NUMPAD4;
		case GLFW_KEY_KP_5:          return VK_NUMPAD5;
		case GLFW_KEY_KP_6:          return VK_NUMPAD6;
		case GLFW_KEY_KP_7:          return VK_NUMPAD7;
		case GLFW_KEY_KP_8:          return VK_NUMPAD8;
		case GLFW_KEY_KP_9:          return VK_NUMPAD9;
		case GLFW_KEY_KP_MULTIPLY:   return VK_MULTIPLY;
		case GLFW_KEY_KP_ADD:        return VK_ADD;
		case GLFW_KEY_KP_SUBTRACT:   return VK_SUBTRACT;
		case GLFW_KEY_KP_DECIMAL:    return VK_DECIMAL;
		case GLFW_KEY_KP_DIVIDE:     return VK_DIVIDE;
		case GLFW_KEY_F1:            return VK_F1;
		case GLFW_KEY_F2:            return VK_F2;
		case GLFW_KEY_F3:            return VK_F3;
		case GLFW_KEY_F4:            return VK_F4;
		case GLFW_KEY_F5:            return VK_F5;
		case GLFW_KEY_F6:            return VK_F6;
		case GLFW_KEY_F7:            return VK_F7;
		case GLFW_KEY_F8:            return VK_F8;
		case GLFW_KEY_F9:            return VK_F9;
		case GLFW_KEY_F10:           return VK_F10;
		case GLFW_KEY_F11:           return VK_F11;
		case GLFW_KEY_F12:           return VK_F12;
		case GLFW_KEY_NUM_LOCK:      return VK_NUMLOCK;
		case GLFW_KEY_LEFT_SHIFT:    return VK_LSHIFT;
		case GLFW_KEY_RIGHT_SHIFT:   return VK_RSHIFT;
		case GLFW_KEY_LEFT_CONTROL:  return VK_LCONTROL;
		case GLFW_KEY_RIGHT_CONTROL: return VK_RCONTROL;
		case GLFW_KEY_LEFT_ALT:      return VK_LMENU; //TODO: This might not be right?
		case GLFW_KEY_RIGHT_ALT:     return VK_RMENU; //TODO: This might not be right?
		case GLFW_KEY_SEMICOLON:     return VK_OEM_1; //TODO: This might not always be right?
		case GLFW_KEY_EQUAL:         return VK_OEM_PLUS;
		case GLFW_KEY_COMMA:         return VK_OEM_COMMA;
		case GLFW_KEY_MINUS:         return VK_OEM_MINUS;
		case GLFW_KEY_PERIOD:        return VK_OEM_PERIOD;
		case GLFW_KEY_SLASH:         return VK_OEM_2; //TODO: This might not always be right?
		case GLFW_KEY_GRAVE_ACCENT:  return VK_OEM_3; //TODO: This might not always be right?
		case GLFW_KEY_LEFT_BRACKET:  return VK_OEM_4; //TODO: This might not always be right?
		case GLFW_KEY_BACKSLASH:     return VK_OEM_5; //TODO: This might not always be right?
		case GLFW_KEY_RIGHT_BRACKET: return VK_OEM_6; //TODO: This might not always be right?
		case GLFW_KEY_APOSTROPHE:    return VK_OEM_7; //TODO: This might not always be right?
		default: return -1;
		
		//Unmapped Virtual Keys (There's a few more unlisted ones as well)
		// VK_CANCEL     VK_XBUTTON1   VK_XBUTTON2
		// VK_CLEAR      VK_SHIFT      VK_CONTROL
		// VK_MENU       VK_PAUSE      VK_KANA
		// VK_HANGUEL    VK_HANGUL     VK_JUNJA
		// VK_FINAL      VK_HANJA      VK_KANJI
		// VK_CONVERT    VK_NONCONVERT VK_ACCEPT
		// VK_MODECHANGE VK_SELECT     VK_PRINT
		// VK_EXECUTE    VK_SNAPSHOT   VK_HELP
		// VK_LWIN       VK_RWIN       VK_APPS
		// VK_SLEEP      VK_SEPARATOR  VK_F13
		// VK_F14        VK_F15        VK_F16
		// VK_F17        VK_F18        VK_F19
		// VK_F20        VK_F21        VK_F22
		// VK_F23        VK_F24        VK_SCROLL
		// VK_OEM_8      VK_PROCESSKEY VK_PACKET
		// VK_ATTN       VK_CRSEL      VK_EXSEL
		// VK_EREOF      VK_PLAY       VK_ZOOM
		// VK_PA1        VK_OEM_CLEAR
	}
}

// +==============================+
// |   Win32_GetGlobalKeyState    |
// +==============================+
// bool GetGlobalKeyState(Buttons_t button)
GetGlobalKeyState_DEFINITION(Win32_GetGlobalKeyState)
{
	Assert(button < Buttons_NumButtons);
	
	i32 keyCodes[3] = {};
	u8 numKeys = 0;
	if (button == MouseButton_Left)
	{
		numKeys = 1;
		keyCodes[0] = VK_LBUTTON;
	}
	else if (button == MouseButton_Right)
	{
		numKeys = 1;
		keyCodes[0] = VK_RBUTTON;
	}
	else if (button == MouseButton_Middle)
	{
		numKeys = 1;
		keyCodes[0] = VK_MBUTTON;
	}
	else
	{
		numKeys = Plat_GetKeysForButton(button, &keyCodes[0]);
		for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
		{
			keyCodes[keyIndex] = Win32_GetVirtualKeyCode(keyCodes[keyIndex]);
		}
	}
	
	bool result = false;
	for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
	{
		if (keyCodes[keyIndex] != -1)
		{
			SHORT keyState = GetAsyncKeyState(keyCodes[keyIndex]);
			if (IsFlagSet(keyState, 0x8000))
			{
				result = true;
				break;
			}
		}
	}
	return result;
}

