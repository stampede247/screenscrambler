/*
File:   win32_files.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _WIN_32_FILES_H
#define _WIN_32_FILES_H

struct FileInfo_t
{
	u32 size;
	void* content;
};

struct OpenFile_t
{
	bool isOpen;
	bool openedForWriting;
	u64 cursorIndex;
	u64 fileSize;
	HANDLE handle;
};

#endif //  _WIN_32_FILES_H
