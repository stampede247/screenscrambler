/*
File:   win32_http.h
Author: Taylor Robbins
Date:   10\04\2019
*/

#ifndef _WIN_32_HTTP_H
#define _WIN_32_HTTP_H

#define MAX_HTTP_URL_LENGTH  255
#define MAX_HTTP_POST_FIELDS 8

typedef enum
{
	HttpRequestType_Get = 0,
	HttpRequestType_Post,
} HttpRequestType_t;

typedef void HttpCallback_f(bool success, const struct HttpRequest_t* request, const char* responseData, u32 responseLength);

struct HttpRequest_t
{
	const char* url;
	HttpRequestType_t requestType;
	u32 numPostFields;
	const char* postKeys[MAX_HTTP_POST_FIELDS];
	const char* postValues[MAX_HTTP_POST_FIELDS];
	u32 rawPayloadLength;
	const char* rawPayload;
	HttpCallback_f* callback;
};

#endif //  _WIN_32_HTTP_H
