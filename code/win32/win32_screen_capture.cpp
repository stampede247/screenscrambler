/*
File:   win32_screen_capture.cpp
Author: Taylor Robbins
Date:   09\13\2020
Description: 
	** Holds functions that help the program retrive bitmap data of what is being presented on the screen by various applications
*/

//NOTE: The calling code should always clear the OtherWindowIter_t to 0
//      before calling this function for the first time
// +==============================+
// |     Win32_GetOtherWindow     |
// +==============================+
// bool GetOtherWindow(OtherWindowIter_t* iter, MemoryArena_t* memArena, OtherWindow_t* windowOut)
GetOtherWindow_DEFINITION(Win32_GetOtherWindow)
{
	Assert(iter != nullptr);
	
	if (iter->windowIndex == 0 && iter->windowHandle == NULL)
	{
		iter->windowHandle = GetDesktopWindow();
		if (iter->windowHandle == NULL) { WriteLine_E("Couldn't get desktop window!"); return false; }
	}
	
	char nameBuffer[64] = {};
	u32 stepIndex = 0;
	do
	{
		iter->windowHandle = GetWindow(iter->windowHandle, (iter->windowIndex == 0 && stepIndex == 0) ? GW_CHILD : GW_HWNDNEXT);
		if (iter->windowHandle == NULL) { break; }
		
		RECT clientRect = {};
		BOOL getClientRectResult = GetClientRect(iter->windowHandle, &clientRect);
		if (getClientRectResult != 0 && clientRect.right > 0 && clientRect.bottom > 0)
		{
			int windowNameLength = GetWindowTextA(iter->windowHandle, &nameBuffer[0], ArrayCount(nameBuffer));
			nameBuffer[ArrayCount(nameBuffer)-1] = '\0';
			if (windowNameLength > 0)
			{
				if (!StrCompareIgnoreCaseNt(nameBuffer, "Default IME"))
				{
					break;
				}
			}
		}
		stepIndex++;
	} while (iter->windowHandle != NULL);
	if (iter->windowHandle == NULL) { return false; }
	iter->windowIndex++;
	
	if (windowOut != nullptr)
	{
		Assert(memArena != nullptr);
		ClearPointer(windowOut);
		windowOut->index = iter->windowIndex-1;
		windowOut->handle = iter->windowHandle;
		int windowNameLength = GetWindowTextLengthA(iter->windowHandle);
		if (windowNameLength > 0)
		{
			windowOut->name = PushArray(memArena, char, (u32)windowNameLength+1);
			Assert(windowOut->name != nullptr);
			MyMemSet(windowOut->name, 0x00, (u32)windowNameLength+1);
			int windowNameLength2 = GetWindowTextA(iter->windowHandle, windowOut->name, (u32)windowNameLength+1);
			Assert(windowNameLength2 == windowNameLength);
		}
		else
		{
			windowOut->name = ArenaNtString(memArena, "");
			Assert(windowOut->name != nullptr);
		}
	}
	
	return true;
}

// +==============================+
// |  Win32_CaptureScreenBitmap   |
// +==============================+
// bool CaptureScreenBitmap(const OtherWindow_t* window, reci sourceRec, ScreenCapture_t* captureOut)
CaptureScreenBitmap_DEFINITION(Win32_CaptureScreenBitmap)
{
	Assert(captureOut != nullptr);
	bool autoSize = (sourceRec.width == 0 && sourceRec.height == 0);
	Assert((sourceRec.width > 0 && sourceRec.height > 0) || autoSize);
	ClearPointer(captureOut);
	
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	HWND desktopWindowHandle = GetDesktopWindow();
	
	v2i captureSize = NewVec2i(screenWidth, screenHeight);
	HWND captureWindow = GetDesktopWindow();
	if (window != nullptr)
	{
		captureWindow = window->handle;
		RECT clientRect = {};
		BOOL getClientRectResult = GetClientRect(captureWindow, &clientRect);
		if (getClientRectResult == 0) { WriteLine_E("Couldn't find the client rect for the specified window!"); return false; }
		if (clientRect.right <= 0 || clientRect.bottom <= 0) { WriteLine_E("The client rect is empty for that window"); return false; }
		captureSize.width = (i32)clientRect.right;
		captureSize.height = (i32)clientRect.bottom;
	}
	
	HDC windowContext = GetDC(captureWindow);
	HDC bitmapContext = CreateCompatibleDC(windowContext);
	HBITMAP bitmapHandle = CreateCompatibleBitmap(windowContext, captureSize.width, captureSize.height);
	HBITMAP oldBitmap = (HBITMAP)SelectObject(bitmapContext, bitmapHandle);
	BitBlt(bitmapContext, 0, 0, captureSize.width, captureSize.height, windowContext, 0, 0, SRCCOPY|CAPTUREBLT);
	SelectObject(bitmapContext, oldBitmap);
	
	HDC deviceContext = GetDC(NULL);
	BITMAPINFO bmpInfo = {};
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	GetDIBits(deviceContext, bitmapHandle, 0, 0, NULL, &bmpInfo, DIB_RGB_COLORS);
	
	if (bmpInfo.bmiHeader.biSizeImage <= 0)
	{
		u32 pixelSize = CeilDivI32((i32)bmpInfo.bmiHeader.biBitCount, 8);
		bmpInfo.bmiHeader.biSizeImage = (DWORD)(bmpInfo.bmiHeader.biWidth * AbsI64(bmpInfo.bmiHeader.biHeight) * pixelSize);
		// PrintLine_D("Intuiting bitmap data size: %dx%d %u-bit(%u byte) = %d bytes", bmpInfo.bmiHeader.biWidth, bmpInfo.bmiHeader.biHeight, bmpInfo.bmiHeader.biBitCount, pixelSize, bmpInfo.bmiHeader.biSizeImage);
	}
	// else { PrintLine_D("Bitmap data is %d bytes", bmpInfo.bmiHeader.biSizeImage); }
	
	u8* bitmapData = (u8*)MyMalloc(bmpInfo.bmiHeader.biSizeImage);
	Assert(bitmapData != nullptr);
	// MyMemSet(bitmapData, 0x00, bmpInfo.bmiHeader.biSizeImage);
	GetDIBits(deviceContext, bitmapHandle, 0, bmpInfo.bmiHeader.biHeight, bitmapData, &bmpInfo, DIB_RGB_COLORS);
	
	Assert(bmpInfo.bmiHeader.biSizeImage > 0);
	captureOut->size = NewVec2i((i32)bmpInfo.bmiHeader.biWidth, (i32)bmpInfo.bmiHeader.biHeight);
	#if 0
	captureOut->dataSize = (u32)bmpInfo.bmiHeader.biSizeImage;
	captureOut->data = (u8*)MyMalloc(captureOut->dataSize);
	Assert(captureOut->data != nullptr);
	// MyMemSet(captureOut->data, 0x00, captureOut->dataSize);
	
	for (u32 xOffset = 0; xOffset < (u32)bmpInfo.bmiHeader.biWidth; xOffset++)
	{
		for (u32 yOffset = 0; yOffset < (u32)bmpInfo.bmiHeader.biHeight; yOffset++)
		{
			const u32* srcValue = (const u32*)(&bitmapData[sizeof(u32) * (xOffset + ((bmpInfo.bmiHeader.biHeight-1 - yOffset) * bmpInfo.bmiHeader.biWidth))]);
			Color_t* destValue = (Color_t*)&captureOut->data[sizeof(u32) * (xOffset + (yOffset * bmpInfo.bmiHeader.biWidth))];
			destValue->a = 255;//(u8)(((*srcValue) & 0xFF000000) >> 24);
			destValue->b = (u8)(((*srcValue) & 0x00FF0000) >> 16);
			destValue->g = (u8)(((*srcValue) & 0x0000FF00) >> 8);
			destValue->r = (u8)(((*srcValue) & 0x000000FF) >> 0);
			b g r a - r g b a
		}
	}
	free(bitmapData);
	#else
	captureOut->dataSize = (u32)bmpInfo.bmiHeader.biSizeImage;
	captureOut->data = bitmapData;
	#endif
	
	ReleaseDC(NULL, deviceContext);
	
	DeleteDC(bitmapContext);
	DeleteObject(bitmapHandle);
	
	captureOut->filled = true;
	return true;
}

// +===============================+
// | Win32_FreeScreenCaptureMemory |
// +===============================+
// void FreeScreenCaptureMemory(ScreenCapture_t* capture)
FreeScreenCaptureMemory_DEFINITION(Win32_FreeScreenCaptureMemory)
{
	Assert(capture != nullptr);
	if (capture->data != nullptr) { MyFree(capture->data); }
	ClearPointer(capture);
}

//TODO: Add more error checking here and support for sourceRec
// +==============================+
// |  Win32_StartScreenRecording  |
// +==============================+
// bool StartScreenRecording(const OtherWindow_t* window, reci sourceRec, ScreenRecording_t* recordingOut)
StartScreenRecording_DEFINITION(Win32_StartScreenRecording)
{
	Assert(recordingOut != nullptr);
	ClearPointer(recordingOut);
	if (window != nullptr)
	{
		MyMemCopy(&recordingOut->window, window, sizeof(OtherWindow_t));
		recordingOut->window.name = nullptr;
		recordingOut->recordingScreen = false;
	}
	else
	{
		recordingOut->recordingScreen = true;
	}
	recordingOut->sourceRec = sourceRec;
	
	recordingOut->desktopSize.width  = GetSystemMetrics(SM_CXSCREEN);
	recordingOut->desktopSize.height = GetSystemMetrics(SM_CYSCREEN);
	recordingOut->desktopWindow = GetDesktopWindow();
	if (window != nullptr)
	{
		RECT clientRect = {};
		BOOL getClientRectResult = GetClientRect(window->handle, &clientRect);
		if (getClientRectResult == 0) { WriteLine_E("Couldn't find the client rect for the specified window!"); return false; }
		if (clientRect.right <= 0 || clientRect.bottom <= 0) { WriteLine_E("The client rect is empty for that window"); return false; }
		recordingOut->bitmapSize.width  = (i32)clientRect.right;
		recordingOut->bitmapSize.height = (i32)clientRect.bottom;
		PrintLine_D("Starting recording of window at resolution: %dx%d", recordingOut->bitmapSize.width, recordingOut->bitmapSize.height);
	}
	else
	{
		recordingOut->bitmapSize = recordingOut->desktopSize;
		recordingOut->window.handle = recordingOut->desktopWindow;
	}
	
	recordingOut->windowContext = GetDC(recordingOut->window.handle);
	recordingOut->bitmapContext = CreateCompatibleDC(recordingOut->windowContext);
	recordingOut->bitmapHandle = CreateCompatibleBitmap(recordingOut->windowContext, recordingOut->bitmapSize.width, recordingOut->bitmapSize.height);
	HBITMAP oldBitmap = (HBITMAP)SelectObject(recordingOut->bitmapContext, recordingOut->bitmapHandle);
	BitBlt(recordingOut->bitmapContext, 0, 0, recordingOut->bitmapSize.width, recordingOut->bitmapSize.height, recordingOut->windowContext, 0, 0, SRCCOPY|CAPTUREBLT);
	SelectObject(recordingOut->bitmapContext, oldBitmap);
	
	recordingOut->deviceContext = GetDC(NULL);
	ClearStruct(recordingOut->bmpInfo);
	recordingOut->bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	GetDIBits(recordingOut->deviceContext, recordingOut->bitmapHandle, 0, 0, NULL, &recordingOut->bmpInfo, DIB_RGB_COLORS);
	Assert(recordingOut->bmpInfo.bmiHeader.biWidth == recordingOut->bitmapSize.width);
	Assert(recordingOut->bmpInfo.bmiHeader.biHeight == recordingOut->bitmapSize.height);
	
	if (recordingOut->bmpInfo.bmiHeader.biSizeImage <= 0)
	{
		u32 pixelSize = CeilDivI32((i32)recordingOut->bmpInfo.bmiHeader.biBitCount, 8);
		recordingOut->bmpInfo.bmiHeader.biSizeImage = (DWORD)(recordingOut->bmpInfo.bmiHeader.biWidth * AbsI64(recordingOut->bmpInfo.bmiHeader.biHeight) * pixelSize);
		// PrintLine_D("Intuiting bitmap data size: %dx%d %u-bit(%u byte) = %d bytes", bmpInfo.bmiHeader.biWidth, bmpInfo.bmiHeader.biHeight, bmpInfo.bmiHeader.biBitCount, pixelSize, bmpInfo.bmiHeader.biSizeImage);
	}
	
	recordingOut->bitmapDataSize = (u32)recordingOut->bmpInfo.bmiHeader.biSizeImage;
	recordingOut->bitmapData = (u8*)MyMalloc(recordingOut->bitmapDataSize);
	Assert(recordingOut->bitmapData != nullptr);
	// MyMemSet(recordingOut->bitmapData, 0x00, recordingOut->bmpInfo.bmiHeader.biSizeImage);
	
	recordingOut->recordingStarted = true;
	return true;
}

// +==============================+
// | Win32_CaptureScreenRecording |
// +==============================+
// bool CaptureScreenRecording(ScreenRecording_t* recording)
CaptureScreenRecording_DEFINITION(Win32_CaptureScreenRecording)
{
	Assert(recording != nullptr);
	Assert(recording->recordingStarted);
	
	HBITMAP oldBitmap = (HBITMAP)SelectObject(recording->bitmapContext, recording->bitmapHandle);
	BitBlt(recording->bitmapContext, 0, 0, recording->bitmapSize.width, recording->bitmapSize.height, recording->windowContext, 0, 0, SRCCOPY|CAPTUREBLT);
	SelectObject(recording->bitmapContext, oldBitmap);
	
	int getBitsResult = GetDIBits(recording->deviceContext, recording->bitmapHandle, 0, recording->bmpInfo.bmiHeader.biHeight, recording->bitmapData, &recording->bmpInfo, DIB_RGB_COLORS);
	if (getBitsResult > 0)
	{
		Assert(recording->bmpInfo.bmiHeader.biWidth == recording->bitmapSize.width);
		Assert(recording->bmpInfo.bmiHeader.biHeight == recording->bitmapSize.height);
		return true;
	}
	else
	{
		return false;
	}
}

// +==============================+
// |  Win32_StopScreenRecording   |
// +==============================+
// void StopScreenRecording(ScreenRecording_t* recording)
StopScreenRecording_DEFINITION(Win32_StopScreenRecording)
{
	Assert(recording != nullptr);
	if (recording->recordingStarted)
	{
		ReleaseDC(NULL, recording->deviceContext);
		DeleteDC(recording->bitmapContext);
		DeleteObject(recording->bitmapHandle);
		ClearPointer(recording);
	}
}
