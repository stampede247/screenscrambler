/*
File:   win32_file_watching.h
Author: Taylor Robbins
Date:   07\01\2019
*/

#ifndef _WIN32_FILE_WATCHING_H
#define _WIN32_FILE_WATCHING_H

struct WatchedFile_t
{
	char* filePath;
	FILETIME lastWriteTime;
	bool changed;
};

struct WatchFilesInfo_t
{
	Mutex_t mutex;
	DynArray_t files;
};

#endif //  _WIN32_FILE_WATCHING_H
