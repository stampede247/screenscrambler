/*
File:   win32_threads.h
Author: Taylor Robbins
Date:   01\26\2020
*/

#ifndef _WIN_32_THREADS_H
#define _WIN_32_THREADS_H

#define WIN32_THREAD_FUNCTION(functionName, userPntr) DWORD WINAPI functionName(LPVOID userPntr)
typedef WIN32_THREAD_FUNCTION(Win32ThreadFunction_f, userPntr);

struct Thread_t
{
	HANDLE handle;
	DWORD id;
	Win32ThreadFunction_f* function;
	void* userPntr;
};

struct Mutex_t
{
	HANDLE handle;
};

struct Win32Semaphore_t
{
	HANDLE handle;
};

#endif //  _WIN_32_THREADS_H
