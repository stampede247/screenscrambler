/*
File:   win32_http.cpp
Author: Taylor Robbins
Date:   10\04\2019
Description: 
	** Provides some functions that help the application make HTTP requests using WinHTTP
*/

const char* GetHttpErrorString(DWORD errorCode)
{
	switch (errorCode)
	{
		case ERROR_WINHTTP_AUTO_PROXY_SERVICE_ERROR:              return "ERROR_WINHTTP_AUTO_PROXY_SERVICE_ERROR";
		case ERROR_WINHTTP_AUTODETECTION_FAILED:                  return "ERROR_WINHTTP_AUTODETECTION_FAILED";
		case ERROR_WINHTTP_BAD_AUTO_PROXY_SCRIPT:                 return "ERROR_WINHTTP_BAD_AUTO_PROXY_SCRIPT";
		case ERROR_WINHTTP_CANNOT_CALL_AFTER_OPEN:                return "ERROR_WINHTTP_CANNOT_CALL_AFTER_OPEN";
		case ERROR_WINHTTP_CANNOT_CALL_AFTER_SEND:                return "ERROR_WINHTTP_CANNOT_CALL_AFTER_SEND";
		case ERROR_WINHTTP_CANNOT_CALL_BEFORE_OPEN:               return "ERROR_WINHTTP_CANNOT_CALL_BEFORE_OPEN";
		case ERROR_WINHTTP_CANNOT_CALL_BEFORE_SEND:               return "ERROR_WINHTTP_CANNOT_CALL_BEFORE_SEND";
		case ERROR_WINHTTP_CANNOT_CONNECT:                        return "ERROR_WINHTTP_CANNOT_CONNECT";
		case ERROR_WINHTTP_CLIENT_AUTH_CERT_NEEDED:               return "ERROR_WINHTTP_CLIENT_AUTH_CERT_NEEDED";
		case ERROR_WINHTTP_CLIENT_CERT_NO_ACCESS_PRIVATE_KEY:     return "ERROR_WINHTTP_CLIENT_CERT_NO_ACCESS_PRIVATE_KEY";
		case ERROR_WINHTTP_CLIENT_CERT_NO_PRIVATE_KEY:            return "ERROR_WINHTTP_CLIENT_CERT_NO_PRIVATE_KEY";
		case ERROR_WINHTTP_CHUNKED_ENCODING_HEADER_SIZE_OVERFLOW: return "ERROR_WINHTTP_CHUNKED_ENCODING_HEADER_SIZE_OVERFLOW";
		case ERROR_WINHTTP_CONNECTION_ERROR:                      return "ERROR_WINHTTP_CONNECTION_ERROR";
		case ERROR_WINHTTP_HEADER_ALREADY_EXISTS:                 return "ERROR_WINHTTP_HEADER_ALREADY_EXISTS";
		case ERROR_WINHTTP_HEADER_COUNT_EXCEEDED:                 return "ERROR_WINHTTP_HEADER_COUNT_EXCEEDED";
		case ERROR_WINHTTP_HEADER_NOT_FOUND:                      return "ERROR_WINHTTP_HEADER_NOT_FOUND";
		case ERROR_WINHTTP_HEADER_SIZE_OVERFLOW:                  return "ERROR_WINHTTP_HEADER_SIZE_OVERFLOW";
		case ERROR_WINHTTP_INCORRECT_HANDLE_STATE:                return "ERROR_WINHTTP_INCORRECT_HANDLE_STATE";
		case ERROR_WINHTTP_INCORRECT_HANDLE_TYPE:                 return "ERROR_WINHTTP_INCORRECT_HANDLE_TYPE";
		case ERROR_WINHTTP_INTERNAL_ERROR:                        return "ERROR_WINHTTP_INTERNAL_ERROR";
		case ERROR_WINHTTP_INVALID_OPTION:                        return "ERROR_WINHTTP_INVALID_OPTION";
		case ERROR_WINHTTP_INVALID_QUERY_REQUEST:                 return "ERROR_WINHTTP_INVALID_QUERY_REQUEST";
		case ERROR_WINHTTP_INVALID_SERVER_RESPONSE:               return "ERROR_WINHTTP_INVALID_SERVER_RESPONSE";
		case ERROR_WINHTTP_INVALID_URL:                           return "ERROR_WINHTTP_INVALID_URL";
		case ERROR_WINHTTP_LOGIN_FAILURE:                         return "ERROR_WINHTTP_LOGIN_FAILURE";
		case ERROR_WINHTTP_NAME_NOT_RESOLVED:                     return "ERROR_WINHTTP_NAME_NOT_RESOLVED";
		case ERROR_WINHTTP_NOT_INITIALIZED:                       return "ERROR_WINHTTP_NOT_INITIALIZED";
		case ERROR_WINHTTP_OPERATION_CANCELLED:                   return "ERROR_WINHTTP_OPERATION_CANCELLED";
		case ERROR_WINHTTP_OPTION_NOT_SETTABLE:                   return "ERROR_WINHTTP_OPTION_NOT_SETTABLE";
		case ERROR_WINHTTP_OUT_OF_HANDLES:                        return "ERROR_WINHTTP_OUT_OF_HANDLES";
		case ERROR_WINHTTP_REDIRECT_FAILED:                       return "ERROR_WINHTTP_REDIRECT_FAILED";
		case ERROR_WINHTTP_RESEND_REQUEST:                        return "ERROR_WINHTTP_RESEND_REQUEST";
		case ERROR_WINHTTP_RESPONSE_DRAIN_OVERFLOW:               return "ERROR_WINHTTP_RESPONSE_DRAIN_OVERFLOW";
		case ERROR_WINHTTP_SCRIPT_EXECUTION_ERROR:                return "ERROR_WINHTTP_SCRIPT_EXECUTION_ERROR";
		case ERROR_WINHTTP_SECURE_CERT_CN_INVALID:                return "ERROR_WINHTTP_SECURE_CERT_CN_INVALID";
		case ERROR_WINHTTP_SECURE_CERT_DATE_INVALID:              return "ERROR_WINHTTP_SECURE_CERT_DATE_INVALID";
		case ERROR_WINHTTP_SECURE_CERT_REV_FAILED:                return "ERROR_WINHTTP_SECURE_CERT_REV_FAILED";
		case ERROR_WINHTTP_SECURE_CERT_REVOKED:                   return "ERROR_WINHTTP_SECURE_CERT_REVOKED";
		case ERROR_WINHTTP_SECURE_CERT_WRONG_USAGE:               return "ERROR_WINHTTP_SECURE_CERT_WRONG_USAGE";
		case ERROR_WINHTTP_SECURE_CHANNEL_ERROR:                  return "ERROR_WINHTTP_SECURE_CHANNEL_ERROR";
		case ERROR_WINHTTP_SECURE_FAILURE:                        return "ERROR_WINHTTP_SECURE_FAILURE";
		case ERROR_WINHTTP_SECURE_INVALID_CA:                     return "ERROR_WINHTTP_SECURE_INVALID_CA";
		case ERROR_WINHTTP_SECURE_INVALID_CERT:                   return "ERROR_WINHTTP_SECURE_INVALID_CERT";
		case ERROR_WINHTTP_SHUTDOWN:                              return "ERROR_WINHTTP_SHUTDOWN";
		case ERROR_WINHTTP_TIMEOUT:                               return "ERROR_WINHTTP_TIMEOUT";
		case ERROR_WINHTTP_UNABLE_TO_DOWNLOAD_SCRIPT:             return "ERROR_WINHTTP_UNABLE_TO_DOWNLOAD_SCRIPT";
		case ERROR_WINHTTP_UNHANDLED_SCRIPT_TYPE:                 return "ERROR_WINHTTP_UNHANDLED_SCRIPT_TYPE";
		case ERROR_WINHTTP_UNRECOGNIZED_SCHEME:                   return "ERROR_WINHTTP_UNRECOGNIZED_SCHEME";
		case ERROR_NOT_ENOUGH_MEMORY:                             return "ERROR_NOT_ENOUGH_MEMORY";
		case ERROR_INSUFFICIENT_BUFFER:                           return "ERROR_INSUFFICIENT_BUFFER";
		case ERROR_INVALID_HANDLE:                                return "ERROR_INVALID_HANDLE";
		case ERROR_NO_MORE_FILES:                                 return "ERROR_NO_MORE_FILES";
		case ERROR_NO_MORE_ITEMS:                                 return "ERROR_NO_MORE_ITEMS";
		case ERROR_NOT_SUPPORTED:                                 return "ERROR_NOT_SUPPORTED";
		default: return "UNKNOWN";
	};
}

u32 GetUrlHostNameLength(const char* fullUrl, u32 fullUrlLength)
{
	for (u32 cIndex = 0; cIndex < fullUrlLength; cIndex++)
	{
		if (fullUrl[cIndex] == '/' || fullUrl[cIndex] == '\\')
		{
			return cIndex;
		}
	}
	return fullUrlLength;
}

const char* GetUrlPathPart(const char* fullUrl, u32 fullUrlLength)
{
	for (u32 cIndex = 0; cIndex < fullUrlLength; cIndex++)
	{
		if (fullUrl[cIndex] == '/' || fullUrl[cIndex] == '\\')
		{
			return &fullUrl[cIndex];
		}
	}
	return nullptr;
}

char* EncodePostFields(MemoryArena_t* arenaPntr, u32 numFields, const char* const* keys, const char* const* values, u32* dataLengthOut)
{
	if (numFields == 0)
	{
		char* result = PushArray(arenaPntr, char, 1);
		result[0] = '\0';
		if (dataLengthOut != nullptr) { *dataLengthOut = 0; }
		return result;
	}
	
	u32 encodedSize = 0;
	for (u32 pIndex = 0; pIndex < numFields; pIndex++)
	{
		Assert(keys[pIndex] != nullptr);
		Assert(values[pIndex] != nullptr);
		
		u32 keyLength = MyStrLength32(keys[pIndex]);
		u32 valueLength = MyStrLength32(values[pIndex]);
		
		//TODO: Sanatize these strings!
		
		if (pIndex > 0) { encodedSize += 1; } //+1 for '&' between fields
		
		encodedSize += keyLength + 1 + valueLength; //+1 for '='
	}
	
	Assert(encodedSize > 0);
	
	char* result = PushArray(arenaPntr, char, encodedSize+1);
	u32 cIndex = 0;
	for (u32 pIndex = 0; pIndex < numFields; pIndex++)
	{
		Assert(keys[pIndex] != nullptr);
		Assert(values[pIndex] != nullptr);
		
		u32 keyLength = MyStrLength32(keys[pIndex]);
		u32 valueLength = MyStrLength32(values[pIndex]);
		
		//TODO: Sanatize these strings!
		
		if (pIndex > 0)
		{
			result[cIndex] = '&'; cIndex++;
		}
		
		MyMemCopy(&result[cIndex], keys[pIndex], keyLength); cIndex += keyLength;
		result[cIndex] = '='; cIndex++;
		MyMemCopy(&result[cIndex], values[pIndex], valueLength); cIndex += valueLength;
	}
	result[encodedSize] = '\0';
	
	if (dataLengthOut != nullptr) { *dataLengthOut = encodedSize; }
	return result;
}

void _Win32_CopyHttRequest(MemoryArena_t* memArena, HttpRequest_t* target, const HttpRequest_t* source)
{
	ClearPointer(target);
	MyMemCopy(target, source, sizeof(HttpRequest_t));
	target->url = nullptr;
	ClearArray(target->postKeys);
	ClearArray(target->postValues);
	
	u32 urlLength = MyStrLength32(source->url);
	target->url = PushArray(memArena, char, urlLength+1);
	MyMemCopy((char*)target->url, source->url, urlLength);
	((char*)target->url)[urlLength] = '\0';
	
	for (u32 pIndex = 0; pIndex < source->numPostFields; pIndex++)
	{
		u32 keyLength = MyStrLength32(source->postKeys[pIndex]);
		target->postKeys[pIndex] = PushArray(memArena, char, keyLength+1);
		MyMemCopy((char*)target->postKeys[pIndex], source->postKeys[pIndex], keyLength);
		((char*)target->postKeys[pIndex])[keyLength] = '\0';
		
		u32 valueLength = MyStrLength32(source->postValues[pIndex]);
		target->postValues[pIndex] = PushArray(memArena, char, valueLength+1);
		MyMemCopy((char*)target->postValues[pIndex], source->postValues[pIndex], valueLength);
		((char*)target->postValues[pIndex])[valueLength] = '\0';
	}
	
	if (source->rawPayloadLength > 0)
	{
		target->rawPayload = PushArray(memArena, char, source->rawPayloadLength);
		MyMemCopy((char*)target->rawPayload, source->rawPayload, source->rawPayloadLength);
		target->rawPayloadLength = source->rawPayloadLength;
	}
}

void _Win32_FreeHttpRequest(MemoryArena_t* memArena, HttpRequest_t* httpRequest)
{
	Assert(httpRequest != nullptr);
	
	if (httpRequest->url != nullptr)
	{
		ArenaPop(memArena, (void*)httpRequest->url);
		httpRequest->url = nullptr;
	}
	
	for (u32 pIndex = 0; pIndex < httpRequest->numPostFields; pIndex++)
	{
		if (httpRequest->postKeys[pIndex] != nullptr)
		{
			ArenaPop(memArena, (void*)httpRequest->postKeys[pIndex]);
			httpRequest->postKeys[pIndex] = nullptr;
		}
		if (httpRequest->postValues[pIndex] != nullptr)
		{
			ArenaPop(memArena, (void*)httpRequest->postValues[pIndex]);
			httpRequest->postValues[pIndex] = nullptr;
		}
	}
	
	if (httpRequest->rawPayload != nullptr)
	{
		ArenaPop(memArena, (void*)httpRequest->rawPayload);
		httpRequest->rawPayload = nullptr;
	}
}

void GenericHandleHttpEvent(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID infoPntr, DWORD infoLength)
{
	const char* statusStr = "Unknown";
	switch (status)
	{
		case WINHTTP_CALLBACK_STATUS_CLOSING_CONNECTION:      statusStr = "CLOSING_CONNECTION"; break;
		case WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER:     statusStr = "CONNECTED_TO_SERVER"; break;
		case WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER:    statusStr = "CONNECTING_TO_SERVER"; break;
		case WINHTTP_CALLBACK_STATUS_CONNECTION_CLOSED:       statusStr = "CONNECTION_CLOSED"; break;
		case WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE:          statusStr = "DATA_AVAILABLE"; break;
		case WINHTTP_CALLBACK_STATUS_HANDLE_CREATED:          statusStr = "HANDLE_CREATED"; break;
		case WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING:          statusStr = "HANDLE_CLOSING"; break;
		case WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:       statusStr = "HEADERS_AVAILABLE"; break;
		case WINHTTP_CALLBACK_STATUS_INTERMEDIATE_RESPONSE:   statusStr = "INTERMEDIATE_RESPONSE"; break;
		case WINHTTP_CALLBACK_STATUS_NAME_RESOLVED:           statusStr = "NAME_RESOLVED"; break;
		case WINHTTP_CALLBACK_STATUS_READ_COMPLETE:           statusStr = "READ_COMPLETE"; break;
		case WINHTTP_CALLBACK_STATUS_RECEIVING_RESPONSE:      statusStr = "RECEIVING_RESPONSE"; break;
		case WINHTTP_CALLBACK_STATUS_REDIRECT:                statusStr = "REDIRECT"; break;
		case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:           statusStr = "REQUEST_ERROR"; break;
		case WINHTTP_CALLBACK_STATUS_REQUEST_SENT:            statusStr = "REQUEST_SENT"; break;
		case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:          statusStr = "RESOLVING_NAME"; break;
		case WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED:       statusStr = "RESPONSE_RECEIVED"; break;
		case WINHTTP_CALLBACK_STATUS_SECURE_FAILURE:          statusStr = "SECURE_FAILURE"; break;
		case WINHTTP_CALLBACK_STATUS_SENDING_REQUEST:         statusStr = "SENDING_REQUEST"; break;
		case WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:    statusStr = "SENDREQUEST_COMPLETE"; break;
		case WINHTTP_CALLBACK_STATUS_WRITE_COMPLETE:          statusStr = "WRITE_COMPLETE"; break;
		case WINHTTP_CALLBACK_STATUS_GETPROXYFORURL_COMPLETE: statusStr = "GETPROXYFORURL_COMPLETE"; break;
		case WINHTTP_CALLBACK_STATUS_CLOSE_COMPLETE:          statusStr = "CLOSE_COMPLETE"; break;
		case WINHTTP_CALLBACK_STATUS_SHUTDOWN_COMPLETE:       statusStr = "SHUTDOWN_COMPLETE"; break;
	};
	
	PrintLine_D("HttpStatus %s(%04X): %p[%u] (%p,%p)", statusStr, status, infoPntr, infoLength, handle, context);
	
	switch (status)
	{
		case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:
		{
			Assert(infoLength == sizeof(WINHTTP_ASYNC_RESULT));
			WINHTTP_ASYNC_RESULT* asyncResultPntr = (WINHTTP_ASYNC_RESULT*)infoPntr;
			const char* functionName = "UnknownFunction";
			switch (asyncResultPntr->dwResult)
			{
				case 1: functionName = "WinHttpReceiveResponse"; break;
				case 2: functionName = "WinHttpQueryDataAvailable"; break;
				case 3: functionName = "WinHttpReadData"; break;
				case 4: functionName = "WinHttpWriteData"; break;
				case 5: functionName = "WinHttpSendRequest"; break;
			};
			PrintLine_D("\tFunction %s: 0x%04X %s", functionName, asyncResultPntr->dwError, GetHttpErrorString(asyncResultPntr->dwError));
		} break;
		case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:
		{
			PrintLine_D("\tName: \"%.*S\"", infoLength, (const char*)infoPntr);
		} break;
	};
}

void Win32_HttpStatusCallback(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID infoPntr, DWORD infoLength)
{
	// GenericHandleHttpEvent(handle, context, status, infoPntr, infoLength);
	
	if (status == WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE)
	{
		WriteLine_D("HTTP Request sent!");
		platform.httpRequestWriting = false;
		bool32 receiveResult = WinHttpReceiveResponse(platform.httpRequestHandle, NULL);
		if (!receiveResult)
		{
			WriteLine_D("WinHttpReceiveResponse Failed!");
			platform.httpRequestFailed = true;
		}
	}
	else if (status == WINHTTP_CALLBACK_STATUS_REQUEST_ERROR)
	{
		WriteLine_D("HTTP Request failed!");
		platform.httpRequestWriting = false;
		platform.httpRequestFailed = true;
	}
	else if (status == WINHTTP_CALLBACK_STATUS_RECEIVING_RESPONSE)
	{
		WriteLine_D("Getting HTTP response...");
		platform.httpRequestReading = true;
	}
	else if (status == WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE)
	{
		Assert(infoLength == sizeof(DWORD));
		
		u32 numBytesAvail = *((DWORD*)infoPntr);
		if (platform.httpResponseData == nullptr)
		{
			Assert(platform.httpResponseDataLength == 0);
			platform.httpResponseData = PushArray(&platform.mainHeap, char, numBytesAvail + 1);
		}
		else
		{
			char* newSpace = PushArray(&platform.mainHeap, char, platform.httpResponseDataLength + numBytesAvail + 1);
			Assert(newSpace != nullptr);
			MyMemCopy(newSpace, platform.httpResponseData, platform.httpResponseDataLength);
			ArenaPop(&platform.mainHeap, platform.httpResponseData);
			platform.httpResponseData = newSpace;
		}
		Assert(platform.httpResponseData != nullptr);
		
		char* newSpacePntr = platform.httpResponseData + platform.httpResponseDataLength;
		MyMemSet(newSpacePntr, 0x00, numBytesAvail+1);
		platform.httpResponseDataLength += numBytesAvail;
		
		DWORD numBytesDownloaded = 0;
		if (WinHttpReadData(platform.httpRequestHandle, (LPVOID)newSpacePntr, numBytesAvail, &numBytesDownloaded))
		{
			Assert(numBytesDownloaded == numBytesAvail);
			
			PrintLine_D("Download %u bytes (%u total)", numBytesDownloaded, platform.httpResponseDataLength);
		}
		else
		{
			PrintLine_D("WinHttpReadData error %u", GetLastError());
		}
	}
	else if (status == WINHTTP_CALLBACK_STATUS_READ_COMPLETE)
	{
		WriteLine_D("Finished getting HTTP response");
		platform.httpRequestSucceeded = true;
	}
	else
	{
		if (status != WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING &&
			status != WINHTTP_CALLBACK_STATUS_HANDLE_CREATED &&
			status != WINHTTP_CALLBACK_STATUS_SENDING_REQUEST &&
			status != WINHTTP_CALLBACK_STATUS_REQUEST_SENT &&
			status != WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED &&
			status != WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE &&
			status != WINHTTP_CALLBACK_STATUS_RESOLVING_NAME &&
			status != WINHTTP_CALLBACK_STATUS_NAME_RESOLVED &&
			status != WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER &&
			status != WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER)
		{
			GenericHandleHttpEvent(handle, context, status, infoPntr, infoLength);
		}
	}
}

void _Win32_UpdateHttpRequest()
{
	// +==============================+
	// |     Update Http Request      |
	// +==============================+
	if (platform.performingHttpRequest && platform.httpRequestReading)
	{
		if (!WinHttpQueryDataAvailable(platform.httpRequestHandle, NULL))
		{
			PrintLine_E("WinHttpQueryDataAvailable error %u", GetLastError());
		}
	}
	
	// +==============================+
	// |    HTTP Request Callback     |
	// +==============================+
	if (platform.performingHttpRequest && (platform.httpRequestFailed || platform.httpRequestSucceeded))
	{
		WinHttpCloseHandle(platform.httpRequestHandle);
		WinHttpCloseHandle(platform.httpConnectionHandle);
		if (platform.httpRequest.callback != nullptr)
		{
			platform.httpRequest.callback(platform.httpRequestSucceeded, &platform.httpRequest, platform.httpResponseData, platform.httpResponseDataLength);
		}
		if (platform.httpResponseData != nullptr) { ArenaPop(&platform.mainHeap, platform.httpResponseData); platform.httpResponseData = nullptr; }
		if (platform.httpEncodedData != nullptr) { ArenaPop(&platform.mainHeap, platform.httpEncodedData); platform.httpEncodedData = nullptr; }
		_Win32_FreeHttpRequest(&platform.mainHeap, &platform.httpRequest);
		platform.performingHttpRequest = false;
	}
}

// +==============================+
// |    Win32_HttpRequestStart    |
// +==============================+
// bool HttpRequestStart(const HttpRequest_t* request)
HttpRequestStart_DEFINITION(Win32_HttpRequestStart)
{
	
	if (request == nullptr) { WriteLine_E("request is nullptr"); return false; }
	if (request->url[0] == '\0') { WriteLine_E("URL is empty!"); return false; }
	if (request->requestType != HttpRequestType_Get && request->requestType != HttpRequestType_Post) { WriteLine_E("Unknown request type!"); return false; }
	//TODO: Check to make sure the url is null-terminated
	if (!platform.httpSessionHandle) { WriteLine_E("No HTTP session!"); return false; }
	if (platform.performingHttpRequest) { WriteLine_E("Already performing!"); return false; }
	if (MyStrLength32(request->url) > MAX_HTTP_URL_LENGTH) { WriteLine_E("URL is too long"); return false; }
	if (request->numPostFields > MAX_HTTP_POST_FIELDS) { WriteLine_E("Too many POST fields"); return false; }
	
	// PrintLine_D("Making an HTTP request to \"%s\"...", request->url);
	TempPushMark();
	
	u32 hostNameLength = GetUrlHostNameLength(request->url, MyStrLength32(request->url));
	const char* urlPath = GetUrlPathPart(request->url, MyStrLength32(request->url));
	if (urlPath == nullptr) { urlPath = &request->url[MyStrLength(request->url)]; }
	u32 urlPathLength = urlPathLength = MyStrLength32(urlPath);
	Assert(hostNameLength <= MAX_HTTP_URL_LENGTH);
	Assert(urlPathLength <= MAX_HTTP_URL_LENGTH);
	
	// PrintLine_D("Host: \"%.*s\" Path: \"%s\"", hostNameLength, request->url, urlPath);
	
	wchar_t hostNameWide[MAX_HTTP_URL_LENGTH+1];
	MyMemSet(hostNameWide, 0x00, sizeof(hostNameWide));
	mbstowcs(hostNameWide, request->url, hostNameLength);
	hostNameWide[hostNameLength] = L'\0';
	
	wchar_t urlPathWide[MAX_HTTP_URL_LENGTH+1];
	MyMemSet(urlPathWide, 0x00, sizeof(urlPathWide));
	if (urlPathLength > 0)
	{
		mbstowcs(urlPathWide, urlPath, urlPathLength);
		urlPathWide[urlPathLength] = L'\0';
	}
	
	platform.httpEncodedDataLength = 0;
	platform.httpEncodedData = nullptr;
	if (request->rawPayloadLength > 0)
	{
		Assert(request->rawPayload != nullptr);
		platform.httpEncodedDataLength = request->rawPayloadLength;
		platform.httpEncodedData = ArenaString(&platform.mainHeap, request->rawPayload, request->rawPayloadLength);
	}
	if (request->numPostFields > 0)
	{
		platform.httpEncodedData = EncodePostFields(&platform.mainHeap, request->numPostFields, &request->postKeys[0], &request->postValues[0], &platform.httpEncodedDataLength);
	}
	
	platform.httpConnectionHandle = WinHttpConnect(platform.httpSessionHandle, hostNameWide, INTERNET_DEFAULT_HTTPS_PORT, 0);
	if (platform.httpConnectionHandle)
	{
		wchar_t* actionWideStr = L"GET";
		if (request->requestType == HttpRequestType_Post) { actionWideStr = L"POST"; }
		
		platform.httpRequestHandle = WinHttpOpenRequest(platform.httpConnectionHandle,
			actionWideStr,
			urlPathWide,
			NULL,
			WINHTTP_NO_REFERER,
			WINHTTP_DEFAULT_ACCEPT_TYPES,
			WINHTTP_FLAG_SECURE
		);
		
		if (platform.httpRequestHandle)
		{
			wchar_t* headersWideStr = L"Content-Type: application/x-www-form-urlencoded\r\n";
			if (request->rawPayloadLength > 0)
			{
				headersWideStr = L"Content-Type: application/octet-stream\r\n";
			}
			
			WinHttpSetOption(platform.httpRequestHandle, WINHTTP_OPTION_CONTEXT_VALUE, (void*)&platform.httpRequest, sizeof(void*));
			
			platform.performingHttpRequest = true;
			_Win32_CopyHttRequest(&platform.mainHeap, &platform.httpRequest, request);
			platform.httpResponseDataLength = 0;
			platform.httpResponseData = nullptr;
			TempPopMark();
			
			bool32 requestResult = WinHttpSendRequest(platform.httpRequestHandle,
				headersWideStr, (DWORD)-1,
				platform.httpEncodedData, platform.httpEncodedDataLength, platform.httpEncodedDataLength,
				0
			);
			
			if (requestResult)
			{
				platform.httpRequestWriting = true;
				platform.httpRequestFailed = false;
				platform.httpRequestSucceeded = false;
				platform.httpRequestReading = false;
				return true;
			}
			else
			{
				WriteLine_E("WinHttpSendRequest Failed!");
			}
			
			WinHttpCloseHandle(platform.httpRequestHandle);
		}
		else
		{
			WriteLine_E("WinHttpOpenRequest Failed!");
		}
		
		WinHttpCloseHandle(platform.httpConnectionHandle);
	}
	else
	{
		DWORD errorCode = GetLastError();
		switch (errorCode)
		{
			case ERROR_WINHTTP_INCORRECT_HANDLE_TYPE: WriteLine_E("WinHttpConnect Failed: WINHTTP_INCORRECT_HANDLE_TYPE"); break;
			case ERROR_WINHTTP_INTERNAL_ERROR: WriteLine_E("WinHttpConnect Failed: WINHTTP_INTERNAL_ERROR"); break;
			case ERROR_WINHTTP_INVALID_URL: WriteLine_E("WinHttpConnect Failed: WINHTTP_INVALID_URL"); break;
			case ERROR_WINHTTP_OPERATION_CANCELLED: WriteLine_E("WinHttpConnect Failed: WINHTTP_OPERATION_CANCELLED"); break;
			case ERROR_WINHTTP_UNRECOGNIZED_SCHEME: WriteLine_E("WinHttpConnect Failed: WINHTTP_UNRECOGNIZED_SCHEME"); break;
			case ERROR_WINHTTP_SHUTDOWN: WriteLine_E("WinHttpConnect Failed: WINHTTP_SHUTDOWN"); break;
			case ERROR_NOT_ENOUGH_MEMORY: WriteLine_E("WinHttpConnect Failed: NOT_ENOUGH_MEMORY"); break;
			default: PrintLine_E("WinHttpConnect Failed: error %u", errorCode); break;
		};
	}
	
	//NOTE: If we get to this point the connection/request failed for some reason
	if (request->callback != nullptr)
	{
		request->callback(false, request, nullptr, 0);
	}
	TempPopMark();
	return false;
}

// +==============================+
// | Win32_HttpRequestInProgress  |
// +==============================+
// bool HttpRequestInProgress()
HttpRequestInProgress_DEFINITION(Win32_HttpRequestInProgress)
{
	return platform.performingHttpRequest;
}
