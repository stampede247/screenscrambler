/*
File:   win32_version.h
Author: Taylor Robbins
Date:   11\04\2017
Desription:
	** Defines the macros that contain the values for the current platform layer version number
	** for the win32 version of the platform layer.
	** The build number is incremented automatically by a python script that runs before each build of the platform layer
*/

#ifndef _WIN_32_VERSION_H
#define _WIN_32_VERSION_H

#define PLATFORM_VERSION_MAJOR    0
#define PLATFORM_VERSION_MINOR    10

//NOTE: Auto-incremented by a python script before each build
#define PLATFORM_VERSION_BUILD    367
//0.09 built 46 times
//0.08 built 117 times
//0.07 built 54 times
//0.06 built 31 times
//0.05 built 143 times
//0.04 built 44 times
//0.03 built 15 times
//0.02 built 53 times
//0.01 built 1214 times

#endif //  _WIN_32_VERSION_H
