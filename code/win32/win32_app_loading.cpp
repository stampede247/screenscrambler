/*
File:   win32_app_loading.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Contains functions that help the platform layer load the application DLL and find
	** the exported functions in order to call out to the application. If the DLL is not found
	** or fails to be loaded, or not all expected functions are found, then we will fill 
	** the functions in the LoadedApp_t structure with stub functions that don't do anything
	** so we can continue to run and maybe try loading the DLL later
*/

// +--------------------------------------------------------------+
// |                        Stub Functions                        |
// +--------------------------------------------------------------+
AppGetVersion_DEFINITION(AppGetVersion_Stub)
{
	Version_t version = { 0, 0, 0 };
	return version;
}
AppGetStartupOptions_DEFINITION(AppGetStartupOptions_Stub)
{
	
}
AppInitialize_DEFINITION(AppInitialize_Stub)
{
	
}
AppReloaded_DEFINITION(AppReloaded_Stub)
{
	
}
AppTaskFinished_DEFINITION(AppTaskFinished_Stub)
{
	
}
AppUpdate_DEFINITION(AppUpdate_Stub)
{
	
}
AppPerformTask_DEFINITION(AppPerformTask_Stub)
{
	
}
AppClosing_DEFINITION(AppClosing_Stub)
{
	
}
#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
AppGetAudioOutput_DEFINITION(AppGetAudioOutput_Stub)
{
	
}
#endif

// +--------------------------------------------------------------+
// |                        Main Functions                        |
// +--------------------------------------------------------------+
bool Win32_LoadDllCode(const char* appDllName, const char* tempDllName, LoadedApp_t* loadedApp)
{
	ClearPointer(loadedApp);
	
	loadedApp->lastWriteTime = Win32_GetFileWriteTime(appDllName);
	
	#if DEBUG
		u32 copyTries = 0;
		while (!CopyFileA(appDllName, tempDllName, false))
		{
			DWORD error = GetLastError();
			if (error != ERROR_SHARING_VIOLATION)
			{
				PrintLine_E("CopyFileA error: %u", GetLastError());
				copyTries++;
				
				if (copyTries >= 100) 
				{
					WriteLine_E("Could not copy DLL.");
					return false;
				}
			}
		}
		// PrintLine_E("Tried to copy %u times", copyTries);
		loadedApp->module = LoadLibraryA(tempDllName);
	#else
		loadedApp->module = LoadLibraryA(appDllName);
	#endif
	
	
	if (loadedApp->module != 0)
	{
		loadedApp->AppGetVersion        = (AppGetVersion_f*)        GetProcAddress(loadedApp->module, "App_GetVersion");
		loadedApp->AppGetStartupOptions = (AppGetStartupOptions_f*) GetProcAddress(loadedApp->module, "App_GetStartupOptions");
		loadedApp->AppInitialize        = (AppInitialize_f*)        GetProcAddress(loadedApp->module, "App_Initialize");
		loadedApp->AppReloaded          = (AppReloaded_f*)          GetProcAddress(loadedApp->module, "App_Reloaded");
		loadedApp->AppTaskFinished      = (AppTaskFinished_f*)      GetProcAddress(loadedApp->module, "App_TaskFinished");
		loadedApp->AppUpdate            = (AppUpdate_f*)            GetProcAddress(loadedApp->module, "App_Update");
		loadedApp->AppPerformTask       = (AppPerformTask_f*)       GetProcAddress(loadedApp->module, "App_PerformTask");
		loadedApp->AppClosing           = (AppClosing_f*)           GetProcAddress(loadedApp->module, "App_Closing");
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		loadedApp->AppGetAudioOutput    = (AppGetAudioOutput_f*)    GetProcAddress(loadedApp->module, "App_GetAudioOutput");
		#endif
		
		loadedApp->isValid = (
			loadedApp->AppGetVersion        != nullptr &&
			loadedApp->AppGetStartupOptions != nullptr &&
			loadedApp->AppInitialize        != nullptr &&
			loadedApp->AppReloaded          != nullptr &&
			loadedApp->AppTaskFinished      != nullptr &&
			loadedApp->AppUpdate            != nullptr &&
			loadedApp->AppPerformTask       != nullptr &&
			#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
			loadedApp->AppGetAudioOutput    != nullptr &&
			#endif
			loadedApp->AppClosing           != nullptr
		);
	}
	
	if (!loadedApp->isValid)
	{
		loadedApp->AppGetVersion        = AppGetVersion_Stub;
		loadedApp->AppGetStartupOptions = AppGetStartupOptions_Stub;
		loadedApp->AppInitialize        = AppInitialize_Stub;
		loadedApp->AppReloaded          = AppReloaded_Stub;
		loadedApp->AppTaskFinished      = AppTaskFinished_Stub;
		loadedApp->AppUpdate            = AppUpdate_Stub;
		loadedApp->AppPerformTask       = AppPerformTask_Stub;
		loadedApp->AppClosing           = AppClosing_Stub;
		#if AUDIO_ENABLED && USE_CUSTOM_AUDIO
		loadedApp->AppGetAudioOutput    = AppGetAudioOutput_Stub;
		#endif
	}
	
	loadedApp->version = loadedApp->AppGetVersion(nullptr);
	
	return loadedApp->isValid;
}

void Win32_FreeDllCode(LoadedApp_t* loadedApp)
{
	if (loadedApp != nullptr &&
		loadedApp->module != 0)
	{
		BOOL freeResult = FreeLibrary(loadedApp->module);
		Assert(freeResult);
	}
	
	ClearPointer(loadedApp);
}

//NOTE: Dll changes are only monitored in DEBUG mode.
#if DEBUG

// +--------------------------------------------------------------+
// |                     DllWatchingFunction                      |
// +--------------------------------------------------------------+
WIN32_THREAD_FUNCTION(DllWatchingFunction, userPntr)
{
	DllWatchingInfo_t* info = (DllWatchingInfo_t*)userPntr;
	if (info == nullptr) { return 1; }
	if (info->dllFilePath == nullptr) { return 1; }
	if (info->mutex.handle == NULL) { return 1; }
	
	while (true)
	{
		Win32_LockMutex(&info->mutex, INFINITE);
		FILETIME newDllFiletime = Win32_GetFileWriteTime(info->dllFilePath);
		if (CompareFileTime(&newDllFiletime, &info->lastWriteTime) != 0)
		{
			Win32_UnlockMutex(&info->mutex);
			Sleep(1000); //A fixed delay to make sure writing completes nicely
			Win32_LockMutex(&info->mutex, INFINITE);
			info->appChanged = true;
			info->lastWriteTime = Win32_GetFileWriteTime(info->dllFilePath);
		}
		Win32_UnlockMutex(&info->mutex);
		Sleep(500);
	}
	
	return 0;
}

bool Win32_CheckAppDllForChanges()
{
	bool result = false;
	
	if (Win32_LockMutex(&platform.dllWatchInfo.mutex, 0))
	{
		bool changed = platform.dllWatchInfo.appChanged;
		platform.dllWatchInfo.appChanged = false;
		Win32_UnlockMutex(&platform.dllWatchInfo.mutex);
		
		if (changed)
		{
			WriteLine_W("Application DLL changed. Attempting to reload...");
			
			Win32_FreeDllCode(&app);
			
			if (Win32_LoadDllCode(platform.appDllPath, platform.appDllTempPath, &app))
			{
				PrintLine_I("Loaded application version %u.%02u(%u)", app.version.major, app.version.minor, app.version.build);
				
				bool resetApplication = false;
				app.AppGetVersion(&resetApplication);
				if (resetApplication)
				{
					WriteLine_W("Resetting application");
					
					MyMemSet(platform.appMemory.permanantPntr, 0x00, platform.appMemory.permanantSize);
					MyMemSet(platform.appMemory.transientPntr, 0x00, platform.appMemory.transientSize);
					
					//TODO: Find a way to reset the opengl context or
					//		maybe re-open the window altogether
					//TODO: Stop all the application file watching
					//TODO: Close all files open by the application
					//TODO: Stop all threads created by the application
					Win32_DumpApplicationAllocations();
					
					app.AppInitialize(&platform.info, &platform.appMemory, platform.currentInput);
				}
				else
				{
					app.AppReloaded(&platform.info, &platform.appMemory);
				}
				
				result = true;
			}
			else
			{
				Win32_ExitOnError("Could not load application DLL!");
			}
		}
	}
	
	return result;
}

#endif //DEBUG
