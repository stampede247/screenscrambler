/*
File:   win32_helpers.cpp
Author: Taylor Robbins
Date:   03\22\2019
Description: 
	** This file contains many of the various functions that the win32 platform layer uses
	** to perform tasks that don't really have a home in any other file.
*/

char* Win32_GetWorkingDirectory(MemoryArena_t* memArena)
{
	Assert(memArena != nullptr);
	
	DWORD pathLength = GetCurrentDirectory(0, nullptr);
	if (pathLength <= 0) { return nullptr; }
	
	char* result = PushArray(memArena, char, pathLength+2);
	DWORD resultLength = GetCurrentDirectory(pathLength+1, result);
	Assert(resultLength <= pathLength);
	if (result[resultLength] != '\\' && result[resultLength] != '/') { result[resultLength] = '\\'; resultLength++; }
	result[resultLength] = '\0';
	return result;
}

char* Win32_GetExecutablePath(MemoryArena_t* memArena)
{
	Assert(memArena != nullptr);
	
	DWORD pathLength = 0;
	{
		TempPushMark();
		u32 tempBufferSize = 512;
		char* tempBuffer = TempArray(char, tempBufferSize);
		pathLength = GetModuleFileNameA(0, tempBuffer, tempBufferSize);
		TempPopMark();
	}
	if (pathLength <= 0) { return nullptr; }
	
	char* result = PushArray(memArena, char, pathLength+1);
	DWORD resultLength = GetModuleFileNameA(0, result, pathLength+1);
	Assert(resultLength == pathLength);
	result[resultLength] = '\0';
	return result;
}

inline FILETIME Win32_GetFileWriteTime(const char* filePath)
{
	FILETIME lastWriteTime = {};
	WIN32_FILE_ATTRIBUTE_DATA attData;
	if (GetFileAttributesExA(filePath, GetFileExInfoStandard, &attData))
	{
		lastWriteTime = attData.ftLastWriteTime;
	}
	return lastWriteTime;
}

void Win32_UpdateWindowTitle(const char* baseName)
{
	#if DEBUG
	const char* newTitle = TempPrint("%s (Platform %u.%02u:%03u App %u.%02u:%03u)", baseName,
		platform.version.major, platform.version.minor, platform.version.build,
		app.version.major, app.version.minor, app.version.build
	);
	#else
	const char* newTitle = baseName;
	#endif
	
	if (MyStrCompareNt(newTitle, platform.windowTitle) != 0)
	{
		PrintLine_D("Window title changed: \"%s\" -> \"%s\"", platform.windowTitle, newTitle);
		ArenaPop(&platform.mainHeap, platform.windowTitle);
		platform.windowTitle = ArenaNtString(&platform.mainHeap, newTitle);
		glfwSetWindowTitle(platform.window, platform.windowTitle);
	}
}

void Win32_UpdateAppInputTimeInfo(AppInput_t* appInput)
{
	Assert(appInput != nullptr);
	
	r64 lastTimeF = appInput->programTimeF;
	u64 lastTime = appInput->programTime;
	appInput->programTimeF = glfwGetTime() * 1000.0;
	appInput->programTime = (u64)(appInput->programTimeF);
	appInput->elapsedMs = appInput->programTimeF - lastTimeF;
	if (appInput->windowInteractionHappened)
	{
		// PrintLine_D("Clamping elapsedMs from %lf to %lf", appInput->elapsedMs, (1000.0 / 60.0));
		appInput->elapsedMs = (1000.0 / 60.0);
	}
	appInput->framerate = 1000.0 / appInput->elapsedMs;
	appInput->timeDelta = appInput->elapsedMs / (1000.0 / appInput->targetFramerate);
	appInput->avgElapsedMs = ((appInput->avgElapsedMs * (NUM_FRAMERATE_AVGS-1)) + appInput->elapsedMs) / NUM_FRAMERATE_AVGS;
	appInput->avgFramerate = ((appInput->avgFramerate * (NUM_FRAMERATE_AVGS-1)) + appInput->framerate) / NUM_FRAMERATE_AVGS;
	
	// SYSTEMTIME systemTime = {};
	// GetSystemTime(&systemTime);
	time_t timeStruct = time(NULL);
	u64 timestamp = (u64)timeStruct; //TODO: Will this work on other platforms?
	
	appInput->systemTime = {};
	GetRealTime(timestamp, false, &appInput->systemTime);
	
	// appInput->localTime = {};
	// GetRealTime(localTimestamp, false, &appInput->localTime);
	
	SYSTEMTIME localTime = {};
	GetLocalTime(&localTime);
	appInput->localTime = {};
	//TODO: Fill the timestamp
	appInput->localTime.year         = localTime.wYear;
	appInput->localTime.month        = (u8)(localTime.wMonth-1);
	appInput->localTime.day          = (u8)(localTime.wDay-1);
	appInput->localTime.hour         = (u8)localTime.wHour;
	appInput->localTime.minute       = (u8)localTime.wMinute;
	appInput->localTime.second       = (u8)localTime.wSecond;
	// appInput->localTime.millisecond  = localTime.wMilliseconds;
	
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		ButtonState_t* buttonState = &appInput->buttons[bIndex];
		if (buttonState->transCount == 0)
		{
			buttonState->lastTransTime += (r32)appInput->elapsedMs;
		}
	}
	for (u32 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
	{
		if (appInput->gamepads[pIndex].isConnected)
		{
			for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
			{
				ButtonState_t* buttonState = &appInput->gamepads[pIndex].buttons[bIndex];
				if (buttonState->transCount == 0)
				{
					buttonState->lastTransTime += (r32)appInput->elapsedMs;
				}
			}
		}
	}
}

void Win32_UpdateManualFrameWaitingEnabled(AppInput_t* appInput)
{
	r32 targetElapsedTime = (1000.0f / appInput->targetFramerate);
	// PrintLine_D("%.1f >= %.1f", appInput->elapsedMs, targetElapsedTime);
	if (appInput->elapsedMs >= targetElapsedTime - PLAT_FLIP_SYNC_DETECT_THRESHOLD)
	{
		platform.numFramesFlipSuccessfullySynced++;
		platform.numFramesFlipFailedToSync = 0;
	}
	else
	{
		platform.numFramesFlipSuccessfullySynced = 0;
		platform.numFramesFlipFailedToSync++;
	}
	if (!platform.doingManualUpdateLoopWait && platform.numFramesFlipFailedToSync >= PLAT_MANUAL_WAIT_KICK_IN_TIME)
	{
		WriteLine_D("Starting manual frame waiting");
		platform.doingManualUpdateLoopWait = true;
	}
	// if (platform.doingManualUpdateLoopWait && platform.numFramesFlipSuccessfullySynced >= PLAT_MANUAL_WAIT_KICK_IN_TIME)
	// {
	// 	WriteLine_D("Stopping manual frame waiting");
	// 	platform.doingManualUpdateLoopWait = false;
	// }
	appInput->doingManualUpdateLoopWait = platform.doingManualUpdateLoopWait;
}

void Win32_ExitOnError(const char* outputString)
{
		WriteLine_E(outputString);
		if (platform.glfwLastErrorStr != nullptr)
		{
			outputString = MallocPrint("%s\nLast GLFW Error %d: \"%s\"", outputString, platform.glfwLastErrorCode, platform.glfwLastErrorStr);
		}
		MessageBoxA(NULL, outputString, "Initialization Error Encountered!", MB_OK);
		if (platform.glfwInitialized)
		{
			glfwTerminate();
		}
		exit(1);
}

bool Win32_AreGlfwVideoModesEqual(const GLFWvidmode* mode1, const GLFWvidmode* mode2)
{
	if (mode1->width != mode2->width)             { return false; }
	if (mode1->height != mode2->height)           { return false; }
	if (mode1->redBits != mode2->redBits)         { return false; }
	if (mode1->greenBits != mode2->greenBits)     { return false; }
	if (mode1->blueBits != mode2->blueBits)       { return false; }
	if (mode1->refreshRate != mode2->refreshRate) { return false; }
	return true;
}

void Win32_GetOpenglContextVersion(u32* versionMajorOut, u32* versionMinorOut)
{
	Assert(versionMajorOut != nullptr);
	Assert(versionMinorOut != nullptr);
	*versionMajorOut = 0;
	*versionMinorOut = 0;
	
	const char* openglVersionStr = (const char*)glGetString(GL_VERSION);
	if (openglVersionStr == nullptr) { return; }
	u32 openglVersionNumCount = 0;
	u32* openglVersionNums = ParseNumbersInString(TempArena, openglVersionStr, MyStrLength32(openglVersionStr), &openglVersionNumCount);
	if (openglVersionNums == nullptr) { return; }
	if (openglVersionNumCount >= 1) { *versionMajorOut = openglVersionNums[0]; }
	if (openglVersionNumCount >= 2) { *versionMinorOut = openglVersionNums[1]; }
}

#if AUDIO_ENABLED && !USE_CUSTOM_AUDIO
const char* Win32_GetOpenAlErrorStr(ALenum errorCode)
{
	switch (errorCode)
	{
		case AL_NO_ERROR:         return "AL_NO_ERROR";
		case AL_INVALID_NAME:     return "AL_INVALID_NAME";
		case AL_INVALID_ENUM:     return "AL_INVALID_ENUM";
		case AL_INVALID_VALUE:    return "AL_INVALID_VALUE";
		case AL_INVALID_OPERATION:return "AL_INVALID_OPERATION";
		case AL_OUT_OF_MEMORY:    return "AL_OUT_OF_MEMORY";
		default: return "Unknown";
	}
}
bool Win32_PrintOpenAlError(const char* lastCallName)
{
	ALenum errorCode = alGetError();
	if (errorCode != AL_NO_ERROR)
	{
		PrintLine_E("OpenAL Error after %s call: %s", (lastCallName != nullptr) ? lastCallName : "some", Win32_GetOpenAlErrorStr(errorCode));
		return true;
	}
	else { return false; }
}
#endif

// +==============================+
// |   Win32_CallAppInitialize    |
// +==============================+
WIN32_THREAD_FUNCTION(Win32_CallAppInitialize, userPntr)
{
	app.AppInitialize(&platform.info, &platform.appMemory, platform.currentInput);
	platform.appLoadingFinished = true;
	return 0;
}

const char* Win32_GetWindowsMsgStr(unsigned int msgType)
{
	switch (msgType)
	{
		case WM_CAPTURECHANGED:             return "WM_CAPTURECHANGED";
		case WM_CHAR:                       return "WM_CHAR";
		case WM_CLOSE:                      return "WM_CLOSE";
		case WM_DEVICECHANGE:               return "WM_DEVICECHANGE";
		case WM_DISPLAYCHANGE:              return "WM_DISPLAYCHANGE";
		case WM_DPICHANGED:                 return "WM_DPICHANGED";
		case WM_DROPFILES:                  return "WM_DROPFILES";
		case WM_DWMCOMPOSITIONCHANGED:      return "WM_DWMCOMPOSITIONCHANGED";
		case WM_ENTERMENULOOP:              return "WM_ENTERMENULOOP";
		case WM_ENTERSIZEMOVE:              return "WM_ENTERSIZEMOVE";
		case WM_ERASEBKGND:                 return "WM_ERASEBKGND";
		case WM_EXITMENULOOP:               return "WM_EXITMENULOOP";
		case WM_EXITSIZEMOVE:               return "WM_EXITSIZEMOVE";
		case WM_GETDPISCALEDSIZE:           return "WM_GETDPISCALEDSIZE";
		case WM_GETMINMAXINFO:              return "WM_GETMINMAXINFO";
		case WM_INPUT:                      return "WM_INPUT";
		case WM_INPUTLANGCHANGE:            return "WM_INPUTLANGCHANGE";
		case WM_KEYDOWN:                    return "WM_KEYDOWN";
		case WM_KEYUP:                      return "WM_KEYUP";
		case WM_KILLFOCUS:                  return "WM_KILLFOCUS";
		case WM_LBUTTONDOWN:                return "WM_LBUTTONDOWN";
		case WM_LBUTTONUP:                  return "WM_LBUTTONUP";
		case WM_MBUTTONDOWN:                return "WM_MBUTTONDOWN";
		case WM_MBUTTONUP:                  return "WM_MBUTTONUP";
		case WM_MOUSEACTIVATE:              return "WM_MOUSEACTIVATE";
		case WM_MOUSEHWHEEL:                return "WM_MOUSEHWHEEL";
		case WM_MOUSELEAVE:                 return "WM_MOUSELEAVE";
		case WM_MOUSEMOVE:                  return "WM_MOUSEMOVE";
		case WM_MOUSEWHEEL:                 return "WM_MOUSEWHEEL";
		case WM_MOVE:                       return "WM_MOVE";
		case WM_NCACTIVATE:                 return "WM_NCACTIVATE";
		case WM_NCCREATE:                   return "WM_NCCREATE";
		case WM_NCPAINT:                    return "WM_NCPAINT";
		case WM_PAINT:                      return "WM_PAINT";
		case WM_RBUTTONDOWN:                return "WM_RBUTTONDOWN";
		case WM_RBUTTONUP:                  return "WM_RBUTTONUP";
		case WM_SETCURSOR:                  return "WM_SETCURSOR";
		case WM_SETFOCUS:                   return "WM_SETFOCUS";
		case WM_SIZE:                       return "WM_SIZE";
		case WM_SIZING:                     return "WM_SIZING";
		case WM_SYSCHAR:                    return "WM_SYSCHAR";
		case WM_SYSCOMMAND:                 return "WM_SYSCOMMAND";
		case WM_SYSKEYDOWN:                 return "WM_SYSKEYDOWN";
		case WM_SYSKEYUP:                   return "WM_SYSKEYUP";
		case WM_UNICHAR:                    return "WM_UNICHAR";
		case WM_XBUTTONDOWN:                return "WM_XBUTTONDOWN";
		case WM_XBUTTONUP:                  return "WM_XBUTTONUP";
		
		case BM_CLICK:                      return "BM_CLICK";
		case BM_GETCHECK:                   return "BM_GETCHECK";
		case BM_GETIMAGE:                   return "BM_GETIMAGE";
		case BM_GETSTATE:                   return "BM_GETSTATE";
		case BM_SETCHECK:                   return "BM_SETCHECK";
		case BM_SETDONTCLICK:               return "BM_SETDONTCLICK";
		case BM_SETIMAGE:                   return "BM_SETIMAGE";
		case BM_SETSTATE:                   return "BM_SETSTATE";
		case BM_SETSTYLE:                   return "BM_SETSTYLE";
		case CBEM_GETCOMBOCONTROL:          return "CBEM_GETCOMBOCONTROL";
		case CBEM_GETEDITCONTROL:           return "CBEM_GETEDITCONTROL";
		case CBEM_GETEXSTYLE:               return "CBEM_GETEXSTYLE";
		case CBEM_GETIMAGELIST:             return "CBEM_GETIMAGELIST";
		case CBEM_GETITEMA:                 return "CBEM_GETITEMA";
		case CBEM_GETITEMW:                 return "CBEM_GETITEMW";
		case CBEM_HASEDITCHANGED:           return "CBEM_HASEDITCHANGED";
		case CBEM_INSERTITEMW:              return "CBEM_INSERTITEMW";
		case CBEM_SETEXSTYLE:               return "CBEM_SETEXSTYLE";
		case CBEM_SETEXTENDEDSTYLE:         return "CBEM_SETEXTENDEDSTYLE";
		case CBEM_SETIMAGELIST:             return "CBEM_SETIMAGELIST";
		case CBEM_SETITEMA:                 return "CBEM_SETITEMA";
		case CBEM_SETITEMW:                 return "CBEM_SETITEMW";
		case CDM_LAST:                      return "CDM_LAST";
		case DL_BEGINDRAG:                  return "DL_BEGINDRAG";
		case DL_CANCELDRAG:                 return "DL_CANCELDRAG";
		case DL_DRAGGING:                   return "DL_DRAGGING";
		case DL_DROPPED:                    return "DL_DROPPED";
		case DM_SETDEFID:                   return "DM_SETDEFID";
		case EM_CANUNDO:                    return "EM_CANUNDO";
		case EM_CHARFROMPOS:                return "EM_CHARFROMPOS";
		case EM_EMPTYUNDOBUFFER:            return "EM_EMPTYUNDOBUFFER";
		case EM_FMTLINES:                   return "EM_FMTLINES";
		case EM_GETFIRSTVISIBLELINE:        return "EM_GETFIRSTVISIBLELINE";
		case EM_GETHANDLE:                  return "EM_GETHANDLE";
		case EM_GETIMESTATUS:               return "EM_GETIMESTATUS";
		case EM_GETLIMITTEXT:               return "EM_GETLIMITTEXT";
		case EM_GETLINE:                    return "EM_GETLINE";
		case EM_GETLINECOUNT:               return "EM_GETLINECOUNT";
		case EM_GETMARGINS:                 return "EM_GETMARGINS";
		case EM_GETMODIFY:                  return "EM_GETMODIFY";
		case EM_GETPASSWORDCHAR:            return "EM_GETPASSWORDCHAR";
		case EM_GETRECT:                    return "EM_GETRECT";
		case EM_GETSEL:                     return "EM_GETSEL";
		case EM_GETTHUMB:                   return "EM_GETTHUMB";
		case EM_GETWORDBREAKPROC:           return "EM_GETWORDBREAKPROC";
		case EM_LIMITTEXT:                  return "EM_LIMITTEXT";
		case EM_LINEFROMCHAR:               return "EM_LINEFROMCHAR";
		case EM_LINEINDEX:                  return "EM_LINEINDEX";
		case EM_LINELENGTH:                 return "EM_LINELENGTH";
		case EM_LINESCROLL:                 return "EM_LINESCROLL";
		case EM_POSFROMCHAR:                return "EM_POSFROMCHAR";
		case EM_REPLACESEL:                 return "EM_REPLACESEL";
		case EM_SCROLL:                     return "EM_SCROLL";
		case EM_SCROLLCARET:                return "EM_SCROLLCARET";
		case EM_SETHANDLE:                  return "EM_SETHANDLE";
		case EM_SETIMESTATUS:               return "EM_SETIMESTATUS";
		case EM_SETMARGINS:                 return "EM_SETMARGINS";
		case EM_SETMODIFY:                  return "EM_SETMODIFY";
		case EM_SETPASSWORDCHAR:            return "EM_SETPASSWORDCHAR";
		case EM_SETREADONLY:                return "EM_SETREADONLY";
		case EM_SETRECT:                    return "EM_SETRECT";
		case EM_SETRECTNP:                  return "EM_SETRECTNP";
		case EM_SETSEL:                     return "EM_SETSEL";
		case EM_SETTABSTOPS:                return "EM_SETTABSTOPS";
		case EM_SETWORDBREAKPROC:           return "EM_SETWORDBREAKPROC";
		case EM_UNDO:                       return "EM_UNDO";
		case LVM_APPROXIMATEVIEWRECT:       return "LVM_APPROXIMATEVIEWRECT";
		case LVM_ARRANGE:                   return "LVM_ARRANGE";
		case LVM_CANCELEDITLABEL:           return "LVM_CANCELEDITLABEL";
		case LVM_CREATEDRAGIMAGE:           return "LVM_CREATEDRAGIMAGE";
		case LVM_DELETEALLITEMS:            return "LVM_DELETEALLITEMS";
		case LVM_DELETECOLUMN:              return "LVM_DELETECOLUMN";
		case LVM_DELETEITEM:                return "LVM_DELETEITEM";
		case LVM_EDITLABELA:                return "LVM_EDITLABELA";
		case LVM_EDITLABELW:                return "LVM_EDITLABELW";
		case LVM_ENABLEGROUPVIEW:           return "LVM_ENABLEGROUPVIEW";
		case LVM_ENSUREVISIBLE:             return "LVM_ENSUREVISIBLE";
		case LVM_FINDITEMA:                 return "LVM_FINDITEMA";
		case LVM_FINDITEMW:                 return "LVM_FINDITEMW";
		case LVM_FIRST:                     return "LVM_FIRST";
		case LVM_GETBKIMAGEA:               return "LVM_GETBKIMAGEA";
		case LVM_GETBKIMAGEW:               return "LVM_GETBKIMAGEW";
		case LVM_GETCALLBACKMASK:           return "LVM_GETCALLBACKMASK";
		case LVM_GETCOLUMNA:                return "LVM_GETCOLUMNA";
		case LVM_GETCOLUMNORDERARRAY:       return "LVM_GETCOLUMNORDERARRAY";
		case LVM_GETCOLUMNW:                return "LVM_GETCOLUMNW";
		case LVM_GETCOLUMNWIDTH:            return "LVM_GETCOLUMNWIDTH";
		case LVM_GETCOUNTPERPAGE:           return "LVM_GETCOUNTPERPAGE";
		case LVM_GETEDITCONTROL:            return "LVM_GETEDITCONTROL";
		case LVM_GETEXTENDEDLISTVIEWSTYLE:  return "LVM_GETEXTENDEDLISTVIEWSTYLE";
		case LVM_GETGROUPINFO:              return "LVM_GETGROUPINFO";
		case LVM_GETGROUPMETRICS:           return "LVM_GETGROUPMETRICS";
		case LVM_GETHEADER:                 return "LVM_GETHEADER";
		case LVM_GETHOTCURSOR:              return "LVM_GETHOTCURSOR";
		case LVM_GETHOTITEM:                return "LVM_GETHOTITEM";
		case LVM_GETHOVERTIME:              return "LVM_GETHOVERTIME";
		case LVM_GETIMAGELIST:              return "LVM_GETIMAGELIST";
		case LVM_GETINSERTMARK:             return "LVM_GETINSERTMARK";
		case LVM_GETINSERTMARKCOLOR:        return "LVM_GETINSERTMARKCOLOR";
		case LVM_GETINSERTMARKRECT:         return "LVM_GETINSERTMARKRECT";
		case LVM_GETISEARCHSTRINGA:         return "LVM_GETISEARCHSTRINGA";
		case LVM_GETISEARCHSTRINGW:         return "LVM_GETISEARCHSTRINGW";
		case LVM_GETITEMA:                  return "LVM_GETITEMA";
		case LVM_GETITEMCOUNT:              return "LVM_GETITEMCOUNT";
		case LVM_GETITEMPOSITION:           return "LVM_GETITEMPOSITION";
		case LVM_GETITEMRECT:               return "LVM_GETITEMRECT";
		case LVM_GETITEMSPACING:            return "LVM_GETITEMSPACING";
		case LVM_GETITEMSTATE:              return "LVM_GETITEMSTATE";
		case LVM_GETITEMTEXTA:              return "LVM_GETITEMTEXTA";
		case LVM_GETITEMTEXTW:              return "LVM_GETITEMTEXTW";
		case LVM_GETITEMW:                  return "LVM_GETITEMW";
		case LVM_GETNEXTITEM:               return "LVM_GETNEXTITEM";
		case LVM_GETNUMBEROFWORKAREAS:      return "LVM_GETNUMBEROFWORKAREAS";
		case LVM_GETORIGIN:                 return "LVM_GETORIGIN";
		case LVM_GETOUTLINECOLOR:           return "LVM_GETOUTLINECOLOR";
		case LVM_GETSELECTEDCOLUMN:         return "LVM_GETSELECTEDCOLUMN";
		case LVM_GETSELECTEDCOUNT:          return "LVM_GETSELECTEDCOUNT";
		case LVM_GETSELECTIONMARK:          return "LVM_GETSELECTIONMARK";
		case LVM_GETSTRINGWIDTHA:           return "LVM_GETSTRINGWIDTHA";
		case LVM_GETSTRINGWIDTHW:           return "LVM_GETSTRINGWIDTHW";
		case LVM_GETSUBITEMRECT:            return "LVM_GETSUBITEMRECT";
		case LVM_GETTEXTBKCOLOR:            return "LVM_GETTEXTBKCOLOR";
		case LVM_GETTEXTCOLOR:              return "LVM_GETTEXTCOLOR";
		case LVM_GETTILEINFO:               return "LVM_GETTILEINFO";
		case LVM_GETTILEVIEWINFO:           return "LVM_GETTILEVIEWINFO";
		case LVM_GETTOOLTIPS:               return "LVM_GETTOOLTIPS";
		case LVM_GETTOPINDEX:               return "LVM_GETTOPINDEX";
		case LVM_GETUNICODEFORMAT:          return "LVM_GETUNICODEFORMAT";
		case LVM_GETVIEW:                   return "LVM_GETVIEW";
		case LVM_GETVIEWRECT:               return "LVM_GETVIEWRECT";
		case LVM_GETWORKAREAS:              return "LVM_GETWORKAREAS";
		case LVM_HASGROUP:                  return "LVM_HASGROUP";
		case LVM_HITTEST:                   return "LVM_HITTEST";
		case LVM_INSERTCOLUMNA:             return "LVM_INSERTCOLUMNA";
		case LVM_INSERTCOLUMNW:             return "LVM_INSERTCOLUMNW";
		case LVM_INSERTGROUP:               return "LVM_INSERTGROUP";
		case LVM_INSERTGROUPSORTED:         return "LVM_INSERTGROUPSORTED";
		case LVM_INSERTITEMA:               return "LVM_INSERTITEMA";
		case LVM_INSERTITEMW:               return "LVM_INSERTITEMW";
		case LVM_INSERTMARKHITTEST:         return "LVM_INSERTMARKHITTEST";
		case LVM_ISGROUPVIEWENABLED:        return "LVM_ISGROUPVIEWENABLED";
		case LVM_ISITEMVISIBLE:             return "LVM_ISITEMVISIBLE";
		case LVM_MAPIDTOINDEX:              return "LVM_MAPIDTOINDEX";
		case LVM_MAPINDEXTOID:              return "LVM_MAPINDEXTOID";
		case LVM_MOVEGROUP:                 return "LVM_MOVEGROUP";
		case LVM_MOVEITEMTOGROUP:           return "LVM_MOVEITEMTOGROUP";
		case LVM_REDRAWITEMS:               return "LVM_REDRAWITEMS";
		case LVM_REMOVEALLGROUPS:           return "LVM_REMOVEALLGROUPS";
		case LVM_REMOVEGROUP:               return "LVM_REMOVEGROUP";
		case LVM_SCROLL:                    return "LVM_SCROLL";
		case LVM_SETBKCOLOR:                return "LVM_SETBKCOLOR";
		case LVM_SETBKIMAGEA:               return "LVM_SETBKIMAGEA";
		case LVM_SETCALLBACKMASK:           return "LVM_SETCALLBACKMASK";
		case LVM_SETCOLUMNA:                return "LVM_SETCOLUMNA";
		case LVM_SETCOLUMNORDERARRAY:       return "LVM_SETCOLUMNORDERARRAY";
		case LVM_SETCOLUMNW:                return "LVM_SETCOLUMNW";
		case LVM_SETCOLUMNWIDTH:            return "LVM_SETCOLUMNWIDTH";
		case LVM_SETEXTENDEDLISTVIEWSTYLE:  return "LVM_SETEXTENDEDLISTVIEWSTYLE";
		case LVM_SETGROUPINFO:              return "LVM_SETGROUPINFO";
		case LVM_SETGROUPMETRICS:           return "LVM_SETGROUPMETRICS";
		case LVM_SETHOTCURSOR:              return "LVM_SETHOTCURSOR";
		case LVM_SETHOTITEM:                return "LVM_SETHOTITEM";
		case LVM_SETHOVERTIME:              return "LVM_SETHOVERTIME";
		case LVM_SETICONSPACING:            return "LVM_SETICONSPACING";
		case LVM_SETIMAGELIST:              return "LVM_SETIMAGELIST";
		case LVM_SETINFOTIP:                return "LVM_SETINFOTIP";
		case LVM_SETINSERTMARK:             return "LVM_SETINSERTMARK";
		case LVM_SETINSERTMARKCOLOR:        return "LVM_SETINSERTMARKCOLOR";
		case LVM_SETITEMA:                  return "LVM_SETITEMA";
		case LVM_SETITEMCOUNT:              return "LVM_SETITEMCOUNT";
		case LVM_SETITEMPOSITION32:         return "LVM_SETITEMPOSITION32";
		case LVM_SETITEMPOSITION:           return "LVM_SETITEMPOSITION";
		case LVM_SETITEMSTATE:              return "LVM_SETITEMSTATE";
		case LVM_SETITEMTEXTA:              return "LVM_SETITEMTEXTA";
		case LVM_SETITEMTEXTW:              return "LVM_SETITEMTEXTW";
		case LVM_SETITEMW:                  return "LVM_SETITEMW";
		case LVM_SETOUTLINECOLOR:           return "LVM_SETOUTLINECOLOR";
		case LVM_SETSELECTEDCOLUMN:         return "LVM_SETSELECTEDCOLUMN";
		case LVM_SETSELECTIONMARK:          return "LVM_SETSELECTIONMARK";
		case LVM_SETTEXTBKCOLOR:            return "LVM_SETTEXTBKCOLOR";
		case LVM_SETTEXTCOLOR:              return "LVM_SETTEXTCOLOR";
		case LVM_SETTILEINFO:               return "LVM_SETTILEINFO";
		case LVM_SETTILEVIEWINFO:           return "LVM_SETTILEVIEWINFO";
		case LVM_SETTOOLTIPS:               return "LVM_SETTOOLTIPS";
		case LVM_SETUNICODEFORMAT:          return "LVM_SETUNICODEFORMAT";
		case LVM_SETVIEW:                   return "LVM_SETVIEW";
		case LVM_SETWORKAREAS:              return "LVM_SETWORKAREAS";
		case LVM_SORTGROUPS:                return "LVM_SORTGROUPS";
		case LVM_SORTITEMS:                 return "LVM_SORTITEMS";
		case LVM_SUBITEMHITTEST:            return "LVM_SUBITEMHITTEST";
		case LVM_UPDATE:                    return "LVM_UPDATE";
		case PSM_HWNDTOINDEX:               return "PSM_HWNDTOINDEX";
		case PSM_INDEXTOHWND:               return "PSM_INDEXTOHWND";
		case PSM_INDEXTOPAGE:               return "PSM_INDEXTOPAGE";
		case PSM_PAGETOINDEX:               return "PSM_PAGETOINDEX";
		case PSM_SETFINISHTEXTW:            return "PSM_SETFINISHTEXTW";
		case PSM_SETHEADERSUBTITLEA:        return "PSM_SETHEADERSUBTITLEA";
		case PSM_SETHEADERSUBTITLEW:        return "PSM_SETHEADERSUBTITLEW";
		case PSM_SETHEADERTITLEA:           return "PSM_SETHEADERTITLEA";
		case PSM_SETHEADERTITLEW:           return "PSM_SETHEADERTITLEW";
		case RB_BEGINDRAG:                  return "RB_BEGINDRAG";
		case RB_DRAGMOVE:                   return "RB_DRAGMOVE";
		case RB_ENDDRAG:                    return "RB_ENDDRAG";
		case RB_GETBANDBORDERS:             return "RB_GETBANDBORDERS";
		case RB_GETBANDINFOA:               return "RB_GETBANDINFOA";
		case RB_GETBANDINFOW:               return "RB_GETBANDINFOW";
		case RB_GETBARHEIGHT:               return "RB_GETBARHEIGHT";
		case RB_GETBKCOLOR:                 return "RB_GETBKCOLOR";
		case RB_GETPALETTE:                 return "RB_GETPALETTE";
		case RB_GETTEXTCOLOR:               return "RB_GETTEXTCOLOR";
		case RB_GETTOOLTIPS:                return "RB_GETTOOLTIPS";
		case RB_IDTOINDEX:                  return "RB_IDTOINDEX";
		case RB_MAXIMIZEBAND:               return "RB_MAXIMIZEBAND";
		case RB_MINIMIZEBAND:               return "RB_MINIMIZEBAND";
		case RB_MOVEBAND:                   return "RB_MOVEBAND";
		case RB_PUSHCHEVRON:                return "RB_PUSHCHEVRON";
		case RB_SETBKCOLOR:                 return "RB_SETBKCOLOR";
		case RB_SETPALETTE:                 return "RB_SETPALETTE";
		case RB_SETTEXTCOLOR:               return "RB_SETTEXTCOLOR";
		case RB_SETTOOLTIPS:                return "RB_SETTOOLTIPS";
		case RB_SHOWBAND:                   return "RB_SHOWBAND";
		case RB_SIZETORECT:                 return "RB_SIZETORECT";
		case SB_SETICON:                    return "SB_SETICON";
		case SBM_ENABLE_ARROWS:             return "SBM_ENABLE_ARROWS";
		case SBM_GETPOS:                    return "SBM_GETPOS";
		case SBM_GETRANGE:                  return "SBM_GETRANGE";
		case SBM_GETSCROLLBARINFO:          return "SBM_GETSCROLLBARINFO";
		case SBM_GETSCROLLINFO:             return "SBM_GETSCROLLINFO";
		case SBM_SETPOS:                    return "SBM_SETPOS";
		case SBM_SETRANGE:                  return "SBM_SETRANGE";
		case SBM_SETRANGEREDRAW:            return "SBM_SETRANGEREDRAW";
		case SBM_SETSCROLLINFO:             return "SBM_SETSCROLLINFO";
		case TB_ADDBUTTONSW:                return "TB_ADDBUTTONSW";
		case TB_ADDSTRINGW:                 return "TB_ADDSTRINGW";
		case TB_AUTOSIZE:                   return "TB_AUTOSIZE";
		case TB_GETANCHORHIGHLIGHT:         return "TB_GETANCHORHIGHLIGHT";
		case TB_GETBITMAP:                  return "TB_GETBITMAP";
		case TB_GETBITMAPFLAGS:             return "TB_GETBITMAPFLAGS";
		case TB_GETBUTTONINFOA:             return "TB_GETBUTTONINFOA";
		case TB_GETBUTTONINFOW:             return "TB_GETBUTTONINFOW";
		case TB_GETBUTTONSIZE:              return "TB_GETBUTTONSIZE";
		case TB_GETBUTTONTEXTA:             return "TB_GETBUTTONTEXTA";
		case TB_GETBUTTONTEXTW:             return "TB_GETBUTTONTEXTW";
		case TB_GETDISABLEDIMAGELIST:       return "TB_GETDISABLEDIMAGELIST";
		case TB_GETEXTENDEDSTYLE:           return "TB_GETEXTENDEDSTYLE";
		case TB_GETHOTIMAGELIST:            return "TB_GETHOTIMAGELIST";
		case TB_GETHOTITEM:                 return "TB_GETHOTITEM";
		case TB_GETIMAGELIST:               return "TB_GETIMAGELIST";
		case TB_GETINSERTMARK:              return "TB_GETINSERTMARK";
		case TB_GETINSERTMARKCOLOR:         return "TB_GETINSERTMARKCOLOR";
		case TB_GETMAXSIZE:                 return "TB_GETMAXSIZE";
		case TB_GETOBJECT:                  return "TB_GETOBJECT";
		case TB_GETPADDING:                 return "TB_GETPADDING";
		case TB_GETRECT:                    return "TB_GETRECT";
		case TB_GETROWS:                    return "TB_GETROWS";
		case TB_GETSTRINGA:                 return "TB_GETSTRINGA";
		case TB_GETSTRINGW:                 return "TB_GETSTRINGW";
		case TB_GETSTYLE:                   return "TB_GETSTYLE";
		case TB_GETTEXTROWS:                return "TB_GETTEXTROWS";
		case TB_HITTEST:                    return "TB_HITTEST";
		case TB_INSERTBUTTONW:              return "TB_INSERTBUTTONW";
		case TB_INSERTMARKHITTEST:          return "TB_INSERTMARKHITTEST";
		case TB_LOADIMAGES:                 return "TB_LOADIMAGES";
		case TB_MAPACCELERATORA:            return "TB_MAPACCELERATORA";
		case TB_MAPACCELERATORW:            return "TB_MAPACCELERATORW";
		case TB_MOVEBUTTON:                 return "TB_MOVEBUTTON";
		case TB_REPLACEBITMAP:              return "TB_REPLACEBITMAP";
		case TB_SAVERESTOREW:               return "TB_SAVERESTOREW";
		case TB_SETANCHORHIGHLIGHT:         return "TB_SETANCHORHIGHLIGHT";
		case TB_SETBUTTONINFOA:             return "TB_SETBUTTONINFOA";
		case TB_SETBUTTONINFOW:             return "TB_SETBUTTONINFOW";
		case TB_SETBUTTONWIDTH:             return "TB_SETBUTTONWIDTH";
		case TB_SETCMDID:                   return "TB_SETCMDID";
		case TB_SETDISABLEDIMAGELIST:       return "TB_SETDISABLEDIMAGELIST";
		case TB_SETDRAWTEXTFLAGS:           return "TB_SETDRAWTEXTFLAGS";
		case TB_SETEXTENDEDSTYLE:           return "TB_SETEXTENDEDSTYLE";
		case TB_SETHOTIMAGELIST:            return "TB_SETHOTIMAGELIST";
		case TB_SETHOTITEM:                 return "TB_SETHOTITEM";
		case TB_SETIMAGELIST:               return "TB_SETIMAGELIST";
		case TB_SETINDENT:                  return "TB_SETINDENT";
		case TB_SETINSERTMARK:              return "TB_SETINSERTMARK";
		case TB_SETINSERTMARKCOLOR:         return "TB_SETINSERTMARKCOLOR";
		case TB_SETMAXTEXTROWS:             return "TB_SETMAXTEXTROWS";
		case TB_SETPADDING:                 return "TB_SETPADDING";
		case TB_SETSTYLE:                   return "TB_SETSTYLE";
		case TB_SETTOOLTIPS:                return "TB_SETTOOLTIPS";
		case TBM_SETBUDDY:                  return "TBM_SETBUDDY";
		case WM_ACTIVATE:                   return "WM_ACTIVATE";
		case WM_ACTIVATEAPP:                return "WM_ACTIVATEAPP";
		case WM_AFXFIRST:                   return "WM_AFXFIRST";
		case WM_AFXLAST:                    return "WM_AFXLAST";
		case WM_APP:                        return "WM_APP";
		case WM_APPCOMMAND:                 return "WM_APPCOMMAND";
		case WM_ASKCBFORMATNAME:            return "WM_ASKCBFORMATNAME";
		case WM_CANCELJOURNAL:              return "WM_CANCELJOURNAL";
		case WM_CANCELMODE:                 return "WM_CANCELMODE";
		case WM_CHANGECBCHAIN:              return "WM_CHANGECBCHAIN";
		case WM_CHANGEUISTATE:              return "WM_CHANGEUISTATE";
		case WM_CHARTOITEM:                 return "WM_CHARTOITEM";
		case WM_CHILDACTIVATE:              return "WM_CHILDACTIVATE";
		case WM_CLEAR:                      return "WM_CLEAR";
		case WM_COMMAND:                    return "WM_COMMAND";
		case WM_COMMNOTIFY:                 return "WM_COMMNOTIFY";
		case WM_COMPACTING:                 return "WM_COMPACTING";
		case WM_COMPAREITEM:                return "WM_COMPAREITEM";
		case WM_CONTEXTMENU:                return "WM_CONTEXTMENU";
		case WM_COPY:                       return "WM_COPY";
		case WM_COPYDATA:                   return "WM_COPYDATA";
		case WM_CREATE:                     return "WM_CREATE";
		case WM_CTLCOLORBTN:                return "WM_CTLCOLORBTN";
		case WM_CTLCOLORDLG:                return "WM_CTLCOLORDLG";
		case WM_CTLCOLOREDIT:               return "WM_CTLCOLOREDIT";
		case WM_CTLCOLORLISTBOX:            return "WM_CTLCOLORLISTBOX";
		case WM_CTLCOLORMSGBOX:             return "WM_CTLCOLORMSGBOX";
		case WM_CTLCOLORSCROLLBAR:          return "WM_CTLCOLORSCROLLBAR";
		case WM_CTLCOLORSTATIC:             return "WM_CTLCOLORSTATIC";
		case WM_CUT:                        return "WM_CUT";
		case WM_DEADCHAR:                   return "WM_DEADCHAR";
		case WM_DELETEITEM:                 return "WM_DELETEITEM";
		case WM_DESTROY:                    return "WM_DESTROY";
		case WM_DESTROYCLIPBOARD:           return "WM_DESTROYCLIPBOARD";
		case WM_DEVMODECHANGE:              return "WM_DEVMODECHANGE";
		case WM_DRAWCLIPBOARD:              return "WM_DRAWCLIPBOARD";
		case WM_DRAWITEM:                   return "WM_DRAWITEM";
		case WM_ENABLE:                     return "WM_ENABLE";
		case WM_ENDSESSION:                 return "WM_ENDSESSION";
		case WM_ENTERIDLE:                  return "WM_ENTERIDLE";
		case WM_FONTCHANGE:                 return "WM_FONTCHANGE";
		case WM_GETDLGCODE:                 return "WM_GETDLGCODE";
		case WM_GETFONT:                    return "WM_GETFONT";
		case WM_GETHOTKEY:                  return "WM_GETHOTKEY";
		case WM_GETICON:                    return "WM_GETICON";
		case WM_GETOBJECT:                  return "WM_GETOBJECT";
		case WM_GETTEXT:                    return "WM_GETTEXT";
		case WM_GETTEXTLENGTH:              return "WM_GETTEXTLENGTH";
		case WM_HANDHELDFIRST:              return "WM_HANDHELDFIRST";
		case WM_HANDHELDLAST:               return "WM_HANDHELDLAST";
		case WM_HELP:                       return "WM_HELP";
		case WM_HOTKEY:                     return "WM_HOTKEY";
		case WM_HSCROLL:                    return "WM_HSCROLL";
		case WM_HSCROLLCLIPBOARD:           return "WM_HSCROLLCLIPBOARD";
		case WM_ICONERASEBKGND:             return "WM_ICONERASEBKGND";
		case WM_IME_CHAR:                   return "WM_IME_CHAR";
		case WM_IME_COMPOSITIONFULL:        return "WM_IME_COMPOSITIONFULL";
		case WM_IME_CONTROL:                return "WM_IME_CONTROL";
		case WM_IME_ENDCOMPOSITION:         return "WM_IME_ENDCOMPOSITION";
		case WM_IME_KEYDOWN:                return "WM_IME_KEYDOWN";
		case WM_IME_KEYLAST:                return "WM_IME_KEYLAST";
		case WM_IME_KEYUP:                  return "WM_IME_KEYUP";
		case WM_IME_NOTIFY:                 return "WM_IME_NOTIFY";
		case WM_IME_REQUEST:                return "WM_IME_REQUEST";
		case WM_IME_SELECT:                 return "WM_IME_SELECT";
		case WM_IME_SETCONTEXT:             return "WM_IME_SETCONTEXT";
		case WM_IME_STARTCOMPOSITION:       return "WM_IME_STARTCOMPOSITION";
		case WM_INITDIALOG:                 return "WM_INITDIALOG";
		case WM_INITMENU:                   return "WM_INITMENU";
		case WM_INITMENUPOPUP:              return "WM_INITMENUPOPUP";
		case WM_INPUTLANGCHANGEREQUEST:     return "WM_INPUTLANGCHANGEREQUEST";
		case WM_LBUTTONDBLCLK:              return "WM_LBUTTONDBLCLK";
		case WM_MBUTTONDBLCLK:              return "WM_MBUTTONDBLCLK";
		case WM_MDIACTIVATE:                return "WM_MDIACTIVATE";
		case WM_MDICASCADE:                 return "WM_MDICASCADE";
		case WM_MDICREATE:                  return "WM_MDICREATE";
		case WM_MDIDESTROY:                 return "WM_MDIDESTROY";
		case WM_MDIGETACTIVE:               return "WM_MDIGETACTIVE";
		case WM_MDIICONARRANGE:             return "WM_MDIICONARRANGE";
		case WM_MDIMAXIMIZE:                return "WM_MDIMAXIMIZE";
		case WM_MDINEXT:                    return "WM_MDINEXT";
		case WM_MDIREFRESHMENU:             return "WM_MDIREFRESHMENU";
		case WM_MDIRESTORE:                 return "WM_MDIRESTORE";
		case WM_MDISETMENU:                 return "WM_MDISETMENU";
		case WM_MDITILE:                    return "WM_MDITILE";
		case WM_MEASUREITEM:                return "WM_MEASUREITEM";
		case WM_MENUCHAR:                   return "WM_MENUCHAR";
		case WM_MENUCOMMAND:                return "WM_MENUCOMMAND";
		case WM_MENUDRAG:                   return "WM_MENUDRAG";
		case WM_MENUGETOBJECT:              return "WM_MENUGETOBJECT";
		case WM_MENURBUTTONUP:              return "WM_MENURBUTTONUP";
		case WM_MENUSELECT:                 return "WM_MENUSELECT";
		case WM_MOUSEHOVER:                 return "WM_MOUSEHOVER";
		case WM_MOVING:                     return "WM_MOVING";
		case WM_NCCALCSIZE:                 return "WM_NCCALCSIZE";
		case WM_NCDESTROY:                  return "WM_NCDESTROY";
		case WM_NCHITTEST:                  return "WM_NCHITTEST";
		case WM_NCLBUTTONDBLCLK:            return "WM_NCLBUTTONDBLCLK";
		case WM_NCLBUTTONDOWN:              return "WM_NCLBUTTONDOWN";
		case WM_NCLBUTTONUP:                return "WM_NCLBUTTONUP";
		case WM_NCMBUTTONDBLCLK:            return "WM_NCMBUTTONDBLCLK";
		case WM_NCMBUTTONDOWN:              return "WM_NCMBUTTONDOWN";
		case WM_NCMBUTTONUP:                return "WM_NCMBUTTONUP";
		case WM_NCMOUSEHOVER:               return "WM_NCMOUSEHOVER";
		case WM_NCMOUSELEAVE:               return "WM_NCMOUSELEAVE";
		case WM_NCMOUSEMOVE:                return "WM_NCMOUSEMOVE";
		case WM_NCRBUTTONDBLCLK:            return "WM_NCRBUTTONDBLCLK";
		case WM_NCRBUTTONDOWN:              return "WM_NCRBUTTONDOWN";
		case WM_NCRBUTTONUP:                return "WM_NCRBUTTONUP";
		case WM_NCXBUTTONDBLCLK:            return "WM_NCXBUTTONDBLCLK";
		case WM_NCXBUTTONDOWN:              return "WM_NCXBUTTONDOWN";
		case WM_NCXBUTTONUP:                return "WM_NCXBUTTONUP";
		case WM_NEXTDLGCTL:                 return "WM_NEXTDLGCTL";
		case WM_NEXTMENU:                   return "WM_NEXTMENU";
		case WM_NOTIFY:                     return "WM_NOTIFY";
		case WM_NOTIFYFORMAT:               return "WM_NOTIFYFORMAT";
		case WM_NULL:                       return "WM_NULL";
		case WM_PAINTCLIPBOARD:             return "WM_PAINTCLIPBOARD";
		case WM_PAINTICON:                  return "WM_PAINTICON";
		case WM_PALETTECHANGED:             return "WM_PALETTECHANGED";
		case WM_PALETTEISCHANGING:          return "WM_PALETTEISCHANGING";
		case WM_PARENTNOTIFY:               return "WM_PARENTNOTIFY";
		case WM_PASTE:                      return "WM_PASTE";
		case WM_PENWINFIRST:                return "WM_PENWINFIRST";
		case WM_PENWINLAST:                 return "WM_PENWINLAST";
		case WM_POWER:                      return "WM_POWER";
		case WM_POWERBROADCAST:             return "WM_POWERBROADCAST";
		case WM_PRINT:                      return "WM_PRINT";
		case WM_PRINTCLIENT:                return "WM_PRINTCLIENT";
		case WM_QUERYDRAGICON:              return "WM_QUERYDRAGICON";
		case WM_QUERYENDSESSION:            return "WM_QUERYENDSESSION";
		case WM_QUERYNEWPALETTE:            return "WM_QUERYNEWPALETTE";
		case WM_QUERYOPEN:                  return "WM_QUERYOPEN";
		case WM_QUERYUISTATE:               return "WM_QUERYUISTATE";
		case WM_QUEUESYNC:                  return "WM_QUEUESYNC";
		case WM_QUIT:                       return "WM_QUIT";
		case WM_RBUTTONDBLCLK:              return "WM_RBUTTONDBLCLK";
		case WM_RENDERALLFORMATS:           return "WM_RENDERALLFORMATS";
		case WM_RENDERFORMAT:               return "WM_RENDERFORMAT";
		case WM_SETFONT:                    return "WM_SETFONT";
		case WM_SETHOTKEY:                  return "WM_SETHOTKEY";
		case WM_SETICON:                    return "WM_SETICON";
		case WM_SETREDRAW:                  return "WM_SETREDRAW";
		case WM_SETTEXT:                    return "WM_SETTEXT";
		case WM_SHOWWINDOW:                 return "WM_SHOWWINDOW";
		case WM_SIZECLIPBOARD:              return "WM_SIZECLIPBOARD";
		case WM_SPOOLERSTATUS:              return "WM_SPOOLERSTATUS";
		case WM_STYLECHANGED:               return "WM_STYLECHANGED";
		case WM_STYLECHANGING:              return "WM_STYLECHANGING";
		case WM_SYNCPAINT:                  return "WM_SYNCPAINT";
		case WM_SYSCOLORCHANGE:             return "WM_SYSCOLORCHANGE";
		case WM_SYSDEADCHAR:                return "WM_SYSDEADCHAR";
		case WM_TCARD:                      return "WM_TCARD";
		case WM_TIMECHANGE:                 return "WM_TIMECHANGE";
		case WM_TIMER:                      return "WM_TIMER";
		case WM_UNDO:                       return "WM_UNDO";
		case WM_UNINITMENUPOPUP:            return "WM_UNINITMENUPOPUP";
		case WM_UPDATEUISTATE:              return "WM_UPDATEUISTATE";
		case WM_USER:                       return "WM_USER";
		case WM_USERCHANGED:                return "WM_USERCHANGED";
		case WM_VKEYTOITEM:                 return "WM_VKEYTOITEM";
		case WM_VSCROLL:                    return "WM_VSCROLL";
		case WM_VSCROLLCLIPBOARD:           return "WM_VSCROLLCLIPBOARD";
		case WM_WINDOWPOSCHANGED:           return "WM_WINDOWPOSCHANGED";
		case WM_WINDOWPOSCHANGING:          return "WM_WINDOWPOSCHANGING";
		case WM_WININICHANGE:               return "WM_WININICHANGE";
		case WM_XBUTTONDBLCLK:              return "WM_XBUTTONDBLCLK";
		
		default: return "Unknown";
	}
}

bool Win32_IsUpsideModeEnabled()
{
	return platform.upsideDownModeEnabled;
}

