/*
File:   game_corners.h
Author: Taylor Robbins
Date:   09\19\2019
*/

#ifndef _GAME_CORNERS_H
#define _GAME_CORNERS_H

typedef enum
{
	Corner_None        = 0x00,
	Corner_TopLeft     = 0x01,
	Corner_TopRight    = 0x02,
	Corner_BottomRight = 0x04,
	Corner_BottomLeft  = 0x08,
	Corner_All = Corner_TopLeft|Corner_TopRight|Corner_BottomRight|Corner_BottomLeft,
	Corner_NumCorners = 4,
} Corner_t;

const char* GetCornerStr(Corner_t corner)
{
	switch (corner)
	{
		case Corner_None:        return "None";
		case Corner_TopLeft:     return "TopLeft";
		case Corner_TopRight:    return "TopRight";
		case Corner_BottomRight: return "BottomRight";
		case Corner_BottomLeft:  return "BottomLeft";
		case Corner_All:         return "All";
		default: return "Unknown";
	}
}

u8 GetCornerIndex(Corner_t corner)
{
	switch (corner)
	{
		case Corner_TopLeft:     return 0;
		case Corner_TopRight:    return 1;
		case Corner_BottomRight: return 2;
		case Corner_BottomLeft:  return 3;
		default: return 0;
	}
}
Corner_t GetCornerByIndex(u8 index)
{
	switch (index)
	{
		case 0: return Corner_TopLeft;
		case 1: return Corner_TopRight;
		case 2: return Corner_BottomRight;
		case 3: return Corner_BottomLeft;
		default: return Corner_None;
	}
}

Dir2_t CornerToDir2(Corner_t corner)
{
	switch (corner)
	{
		case Corner_TopLeft:     return (Dir2_t)(Dir2_Up|Dir2_Left);
		case Corner_TopRight:    return (Dir2_t)(Dir2_Up|Dir2_Right);
		case Corner_BottomRight: return (Dir2_t)(Dir2_Down|Dir2_Left);
		case Corner_BottomLeft:  return (Dir2_t)(Dir2_Down|Dir2_Right);
		default: return Dir2_None;
	}
}
Corner_t Dir2ToCorner(Dir2_t direction)
{
	switch ((u8)direction)
	{
		case (Dir2_Up|Dir2_Left):    return Corner_TopLeft;
		case (Dir2_Up|Dir2_Right):   return Corner_TopRight;
		case (Dir2_Down|Dir2_Left):  return Corner_BottomRight;
		case (Dir2_Down|Dir2_Right): return Corner_BottomLeft;
		default: return Corner_None;
	}
}

Corner_t CornerOpposite(Corner_t corner)
{
	switch (corner)
	{
		case Corner_TopLeft:     return Corner_BottomRight;
		case Corner_TopRight:    return Corner_BottomLeft;
		case Corner_BottomRight: return Corner_TopLeft;
		case Corner_BottomLeft:  return Corner_TopRight;
		default: return Corner_TopLeft;
	}
}

Corner_t CornerClockwise(Corner_t corner)
{
	switch (corner)
	{
		case Corner_TopLeft:     return Corner_TopRight;
		case Corner_TopRight:    return Corner_BottomRight;
		case Corner_BottomRight: return Corner_BottomLeft;
		case Corner_BottomLeft:  return Corner_TopLeft;
		default: return Corner_TopLeft;
	}
}
Corner_t CornerCounterClockwise(Corner_t corner)
{
	switch (corner)
	{
		case Corner_TopLeft:     return Corner_BottomLeft;
		case Corner_TopRight:    return Corner_TopLeft;
		case Corner_BottomRight: return Corner_TopRight;
		case Corner_BottomLeft:  return Corner_BottomRight;
		default: return Corner_TopLeft;
	}
}

Corner_t CornerClockwise(Corner_t corner, u32 numTurns)
{
	Corner_t result = corner;
	for (u32 tIndex = 0; tIndex < numTurns; tIndex++)
	{
		result = CornerClockwise(result);
	}
	return result;
}
Corner_t CornerCounterClockwise(Corner_t corner, u32 numTurns)
{
	Corner_t result = corner;
	for (u32 tIndex = 0; tIndex < numTurns; tIndex++)
	{
		result = CornerCounterClockwise(result);
	}
	return result;
}

#endif //  _GAME_CORNERS_H
