/*
File:   game.cpp
Author: Taylor Robbins
Date:   11\17\2017
Description: 
	** Holds all the main gameplay code 
*/

#include "game/game_func_defs.h"
#include "game/game_helpers.cpp"

#include "game/game_level.cpp"
#include "game/game_level_design_serialization_text.cpp"
#include "game/game_entity_lookup.cpp"

void GameClearWindowList()
{
	for (u32 wIndex = 0; wIndex < game->windowList.length; wIndex++)
	{
		OtherWindow_t* window = DynArrayGet(&game->windowList, OtherWindow_t, wIndex);
		Assert(window != nullptr);
		if (window->name != nullptr) { ArenaPop(mainHeap, window->name); }
	}
	DynArrayClear(&game->windowList);
}
void GameUpdateOtherWindowList()
{
	GameClearWindowList();
	
	OtherWindowIter_t windowIter = {};
	OtherWindow_t window = {};
	while (platform->GetOtherWindow(&windowIter, mainHeap, &window))
	{
		Assert(window.name != nullptr);
		OtherWindow_t* newItem = DynArrayAdd(&game->windowList, OtherWindow_t);
		Assert(newItem != nullptr);
		MyMemCopy(newItem, &window, sizeof(OtherWindow_t));
	}
}

void GameSaveImage()
{
	TempPushMark();
	char* screenshotFolderPath = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Screenshots, GAME_NAME_FOR_FOLDERS);
	if (screenshotFolderPath == nullptr)//just save it in the local directory next to the game executable instead
	{
		Notify_E("Failed to get regular screenshots folder path. Saving to game directory instead");
		screenshotFolderPath = "";
	}
	else if (!platform->DoesFolderExist(screenshotFolderPath))
	{
		bool createSuccess = platform->CreateFolder(screenshotFolderPath);
		if (!createSuccess)
		{
			NotifyPrint_E("Failed to create folder at \"%s\". Saving to game directory instead", screenshotFolderPath);
			screenshotFolderPath = "";
		}
	}
	char* screenshotFilePath = TempPrint("%sscreenshot_%u-%u-%u_%u-%u-%u.png", screenshotFolderPath, LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
	
	FrameBuffer_t* currentBuffer = (game->bufferParity ? &game->offscreenBuffer1 : &game->offscreenBuffer2);
	
	u32 textureWidth = (u32)currentBuffer->texture.width;
	u32 textureHeight = (u32)currentBuffer->texture.height;
	u8 pixelWidth = (currentBuffer->hasTransparency ? 4 : 3);
	reci sourceRec = NewReci(0, 0, (i32)textureWidth, (i32)textureHeight);
	u32 rowWidth = textureWidth * pixelWidth;
	rowWidth = (u32)(CeilR32i((r32)rowWidth / 4) * 4);
	u32 pixelDataSize = textureHeight * rowWidth;
	u8* pixelData = PushArray(TempArena, u8, pixelDataSize);
	Assert(pixelData != nullptr);
	MyMemSet(pixelData, 0x00, pixelDataSize);
	
	// if (CurrentAppState() == AppState_Game && app->takeGameWindowScreenshot)
	// {
	// 	sourceRec = NewReci((i32)game->instance.gameViews[0].screenRec.x, (i32)game->instance.gameViews[0].screenRec.y, (i32)game->instance.gameViews[0].screenRec.width, (i32)game->instance.gameViews[0].screenRec.height);
	// 	sourceRec = ReciInflate(sourceRec, (i32)game->instance.gameViews[0].scale);
	// 	sourceRec = ReciOverlap(sourceRec, NewReci(0, 0, (i32)textureWidth, (i32)textureHeight));
	// }
	
	glBindTexture(GL_TEXTURE_2D, currentBuffer->texture.id);
	glGetTexImage(GL_TEXTURE_2D, 0, (currentBuffer->hasTransparency ? GL_RGBA : GL_RGB), GL_UNSIGNED_BYTE, (GLvoid*)pixelData);
	
	OpenFile_t screenshotFile;
	if (platform->OpenFile(screenshotFilePath, true, &screenshotFile))
	{
		if (stbi_write_png_to_func(StbImageWriteCallback, &screenshotFile, sourceRec.width, sourceRec.height, pixelWidth, pixelData + (sourceRec.x*pixelWidth + sourceRec.y*rowWidth), rowWidth) != 0)
		{
			NotifyPrintTimed_I(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Screenshot saved to \"%s\"", screenshotFilePath);
		}
		else
		{
			NotifyPrintTimed_E(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Failed to write %u pixels to \"%s\"", sourceRec.width * sourceRec.height, screenshotFilePath);
		}
		platform->CloseFile(&screenshotFile);
	}
	else
	{
		NotifyPrintTimed_E(SCREENSHOT_MESSAGE_DISPLAY_TIME, "Failed to create screenshot file at \"%s\"", screenshotFilePath);
	}
	
	TempPopMark();
}

// +--------------------------------------------------------------+
// |                       Initialize Game                        |
// +--------------------------------------------------------------+
void InitializeGame()
{
	Assert(game != nullptr);
	
	CreateDynamicArray(&game->windowList, mainHeap, sizeof(OtherWindow_t));
	
	game->initialized = true;
}

// +--------------------------------------------------------------+
// |                          Start Game                          |
// +--------------------------------------------------------------+
bool StartGame(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	Assert(game->initialized);
	SoftQueueMusicChange(nullptr);
	
	GameUpdateOtherWindowList();
	game->selectedWindowIndex = -1;
	
	return true;
}

// +--------------------------------------------------------------+
// |                      Deinitialize Game                       |
// +--------------------------------------------------------------+
void DeinitializeGame()
{
	Assert(game != nullptr);
	Assert(game->initialized == true);
	StopAllLoopingSounds();
	
	GameClearWindowList();
	DestroyDynamicArray(&game->windowList);
	
	game->initialized = false;
	ClearPointer(game);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesGameCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                         Game Update                          |
// +--------------------------------------------------------------+
void UpdateGame(AppMenu_t appMenuAbove)
{
	Assert(game != nullptr);
	Assert(game->initialized);
	
	bool captureScreenshot = false;
	bool saveImage = false;
	bool clearImage = false;
	bool isTildePressed = platform->GetGlobalKeyState(Button_Tilde);
	bool isHomePressed = platform->GetGlobalKeyState(Button_Home);
	
	game->captureBtnRec = NewRec(-400, -30, 100, 30);
	game->recordBtnRec = NewRec(-300, -30, 100, 30);
	
	// +==============================+
	// |  Handle Save Image Shortcut  |
	// +==============================+
	if (ButtonPressed(Button_S) && ButtonDownNoHandling(Button_Control))
	{
		HandleButton(Button_S);
		saveImage = true;
	}
	
	// +==============================+
	// |  Handle Global Save Hotkey   |
	// +==============================+
	if (isHomePressed && !game->wasHomePressed)
	{
		saveImage = true;
	}
	
	// +==============================+
	// | Handle Clear Image Shortcut  |
	// +==============================+
	if (ButtonPressed(Button_R) && ButtonDownNoHandling(Button_Control))
	{
		HandleButton(Button_R);
		clearImage = true;
	}
	
	// +==============================+
	// |        View Movement         |
	// +==============================+
	r32 viewSpeed = 10;
	if (ButtonDown(Button_W))
	{
		HandleButton(Button_W);
		game->viewPosition.y -= viewSpeed;
	}
	if (ButtonDown(Button_A))
	{
		HandleButton(Button_A);
		game->viewPosition.x -= viewSpeed;
	}
	if (ButtonDown(Button_S))
	{
		HandleButton(Button_S);
		game->viewPosition.y += viewSpeed;
	}
	if (ButtonDown(Button_D))
	{
		HandleButton(Button_D);
		game->viewPosition.x += viewSpeed;
	}
	
	// +==============================+
	// |  Refresh Window List Hotkey  |
	// +==============================+
	if (ButtonPressed(Button_Space))
	{
		HandleButton(Button_Space);
		GameUpdateOtherWindowList();
	}
	
	v2 worldMousePos = RenderMousePos + game->viewPosition;
	
	// +==============================+
	// |     Handle Record Button     |
	// +==============================+
	if (IsCustomMouseInside(game->recordBtnRec, worldMousePos))
	{
		CaptureMouse("RecordBtn");
		appOutput->cursorType = Cursor_Pointer;
		if (HandleMouseClickedOn(MouseButton_Left, "RecordBtn"))
		{
			game->takingRecording = !game->takingRecording;
			if (game->takingRecording)
			{
				if (ButtonDownNoHandling(Button_Shift))
				{
					game->recordingStartCountdown = 2000; //ms
					game->recordingMaxLength      = 30000; //ms
				}
				else
				{
					game->recordingStartCountdown = 0; //ms
					game->recordingMaxLength      = 0; //ms
				}
				game->recordingLength = 0;
			}
			else if (game->recording.recordingStarted)
			{
				platform->StopScreenRecording(&game->recording);
			}
		}
	}
	
	// +==============================+
	// | Handle Global Record Hotkey  |
	// +==============================+
	if (isTildePressed && !game->wasTildePressed)
	{
		game->takingRecording = !game->takingRecording;
		PrintLine_D("Tilde Pressed. %s Recording", game->takingRecording ? "Starting" : "Stopping");
		if (game->takingRecording)
		{
			game->recordingStartCountdown = 0;
			game->recordingMaxLength = 0;
			game->recordingLength = 0;
			clearImage = true;
		}
		else if (game->recording.recordingStarted)
		{
			platform->StopScreenRecording(&game->recording);
		}
	}
	
	// +==================================+
	// | Update Recording Start Countdown |
	// +==================================+
	if (game->recordingStartCountdown > 0 && game->takingRecording)
	{
		game->recordingStartCountdown -= ElapsedMs;
		if (game->recordingStartCountdown <= 0) { game->recordingStartCountdown = 0.0f; }
	}
	if (game->takingRecording && game->recordingStartCountdown == 0)
	{
		game->recordingLength += ElapsedMs;
		if (game->recordingMaxLength > 0 && game->recordingLength >= game->recordingMaxLength)
		{
			game->takingRecording = false;
			if (game->recording.recordingStarted) { platform->StopScreenRecording(&game->recording); }
		}
	}
	
	// +==============================+
	// |    Handle Capture Button     |
	// +==============================+
	if (IsCustomMouseInside(game->captureBtnRec, worldMousePos))
	{
		CaptureMouse("CaptureBtn");
		appOutput->cursorType = Cursor_Pointer;
		if (HandleMouseClickedOn(MouseButton_Left, "CaptureBtn"))
		{
			captureScreenshot = true;
		}
	}
	
	// +==============================+
	// |   Constant Capture Hotkey    |
	// +==============================+
	if (ButtonDown(Button_Enter))
	{
		HandleButton(Button_Enter);
		captureScreenshot = true;
	}
	
	// +==============================+
	// |      Capture Screenshot      |
	// +==============================+
	if (captureScreenshot)
	{
		if (game->selectedWindowIndex >= 0 && (u32)game->selectedWindowIndex < game->windowList.length)
		{
			OtherWindow_t* targetWindow = DynArrayGet(&game->windowList, OtherWindow_t, (u32)game->selectedWindowIndex);
			Assert(targetWindow != nullptr);
			ScreenCapture_t capture = {};
			if (platform->CaptureScreenBitmap(targetWindow, Reci_Zero, &capture))
			{
				// PrintLine_I("Captured %dx%d screenshot of target program", capture.size.width, capture.size.height);
				
				DestroyTexture(&game->captureTexture);
				game->captureTexture = CreateTextureEx(capture.data, GL_BGRA, capture.size.width, capture.size.height, true, false);
				
				platform->FreeScreenCaptureMemory(&capture);
				
				game->drawToOffscreenBuffer = true;
				game->clearOffscreenBuffers = ButtonDownNoHandling(Button_Shift);
			}
			else
			{
				WriteLine_E("Failed to capture the screen of target program");
			}
		}
		else { Notify_W("No window selected"); }
	}
	
	// +==============================+
	// |   Capture Recording Frame    |
	// +==============================+
	if (game->takingRecording && game->recordingStartCountdown == 0)
	{
		if (!game->recording.recordingStarted)
		{
			if (game->selectedWindowIndex >= 0 && (u32)game->selectedWindowIndex < game->windowList.length)
			{
				OtherWindow_t* targetWindow = DynArrayGet(&game->windowList, OtherWindow_t, (u32)game->selectedWindowIndex);
				Assert(targetWindow != nullptr);
				if (!platform->StartScreenRecording(targetWindow, Reci_Zero, &game->recording))
				{
					NotifyPrint_E("Failed to start recording on window \"%s\"", targetWindow->name);
				}
			}
			else { Notify_W("No window selected"); }
		}
		
		if (game->recording.recordingStarted)
		{
			if (platform->CaptureScreenRecording(&game->recording))
			{
				DestroyTexture(&game->captureTexture);
				game->captureTexture = CreateTextureEx(game->recording.bitmapData, GL_BGRA, game->recording.bitmapSize.width, game->recording.bitmapSize.height, true, false);
				
				game->drawToOffscreenBuffer = true;
				game->clearOffscreenBuffers = false;
			}
			else
			{
				Notify_E("Failed to capture frame of recording");
				platform->StopScreenRecording(&game->recording);
			}
		}
		else
		{
			game->takingRecording = false;
		}
	}
	
	// +==============================+
	// |      Handle Save Button      |
	// +==============================+
	//TODO: Implement me!
	
	// +==============================+
	// |          Save Image          |
	// +==============================+
	if (saveImage)
	{
		GameSaveImage();
	}
	
	// +==============================+
	// |     Handle Clear Button      |
	// +==============================+
	//TODO: Implement me!
	
	// +==============================+
	// |         Clear Image          |
	// +==============================+
	if (clearImage)
	{
		DestroyFrameBuffer(&game->offscreenBuffer1);
		DestroyFrameBuffer(&game->offscreenBuffer2);
	}
	
	// +==================================+
	// | Handle Window List Item Clicked  |
	// +==================================+
	RcBindFont(GetFont(debugFont));
	game->hoverWindowIndex = -1;
	rec listItemBaseRec = NewRec(-400, 0, 400, RcLineHeight()*2);
	for (u32 wIndex = 0; wIndex < game->windowList.length; wIndex++)
	{
		OtherWindow_t* window = DynArrayGet(&game->windowList, OtherWindow_t, wIndex);
		Assert(window != nullptr);
		rec itemRec = listItemBaseRec;
		v2 textPos = itemRec.topLeft + NewVec2(5, RcMaxExtendUp());
		v2 textSize = MeasureFormattedString(GetFont(debugFont), window->name, MyStrLength32(window->name), itemRec.width - 5*2, true);
		if (itemRec.height < textSize.height + 10) { itemRec.height = textSize.height + 10; }
		
		if (IsCustomMouseInside(itemRec, worldMousePos))
		{
			char* itemMouseName = TempPrint("Window[%u]", wIndex);
			CaptureMouse(itemMouseName);
			appOutput->cursorType = Cursor_Pointer;
			game->hoverWindowIndex = (i32)wIndex;
			if (HandleMouseClickedOn(MouseButton_Left, itemMouseName))
			{
				game->selectedWindowIndex = (i32)wIndex;
			}
		}
		
		listItemBaseRec.y += itemRec.height;
	}
	
	game->wasTildePressed = isTildePressed;
	game->wasHomePressed = isHomePressed;
}

// +--------------------------------------------------------------+
// |                         Game Render                          |
// +--------------------------------------------------------------+
void RenderGame(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |    Offscreen Render Setup    |
	// +==============================+
	if (game->drawToOffscreenBuffer)
	{
		FrameBuffer_t* writeBuffer = (game->bufferParity ? &game->offscreenBuffer2 : &game->offscreenBuffer1);
		FrameBuffer_t* readBuffer = (game->bufferParity ? &game->offscreenBuffer1 : &game->offscreenBuffer2);
		
		if (!writeBuffer->texture.isValid || writeBuffer->texture.width != game->captureTexture.width || writeBuffer->texture.height != game->captureTexture.height)
		{
			if (writeBuffer->texture.isValid) { DestroyFrameBuffer(writeBuffer); }
			*writeBuffer = CreateFrameBuffer(game->captureTexture.size, false, false);
			PrintLine_D("Resizing offscreen buffer %s", game->bufferParity ? "2" : "1");
		}
		RcBegin(GetShader(longExposureShader), NewRec(Vec2_Zero, NewVec2(writeBuffer->texture.size)), GetFont(debugFont), writeBuffer);
		RcClear(Black, 1.0f);
		
		rec captureRec = NewRec(Vec2_Zero, NewVec2(game->captureTexture.size));
		if (!game->clearOffscreenBuffers && readBuffer->texture.isValid && readBuffer->texture.size == writeBuffer->texture.size)
		{
			RcBindTexture(&game->captureTexture);
			RcBindAltTexture(&readBuffer->texture);
			RcSetAltSourceRectangle(NewRec(0, 0, (r32)readBuffer->texture.width, (r32)readBuffer->texture.height));
			// PrintLine_D("Texture: %d", rc->boundShader->locations.texture);
			// PrintLine_D("TextureSize: %d", rc->boundShader->locations.textureSize);
			// PrintLine_D("SourceRectangle: %d", rc->boundShader->locations.sourceRectangle);
			// PrintLine_D("AltTexture: %d", rc->boundShader->locations.altTexture);
			// PrintLine_D("AltTextureSize: %d", rc->boundShader->locations.altTextureSize);
			// PrintLine_D("AltSourceRectangle: %d", rc->boundShader->locations.altSourceRectangle);
			RcDrawTexturedRec(captureRec, White, NewRec(0, captureRec.height, captureRec.width, -captureRec.height));
		}
		else
		{
			PrintLine_D("Direct writing to offscreen buffer %s", game->bufferParity ? "2" : "1");
			RcBindShader(GetShader(mainShader));
			RcBindTexture(&game->captureTexture);
			RcDrawTexturedRec(captureRec, White, NewRec(0, captureRec.height, captureRec.width, -captureRec.height));
		}
		
		game->drawToOffscreenBuffer = false;
		game->bufferParity = !game->bufferParity;
	}
	
	bool isTildeDown = platform->GetGlobalKeyState(Button_Tilde);
	
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer);
	if (isTildeDown) { RcClear(PalGreen, 1.0f); }
	else { RcClear(PalGreenDarker, 1.0f); }
	mat4 viewMatrix = Mat4Translate(-game->viewPosition);
	RcSetViewMatrix(viewMatrix);
	
	// +==============================+
	// |         Draw Capture         |
	// +==============================+
	FrameBuffer_t* currentBuffer = (game->bufferParity ? &game->offscreenBuffer1 : &game->offscreenBuffer2);
	if (currentBuffer->texture.isValid)
	{
		rec captureRec = NewRec(Vec2_Zero, NewVec2(currentBuffer->texture.size));
		RcBindTexture(&currentBuffer->texture);
		RcDrawTexturedRec(captureRec, White);
	}
	else if (game->captureTexture.isValid)
	{
		rec captureRec = NewRec(Vec2_Zero, NewVec2(game->captureTexture.size));
		RcBindTexture(&game->captureTexture);
		RcDrawTexturedRec(captureRec, White, NewRec(0, captureRec.height, captureRec.width, -captureRec.height));
	}
	
	// +==============================+
	// |         Draw Timers          |
	// +==============================+
	RcBindFont(GetFont(debugFont));
	if (game->takingRecording && game->recordingStartCountdown > 0)
	{
		char* timeStr = TempPrint("-%s", FormattedMillisecondsGame((u64)RoundR32i(game->recordingStartCountdown)));
		v2 timeStrPos = NewVec2(5, -2 - RcMaxExtendDown());
		RcDrawStringNt(timeStr, timeStrPos, White);
	}
	if (game->takingRecording && game->recordingStartCountdown == 0)
	{
		char* timeStr = nullptr;
		if (game->recordingMaxLength > 0)
		{
			timeStr = TempPrint("%s/%s", FormattedMillisecondsGame((u64)RoundR32i(game->recordingLength)), FormattedMillisecondsGame((u64)RoundR32i(game->recordingMaxLength)));
		}
		else
		{
			timeStr = TempPrint("%s", FormattedMillisecondsGame((u64)RoundR32i(game->recordingLength)));
		}
		v2 timeStrPos = NewVec2(5, -2 - RcMaxExtendDown());
		RcDrawStringNt(timeStr, timeStrPos, White);
	}
	
	// +==============================+
	// |       Draw Window List       |
	// +==============================+
	RcBindFont(GetFont(debugFont));
	rec listItemBaseRec = NewRec(-400, 0, 400, RcLineHeight()*2);
	for (u32 wIndex = 0; wIndex < game->windowList.length; wIndex++)
	{
		OtherWindow_t* window = DynArrayGet(&game->windowList, OtherWindow_t, wIndex);
		Assert(window != nullptr);
		rec itemRec = listItemBaseRec;
		v2 textPos = itemRec.topLeft + NewVec2(5, RcMaxExtendUp());
		v2 textSize = MeasureFormattedString(GetFont(debugFont), window->name, MyStrLength32(window->name), itemRec.width - 5*2, true);
		if (itemRec.height < textSize.height + 10) { itemRec.height = textSize.height + 10; }
		bool isSelected = (game->selectedWindowIndex >= 0 && (u32)game->selectedWindowIndex == wIndex);
		bool isHovered = (game->hoverWindowIndex >= 0 && (u32)game->hoverWindowIndex == wIndex);
		Color_t itemColor = PalBackground;
		Color_t textColor = White;
		if ((wIndex%2) != 0) { itemColor = PalBackgroundLight; }
		if (isHovered) { itemColor = ColorLighten(itemColor, 20); }
		RcDrawRectangle(itemRec, itemColor);
		if (isSelected) { RcDrawButton(itemRec, Transparent, PalYellow, 2); }
		RcDrawFormattedString(window->name, textPos, itemRec.width - 5*2, textColor);
		listItemBaseRec.y += itemRec.height;
	}
	
	// +==============================+
	// |     Draw Capture Button      |
	// +==============================+
	{
		RcBindFont(GetFont(debugFont));
		const char* buttonText = "Capture";
		v2 buttonTextSize = RcMeasureStringNt(buttonText);
		v2 textPos = NewVec2(game->captureBtnRec.x + game->captureBtnRec.width/2 - buttonTextSize.width/2, game->captureBtnRec.y + game->captureBtnRec.height/2 + RcCenterOffset());
		textPos = Vec2Round(textPos);
		Color_t backColor = PalBackgroundLighter;
		Color_t textColor = White;
		bool isHovered = IsMouseOver("CaptureBtn");
		bool isPressed = IsMouseClickingOn(MouseButton_Left, "CaptureBtn");
		if (isPressed) { backColor = PalBackgroundDark; }
		else if (isHovered) { backColor = PalBackgroundLight; }
		RcDrawRectangle(game->captureBtnRec, backColor);
		RcDrawStringNt(buttonText, textPos, textColor);
	}
	
	// +==============================+
	// |      Draw Record Button      |
	// +==============================+
	{
		RcBindFont(GetFont(debugFont));
		const char* buttonText = game->takingRecording ? "Stop" : "Record";
		v2 buttonTextSize = RcMeasureStringNt(buttonText);
		v2 textPos = NewVec2(game->recordBtnRec.x + game->recordBtnRec.width/2 - buttonTextSize.width/2, game->recordBtnRec.y + game->recordBtnRec.height/2 + RcCenterOffset());
		textPos = Vec2Round(textPos);
		Color_t backColor = PalBackgroundLighter;
		Color_t textColor = White;
		bool isHovered = IsMouseOver("RecordBtn");
		bool isPressed = IsMouseClickingOn(MouseButton_Left, "RecordBtn");
		if (isPressed) { backColor = PalBackgroundDark; }
		else if (isHovered) { backColor = PalBackgroundLight; }
		RcDrawRectangle(game->recordBtnRec, backColor);
		RcDrawStringNt(buttonText, textPos, textColor);
	}
	
	// +==============================+
	// |       UI Render Setup        |
	// +==============================+
	RcBindShader(GetShader(mainShader));
	RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	RcSetViewMatrix(Mat4_Identity);
	RcSetDepth(0.0f);
	
	// +================================+
	// |       Draw Debug Overlay       |
	// +================================+
	if (app->showDebugOverlay)
	{
		rec overlayRec = NewRec(10, 10, (r32)RenderScreenSize.x - 10*2, (r32)RenderScreenSize.y - 10*2);
		RcDrawButton(overlayRec, ColorTransparent(Black, 0.5f), ColorTransparent(White, 0.5f));
		
		RcBindFont(GetFont(debugFont));
		v2 textPos = NewVec2(overlayRec.x + 5, overlayRec.y + 5 + RcMaxExtendUp());
		
		// RcPrintString(textPos, White, 1.0f, "AppData Size: %u/%u (%.3f%%)",
		// 	sizeof(AppData_t), AppMemory->permanantSize,
		// 	(r32)sizeof(AppData_t) / (r32)AppMemory->permanantSize * 100.0f);
		// textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Main Heap: %s/%s (%.3f%%)",
			FormattedSizeStr(mainHeap->used), FormattedSizeStr(mainHeap->size),
			(r32)mainHeap->used / (r32)mainHeap->size * 100.0f);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Temp Init: %s/%s (%.3f%%)",
			FormattedSizeStr(app->appInitTempHighWaterMark), FormattedSizeStr(TempArena->size),
			(r32)app->appInitTempHighWaterMark / (r32)TempArena->size * 100.0f);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Temp Update: %s/%s (%.3f%%) %u marks",
			FormattedSizeStr(ArenaGetHighWaterMark(TempArena)), FormattedSizeStr(TempArena->size),
			(r32)ArenaGetHighWaterMark(TempArena) / (r32)TempArena->size * 100.0f, ArenaNumMarks(TempArena));
		textPos.y += RcLineHeight();
		
		#if DEBUG
		const DynArray_t* appAllocations = (const DynArray_t*)platform->appAllocationsPntr;
		RcPrintString(textPos, White, 1.0f, "App Allocations: %u", appAllocations->length);
		textPos.y += RcLineHeight();
		#if 0
		for (u32 aIndex = 0; aIndex < appAllocations->length; aIndex++)
		{
			u8* allocationStructPntr = (u8*)DynArrayGet_(appAllocations, aIndex);
			u32 allocationSize = *((u32*)(allocationStructPntr + 0));
			void* allocationBase = *((void**)(allocationStructPntr + sizeof(u32)));
			
			RcPrintString(textPos, White, 1.0f, "  [%u]: %s %p", aIndex, FormattedSizeStr(allocationSize), allocationBase);
			textPos.y += RcLineHeight();
		}
		#endif
		#endif
		
		RcPrintString(textPos, White, 1.0f, "Program Time: %lu", ProgramTime);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Time Delta: %.2f", appInput->timeDelta);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "System Timestamp: %ld", appInput->systemTime.timestamp);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "System Time: %s", FormattedTimeStr(appInput->systemTime));
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Local Timestamp: %ld", LocalTime.timestamp);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Local Time: %s", FormattedTimeStr(LocalTime));
		textPos.y += RcLineHeight();
		
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Framerate: %.0lf", appInput->avgFramerate);
		textPos.y += RcLineHeight();
		
		RcPrintString(textPos, White, 1.0f, "Elapsed Time: %.1fms", appInput->avgElapsedMs);
		textPos.y += RcLineHeight();
		
		//Sound Instance List
		textPos = NewVec2(overlayRec.x + 5 + 500, overlayRec.y + 5 + RcMaxExtendUp());
		
		u32 numActiveInstances = 0;
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				numActiveInstances++;
			}
		}
		//TODO: Re-implement this display when the custom audio layer is working
		#if AUDIO_ENABLED && !USE_CUSTOM_AUDIO
		RcPrintString(textPos, White, 1.0f, "%u instance%s", numActiveInstances, (numActiveInstances == 1) ? "" : "s");
		textPos.y += RcLineHeight();
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				const char* sndName = nullptr;
				for (u32 rIndex = 0; rIndex < ArrayCount(soundsList->sounds); rIndex++)
				{
					SoundResource_t* sndResource = &soundsList->sounds[rIndex];
					if (instance->soundId == sndResource->sound.id)
					{
						sndName = sndResource->readableName;
						break;
					}
				}
				for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
				{
					Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
					Assert(musicResource != nullptr);
					if (IsResourceLoaded(musicResource) && instance->soundId == musicResource->music.id)
					{
						Assert(musicResource->readableName != nullptr);
						sndName = musicResource->readableName;
						break;
					}
				}
				if (sndName == nullptr) { sndName = TempPrint("%u", instance->soundId); }
				RcPrintString(textPos, White, 1.0f, "[%u] %s %s %.1f%%", sIndex, sndName, FormattedMilliseconds(TimeSince(instance->startTime)), instance->distVolume * 100);
				textPos.y += RcLineHeight();
			}
		}
		#endif
	}
}

