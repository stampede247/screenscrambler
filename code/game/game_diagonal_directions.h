/*
File:   game_diagonal_directions.h
Author: Taylor Robbins
Date:   09\08\2019
*/

#ifndef _GAME_DIAGONAL_DIRECTIONS_H
#define _GAME_DIAGONAL_DIRECTIONS_H

typedef enum
{
	Dir2Diag_PolarityBit = 0x10,
	Dir2Diag_None = Dir2_None,
	
	Dir2Diag_Right     = Dir2_Right,
	Dir2Diag_RightDown = Dir2_Right|Dir2_Down,
	Dir2Diag_DownRight = Dir2Diag_PolarityBit|Dir2_Right|Dir2_Down,
	Dir2Diag_Down      = Dir2_Down,
	Dir2Diag_DownLeft  = Dir2_Left|Dir2_Down,
	Dir2Diag_LeftDown  = Dir2Diag_PolarityBit|Dir2_Left|Dir2_Down,
	Dir2Diag_Left      = Dir2_Left,
	Dir2Diag_LeftUp    = Dir2_Left|Dir2_Up,
	Dir2Diag_UpLeft    = Dir2Diag_PolarityBit|Dir2_Left|Dir2_Up,
	Dir2Diag_Up        = Dir2_Up,
	Dir2Diag_UpRight   = Dir2_Right|Dir2_Up,
	Dir2Diag_RightUp   = Dir2Diag_PolarityBit|Dir2_Right|Dir2_Up,
	
	Dir2Diag_NumOptions = 12,
} Dir2Diag_t;

const char* GetDir2DiagStr(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return "Right";
		case Dir2Diag_RightDown: return "RightDown";
		case Dir2Diag_DownRight: return "DownRight";
		case Dir2Diag_Down:      return "Down";
		case Dir2Diag_DownLeft:  return "DownLeft";
		case Dir2Diag_LeftDown:  return "LeftDown";
		case Dir2Diag_Left:      return "Left";
		case Dir2Diag_LeftUp:    return "LeftUp";
		case Dir2Diag_UpLeft:    return "UpLeft";
		case Dir2Diag_Up:        return "Up";
		case Dir2Diag_UpRight:   return "UpRight";
		case Dir2Diag_RightUp:   return "RightUp";
		default: return "Unknown";
	}
}
Dir2Diag_t GetDir2DiagByIndex(u8 index)
{
	Assert(index < Dir2Diag_NumOptions);
	switch (index)
	{
		case 0:  return Dir2Diag_Right;
		case 1:  return Dir2Diag_RightDown;
		case 2:  return Dir2Diag_DownRight;
		case 3:  return Dir2Diag_Down;
		case 4:  return Dir2Diag_DownLeft;
		case 5:  return Dir2Diag_LeftDown;
		case 6:  return Dir2Diag_Left;
		case 7:  return Dir2Diag_LeftUp;
		case 8:  return Dir2Diag_UpLeft;
		case 9:  return Dir2Diag_Up;
		case 10: return Dir2Diag_UpRight;
		case 11: return Dir2Diag_RightUp;
		default: return Dir2Diag_None;
	}
}
u8 GetDir2DiagIndex(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:      return 0;
		case Dir2Diag_RightDown:  return 1;
		case Dir2Diag_DownRight:  return 2;
		case Dir2Diag_Down:       return 3;
		case Dir2Diag_DownLeft:   return 4;
		case Dir2Diag_LeftDown:   return 5;
		case Dir2Diag_Left:       return 6;
		case Dir2Diag_LeftUp:     return 7;
		case Dir2Diag_UpLeft:     return 8;
		case Dir2Diag_Up:         return 9;
		case Dir2Diag_UpRight:    return 10;
		case Dir2Diag_RightUp:    return 11;
		default: Assert(false); return 0;
	}
}
Dir2Diag_t Dir2DiagClockwise(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return Dir2Diag_RightDown;
		case Dir2Diag_RightDown: return Dir2Diag_DownRight;
		case Dir2Diag_DownRight: return Dir2Diag_Down;
		case Dir2Diag_Down:      return Dir2Diag_DownLeft;
		case Dir2Diag_DownLeft:  return Dir2Diag_LeftDown;
		case Dir2Diag_LeftDown:  return Dir2Diag_Left;
		case Dir2Diag_Left:      return Dir2Diag_LeftUp;
		case Dir2Diag_LeftUp:    return Dir2Diag_UpLeft;
		case Dir2Diag_UpLeft:    return Dir2Diag_Up;
		case Dir2Diag_Up:        return Dir2Diag_UpRight;
		case Dir2Diag_UpRight:   return Dir2Diag_RightUp;
		case Dir2Diag_RightUp:   return Dir2Diag_Right;
		default: return Dir2Diag_Down;
	}
}
Dir2Diag_t Dir2DiagCounterClockwise(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return Dir2Diag_RightUp;
		case Dir2Diag_RightDown: return Dir2Diag_Right;
		case Dir2Diag_DownRight: return Dir2Diag_RightDown;
		case Dir2Diag_Down:      return Dir2Diag_DownRight;
		case Dir2Diag_DownLeft:  return Dir2Diag_Down;
		case Dir2Diag_LeftDown:  return Dir2Diag_DownLeft;
		case Dir2Diag_Left:      return Dir2Diag_LeftDown;
		case Dir2Diag_LeftUp:    return Dir2Diag_Left;
		case Dir2Diag_UpLeft:    return Dir2Diag_LeftUp;
		case Dir2Diag_Up:        return Dir2Diag_UpLeft;
		case Dir2Diag_UpRight:   return Dir2Diag_Up;
		case Dir2Diag_RightUp:   return Dir2Diag_UpRight;
		default: return Dir2Diag_Down;
	}
}
Dir2Diag_t Dir2DiagOpposite(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return Dir2Diag_Left;
		case Dir2Diag_RightDown: return Dir2Diag_LeftUp;
		case Dir2Diag_DownRight: return Dir2Diag_UpLeft;
		case Dir2Diag_Down:      return Dir2Diag_Up;
		case Dir2Diag_DownLeft:  return Dir2Diag_UpRight;
		case Dir2Diag_LeftDown:  return Dir2Diag_RightUp;
		case Dir2Diag_Left:      return Dir2Diag_Right;
		case Dir2Diag_LeftUp:    return Dir2Diag_RightDown;
		case Dir2Diag_UpLeft:    return Dir2Diag_DownRight;
		case Dir2Diag_Up:        return Dir2Diag_Down;
		case Dir2Diag_UpRight:   return Dir2Diag_DownLeft;
		case Dir2Diag_RightUp:   return Dir2Diag_LeftDown;
		default: return Dir2Diag_Down;
	}
}

Dir2_t Dir2DiagCollapseToDir2(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return Dir2_Right;
		case Dir2Diag_RightDown: return Dir2_Right;
		case Dir2Diag_DownRight: return Dir2_Down;
		case Dir2Diag_Down:      return Dir2_Down;
		case Dir2Diag_DownLeft:  return Dir2_Down;
		case Dir2Diag_LeftDown:  return Dir2_Left;
		case Dir2Diag_Left:      return Dir2_Left;
		case Dir2Diag_LeftUp:    return Dir2_Left;
		case Dir2Diag_UpLeft:    return Dir2_Up;
		case Dir2Diag_Up:        return Dir2_Up;
		case Dir2Diag_UpRight:   return Dir2_Up;
		case Dir2Diag_RightUp:   return Dir2_Right;
		default: return Dir2_Down;
	}
}
bool Dir2DiagIsDiagonal(Dir2Diag_t dir2Diag)
{
	switch (dir2Diag)
	{
		case Dir2Diag_Right:     return false;
		case Dir2Diag_RightDown: return true;
		case Dir2Diag_DownRight: return true;
		case Dir2Diag_Down:      return false;
		case Dir2Diag_DownLeft:  return true;
		case Dir2Diag_LeftDown:  return true;
		case Dir2Diag_Left:      return false;
		case Dir2Diag_LeftUp:    return true;
		case Dir2Diag_UpLeft:    return true;
		case Dir2Diag_Up:        return false;
		case Dir2Diag_UpRight:   return true;
		case Dir2Diag_RightUp:   return true;
		default: return false;
	}
}

Dir2Diag_t Dir2SupplementWith(Dir2_t mainDir, Dir2_t secondDir)
{
	switch (mainDir)
	{
		case Dir2_Right: if (secondDir == Dir2_Up) { return Dir2Diag_RightUp; } else if (secondDir == Dir2_Down) { return Dir2Diag_RightDown; } else { return Dir2Diag_Right; }
		case Dir2_Down:  if (secondDir == Dir2_Left) { return Dir2Diag_DownLeft; } else if (secondDir == Dir2_Right) { return Dir2Diag_DownRight; } else { return Dir2Diag_Down; }
		case Dir2_Left:  if (secondDir == Dir2_Up) { return Dir2Diag_LeftUp; } else if (secondDir == Dir2_Down) { return Dir2Diag_LeftDown; } else { return Dir2Diag_Left; }
		case Dir2_Up:    if (secondDir == Dir2_Left) { return Dir2Diag_UpLeft; } else if (secondDir == Dir2_Right) { return Dir2Diag_UpRight; } else { return Dir2Diag_Up; }
		default: return (Dir2Diag_t)mainDir;
	}
}

#endif //  _GAME_DIAGONAL_DIRECTIONS_H
