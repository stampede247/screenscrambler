/*
File:   game_level_design_serialization_text.cpp
Author: Taylor Robbins
Date:   02\15\2019
Description: 
	** Holds the string serialization and deserialization functions for level designs and the map
*/

#define LEVEL_DESIGN_VERS_MAJOR 1
#define LEVEL_DESIGN_VERS_MINOR 20

#define TEXT_DESIGN_TITLE "Princess Dungeon Smasher Design :)"
#define TEXT_DESIGN_TITLE_LENGTH 34

#define TEXT_PACKS_LIST_VERS_MAJOR 1
#define TEXT_PACKS_LIST_VERS_MINOR 0

#define TEXT_PACKS_LIST_TITLE "Princess Dungeon Smasher Pack List :D"
#define TEXT_PACKS_LIST_TITLE_LENGTH 37

// +--------------------------------------------------------------+
// |               Text and Bin Design Shared Stuff               |
// +--------------------------------------------------------------+
void TileVersionChangeFixups(u8 versionMajor, u8 versionMinor, Tile_t* tileOut)
{
	
}

bool EntityVersionChangeFixups(u8 versionMajor, u8 versionMinor, Entity_t* entityOut)
{
	bool fixupPerformed = false;
	
	// +==============================+
	// |     Version 1.20 Changes     |
	// +==============================+
	if (IsVersionBelow(versionMajor, versionMinor, 1, 20))
	{
		if (entityOut->type == Entity_Spawner)
		{
			bool autoRespawn    = (entityOut->info.options[0] != 0x00);
			bool spawnTriggered = (entityOut->info.options[1] != 0x00);
			bool spawnParticles = (entityOut->info.options[2] != 0x00);
			u8 maxCount = entityOut->info.options[3];
			u8 countArea = entityOut->info.options[4];
			entityOut->info.options[0] = 0x00;
			if (autoRespawn)    { FlagSet(entityOut->info.options[0], 0x01); }
			if (spawnTriggered) { FlagSet(entityOut->info.options[0], 0x02); }
			if (spawnParticles) { FlagSet(entityOut->info.options[0], 0x04); }
			entityOut->info.options[1] = maxCount;
			entityOut->info.options[2] = 0x00;
			entityOut->info.options[3] = countArea;
			entityOut->info.options[4] = 0x00;
		}
		if (entityOut->type == Entity_Sensor)
		{
			u8 oldDetectTypeValue = entityOut->info.options[1];
			if (oldDetectTypeValue == 0x04) { oldDetectTypeValue = SensorDetectType_TableDecor; FlagSet(entityOut->info.options[0], 0x08); } //We got rid of SameColorEntity option
			else if (oldDetectTypeValue > 0x04) { oldDetectTypeValue -= 1; } //All other options shift down in value
		}
	}
	
	return fixupPerformed;
}

// +--------------------------------------------------------------+
// |                      Consume Functions                       |
// +--------------------------------------------------------------+
bool SrlParseRoomContent(const char* content, u32 contentLength, Room_t* roomOut)
{
	Assert(roomOut != nullptr);
	ClearPointer(roomOut);
	
	VALUE_TYPE_LIST(6);
	VALUE_ENTRY(0, id,       SrlType_u32, &roomOut->id);
	VALUE_ENTRY(1, viewMode, SrlType_u8,  &roomOut->viewMode);
	VALUE_ENTRY(2, x,        SrlType_i32, &roomOut->chunkRec.x);
	VALUE_ENTRY(3, y,        SrlType_i32, &roomOut->chunkRec.y);
	VALUE_ENTRY(4, width,    SrlType_i32, &roomOut->chunkRec.width);
	VALUE_ENTRY(5, height,   SrlType_i32, &roomOut->chunkRec.height);
	
	TempPushMark();
	const char* errorStr = ParseValueList(TempArena, content, contentLength, ',', entryInfos, 6);
	if (errorStr != nullptr) { PrintLine_W("WARNING: Room parsing error: \"%s\" \"%.*s\"", errorStr, contentLength, content); TempPopMark(); return false; }
	TempPopMark();
	
	if (roomOut->viewMode >= RoomViewMode_NumModes) { PrintLine_W("WARNING: Room viewMode was invalid value: 0x%02X", roomOut->viewMode); roomOut->viewMode = 0x00; }
	if (roomOut->chunkRec.width <= 0) { PrintLine_W("WARNING: Room width is invalid/negative: %d", roomOut->chunkRec.width); return false; }
	if (roomOut->chunkRec.height <= 0) { PrintLine_W("WARNING: Room height is invalid/negative: %d", roomOut->chunkRec.height); return false; }
	
	return true;
}

bool SrlParseTileContent(const char* content, u32 contentLength, Tile_t* tileOut, u8 versionMajor, u8 versionMinor)
{
	Assert(tileOut != nullptr);
	Tile_t tempTile = {};
	tempTile.hasRoof = false;
	tempTile.roofVariant = 0x00;
	tempTile.roofColor = 0x00;
	
	VALUE_TYPE_LIST(9);
	VALUE_ENTRY(0, tileType,    SrlType_TileType,  &tempTile.type);
	VALUE_ENTRY(1, tileVariant, SrlType_u8,        &tempTile.variant);
	VALUE_ENTRY(2, tileColor,   SrlType_u8,        &tempTile.color);
	VALUE_ENTRY(3, hasWall,     SrlType_Bool,           &tempTile.hasWall); entryInfos[3].optional = true;
	VALUE_ENTRY_CONDITIONAL(4, wallVariant, SrlType_u8, &tempTile.wallVariant, 3); entryInfos[4].optional = true;
	VALUE_ENTRY_CONDITIONAL(5, wallColor,   SrlType_u8, &tempTile.wallColor,   3); entryInfos[5].optional = true;
	VALUE_ENTRY(6, hasRoof,     SrlType_Bool,           &tempTile.hasRoof); entryInfos[6].optional = true;
	VALUE_ENTRY_CONDITIONAL(7, roofVariant, SrlType_u8, &tempTile.roofVariant, 6); entryInfos[7].optional = true;
	VALUE_ENTRY_CONDITIONAL(8, roofColor,   SrlType_u8, &tempTile.roofColor,   6); entryInfos[8].optional = true;
	
	TempPushMark();
	const char* errorStr = ParseValueList(TempArena, content, contentLength, ',', entryInfos, 9);
	if (errorStr != nullptr) { PrintLine_W("WARNING: Tile parsing error: \"%s\" \"%.*s\"", errorStr, contentLength, content); TempPopMark(); return false; }
	TempPopMark();
	
	if (tempTile.hasWall && tempTile.wallVariant >= WallVariant_NumVariants) { PrintLine_W("WARNING: Tile has invalid wall variant 0x%02X", tempTile.wallVariant); return false; }
	if (tempTile.hasRoof && tempTile.roofVariant >= RoofVariant_NumVariants) { PrintLine_W("WARNING: Tile has invalid roof variant 0x%02X", tempTile.roofVariant); return false; }
	
	InitializeTile(tileOut,
		tempTile.type, tempTile.variant, tempTile.color,
		tempTile.hasWall, tempTile.wallVariant, tempTile.wallColor,
		tempTile.hasRoof, tempTile.roofVariant, tempTile.roofColor
	);
	
	TileVersionChangeFixups(versionMajor, versionMinor, tileOut);
	return true;
}

bool SrlParseEntityContent(const char* content, u32 contentLength, Entity_t* entityOut, u8 versionMajor, u8 versionMinor)
{
	Assert(entityOut != nullptr);
	Entity_t tempEntity = {};
	
	VALUE_TYPE_LIST(9 + NUM_ENTITY_OPTIONS);
	VALUE_ENTRY(0, id,          SrlType_u32,        &tempEntity.id);
	VALUE_ENTRY(1, x,           SrlType_i32,        &tempEntity.tilePos.x);
	VALUE_ENTRY(2, y,           SrlType_i32,        &tempEntity.tilePos.y);
	VALUE_ENTRY(3, entityType,  SrlType_EntityType, &tempEntity.type);
	VALUE_ENTRY(4, variant,     SrlType_u8,         &tempEntity.info.variant);
	VALUE_ENTRY(5, rotation,    SrlType_Dir2Diag,   &tempEntity.info.rotation);
	VALUE_ENTRY(6, group,       SrlType_u8,         &tempEntity.info.group);
	VALUE_ENTRY(7, color,       SrlType_u8,         &tempEntity.info.color);
	VALUE_ENTRY(8, otherId,     SrlType_u32,        &tempEntity.otherId);
	for (u8 oIndex = 0; oIndex < NUM_ENTITY_OPTIONS; oIndex++)
	{
		VALUE_ENTRY_OPTIONAL(9+oIndex, option, SrlType_u8, &tempEntity.info.options[oIndex]);
	}
	
	TempPushMark();
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = nullptr;
	const char* errorStr = ParseValueList(TempArena, content, contentLength, ',', entryInfos, 9+NUM_ENTITY_OPTIONS, 0, false, &pieces, &numPieces);
	if (errorStr != nullptr) { PrintLine_W("WARNING: Entity parsing error: \"%s\" \"%.*s\"", errorStr, contentLength, content); TempPopMark(); return false; }
	if (numPieces >= 4 && IsVersionBelow(versionMajor, versionMinor, 1, 12))
	{
		Assert(pieces != nullptr && pieces[3].pntr != nullptr);
		if      (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("BT", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Bat;        }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("SK", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Skeleton;   }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("MS", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Mouse;      }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("GH", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Ghost;      }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("RB", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Robot;      }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("GB", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Goblin;     }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("SR", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Spider;     }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("PG", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_Penguin;   }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("BN", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_Bunny;     }
		else if (pieces[3].length == ENTITY_ID_STR_LENGTH && StrCompareIgnoreCase("RN", pieces[3].pntr, ENTITY_ID_STR_LENGTH)) { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_RainCloud; }
		
		else if (pieces[3].length == 3  && StrCompareIgnoreCase("Bat",        pieces[3].pntr, 3))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Bat;        }
		else if (pieces[3].length == 8  && StrCompareIgnoreCase("Skeleton",   pieces[3].pntr, 8))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Skeleton;   }
		else if (pieces[3].length == 5  && StrCompareIgnoreCase("Mouse",      pieces[3].pntr, 5))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Mouse;      }
		else if (pieces[3].length == 5  && StrCompareIgnoreCase("Ghost",      pieces[3].pntr, 5))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Ghost;      }
		else if (pieces[3].length == 5  && StrCompareIgnoreCase("Robot",      pieces[3].pntr, 5))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Robot;      }
		else if (pieces[3].length == 6  && StrCompareIgnoreCase("Goblin",     pieces[3].pntr, 6))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Goblin;     }
		else if (pieces[3].length == 6  && StrCompareIgnoreCase("Spider",     pieces[3].pntr, 6))  { tempEntity.type = Entity_Monster;  tempEntity.info.variant = MonsterVariant_Spider;     }
		else if (pieces[3].length == 7  && StrCompareIgnoreCase("Penguin",    pieces[3].pntr, 7))  { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_Penguin;   }
		else if (pieces[3].length == 5  && StrCompareIgnoreCase("Bunny",      pieces[3].pntr, 5))  { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_Bunny;     }
		else if (pieces[3].length == 10 && StrCompareIgnoreCase("Rain Cloud", pieces[3].pntr, 10)) { tempEntity.type = Entity_Friendly; tempEntity.info.variant = FriendlyVariant_RainCloud; }
	}
	TempPopMark();
	
	InitializeEntity(entityOut, tempEntity.type, tempEntity.tilePos, tempEntity.info.variant);
	entityOut->id = tempEntity.id;
	entityOut->info.rotation = tempEntity.info.rotation;
	entityOut->info.group = tempEntity.info.group;
	entityOut->info.color = tempEntity.info.color;
	for (u32 oIndex = 0; oIndex < NUM_ENTITY_OPTIONS; oIndex++)
	{
		entityOut->info.options[oIndex] = tempEntity.info.options[oIndex];
	}
	entityOut->otherId = tempEntity.otherId;
	
	bool entityFixed = EntityVersionChangeFixups(versionMajor, versionMinor, entityOut);
	
	return true;
}

// +--------------------------------------------------------------+
// |                   Text Level Design Format                   |
// +--------------------------------------------------------------+
char* SerializeDesignText(MemoryArena_t* arenaPntr, const Level_t* level, u32* numBytesOut)
{
	Assert(level != nullptr);
	u32 numEntities = 0;
	u32 numBlueDiamonds = 0;
	u32 numRedDiamonds = 0;
	u32 numGoldBars = 0;
	u32 numPuzzles = 0;
	u32 numSecrets = 0;
	u32 numPets = 0;
	const LoopAllEntities(countEntity, level)
	{
		numEntities++;
		if (countEntity->type == Entity_Diamond) //TODO: This won't work for an entity counter holding a diamond that shouldn't count towards the total
		{
			if (countEntity->info.variant == DiamondVariant_Default) { numBlueDiamonds += countEntity->info.diamond.count; }
			if (countEntity->info.variant == DiamondVariant_Red)     { numRedDiamonds  += countEntity->info.diamond.count; }
			if (countEntity->info.variant == DiamondVariant_GoldBar) { numGoldBars     += countEntity->info.diamond.count; }
		}
		else if (countEntity->type == Entity_Region && countEntity->info.variant == RegionVariant_Puzzle) { numPuzzles++; }
		else if (countEntity->type == Entity_Region && countEntity->info.variant == RegionVariant_Secret) { numSecrets++; }
		else if (countEntity->type == Entity_Pet) { numPets++; }
	}
	u32 numValidChunks = 0;
	reci chunkLimits = GetLevelChunkLimits(level);
	for (i32 chunkX = chunkLimits.x; chunkX < chunkLimits.x + chunkLimits.width; chunkX++)
	{
		for (i32 chunkY = chunkLimits.y; chunkY < chunkLimits.y + chunkLimits.height; chunkY++)
		{
			v2i chunkPos = NewVec2i(chunkX, chunkY);
			reci chunkRec = GetChunkRec(level, chunkPos);
			if (ShouldLevelChunkBeCounted(level, chunkPos)) { numValidChunks++; }
		}
	}
	
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 offset = 0;
		
		Assert(level->width >= 0 && level->height >= 0);
		SrlPrint(result, resultSize, &offset, "%s\n", TEXT_DESIGN_TITLE);
		SrlPrint(result, resultSize, &offset, "// Saved on %s %s %u\n", GetMonthStr((Month_t)LocalTime.month), GetDayOfMonthString(LocalTime.day), LocalTime.year);
		SrlPrint(result, resultSize, &offset, "Size: %d,%d\n", level->width, level->height);
		SrlPrint(result, resultSize, &offset, "ChunkSize: %d,%d\n", level->chunkSize.width, level->chunkSize.height);
		SrlPrint(result, resultSize, &offset, "ChunkOffset: %d,%d\n", level->chunkOffset.width, level->chunkOffset.height);
		SrlPrint(result, resultSize, &offset, "Version: %u.%u\n", LEVEL_DESIGN_VERS_MAJOR, LEVEL_DESIGN_VERS_MINOR);
		SrlPrint(result, resultSize, &offset, "Title: %s\n", level->title);
		SrlPrint(result, resultSize, &offset, "Id: %u\n", level->id);
		SrlPrint(result, resultSize, &offset, "Size: (%u,%u)\n", level->width, level->height);
		SrlPrint(result, resultSize, &offset, "NumEntities: %u\n", numEntities);
		SrlPrint(result, resultSize, &offset, "NumRooms: %u\n", level->numRooms);
		SrlPrint(result, resultSize, &offset, "Powerups: 0x00000000\n"); //backwards compatibility
		SrlPrint(result, resultSize, &offset, "NumDiamonds: {%u,%u,%u}\n", numBlueDiamonds, numGoldBars, numRedDiamonds);
		SrlPrint(result, resultSize, &offset, "NumValidChunks: %u\n", numValidChunks);
		SrlPrint(result, resultSize, &offset, "NumPuzzles: %u\n", numPuzzles);
		SrlPrint(result, resultSize, &offset, "NumSecrets: %u\n", numSecrets);
		SrlPrint(result, resultSize, &offset, "NumPets: %u\n", numPets);
		
		SrlPrint(result, resultSize, &offset, "[Tiles]\n");
		for (i32 yPos = 0; yPos < level->height; yPos++)
		{
			u32 repeatCount = 0;
			for (i32 xPos = 0; xPos < level->width; xPos++)
			{
				TempPushMark();
				Tile_t* tilePntr = &level->tiles[(yPos * level->width) + xPos];
				const char* tileTypeIdStr = GetTileTypeIdStr(tilePntr->type);
				if (tileTypeIdStr == nullptr || StrCompareIgnoreCaseNt(tileTypeIdStr, "UK")) { tileTypeIdStr = TempPrint("0x%02X", tilePntr->type); }
				
				bool isRepeat = false;
				if (xPos > 0)
				{
					Tile_t* previousTile = &level->tiles[(yPos * level->width) + (xPos-1)];
					if (AreTilesEqual(previousTile, tilePntr)) { repeatCount++; isRepeat = true; }
				}
				
				if (!isRepeat)
				{
					if (repeatCount > 0) { SrlPrint(result, resultSize, &offset, "{%u}%s", repeatCount, ((xPos+1) < level->width) ? "\n" : ""); repeatCount = 0; }
					
					// if (xPos > 0 && (xPos%5) == 0) { SrlPrint(result, resultSize, &offset, "\n"); }
					SrlPrint(result, resultSize, &offset, "{%.*s,%u,%u", TILE_ID_STR_LENGTH, tileTypeIdStr, tilePntr->variant, tilePntr->color);
					if (tilePntr->hasWall)
					{
						SrlPrint(result, resultSize, &offset, ",%s", tilePntr->hasWall ? "T" : "F");
						SrlPrint(result, resultSize, &offset, ",%u,%u", tilePntr->wallVariant, tilePntr->wallColor);
					}
					if (tilePntr->hasRoof)
					{
						if (!tilePntr->hasWall) { SrlPrint(result, resultSize, &offset, ",F"); }
						SrlPrint(result, resultSize, &offset, ",%s", tilePntr->hasRoof ? "T" : "F");
						SrlPrint(result, resultSize, &offset, ",%u,%u", tilePntr->roofVariant, tilePntr->roofColor);
					}
					SrlPrint(result, resultSize, &offset, "}");
				}
				TempPopMark();
			}
			if (repeatCount > 0) { SrlPrint(result, resultSize, &offset, "{%u}", repeatCount); repeatCount = 0; }
			SrlPrint(result, resultSize, &offset, "\n\n");
		}
		
		SrlPrint(result, resultSize, &offset, "[Entities]\n");
		const LoopAllEntities(entityPntr, level)
		{
			TempPushMark();
			const char* dir2Str = GetDir2DiagStr((Dir2Diag_t)entityPntr->info.rotation);
			if (MyStrCompareNt(dir2Str, "Unknown") == 0) { dir2Str = TempPrint("0x%02X", entityPntr->info.rotation); }
			
			const char* entityIdStr = GetEntityTypeIdStr(entityPntr->type);
			if (MyStrCompare(entityIdStr, "UK", ENTITY_ID_STR_LENGTH) == 0) { entityIdStr = TempPrint("0x%02X", entityPntr->type); }
			u8 numNonZeroOptions = 0;
			for (u8 oIndex = 0; oIndex < NUM_ENTITY_OPTIONS; oIndex++)
			{
				if (entityPntr->info.options[oIndex] != 0x00) { numNonZeroOptions = oIndex+1; }
			}
			SrlPrint(result, resultSize, &offset, "{%u,%d,%d,%.*s,%u,%s,%u,%u,%u",
				entityPntr->id,
				entityPntr->tilePos.x, entityPntr->tilePos.y,
				ENTITY_ID_STR_LENGTH, entityIdStr,
				entityPntr->info.variant,
				dir2Str,
				entityPntr->info.group,
				entityPntr->info.color,
				entityPntr->otherId
			);
			for (u32 oIndex = 0; oIndex < numNonZeroOptions; oIndex++)
			{
				SrlPrint(result, resultSize, &offset, ",%u", entityPntr->info.options[oIndex]);
			}
			SrlPrint(result, resultSize, &offset, "}\n");
			TempPopMark();
		}
		
		SrlPrint(result, resultSize, &offset, "[Rooms]\n");
		for (u32 rIndex = 0; rIndex < level->numRooms; rIndex++)
		{
			Room_t* room = &level->rooms[rIndex];
			SrlPrint(result, resultSize, &offset, "{%u,0x%02X,%d,%d,%d,%d}\n", room->id, room->viewMode, room->chunkRec.x, room->chunkRec.y, room->chunkRec.width, room->chunkRec.height);
		}
		
		if (pass == 0)
		{
			Assert(result == nullptr);
			resultSize = offset + 1; //add 1 byte to compensate for vsnprintf wierdness in SrlPrint
			if (numBytesOut != nullptr) { *numBytesOut = resultSize; }
			if (arenaPntr == nullptr) { return nullptr; }
			if (resultSize == 0) { return nullptr; }
			result = PushArray(arenaPntr, char, resultSize);
		}
		else
		{
			Assert(offset+1 == resultSize);
			result[offset] = ' '; offset++; //Replaced the null-terminator character with a space
		}
	}
	Assert(result != nullptr);
	return result;
}

bool DeserializeDesignText(MemoryArena_t* arenaPntr, Level_t* level, const void* fileData, u32 fileLength, u8* versionMajorOut = nullptr, u8* versionMinorOut = nullptr)
{
	Assert(arenaPntr != nullptr);
	Assert(level != nullptr);
	const char* charPntr = (const char*)fileData;
	u32 numCharsLeft = fileLength;
	
	if (fileData == nullptr) { return false; }
	if (numCharsLeft < TEXT_DESIGN_TITLE_LENGTH) { return false; }
	if (MyMemCompare(charPntr, TEXT_DESIGN_TITLE, TEXT_DESIGN_TITLE_LENGTH) != 0) { return false; }
	charPntr += TEXT_DESIGN_TITLE_LENGTH;
	numCharsLeft -= TEXT_DESIGN_TITLE_LENGTH;
	
	TextParser_t parser;
	CreateTextParser(&parser, charPntr, numCharsLeft);
	
	bool foundVersion     = false;
	u8   versionMajor     = 0;
	u8   versionMinor     = 0;
	bool isOldVersion     = false;
	bool foundTitle       = false;
	char title[LEVEL_TITLE_MAX_LENGTH+1];
	bool foundId          = false;
	u32  levelId          = 0;
	bool foundSize        = false;
	v2i  size             = Vec2i_Zero;
	bool foundNumEntities = false;
	u32  numEntities      = 0;
	bool foundChunkSize   = false;
	v2i  chunkSize        = Vec2i_Zero;
	bool foundChunkOffset = false;
	v2i  chunkOffset      = Vec2i_Zero;
	bool foundNumRooms    = false;
	u32  numRooms         = 0;
	
	// +--------------------------------------------------------------+
	// |                      Main Parsing Loop                       |
	// +--------------------------------------------------------------+
	bool foundTilesTag = false;
	bool foundEntitiesTag = false;
	bool foundRoomsTag = false;
	bool levelCreated = false;
	u32 numTilesFound = 0;
	u32 numEntitiesFound = 0;
	u32 numRoomsFound = 0;
	u32 numTemplateRowsFound = 0;
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token;
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing level design line %u: %s", parser.lineNumber, errorStr);
			if (levelCreated && arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		// +==============================+
		// |          Key-Value           |
		// +==============================+
		if (token.type == ParsingTokenType_KeyValue)
		{
			if (levelCreated)
			{
				PrintLine_W("Found key value after first tag in level design");
				if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
			
			// +==============================+
			// |           Version            |
			// +==============================+
			if (StrCompareIgnoreCase(token.key.pntr, "Version", token.key.length))
			{
				v2i versionV2i = {};
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &versionV2i);
				if (errorStr != nullptr)
				{
					PrintLine_W("Error while parsing level version number: %s", errorStr);
					return false;
				}
				if (versionV2i.x < 0 || versionV2i.x > 255)
				{
					PrintLine_W("Invalid level major version number: %d", versionV2i.x);
					return false;
				}
				if (versionV2i.y < 0 || versionV2i.y > 255)
				{
					PrintLine_W("Invalid level minor version number: %d", versionV2i.y);
					return false;
				}
				
				versionMajor = (u8)versionV2i.x;
				versionMinor = (u8)versionV2i.y;
				if (versionMajorOut != nullptr) { *versionMajorOut = versionMajor; }
				if (versionMinorOut != nullptr) { *versionMinorOut = versionMinor; }
				
				// if (!(versionMajor == LEVEL_DESIGN_VERS_MAJOR && versionMinor == LEVEL_DESIGN_VERS_MINOR) &&
				// 	!(versionMajor == LEVEL_DESIGN_VERS_MAJOR && versionMinor == LEVEL_DESIGN_VERS_MINOR-1))
				// {
				// 	PrintLine_W("Design is version %u.%u not %u.%u", versionMajor, versionMinor, LEVEL_DESIGN_VERS_MAJOR, LEVEL_DESIGN_VERS_MINOR);
				// }
				if (IsVersionAbove(versionMajor, versionMinor, LEVEL_DESIGN_VERS_MAJOR, LEVEL_DESIGN_VERS_MINOR))
				{
					PrintLine_W("Unsupported newer level version: v%u.%u. This version of the game only supports v%u.%u", versionMajor, versionMinor, LEVEL_DESIGN_VERS_MAJOR, LEVEL_DESIGN_VERS_MINOR);
					return false;
				}
				isOldVersion = IsVersionBelow(versionMajor, versionMinor, LEVEL_DESIGN_VERS_MAJOR, LEVEL_DESIGN_VERS_MINOR);
				foundVersion = true;
			}
			// +==============================+
			// |            Title             |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "Title", token.key.length))
			{
				if (token.value.length > LEVEL_TITLE_MAX_LENGTH)
				{
					PrintLine_W("WARNING: Level title is too long! %u > %u \"%.*s\"", token.value.length, LEVEL_TITLE_MAX_LENGTH, token.value.length, token.value.pntr);
					token.value.length = LEVEL_TITLE_MAX_LENGTH;
				}
				BufferPrint(title, "%.*s", token.value.length, token.value.pntr);
				foundTitle = true;
			}
			// +==============================+
			// |              Id              |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "Id", token.key.length))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &levelId);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing level ID: %s", errorStr);
					return false;
				}
				foundId = true;
			}
			// +==============================+
			// |             Size             |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "Size", token.key.length))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &size);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing level size: %s", errorStr);
					return false;
				}
				if (size.width <= 0)
				{
					PrintLine_W("WARNING: Level width has to be > 0! %d \"%.*s\"", size.width, token.value.length, token.value.pntr);
					continue;
				}
				if (size.height <= 0)
				{
					PrintLine_W("WARNING: Level height has to be > 0! %d \"%.*s\"", size.height, token.value.length, token.value.pntr);
					continue;
				}
				foundSize = true;
			}
			// +==============================+
			// |          ChunkSize           |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "ChunkSize", token.key.length))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &chunkSize);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing level chunk size: %s", errorStr);
					return false;
				}
				if (chunkSize.width <= 0)
				{
					PrintLine_W("WARNING: Chunk width has to be > 0! %d \"%.*s\"", chunkSize.width, token.value.length, token.value.pntr);
					continue;
				}
				if (chunkSize.height <= 0)
				{
					PrintLine_W("WARNING: Chunk height has to be > 0! %d \"%.*s\"", chunkSize.height, token.value.length, token.value.pntr);
					continue;
				}
				foundChunkSize = true;
			}
			// +==============================+
			// |         ChunkOffset          |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "ChunkOffset", token.key.length))
			{
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &chunkOffset);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing level chunk offset: %s", errorStr);
					return false;
				}
				if (chunkOffset.width < 0)
				{
					PrintLine_W("WARNING: Chunk offset X has to be >= 0! %d \"%.*s\"", chunkOffset.x, token.value.length, token.value.pntr);
					continue;
				}
				if (chunkOffset.height < 0)
				{
					PrintLine_W("WARNING: Chunk offset Y has to be >= 0! %d \"%.*s\"", chunkOffset.y, token.value.length, token.value.pntr);
					continue;
				}
				foundChunkOffset = true;
			}
			// +==============================+
			// |         NumEntities          |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumEntities", token.key.length))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &numEntities);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing NumEntities: %s", errorStr);
					return false;
				}
				foundNumEntities = true;
			}
			// +==============================+
			// |           NumRooms           |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumRooms", token.key.length))
			{
				errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &numRooms);
				if (errorStr != nullptr)
				{
					PrintLine_W("WARNING: Error while parsing NumRooms: %s", errorStr);
					return false;
				}
				foundNumRooms = true;
			}
			// +==============================+
			// |           Powerups           |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "Powerups", token.key.length))
			{
				//Backwards compatibility, we understand this key but don't care about it's value
			}
			// +==============================+
			// |         NumDiamonds          |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumDiamonds", token.key.length))
			{
				//This key is only used by the level info deserialization so it doesn't have to deserialize the entire file to get the diamond count
			}
			// +==============================+
			// |        NumValidChunks        |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumValidChunks", token.key.length))
			{
				//This key is only used by the level info deserialization so it doesn't have to deserialize the entire file to get the number of valid chunks
			}
			// +==============================+
			// |          NumPuzzles          |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumPuzzles", token.key.length))
			{
				//This key is only used by the level info deserialization so it doesn't have to deserialize the entire file to get the number of puzzle regions
			}
			// +==============================+
			// |          NumSecrets          |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumSecrets", token.key.length))
			{
				//This key is only used by the level info deserialization so it doesn't have to deserialize the entire file to get the number of secret regions
			}
			// +==============================+
			// |           NumPets            |
			// +==============================+
			else if (StrCompareIgnoreCase(token.key.pntr, "NumPets", token.key.length))
			{
				//This key is only used by the level info deserialization so it doesn't have to deserialize the entire file to get the number of pets in a level
			}
			// +==============================+
			// |         Unknown Key          |
			// +==============================+
			else
			{
				PrintLine_W("WARNING: Unknown key \"%.*s\" found in level design line %u", token.key.length, token.key.pntr, parser.lineNumber);
				continue;
			}
		}
		else if (token.type == ParsingTokenType_Comment)
		{
			//Ignore the comment
		}
		// +==============================+
		// |            Tiles             |
		// +==============================+
		else if (token.type == ParsingTokenType_ValueList && foundTilesTag)
		{
			Assert(levelCreated);
			
			u32 repeatCount = 0;
			errorStr = TryDeserializeU32(token.content.pntr, token.content.length, &repeatCount);
			if (errorStr == nullptr)
			{
				if ((numTilesFound % size.width) == 0)
				{
					PrintLine_E("Found tile repeat x%u token at beginning of row %u", repeatCount, numTilesFound/size.width);
					if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
					return false;
				}
				v2i repeatPos = NewVec2i((numTilesFound % size.width) - 1, numTilesFound/size.width);
				Tile_t* repeatTilePntr = GetTileAt(level, repeatPos);
				Assert(repeatTilePntr != nullptr);
				for (u32 rIndex = 0; rIndex < repeatCount; rIndex++)
				{
					if (numTilesFound >= (u32)(size.width * size.height))
					{
						PrintLine_E("Too many tiles (during repeat %u) in level design: %u declared", repeatCount, (u32)(size.width*size.height));
						if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
						return false;
					}
					v2i tilePos = NewVec2i(numTilesFound%size.width, numTilesFound/size.width);
					Tile_t* tilePntr = GetTileAt(level, tilePos);
					Assert(tilePntr != nullptr);
					InitializeTile(tilePntr,
						repeatTilePntr->type, repeatTilePntr->variant, repeatTilePntr->color,
						repeatTilePntr->hasWall, repeatTilePntr->wallVariant, repeatTilePntr->wallColor,
						repeatTilePntr->hasRoof, repeatTilePntr->roofVariant, repeatTilePntr->roofColor
					);
					numTilesFound++;
				}
			}
			else
			{
				if (numTilesFound >= (u32)(size.width * size.height))
				{
					PrintLine_E("Too many tiles in level design: %u declared", (u32)(size.width*size.height));
					if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
					return false;
				}
				
				v2i tilePos = NewVec2i(numTilesFound%size.width, numTilesFound/size.width);
				Tile_t* tilePntr = GetTileAt(level, tilePos);
				Assert(tilePntr != nullptr);
				if (SrlParseTileContent(token.content.pntr, token.content.length, tilePntr, versionMajor, versionMinor))
				{
					numTilesFound++;
				}
				else
				{
					PrintLine_E("Failed to parse tile (%u, %u)", (numTilesFound % (u32)size.width), (numTilesFound / (u32)size.width));
					if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
					return false;
				}
			}
		}
		// +==============================+
		// |           Entities           |
		// +==============================+
		else if (token.type == ParsingTokenType_ValueList && foundEntitiesTag)
		{
			Assert(levelCreated);
			if (numEntitiesFound >= numEntities)
			{
				PrintLine_E("Too many entities in level design: %u declared", numEntities);
				if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
			Entity_t newEntity = {};
			if (SrlParseEntityContent(token.content.pntr, token.content.length, &newEntity, versionMajor, versionMinor))
			{
				if (IsVersionBelow(versionMajor, versionMinor, 1, 9) && newEntity.type == Entity_PistonArm)
				{
					PrintLine_W("Removing PistonArm %u from old level design", newEntity.id);
					numEntitiesFound++;
				}
				else
				{
					AllocExistingEntity(level, &newEntity);
					numEntitiesFound++;
				}
			}
			else
			{
				PrintLine_E("Failed to parse entity %u", numEntitiesFound);
				if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
		}
		// +==============================+
		// |            Rooms             |
		// +==============================+
		else if (token.type == ParsingTokenType_ValueList && foundRoomsTag)
		{
			Assert(levelCreated);
			if (numRoomsFound >= numRooms)
			{
				PrintLine_E("Too many rooms in level design: %u declared", numRooms);
				if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
			Room_t newRoom = {};
			if (SrlParseRoomContent(token.content.pntr, token.content.length, &newRoom))
			{
				AddExistingRoom(level, &newRoom);
				numRoomsFound++;
			}
			else
			{
				PrintLine_E("Failed to parse room %u", numRoomsFound);
				if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
		}
		// +==============================+
		// |          Header Tag          |
		// +==============================+
		else if (token.type == ParsingTokenType_Header)
		{
			// +==============================+
			// | Check Everything was defined |
			// +==============================+
			if (!levelCreated)
			{
				if (!foundVersion)
				{
					WriteLine_W("Design contained no Version!");
					return false;
				}
				if (!foundTitle)
				{
					WriteLine_W("Design contained no Title!");
					return false;
				}
				if (!foundSize)
				{
					WriteLine_W("Design contained no Size!");
					return false;
				}
				if (!foundNumEntities)
				{
					WriteLine_W("Design contained no NumEntities!");
					return false;
				}
				if (!foundNumRooms)
				{
					numRooms = 0;
				}
				if (!foundChunkSize)
				{
					chunkSize = size;
				}
				if (!foundChunkOffset)
				{
					chunkOffset = Vec2i_Zero;
				}
				
				InitializeLevel(arenaPntr, level, size, numEntities, title);
				if (foundId) { level->id = levelId; }
				level->chunkSize = chunkSize;
				level->chunkOffset = chunkOffset;
				levelCreated = true;
			}
			
			if (StrCompareIgnoreCase(token.header.pntr, "Tiles", token.header.length))
			{
				foundTilesTag    = true;
				foundEntitiesTag = false;
				foundRoomsTag    = false;
			}
			else if (StrCompareIgnoreCase(token.header.pntr, "Entities", token.header.length))
			{
				foundTilesTag    = false;
				foundEntitiesTag = true;
				foundRoomsTag    = false;
			}
			else if (StrCompareIgnoreCase(token.header.pntr, "Rooms", token.header.length))
			{
				foundTilesTag    = false;
				foundEntitiesTag = false;
				foundRoomsTag    = true;
			}
			else
			{
				PrintLine_W("Unknown tag in level design [%.*s]", token.header.length, token.header.pntr);
				if (levelCreated && arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
				return false;
			}
		}
		else
		{
			PrintLine_W("Found unknown token in level design line %u: \"%.*s\"", parser.lineNumber, token.pieces[0].length, token.pieces[0].pntr);
			if (levelCreated && arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
			return false;
		}
	}
	
	if (!levelCreated)
	{
		WriteLine_W("WARNING: Level file contained no [Tiles] tag");
		return false;
	}
	
	if (numTilesFound != (u32)(size.width*size.height))
	{
		PrintLine_W("WARNING: Found %u/%u tiles expected in design!", numTilesFound, size.width*size.height);
		if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
		return false;
	}
	if (numEntitiesFound != numEntities)
	{
		PrintLine_W("WARNING: Found %u entities instead of declared %u", numEntitiesFound, numEntities);
		if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
		return false;
	}
	if (numRoomsFound != numRooms)
	{
		PrintLine_W("WARNING: Found %u rooms instead of declared %u", numRoomsFound, numRooms);
		if (arenaPntr->type != MemoryArenaType_Temp) { DeleteLevel(level); }
		return false;
	}
	
	return true;
}
