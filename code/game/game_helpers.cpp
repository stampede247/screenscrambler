/*
File:   game_helpers.cpp
Author: Taylor Robbins
Date:   02\01\2019
Description: 
	** Holds functions that can be utilized by anything related to the Game AppState
*/

void GameControllerDisconnected(u32 controllerIndex) //pre-declared in app_func_defs.h
{
	bool disconnectMattered = false;
	
	//TODO: Anything we need to do?
	
	if (!disconnectMattered)
	{
		PrintLine_I("Controller[%u] disconnected. But we don't need to pause", controllerIndex);
	}
}
