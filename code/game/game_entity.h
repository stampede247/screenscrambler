/*
File:   game_entity.h
Author: Taylor Robbins
Date:   11\05\2017
Description:
	** Holds the EntityType_t enumeration and the Entity_t structure which is the backbone for all entities in the game.
	** Also #includes game_entity_structs.h, game_entity_meta_info.h, and game_entity_depth.h
*/

#ifndef _GAME_ENTITY_H
#define _GAME_ENTITY_H

#define ENTITY_ID_STR_LENGTH 2

typedef enum
{
	//Core
	Entity_Player = 0x00,
	Entity_Exit,
	Entity_Resetter,
	Entity_SaveSpot,
	Entity_Saver,
	Entity_Diamond,
	Entity_CrystalBall,
	Entity_CrystalCamera,
	Entity_ProgressArrow,
	Entity_Chest,
	Entity_Giver,
	Entity_Taker,
	Entity_Key,
	Entity_Door,
	Entity_LedPad,
	Entity_LedDoor,
	Entity_DiamondDoor,
	Entity_MonsterDoor,
	Entity_TetrisDoor,
	Entity_Teleporter,
	Entity_Portal,
	Entity_Disruptor,
	Entity_Rail,
	Entity_Piston,
	
	//Triggers
	Entity_PressurePlate,
	Entity_Lever,
	Entity_HitSwitch,
	Entity_WallButton,
	Entity_Timer,
	
	//Boxes
	Entity_Box,
	Entity_Lockbox,
	Entity_Barrel,
	Entity_Boulder,
	Entity_Bag,
	
	//Monsters
	Entity_Monster,
	Entity_OldMonster1,
	Entity_OldMonster2,
	Entity_OldMonster3,
	Entity_OldMonster4,
	Entity_OldMonster5,
	Entity_OldMonster6,
	Entity_Friendly,
	Entity_OldFriendly1,
	Entity_OldFriendly2,
	Entity_Moth,
	Entity_Critter,
	
	//Lighting
	Entity_Torch,
	Entity_FireStand,
	Entity_Light,
	Entity_Candle,
	
	//Walls
	Entity_FakeWall,
	Entity_ToggleWall,
	
	//Powerups
	Entity_Shoes,
	Entity_Gloves,
	
	//Coop
	Entity_FriendshipPad,
	Entity_FriendshipDoor,
	Entity_PlayerSeparator,
	Entity_FriendshipEnabler,
	Entity_RespawnZone,
	
	//Meta
	Entity_Region,
	Entity_RoomCanceller,
	Entity_PreviewCamera,
	Entity_LevelOptions,
	Entity_ChunkChangeTrigger,
	Entity_Sensor,
	Entity_Threshold,
	Entity_MonsterTrigger,
	Entity_TurnTrigger,
	Entity_ChestTrap,
	Entity_DeathTimer,
	Entity_PlayerTeleport,
	
	//Other
	Entity_Trap,
	Entity_FireTrap,
	Entity_Pusher,
	Entity_PistonArm,
	Entity_Rotator,
	Entity_Spawner,
	Entity_Turret,
	Entity_Projectile,
	Entity_Bush,
	Entity_Pet,
	Entity_SwordPedestal,
	Entity_Text,
	Entity_Powerup,
	Entity_Peach,
	Entity_Explosion,
	Entity_Animation,
	
	//Math
	Entity_EntityCounter,
	Entity_Operator,
	Entity_Number,
	Entity_Comparator,
	
	//Decorations
	Entity_Table,
	Entity_TableDecoration,
	Entity_PathStones,
	
	//Overhead Decorations
	Entity_OverheadDecor,
	
	//Solid Decorations
	Entity_SolidDecoration,
	
	//Other Decorations
	Entity_FragileDecoration,
	
	//Glyphs
	Entity_GlyphGroup1,
	Entity_GlyphGroup2,
	Entity_GlyphGroup3,
	Entity_GlyphGroup4,
	
	Entity_MusicQueue,
	Entity_RequireDoor,
	Entity_Crack,
	Entity_Painter,
	Entity_PopupWall,
	Entity_Laser,
	Entity_LaserReceptacle,
	Entity_Mirror,
	
	Entity_IceBlock,
	Entity_EffectPad,
	Entity_Occlusion,
	Entity_GroundCover,
	Entity_CustomGlyph,
	Entity_Display,
	Entity_AttachedDecor,
	Entity_MusicDirector,
	Entity_Relay,
	Entity_TriggerGate,
	Entity_Passalong,
	Entity_ViewTarget,
	Entity_FloorMark,
	Entity_ColorField,
	Entity_EventTrigger,
	
	NumEntityTypes,
} EntityType_t;

const char* GetEntityTypeIdStr(EntityType_t entityType)
{
	switch (entityType)
	{
		case Entity_Animation:          return "AN";
		case Entity_AttachedDecor:      return "AD";
		case Entity_Bag:                return "BG";
		case Entity_Barrel:             return "BR";
		case Entity_Boulder:            return "BL";
		case Entity_Box:                return "BX";
		case Entity_Bush:               return "BS";
		case Entity_Candle:             return "CN";
		case Entity_Chest:              return "CH";
		case Entity_ChestTrap:          return "CT";
		case Entity_ChunkChangeTrigger: return "CG";
		case Entity_ColorField:         return "CF";
		case Entity_Comparator:         return "CM";
		case Entity_Crack:              return "CK";
		case Entity_Critter:            return "CR";
		case Entity_CrystalBall:        return "CB";
		case Entity_CrystalCamera:      return "CC";
		case Entity_CustomGlyph:        return "CU";
		case Entity_DeathTimer:         return "DT";
		case Entity_Diamond:            return "DM";
		case Entity_DiamondDoor:        return "DD";
		case Entity_Display:            return "DI";
		case Entity_Disruptor:          return "DS";
		case Entity_Door:               return "DO";
		case Entity_EffectPad:          return "EP";
		case Entity_EntityCounter:      return "EC";
		case Entity_EventTrigger:       return "ET";
		case Entity_Exit:               return "EX";
		case Entity_Explosion:          return "ES";
		case Entity_FakeWall:           return "HP";
		case Entity_FireStand:          return "FS";
		case Entity_FireTrap:           return "FT";
		case Entity_FloorMark:          return "FM";
		case Entity_FragileDecoration:  return "FD";
		case Entity_Friendly:           return "FL";
		case Entity_FriendshipDoor:     return "FR";
		case Entity_FriendshipEnabler:  return "FE";
		case Entity_FriendshipPad:      return "FP";
		case Entity_Giver:              return "GI";
		case Entity_Gloves:             return "GV";
		case Entity_GlyphGroup1:        return "G1";
		case Entity_GlyphGroup2:        return "G2";
		case Entity_GlyphGroup3:        return "G3";
		case Entity_GlyphGroup4:        return "G4";
		case Entity_GroundCover:        return "GC";
		case Entity_HitSwitch:          return "HS";
		case Entity_IceBlock:           return "IB";
		case Entity_Key:                return "KE";
		case Entity_Laser:              return "LS";
		case Entity_LaserReceptacle:    return "LR";
		case Entity_LedDoor:            return "LD";
		case Entity_LedPad:             return "LP";
		case Entity_LevelOptions:       return "LO";
		case Entity_Lever:              return "LV";
		case Entity_Light:              return "LT";
		case Entity_Lockbox:            return "LB";
		case Entity_Mirror:             return "MR";
		case Entity_Monster:            return "MN";
		case Entity_MonsterDoor:        return "MD";
		case Entity_MonsterTrigger:     return "MT";
		case Entity_Moth:               return "MH";
		case Entity_MusicDirector:      return "DR";
		case Entity_MusicQueue:         return "MQ";
		case Entity_Number:             return "NM";
		case Entity_Occlusion:          return "OC";
		case Entity_OldFriendly1:       return "07";
		case Entity_OldFriendly2:       return "08";
		case Entity_OldMonster1:        return "01";
		case Entity_OldMonster2:        return "02";
		case Entity_OldMonster3:        return "03";
		case Entity_OldMonster4:        return "04";
		case Entity_OldMonster5:        return "05";
		case Entity_OldMonster6:        return "06";
		case Entity_Operator:           return "OP";
		case Entity_OverheadDecor:      return "OD";
		case Entity_Painter:            return "NT";
		case Entity_Passalong:          return "AL";
		case Entity_PathStones:         return "PS";
		case Entity_Peach:              return "EA";
		case Entity_Pet:                return "PE";
		case Entity_Piston:             return "PI";
		case Entity_PistonArm:          return "PA";
		case Entity_Player:             return "PL";
		case Entity_PlayerSeparator:    return "PO";
		case Entity_PlayerTeleport:     return "PT";
		case Entity_PopupWall:          return "UP";
		case Entity_Portal:             return "PR";
		case Entity_Powerup:            return "PW";
		case Entity_PressurePlate:      return "PP";
		case Entity_PreviewCamera:      return "PC";
		case Entity_ProgressArrow:      return "AR";
		case Entity_Projectile:         return "PJ";
		case Entity_Pusher:             return "PH";
		case Entity_Rail:               return "RL";
		case Entity_Region:             return "RG";
		case Entity_Relay:              return "RE";
		case Entity_RequireDoor:        return "RD";
		case Entity_Resetter:           return "RS";
		case Entity_RespawnZone:        return "RZ";
		case Entity_RoomCanceller:      return "RC";
		case Entity_Rotator:            return "RT";
		case Entity_Saver:              return "SV";
		case Entity_SaveSpot:           return "SS";
		case Entity_Sensor:             return "SN";
		case Entity_Shoes:              return "SH";
		case Entity_SolidDecoration:    return "SD";
		case Entity_Spawner:            return "SP";
		case Entity_SwordPedestal:      return "SW";
		case Entity_Table:              return "TB";
		case Entity_TableDecoration:    return "TD";
		case Entity_Taker:              return "TK";
		case Entity_Teleporter:         return "TP";
		case Entity_TetrisDoor:         return "TT";
		case Entity_Text:               return "TX";
		case Entity_Threshold:          return "TH";
		case Entity_Timer:              return "TM";
		case Entity_ToggleWall:         return "TW";
		case Entity_Torch:              return "TC";
		case Entity_Trap:               return "ST";
		case Entity_TriggerGate:        return "GT";
		case Entity_TurnTrigger:        return "TG";
		case Entity_Turret:             return "TR";
		case Entity_ViewTarget:         return "VT";
		case Entity_WallButton:         return "WB";
		default: return "UK";
	}
}

typedef enum
{
	EntFlag_IsAlive     = 0x01,
	EntFlag_IsMeta      = 0x02,
	EntFlag_IsInside    = 0x04,
	EntFlag_IsSolid     = 0x08,
	EntFlag_IsMoving    = 0x10,
	EntFlag_JustStopped = 0x20,
} EntFlag_t;

#include "game/game_entity_structs.h"

struct Entity_t
{
	u8 flags;
	Entity_t* next;
	
	u32 id;
	EntityType_t type;
	EntityInfo_t info;
	
	u8 state;
	u8 animState;
	r32 animProgress;
	
	v2i tilePosLast;
	v2i tilePos;
	v2i portalTilePos;
	Dir2_t portalEntityRotation;
	
	u32 otherId; //holds the id of another entity of interest/connection to this one
	u32 refId; //anotherId, this one isn't saved in level designs
};

#define IsEntityAlive(entityPntr)     (IsFlagSet((entityPntr)->flags, EntFlag_IsAlive))

#endif //  _GAME_ENTITY_H
