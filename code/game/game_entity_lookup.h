/*
File:   game_entity_lookup.h
Author: Taylor Robbins
Date:   02\19\2020
*/

#ifndef _GAME_ENTITY_LOOKUP_H
#define _GAME_ENTITY_LOOKUP_H

typedef enum
{
	EntFltr_Id                = 0x00000002,
	EntFltr_Type              = 0x00000004,
	EntFltr_Variant           = 0x00000008,
	EntFltr_At                = 0x00000010,
	EntFltr_In                = 0x00000040,
	EntFltr_RegionOver        = 0x00000080,
	EntFltr_Group             = 0x00000100,
	EntFltr_Expression        = 0x00004000,
} EntFltr_t;

struct EntityLookupInfo_t
{
	u32 entityIndex;
	Entity_t* entityPntr;
	u32 foundIndex;
	u32 filterFlags;
	u32 id;
	bool isMeta;
	bool isInside;
	v2i position;
	EntityType_t type;
	u8 variant;
	u8 group;
	Dir2_t side;
	bool isOfFilter;
};

// +--------------------------------------------------------------+
// |                    Function Declarations                     |
// +--------------------------------------------------------------+
Entity_t* LookupEntity(Level_t* level, EntityLookupInfo_t* info);
const Entity_t* LookupEntity(const Level_t* level, EntityLookupInfo_t* info); //const-version
bool IsEntityByTypeAt(const Level_t* level, EntityType_t entityType, v2i tilePos);
bool DoesEntityIdExist(const Level_t* level, u32 id);
bool IsExpressionEntityAt(const Level_t* level, v2i tilePos);
u32 CountEntitiesByType(const Level_t* level, EntityType_t type);
u32 CountEntitiesAt(const Level_t* level, v2i tilePos);
u32 CountAllEntitiesAt(const Level_t* level, v2i tilePos);
u32 CountWallEntitiesAt(const Level_t* level, v2i tilePos);
u32 CountNonWallEntitiesAt(const Level_t* level, v2i tilePos);

// +--------------------------------------------------------------+
// |                            Macros                            |
// +--------------------------------------------------------------+
#define LoopEntities(iterVarName, level) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                            \
	iterVarName##Iter.filterFlags = 0x00;                                 \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopNonMetaEntities(iterVarName, level) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                   \
	iterVarName##Iter.filterFlags = 0x00;                                        \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopAllEntities(iterVarName, level) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                               \
	iterVarName##Iter.filterFlags = 0x00000000;                              \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)

#define GetEntityById(resultName, level, entityId) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                      \
	resultName##Iter.filterFlags = (EntFltr_Id);                                   \
	resultName##Iter.id = (entityId);                                              \
	resultName = LookupEntity(level, &resultName##Iter)
#define StoreEntityById(pointerVarName, level, entityId) do          \
{                                                                    \
	EntityLookupInfo_t pointerVarName##IterTemp = {};                \
	pointerVarName##IterTemp.filterFlags = (EntFltr_Id);             \
	pointerVarName##IterTemp.id = (entityId);                        \
	pointerVarName = LookupEntity(level, &pointerVarName##IterTemp); \
} while (0)

#define GetEntityByType(resultName, level, entityType) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                          \
	resultName##Iter.filterFlags = (EntFltr_Type);                                     \
	resultName##Iter.type = (entityType);                                              \
	resultName = LookupEntity(level, &resultName##Iter)
#define GetEntityByTypeVariant(resultName, level, entityType, entityVariant) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                                                \
	resultName##Iter.filterFlags = (EntFltr_Type|EntFltr_Variant);                                           \
	resultName##Iter.type = (entityType);                                                                    \
	resultName##Iter.variant = (entityVariant);                                                              \
	resultName = LookupEntity(level, &resultName##Iter)
#define GetEntityByTypeAt(resultName, level, entityType, tilePos) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                                     \
	resultName##Iter.filterFlags = (EntFltr_Type|EntFltr_At);                                     \
	resultName##Iter.type = (entityType);                                                         \
	resultName##Iter.position = (tilePos);                                                        \
	resultName = LookupEntity(level, &resultName##Iter)
#define StoreEntityByTypeAt(resultName, level, entityType, tilePos) \
	EntityLookupInfo_t resultName##Iter = {};                       \
	resultName##Iter.filterFlags = (EntFltr_Type|EntFltr_At);       \
	resultName##Iter.type = (entityType);                           \
	resultName##Iter.position = (tilePos);                          \
	resultName = LookupEntity(level, &resultName##Iter)
#define GetEntityByTypeVariantAt(resultName, level, entityType, entityVariant, tilePos) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                                                           \
	resultName##Iter.filterFlags = (EntFltr_Type|EntFltr_At|EntFltr_Variant);                                           \
	resultName##Iter.type = (entityType);                                                                               \
	resultName##Iter.position = (tilePos);                                                                              \
	resultName##Iter.variant = (entityVariant);                                                                         \
	resultName = LookupEntity(level, &resultName##Iter)

#define LoopEntitiesByType(iterVarName, level, entityType) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                              \
	iterVarName##Iter.filterFlags = (EntFltr_Type);                                         \
	iterVarName##Iter.type = (entityType);                                                  \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopAllEntitiesByType(iterVarName, level, entityType) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                                 \
	iterVarName##Iter.filterFlags = (EntFltr_Type);                                            \
	iterVarName##Iter.type = (entityType);                                                     \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopEntitiesByTypeVariant(iterVarName, level, entityType, entityVariant) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                                                    \
	iterVarName##Iter.filterFlags = (EntFltr_Type|EntFltr_Variant);                                               \
	iterVarName##Iter.type = (entityType);                                                                        \
	iterVarName##Iter.variant = (entityVariant);                                                                  \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopEntitiesByTypeAt(iterVarName, level, entityType, tilePos) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                                         \
	iterVarName##Iter.filterFlags = (EntFltr_Type|EntFltr_At);                                         \
	iterVarName##Iter.type = (entityType);                                                             \
	iterVarName##Iter.position = (tilePos);                                                            \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopEntitiesByTypeVariantAt(iterVarName, level, entityType, entityVariant, tilePos) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                                                               \
	iterVarName##Iter.filterFlags = (EntFltr_Type|EntFltr_At|EntFltr_Variant);                                               \
	iterVarName##Iter.type = (entityType);                                                                                   \
	iterVarName##Iter.position = (tilePos);                                                                                  \
	iterVarName##Iter.variant = (entityVariant);                                                                             \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)
#define LoopEntitiesByTypeThatWereAt(iterVarName, level, entityType, tilePos) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                                                 \
	iterVarName##Iter.filterFlags = (EntFltr_Type|EntFltr_WasAt);                                              \
	iterVarName##Iter.type = (entityType);                                                                     \
	iterVarName##Iter.position = (tilePos);                                                                    \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)

#define GetEntityAt(resultName, level, tilePos) Entity_t* resultName = nullptr; \
	EntityLookupInfo_t resultName##Iter = {};                                   \
	resultName##Iter.filterFlags = (EntFltr_At);                                \
	resultName##Iter.position = (tilePos);                                      \
	resultName = LookupEntity(level, &resultName##Iter
#define LoopEntitiesAt(iterVarName, level, tilePos) Entity_t* iterVarName = nullptr; \
	EntityLookupInfo_t iterVarName##Iter = {};                                       \
	iterVarName##Iter.filterFlags = (EntFltr_At);                                    \
	iterVarName##Iter.position = (tilePos);                                          \
	while ((iterVarName = LookupEntity(level, &iterVarName##Iter)) != nullptr)

#define GetRegionEntityOver(resultName, level, tilePos) GetEntityByTypeOver(resultName, level, Entity_Region, tilePos)
#define LoopRegionEntitiesOver(iterVarName, level, tilePos) LoopEntitiesByTypeOver(iterVarName, level, Entity_Region, tilePos)

#endif //  _GAME_ENTITY_LOOKUP_H
