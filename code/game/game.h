/*
File:   game.h
Author: Taylor Robbins
Date:   11\17\2017
*/

#ifndef _GAME_H
#define _GAME_H

#include "game_structs.h"
#include "game_diagonal_directions.h"
#include "game_corners.h"

#include "game/game_tile.h"
#include "game/game_entity.h"
#include "game/game_level.h"
#include "game/game_entity_lookup.h"

struct GameData_t
{
	bool initialized;
	bool activatedWipe;
	AppState_t wipeDoneState;
	
	v2 viewPosition;
	bool wasTildePressed;
	bool wasHomePressed;
	
	ScreenRecording_t recording;
	Texture_t captureTexture;
	FrameBuffer_t offscreenBuffer1;
	FrameBuffer_t offscreenBuffer2;
	bool drawToOffscreenBuffer;
	bool clearOffscreenBuffers;
	bool bufferParity;
	bool takingRecording;
	r32 recordingStartCountdown;
	r32 recordingLength;
	r32 recordingMaxLength;
	
	i32 selectedWindowIndex;
	i32 hoverWindowIndex;
	DynArray_t windowList;
	rec captureBtnRec;
	rec recordBtnRec;
	rec saveBtnRec;
	rec clearBtnRec;
};

#endif //  _GAME_H
