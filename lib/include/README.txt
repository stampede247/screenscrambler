This folder should contain copies of all the include folders from libraries that the project depends on.

The list of libraries is given below:

# mylib
https://www.bitbucket.org/stampede247/mylib
mylib folder

# STB Libraries
https://www.github.com/nothings/stb
stb folder

# GLEW 2.0.0
http://glew.sourceforge.net/
https://sourceforge.net/projects/glew/files/glew/2.0.0/
GL folder

# GLFW 3.2.1
https://www.glfw.org/
https://github.com/glfw/glfw
GLFW folder

# OpenAL SDK
https://www.openal.org/downloads/
openal folder

# libogg 1.3.3
https://www.xiph.org/ogg/
https://www.xiph.org/downloads/
ogg folder

# libvorbis 1.3.6
https://www.xiph.org/vorbis/
https://www.xiph.org/downloads/
vorbis folder

#Steam SDK v1.43
steam folder

#Tiny-AES-C
https://github.com/kokke/tiny-AES-c
AES folder

